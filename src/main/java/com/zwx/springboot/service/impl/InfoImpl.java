package com.zwx.springboot.service.impl;

import com.zwx.springboot.dao.InfoMapper;
import com.zwx.springboot.domain.InfoAddVo;
import com.zwx.springboot.entity.Info;
import com.zwx.springboot.service.InfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class InfoImpl implements InfoService {

    @Autowired
    private InfoMapper infoMapper;

    @Override
    public int add(InfoAddVo info) {
        Info info1 = new Info();
        BeanUtils.copyProperties(info, info1);

        return infoMapper.insert(info1);
    }
}
