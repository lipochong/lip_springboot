package com.zwx.springboot.service;

import com.github.pagehelper.PageInfo;

import com.zwx.springboot.domain.InfoAddVo;
import com.zwx.springboot.entity.Info;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface InfoService {


    int add(InfoAddVo info);
}
