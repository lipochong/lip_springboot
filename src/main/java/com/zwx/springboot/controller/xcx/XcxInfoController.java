package com.zwx.springboot.controller.xcx;

import com.zwx.springboot.common.util.JsonResult;
import com.zwx.springboot.domain.InfoAddVo;
import com.zwx.springboot.service.InfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "助我选用户相关api")
@RestController
@RequestMapping("/xcx/info")
public class XcxInfoController {

    @Autowired
    private InfoService infoService;

    @ApiOperation(value = "用户信息列表", produces = "application/json", notes = "用户信息列表")
    @ApiImplicitParam(paramType = "path", name = "businessActivityAddVo", value = "businessActivityAddVo", required = true, dataType = "BusinessActivityAddVo", example = "vcxf45s465ds4afs")
    @PostMapping("/addInfo")
    public JsonResult activityList(@RequestBody InfoAddVo infoAddVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(infoService.add(infoAddVo));
    }

}
