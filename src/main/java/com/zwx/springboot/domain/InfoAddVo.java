package com.zwx.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
@ToString
public class InfoAddVo {

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @ApiModelProperty(value = "name", example = "hh", required = true, position = 2)
    private String name;

    @ApiModelProperty(value = "age", example = "1", required = true, position = 2)
    private Integer age;

    @ApiModelProperty(value = "sex", example = "1", required = true, position = 2)
    private Integer sex;
}
