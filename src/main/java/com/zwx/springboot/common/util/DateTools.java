package com.zwx.springboot.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述：日期处理常用类
 * 
 * @author jaryn 
 * 创建时间 2011-4-20
 */
public class DateTools {

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * 功能：指定格式输出时间
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String format(Date date, String format) {
		if (StringTools.isBlank(format)) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat fm = new SimpleDateFormat(format);
		return fm.format(date);
	}
	/**
	 * 功能：将时间字符串从指定格式，转成指定格式
	 * @param dateString
	 * @param fromFomate
	 * @param toFormate
	 * @return
	 */
	public static String format(String dateString,String fromFomate,String toFormate){
		Date date = parseDate(dateString);
		return format(date, toFormate);
	}
	/**
	 * 功能：获取系统时间，按定格式输出
	 * 
	 * @param format
	 * @return
	 */
	public static String getSysDateFormate(String format) {
		return format(new Date(), format);
	}
	/**
	 * 功能：指定格式转换日期对象
	 * 
	 * @param dateString
	 * @param format
	 * @return
	 */
	public static Date parseDate(String dateString, String format) {
		SimpleDateFormat fm = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = fm.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	/**
	 * 功能：指定yyyy-MM-dd格式转换日期对象
	 * 
	 * @param dateString
	 * @return
	 */
	public static Date parseDate(String dateString) {
		return parseDate(dateString, "yyyy-MM-dd");
	}

	/**
	 * 功能：指定yyyy-MM-dd HH:mm:ss格式转换日期对象
	 * 
	 * @param dateString
	 * @return
	 */
	public static Date parseDateTime(String dateString) {
		return parseDate(dateString, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 功能：计算两个日期之间的天数
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int calcDays(String startDate, String endDate) {
		if (StringTools.isBlank(startDate,endDate)) {
			return 0;
		}
		long start = parseDateTime(startDate).getTime();
		long end = parseDateTime(endDate).getTime();
		int days = (int) ((end - start) / (24 * 60 * 60 * 1000));
		return days;
	}
	/**
	 * 
	 * 功能：计算两个日期之间的天数
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int calcDays(Date startDate, Date endDate) {
		if (startDate == null || endDate == null) {
			return 0;
		}
		long start = startDate.getTime();
		long end = endDate.getTime();
		int days = (int) ((end - start) / (24 * 60 * 60 * 1000));
		return days;
	}

	/**
	 * 两个时间之间的比较
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int calcTimes(Date startDate, Date endDate) {
		if (startDate == null || endDate == null) {
			return 0;
		}
		long start = startDate.getTime();
		long end = endDate.getTime();
		int times = (int) ((end - start));
		if(times>0){
			return 2;
		} else if(times==0){
			return 1;
		}else{
			return 0;
		}

	}

	/**
	 * 两个时间之间的比较
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int calcYearTimes(Date startDate, Date endDate) {
		if (startDate == null || endDate == null) {
			return 0;
		}
		if (startDate.getTime() > endDate.getTime()) {
			return 1;
		}  else if (startDate.getTime() < endDate.getTime()) {
			return 2;
		} else {
			return 3;
		}

	}

	/**
	 * 功能：指定日期加上指定天数
	 * 
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date addDate(Date date, int day) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);
		return c.getTime();
	}

	/**
	 * 功能：指定日期减去指定天数
	 * 
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date diffDate(Date date, int day) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) - ((long) day) * 24 * 3600 * 1000);
		return c.getTime();
	}

	/**
	 * 
	 * 功能：加上指定分钟数
	 * 
	 * @param date
	 * @param minute
	 * @return
	 */
	public static Date getDateByMinuteAdd(Date date, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minute);
		return calendar.getTime();
	}
	
	/**
	 * 功能:两个日期相隔天数
	 * 
	 * @param startDate
	 * @param endDate
	 * @return 
	 */
	public static int diffDate(Date startDate, Date endDate) {
		return (int) ((getMillis(endDate) - getMillis(startDate)) / (24 * 3600 * 1000));
	}

	/**
	 * 功能：返回年
	 * 
	 * @param date
	 * @return
	 */
	public static int getYear(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.YEAR);

	}

	/**
	 * 功能：返回短格式的年,[2011],返回11
	 * 
	 * @param date
	 * @return
	 */
	public static int getSimpleYear(Date date) {
		String year = String.valueOf(getYear(date));
		return Integer.parseInt(year.substring(2, 4));
	}

	/**
	 * 功能：返回月
	 * 
	 * @param date
	 * @return
	 */
	public static int getMonth(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MONTH) + 1;
	}

	/**
	 * 功能：返回日
	 * 
	 * @param date
	 * @return
	 */
	public static int getDay(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 功能：返回小时
	 * 
	 * @param date
	 * @return
	 */
	public static int getHour(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 功能：返回分
	 * 
	 * @param date
	 * @return
	 */
	public static int getMinute(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MINUTE);
	}

	/**
	 * 功能：返回星期 1：星期一 2:星期二 ... 6:星期六 7:星期日
	 * 
	 * @param date
	 * @return
	 */
	public static int getChinaWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int week = c.get(Calendar.DAY_OF_WEEK) - 1;
		if (week == 0) {
			return 7;
		} else {
			return week;
		}
	}

	/**
	 * 功能：返回秒
	 * 
	 * @param date
	 * @return
	 */
	public static int getSecond(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.SECOND);
	}

	/**
	 * 功能：返回毫秒
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillis(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.getTimeInMillis();
	}

	/**
	 * 功能：获取当前月的第一天日期
	 * 
	 * @return
	 */
	public static Date getMonFirstDay() {
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.set(getYear(date), getMonth(date) - 1, 1);
		return c.getTime();
	}
	
	/**
	 * 功能：获取当前月的最后一天日期
	 * 
	 * @return
	 */
	public static Date getMonLastDay() {
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.set(getYear(date), getMonth(date), 1);

		c.setTimeInMillis(c.getTimeInMillis() - (24 * 3600 * 1000));
		return c.getTime();
	}

	/**
	 * 获取某段时这里写代码片间内的所有日期
	 * @param dBegin
	 * @param dEnd
	 * @return
	 */
	public static List<Date> findDates(Date dBegin, Date dEnd) {
		List<Date> lDate = new ArrayList<Date>();
		lDate.add(dBegin);
		Calendar calBegin = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calBegin.setTime(dBegin);
		Calendar calEnd = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calEnd.setTime(dEnd);
		// 测试此日期是否在指定日期之后
		while (dEnd.after(calBegin.getTime()))  {
			// 根据日历的规则，为给定的日历字段添加或减去指定的时间量
			calBegin.add(Calendar.DAY_OF_MONTH, 1);
			lDate.add(calBegin.getTime());
		}
		return lDate;
	}

	/**
	 * 获取当前时间加几个月时间之后时间
	 * @param i
	 * @return
	 */
	public static Date getMonth(Integer i){
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH, i);
		return c.getTime();
	}

	/**
	 * 获取当前时间加上几年之后时间
	 * @param i
	 * @return
	 */
	public static Date getYear(Integer i){
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.YEAR, i);
		return c.getTime();
	}
}
