package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyCompanynameVo {

    @ApiModelProperty(value = "商户名称", example = "test", required = false, position = 3)

    private String companyname;

}
