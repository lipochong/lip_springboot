package com.jkdl.springboot.domain;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UsersIdStatusVo {

    private int id;

    private Integer wxUserId;

    private Integer wxTowUserId;
}
