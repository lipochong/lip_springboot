package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class OdrOrderMonthListVo {

    @ApiModelProperty(value = "公司id", example = "test", required = false, position = 3)
    @NotNull
    private int companyId;

    @ApiModelProperty(value = "unionid", example = "test", required = false, position = 3)
    @NotNull
    private String unionid;
}
