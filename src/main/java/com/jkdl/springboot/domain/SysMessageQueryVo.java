package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class SysMessageQueryVo extends PageEntity{


    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "是否删除，默认为0，删除为1", example = "test", required = false, position = 3)
    private Integer bdelete;



    @ApiModelProperty(value = "结束时间开始", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "结束时间结束", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;
}
