package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ToString
public class CloseAnAccountVo   {
    //-代理商渠道
    private Integer parentid;
    //代理商渠道
    private Integer grandParentId;
    //商户名称
    private String companyname;
    //商户ID
     private Integer id;
    //充电金额
    private String nationpostage;
    //订单状态
    private Integer status;

    @ApiModelProperty(value = "订单时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    //订单最后日期
    @ApiModelProperty(value = "订单时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;

    @ApiModelProperty(value = "", example = "test", required = false, position = 3)
    private Integer selectListSecondSelect;


//    @NotNull(message = "页数不能为空")
//    @ApiModelProperty(value = "页数", example = "1", required = true, position = 1)
//    protected Integer pageNum = 1;
//
//    @NotNull(message = "每页的总条数不能为空")
//    @ApiModelProperty(value = "每页总条数", example = "10", required = true, position = 2)
//    protected Integer pageSize = 10;
}
