package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
@Data
@ToString
public class PdProductCountQueryVo {
    @ApiModelProperty(value = "商家id：companyid", example = "test", required = false, position = 3)
    private List<String> companyid;
    @ApiModelProperty(value = "商户名", example = "test", required = false, position = 3)
    private String companyname;
}
