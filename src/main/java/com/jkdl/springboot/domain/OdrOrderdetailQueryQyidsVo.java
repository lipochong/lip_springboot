package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class OdrOrderdetailQueryQyidsVo {

    @ApiModelProperty(value = "商家id：companyid", example = "test", required = false, position = 3)
    private List<String> qyids;


}
