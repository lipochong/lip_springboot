package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class PdProductsFaultAddVo {

    @ApiModelProperty(value = "故障设备id", example = "test", required = false, position = 3)
    @NotNull(message="故障设备号不能为空")
    private String productid;

    @ApiModelProperty(value = "故障原因不可以为空10:手机音量调大了充电器绿灯没闪20:设备插头插上但二维码的灯光没有亮30:数据线插头被损坏40:绿灯在闪但不能充电50:其它", example = "test", required = false, position = 3)
    @NotNull(message="故障原因不可以为空")
    private Integer faultreason;

   @ApiModelProperty(value = "微信用户id", example = "test", required = false, position = 3)
    @NotNull(message="微信用户id不可以为空")
    private Integer seconduserid;

    @ApiModelProperty(value = "联系电话不可以为空", example = "test", required = false, position = 3)
    @NotNull(message="联系电话不可以为空")
    private String linkphone;

    @ApiModelProperty(value = "微信呢称不可以为空", example = "test", required = false, position = 3)
    @NotNull(message="微信呢称不可以为空")
    private String linkname;


    @ApiModelProperty(value = "企业id不可以为空", example = "test", required = false, position = 3)
    @NotNull(message="企业id不可以为空")
    private Integer qyid;

    @ApiModelProperty(value = "其他原因不可以为空", example = "test", required = false, position = 3)
    @NotNull(message="其他原因不可以为空")
    private String faultreasontext;


}
