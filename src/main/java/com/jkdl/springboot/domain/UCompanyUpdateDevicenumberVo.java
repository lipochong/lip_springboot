package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyUpdateDevicenumberVo  {

    @ApiModelProperty(value = "商户id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "设备数量不能为空", example = "test", required = false, position = 3)
    private Integer devicenumber;

}
