package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PdProductsBatchQueryVo extends PageEntity{


    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private String id;

    @ApiModelProperty(value = "批次名称", example = "test", required = false, position = 3)
    private String producttypename;




}
