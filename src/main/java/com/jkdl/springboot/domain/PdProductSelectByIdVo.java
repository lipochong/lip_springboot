package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class PdProductSelectByIdVo {

    @NotNull(message = "设备id不能为空")
    @ApiModelProperty(value = "productId", example = "100100", required = true, position = 1)
    private Integer productId;



}
