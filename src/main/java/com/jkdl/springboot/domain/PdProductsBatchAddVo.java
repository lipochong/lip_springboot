package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PdProductsBatchAddVo {


    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private String id;

    @ApiModelProperty(value = "设备添加类型qj旗舰版，sy声音版", example = "test", required = false, position = 3)
    private String devtype;

    @ApiModelProperty(value = "批次名称", example = "test", required = false, position = 3)
    private String producttypename;

    @ApiModelProperty(value = "序号", example = "test", required = false, position = 3)
    private Double sort;

    @ApiModelProperty(value = "声音编号，qj旗舰版不添加声音编号，sy声音版则添加声音批次", example = "test", required = false, position = 3)
    private Integer synthid;
}
