package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyQueryInfoVo{

    @ApiModelProperty(value = "公司id", example = "test", required = false, position = 3)
    private Integer companyId;


}
