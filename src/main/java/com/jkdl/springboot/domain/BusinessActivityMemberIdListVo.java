package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@ToString
public class BusinessActivityMemberIdListVo{

    @ApiModelProperty(value = "joinCount", example = "1", required = true, position = 2)
    private String order;

    @ApiModelProperty(value = "joinCount", example = "1", required = true, position = 2)
    private Integer joinCount;

    @ApiModelProperty(value = "useCount", example = "1", required = true, position = 2)
    private Integer useCount;

    @ApiModelProperty(value = "verificationCode", example = "1", required = true, position = 2)
    private String verificationCode;

}
