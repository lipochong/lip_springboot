package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class PdProductsBatchVoiceQueryVo extends PageEntity{


    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "批次id", example = "test", required = false, position = 3)
    private Integer batchid;




}
