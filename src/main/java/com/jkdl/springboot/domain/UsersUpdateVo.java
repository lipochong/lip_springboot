package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UsersUpdateVo {

    @ApiModelProperty(value = "用户id", example = "test", required = false, position = 3)
    @NotNull(message="用户不能为空")
    private Integer id;

    @ApiModelProperty(value = "登录名称", example = "test", required = false, position = 3)
    @NotNull(message="登录名称不能为空")
    private String loginName;

    @ApiModelProperty(value = "邮箱名称", example = "test", required = false, position = 3)
    @NotNull(message="邮箱不能为空")
    private String email;

    @ApiModelProperty(value = "企业id名称", example = "test", required = false, position = 3)
    @NotNull(message="企业id不能为空")
    private Integer companyid;
}
