package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ToString
public class UCompanyQueryVo extends PageEntity{


    @ApiModelProperty(value = "主键id", example = "test", required = false, position = 1)
    private Integer companyId;

    @ApiModelProperty(value = "上级id（代理商id）", example = "test", required = false, position = 2)
    private Integer parentId;

    @ApiModelProperty(value = "上上级id（平台id）", example = "test", required = false, position = 2)
    private Integer grandParentId;

    @ApiModelProperty(value = "公司名称", example = "test", required = false, position = 3)
    private String companyName;

    @ApiModelProperty(value = "", example = "test", required = false, position = 3)
    private Integer selectListSecondSelect;

    @ApiModelProperty(value = "登录的商户类型", example = "test", required = false, position = 3)
    private Integer userCompanyType;


}
