package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class UsersPhoneVo {

    private Integer id;

    private String phone;

    private String code;

    private Integer companyId;

    private String nickName;

    private String password;

    private String avatarUrl;

    private String verificationCode;

}
