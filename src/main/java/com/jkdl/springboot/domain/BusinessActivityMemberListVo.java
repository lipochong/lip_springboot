package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@ToString
public class BusinessActivityMemberListVo extends PageEntity{

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @ApiModelProperty(value = "userId", example = "1", required = true, position = 2)
    private Integer userId;

    @ApiModelProperty(value = "businessActivityId", example = "1", required = true, position = 2)
    private Integer businessActivityId;

    @ApiModelProperty(value = "qyid", example = "1", required = true, position = 2)
    private Integer qyid;

    @ApiModelProperty(value = "qyUserId", example = "1", required = true, position = 2)
    private Integer qyUserId;

    @ApiModelProperty(value = "openid", example = "fddfg234234", required = true, position = 2)
    private String openid;

    @ApiModelProperty(value = "companyname", example = "fddfg234234", required = true, position = 2)
    private String companyname;

    @ApiModelProperty(value = "状态", example = "1", required = true, position = 2)
    private Integer status;

    @ApiModelProperty(value = "开始时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;


}
