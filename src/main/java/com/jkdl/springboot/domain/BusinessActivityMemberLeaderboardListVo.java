package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;


@Data
@ToString
public class BusinessActivityMemberLeaderboardListVo {

    @ApiModelProperty(value = "content", example = "1", required = true, position = 2)
    private String content;

    @ApiModelProperty(value = "count", example = "1", required = true, position = 2)
    private String count;

    @ApiModelProperty(value = "createTime", example = "1", required = true, position = 2)
    private String createTime;

    @ApiModelProperty(value = "lastTime", example = "1", required = true, position = 2)
    private String lastTime;

    private List<?> lst;

}
