package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class SystemLoginVo {

    @ApiModelProperty(value = "登录名", example = "test", required = false, position = 3)
    @NotNull(message="登录名不能为空")
    private String loginName;

    @ApiModelProperty(value = "登录名", example = "test", required = false, position = 3)
    @NotNull(message="登录密码为空")
    private String password;
}
