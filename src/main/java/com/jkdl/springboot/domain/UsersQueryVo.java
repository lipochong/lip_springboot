package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;


@Data
@ToString
public class UsersQueryVo extends PageEntity{
    @ApiModelProperty(value = "用户名称", example = "test", required = false, position = 3)
    private String name;


}
