package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class XcxPayOrderVo {

    @ApiModelProperty(value = "用户openId", example = "100100", required = true, position = 1)
    @NotNull
    private String openId;

    @ApiModelProperty(value = "id主键", example = "100100", required = true, position = 2)
    @NotNull
    private int id;

    @ApiModelProperty(value = "订单id", example = "100100", required = true, position = 2)
    @NotNull
    private String orderId;



}
