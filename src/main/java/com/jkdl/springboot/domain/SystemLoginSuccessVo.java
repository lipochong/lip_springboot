package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class SystemLoginSuccessVo {

    @ApiModelProperty(value = "登录名", example = "test", required = false, position = 3)

    private String loginName;

    @ApiModelProperty(value = "登录id", example = "test", required = false, position = 3)

    private Integer id;
}
