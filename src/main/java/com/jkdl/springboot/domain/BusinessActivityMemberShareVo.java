package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@ToString
public class BusinessActivityMemberShareVo{

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @ApiModelProperty(value = "userId", example = "1", required = true, position = 2)
    private Integer userId;

    @ApiModelProperty(value = "bamId", example = "1", required = true, position = 2)
    private Integer bamId;

    @ApiModelProperty(value = "qyUserId", example = "1", required = true, position = 2)
    private Integer qyUserId;

}
