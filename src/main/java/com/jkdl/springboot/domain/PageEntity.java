package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * 分页
 */
@Data
@ToString
public class PageEntity {

    @NotNull(message = "页数不能为空")
    @ApiModelProperty(value = "页数", example = "1", required = true, position = 1)
    protected Integer pageNum = 1;

    @NotNull(message = "每页的总条数不能为空")
    @ApiModelProperty(value = "每页总条数", example = "10", required = true, position = 2)
    protected Integer pageSize = 10;
}
