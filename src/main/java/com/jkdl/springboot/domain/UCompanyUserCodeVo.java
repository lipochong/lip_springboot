package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyUserCodeVo {

    @ApiModelProperty(value = "code", example = "test", required = false, position = 3)
    private String code;

    @ApiModelProperty(value = "手机号", example = "test", required = false, position = 3)
    private String phone;


}
