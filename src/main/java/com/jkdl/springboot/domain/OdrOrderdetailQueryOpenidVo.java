package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class OdrOrderdetailQueryOpenidVo extends PageEntity{

    @ApiModelProperty(value = "微信openid", example = "test", required = false, position = 3)
    private String openids;

}
