package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
@Data
@ToString
public class SysMessageDeleteVo {


    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "是否删除，默认为0，删除为1", example = "test", required = false, position = 3)
    private Integer bdelete;


}
