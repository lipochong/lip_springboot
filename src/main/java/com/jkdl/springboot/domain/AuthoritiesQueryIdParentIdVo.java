package com.jkdl.springboot.domain;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AuthoritiesQueryIdParentIdVo {

    private int id;

    private int parentId;

}
