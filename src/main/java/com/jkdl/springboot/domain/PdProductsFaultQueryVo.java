package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class PdProductsFaultQueryVo extends PageEntity{


    @ApiModelProperty(value = "公司名称", example = "test", required = false, position = 3)
    private String companyname;

    @ApiModelProperty(value = "故障设备号", example = "test", required = false, position = 3)
    private String productid;

    @ApiModelProperty(value = "创建开始时间", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "创建结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;


}
