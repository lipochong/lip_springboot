package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ToString
public class USecondUserListQueryVo extends PageEntity{

    @ApiModelProperty(value = "创建开始时间", example = "2018-10-12", required = false, position = 1)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "创建结束时间", example = "2018-12-12", required = false, position = 2)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;

    @ApiModelProperty(value = "openid", example = "fdsaoifewq142543", required = true, position = 3)
    private String openid;

    @ApiModelProperty(value = "免支付", example = "1", required = true, position = 4)
    private Integer smlock;

    @ApiModelProperty(value = "id", example = "1", required = true, position = 4)
    private Integer id;

    @ApiModelProperty(value = "nickname", example = "哈哈", required = true, position = 3)
    private String nickname;

    @ApiModelProperty(value = "起始index 从0开始", example = "test", required = false, position = 3)
    private int pageStart;
}
