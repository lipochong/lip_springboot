package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.NonNull;
import javax.validation.constraints.NotNull;
@Data
@ToString
public class UsersDeleteVo {

    @ApiModelProperty(value = "用户id", example = "test", required = false, position = 3)
    @NotNull(message="用户不能为空")
    private Integer id;
}
