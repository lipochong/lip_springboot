package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;


@Data
@ToString
public class BusinessActivityMemberWriteoffVo {

    @ApiModelProperty(value = "code", example = "fddfg234234", required = true, position = 2)
    private String code;

    @ApiModelProperty(value = "状态", example = "1", required = true, position = 2)
    private Integer status;

    @ApiModelProperty(value = "到期时间", example = "2018-12-12", required = false, position = 4)
    private String lastDate;

    @ApiModelProperty(value = "内容", example = "内容", required = true, position = 2)
    private String content;

    @ApiModelProperty(value = "标题", example = "标题", required = true, position = 2)
    private String title;

    @ApiModelProperty(value = "金额", example = "2.22", required = true, position = 2)
    private String money;

    @ApiModelProperty(value = "创建时间", example = "2018-12-12", required = false, position = 4)
    private String createTime;

    @ApiModelProperty(value = "截止时间", example = "2018-12-12", required = false, position = 4)
    private String lastTime;

    @ApiModelProperty(value = "编号", example = "2018-12-12", required = false, position = 4)
    private String verificationCode;

}
