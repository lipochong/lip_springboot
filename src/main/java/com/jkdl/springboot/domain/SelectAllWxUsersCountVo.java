package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SelectAllWxUsersCountVo {

    @ApiModelProperty(value = "查询条件", example = "test", required = false, position = 1)
    private Integer id;

}
