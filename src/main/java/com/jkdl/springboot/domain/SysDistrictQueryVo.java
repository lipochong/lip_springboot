package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SysDistrictQueryVo{

    @ApiModelProperty(value = "市id", example = "test", required = false, position = 3)
    private Integer cityId;


}
