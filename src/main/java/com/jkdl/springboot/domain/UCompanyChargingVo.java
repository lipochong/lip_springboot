package com.jkdl.springboot.domain;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class UCompanyChargingVo {

    private Integer chargingtime;

    private String mobile;

    private Integer id;

    private Integer qyid;

    private String companyname;

}
