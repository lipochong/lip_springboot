package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@ToString
public class PdProductsQueryVo extends PageEntity{

    @ApiModelProperty(value = "设备号", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "公司id", example = "test", required = false, position = 3)
    private Integer companyid;

    @ApiModelProperty(value = "公司名称", example = "test", required = false, position = 3)
    private String companyname;

    @ApiModelProperty(value = "设备状态0:未分配,1:已分配可使用，2:维护中", example = "test", required = false, position = 3)
    private Integer auditstatus;
 /*   @ApiModelProperty(value = "创建日期", example = "test", required = false, position = 3)
    private String createTime;

    @ApiModelProperty(value = "创建日期", example = "test", required = false, position = 3)
    private String lasttime;*/

    @ApiModelProperty(value = "创建开始时间", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "创建结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;


    @ApiModelProperty(value = "修改开始时间", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDatelast;

    @ApiModelProperty(value = "修改结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDatelast;

    @ApiModelProperty(value = "产品名称", example = "test", required = false, position = 3)
    private String productName;

    @ApiModelProperty(value = "商户名称模糊查询下的商户ids", example = "test", required = false, position = 3)
    private List<String> companynameQyIds;

    @ApiModelProperty(value = "平台id", example = "test", required = false, position = 3)
    private int platformId;

    @ApiModelProperty(value = "一级代理商id", example = "test", required = false, position = 3)
    private int oneCompanyId;

    @ApiModelProperty(value = "二级代理商id", example = "test", required = false, position = 3)
    private int twoCompanyId;

    @ApiModelProperty(value = "一级代理商下的商户ids", example = "test", required = false, position = 3)
    private List<String> oneCompanyQyIds;

    @ApiModelProperty(value = "二级代理商下的商户ids", example = "test", required = false, position = 3)
    private List<String> twoCompanyQyIds;





}
