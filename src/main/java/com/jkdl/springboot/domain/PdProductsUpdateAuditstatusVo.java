package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PdProductsUpdateAuditstatusVo {

    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "设备状态0:未分配,1:已分配可使用，2:维护中", example = "test", required = false, position = 3)
    private Integer auditstatus;






}
