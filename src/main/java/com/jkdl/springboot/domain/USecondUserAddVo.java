package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class USecondUserAddVo {

    @NotNull(message = "code不能为空")
    @ApiModelProperty(value = "code", example = "fdsaoifewq142543", required = true, position = 1)
    private String code;
}
