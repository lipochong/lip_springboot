package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString
public class DongguanOrderVo {

    @ApiModelProperty(value = "商户id", example = "test", required = false, position = 3)
    private int id;

    @ApiModelProperty(value = "所有上级id", example = "test", required = false, position = 3)
    private String parentid;

    @ApiModelProperty(value = "开始时间", example = "test", required = false, position = 3)
    private Date startDate;

    @ApiModelProperty(value = "结束时间", example = "test", required = false, position = 3)
    private Date endDate;

    @ApiModelProperty(value = "支付状态", example = "test", required = false, position = 3)
    private int status;


}
