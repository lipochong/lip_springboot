package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PdProductsUpdateVo {

    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "产品名称", example = "test", required = false, position = 3)
    private String productName;

    @ApiModelProperty(value = "产品规格", example = "test", required = false, position = 3)
    private String specifications;

    @ApiModelProperty(value = "市场零售价", example = "test", required = false, position = 3)
    private Double money;

    @ApiModelProperty(value = "所属商家", example = "test", required = false, position = 3)
    private Integer  companyid;

    @ApiModelProperty(value = "房间号", example = "test", required = false, position = 3)
    private String  detail;

    @ApiModelProperty(value = "重置合成音", example = "test", required = false, position = 3)
    private Integer  resetvoiceid;

    @ApiModelProperty(value = "设备批次", example = "test", required = false, position = 3)
    private Integer  batchnumber;

    @ApiModelProperty(value = "主要特点/备注", example = "test", required = false, position = 3)
    private String  productstyle;


    @ApiModelProperty(value = "功能说明/上报人呢称", example = "test", required = false, position = 3)
    private String  declaration;

    @ApiModelProperty(value = "技术特征/上报人电话", example = "test", required = false, position = 3)
    private String  technical;




}
