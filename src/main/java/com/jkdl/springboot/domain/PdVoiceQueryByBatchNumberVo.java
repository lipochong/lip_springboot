package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class PdVoiceQueryByBatchNumberVo {

    @NotNull(message = "设备id不能为空")
    @ApiModelProperty(value = "productId", example = "100100", required = true, position = 1)
    private Integer productId;

    @NotNull(message = "设备合成音不能为空")
    @ApiModelProperty(value = "batchNumber", example = "10", required = true, position = 2)
    private Long batchNumber;

    @NotNull(message = "订单编号不能为空")
    @ApiModelProperty(value = "orderId", example = "10", required = true, position = 3)
    private String orderId;

}
