package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class DongguanOrderQueryVo {

    @ApiModelProperty(value = "商家id", example = "test", required = false, position = 3)
    private int id;

    @ApiModelProperty(value = "商家名称", example = "test", required = false, position = 3)
    private String companyname;

    @ApiModelProperty(value = "设备总数", example = "test", required = false, position = 3)
    private int devicenumber;

    @ApiModelProperty(value = "支付状态", example = "test", required = false, position = 3)
    private int status;

    @ApiModelProperty(value = "数量", example = "test", required = false, position = 3)
    private int count;

    @ApiModelProperty(value = "订单时间", example = "test", required = false, position = 3)
    private Date orderDate;


}
