package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UCompanyDeleteVo{

    @ApiModelProperty(value = "商户id", example = "test", required = false, position = 3)
    @NotNull(message="商户不能为空")
    private Integer id;

    @ApiModelProperty(value = "商户bdelete", example = "test", required = false, position = 3)
    @NotNull(message="商户状态")
    private Integer bdelete;

}
