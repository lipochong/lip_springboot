package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jkdl.springboot.entity.OdrOrderdetailWithBLOBs;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class OdrOrderdetailExportQueryVo extends OdrOrderdetailWithBLOBs {

    @ApiModelProperty(value = "订单号", example = "test", required = false, position = 3)
    private String orderid;

    @ApiModelProperty(value = "产品设备id", example = "test", required = false, position = 3)
    private Integer productid;

    @ApiModelProperty(value = "公司名称", example = "test", required = false, position = 3)
    private String companyname;

    @ApiModelProperty(value = "微信openid", example = "test", required = false, position = 3)
    private String openids;

    @ApiModelProperty(value = "支付金额", example = "test", required = false, position = 3)
    private String payamount;

    @ApiModelProperty(value = "代理商名称", example = "test", required = false, position = 3)
    private String agentnamess;

    @ApiModelProperty(value = "产品设备id", example = "test", required = false, position = 3)
    private String cuname;

    @ApiModelProperty(value = "订单状态：待支付：0，支付成功：10", example = "test", required = false, position = 3)
    private String status;

    /*@ApiModelProperty(value = "订单时间", example = "test", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private String createdate;*/

    @ApiModelProperty(value = "订单开始时间", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "订单结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;
}
