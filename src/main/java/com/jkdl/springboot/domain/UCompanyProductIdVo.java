package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyProductIdVo extends PageEntity{

    @ApiModelProperty(value = "设备id", example = "test", required = false, position = 3)

    private int productId;

}
