package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UCompanyAddVo{

    @ApiModelProperty(value = "商户名称", example = "test", required = false, position = 3)
    @NotNull(message="商户名称不能为空")
    private String companyname;

    @ApiModelProperty(value = "商户类型", example = "test", required = false, position = 3)
    @NotNull(message="商户类型")
    private String companytype;

    @ApiModelProperty(value = "登录名", example = "test", required = false, position = 3)
    @NotNull(message="登录名不能为空")
    private String code;

    @ApiModelProperty(value = "商户邮箱", example = "test", required = false, position = 3)
    @NotNull(message="商户邮箱不能为空")
    private String email;

    @ApiModelProperty(value = "详细地址", example = "test", required = false, position = 3)
    @NotNull(message="详细地址不能为空")
    private String address;

    @ApiModelProperty(value = "代理商客户电话", example = "test", required = false, position = 3)
    @NotNull(message="代理商客户电话不能为空")
    private String mobile;

    @ApiModelProperty(value = "代理商联系电话", example = "test", required = false, position = 3)
    @NotNull(message="代理商联系电话")
    private String phone;

//    @ApiModelProperty(value = "与上级分润比例", example = "test", required = false, position = 3)
//    @NotNull(message="与上级分润比例不能为空")
//    private String rebateparent;

    @ApiModelProperty(value = "充电金额", example = "test", required = false, position = 3)
    @NotNull(message="充电金额不能为空")
    private Double nationpostage;

//    @ApiModelProperty(value = "提现类型", example = "test", required = false, position = 3)
//    @NotNull(message="提现类型不能为空")
//    private Integer txtype;

    @ApiModelProperty(value = "省", example = "test", required = false, position = 3)
    @NotNull(message="省不能为空")
    private String province;

    @ApiModelProperty(value = "市", example = "test", required = false, position = 3)
    @NotNull(message="市不能为空")
    private String city;

    @ApiModelProperty(value = "区", example = "test", required = false, position = 3)
    @NotNull(message="区不能为空")
    private String area;

    @ApiModelProperty(value = "充电时间", example = "test", required = false, position = 3)
    @NotNull(message="充电时间不能为空")
    private Integer chargingtime;

    @ApiModelProperty(value = "设备数", example = "test", required = false, position = 3)
    @NotNull(message="设备数不能为空")
    private Integer devicenumber;

    @ApiModelProperty(value = "上级id", example = "test", required = false, position = 3)
    @NotNull(message="上级id")
    private Integer parentid;

    @ApiModelProperty(value = "上上级id", example = "test", required = false, position = 3)
    private Integer grandParentId;

    @ApiModelProperty(value = "商户姓名", example = "test", required = false, position = 3)
    private String name;



}
