package com.jkdl.springboot.domain;

import lombok.Data;
import lombok.ToString;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UsersAddVo {

    @ApiModelProperty(value = "登录名称", example = "test", required = false, position = 3)
    @NotNull(message="登录名称不能为空")
    private String loginName;

    @ApiModelProperty(value = "密码名称", example = "test", required = false, position = 3)
    @NotNull(message="密码不能为空")
    private String password;

    @ApiModelProperty(value = "用户名称", example = "test", required = false, position = 3)
    @NotNull(message="用户名称不能为空")
    private String name;

    @ApiModelProperty(value = "邮箱名称", example = "test", required = false, position = 3)
    @NotNull(message="邮箱不能为空")
    private String email;

    @ApiModelProperty(value = "企业id名称", example = "test", required = false, position = 3)
    @NotNull(message="企业id不能为空")
    private Integer companyid;

    @ApiModelProperty(value = "企业类型", example = "test", required = false, position = 3)
    @NotNull(message="企业类型")
    private String usertype;
}
