package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
@ToString
public class BAMerberPageVo {

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @NotNull(message = "页数不能为空")
    @ApiModelProperty(value = "页数", example = "1", required = true, position = 1)
    protected Integer pageNum = 1;

    @NotNull(message = "每页的总条数不能为空")
    @ApiModelProperty(value = "每页总条数", example = "10", required = true, position = 2)
    protected Integer pageSize = 2;
}
