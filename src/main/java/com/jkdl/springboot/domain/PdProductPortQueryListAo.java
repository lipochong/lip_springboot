package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class PdProductPortQueryListAo {

    @NotNull(message = "设备id不能为空")
    @ApiModelProperty(value = "productId", example = "100100", required = true, position = 1)
    private Integer productId;

    @NotNull(message = "设备充电状态不能为空")
    @ApiModelProperty(value = "statusCf", example = "10", required = true, position = 2)
    private Integer statusCf;




}
