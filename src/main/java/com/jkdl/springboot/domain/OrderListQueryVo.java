package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ToString
public class OrderListQueryVo extends PageEntity{

    @ApiModelProperty(value = "订单号", example = "test", required = false, position = 3)
    private String orderid;

    @ApiModelProperty(value = "创建开始时间", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "创建结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;

    @ApiModelProperty(value = "订单状态", example = "test", required = false, position = 3)
    private Integer status;

    @ApiModelProperty(value = "订单金额", example = "test", required = false, position = 3)
    private BigDecimal amount;

    @ApiModelProperty(value = "设备编号", example = "test", required = false, position = 3)
    private int productid;

    @ApiModelProperty(value = "房间号", example = "test", required = false, position = 3)
    private String detail;

    @ApiModelProperty(value = "商户名称", example = "test", required = false, position = 3)
    private String companyname;

    @ApiModelProperty(value = "商户名称模糊查询下的商户ids", example = "test", required = false, position = 3)
    private List<String> companynameQyIds;

    @ApiModelProperty(value = "平台id", example = "test", required = false, position = 3)
    private int platformId;

    @ApiModelProperty(value = "一级代理商id", example = "test", required = false, position = 3)
    private int oneCompanyId;

    @ApiModelProperty(value = "二级代理商id", example = "test", required = false, position = 3)
    private int twoCompanyId;

    @ApiModelProperty(value = "一级代理商下的商户ids", example = "test", required = false, position = 3)
    private List<String> oneCompanyQyIds;

    @ApiModelProperty(value = "二级代理商下的商户ids", example = "test", required = false, position = 3)
    private List<String> twoCompanyQyIds;

    @ApiModelProperty(value = "起始index 从0开始", example = "test", required = false, position = 3)
    private int pageStart;

    @ApiModelProperty(value = "订单号ids", example = "test", required = false, position = 3)
    private List<String> orderids;


}
