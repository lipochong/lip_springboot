package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;


@Data
@ToString
public class UsersWxVo {

    @ApiModelProperty(value = "用户昵称", example = "test昵称", required = true, position = 1)
    private String nickname;

    @ApiModelProperty(value = "用户头像", example = "www.baidu.com", required = true, position = 2)
    private String headimgurl;

    @ApiModelProperty(value = "encryptedData", example = "fdsaoifewq142543", required = true, position = 4)
    private String encryptedData;

    @ApiModelProperty(value = "code", example = "fdsaoifewq142543", required = true, position = 3)
    private String code;

    @ApiModelProperty(value = "iv", example = "fdsaoifewq142543", required = true, position = 5)
    private String iv;

    @ApiModelProperty(value = "unionid", example = "safsd23df24fwf", required = true, position = 2)
    private String unionid;

    @ApiModelProperty(value = "openid", example = "sdfsdf234234", required = true, position = 2)
    private String openid;

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @ApiModelProperty(value = "loginName", example = "1", required = true, position = 2)
    private String loginName;

    @ApiModelProperty(value = "password", example = "1", required = true, position = 2)
    private String password;



}
