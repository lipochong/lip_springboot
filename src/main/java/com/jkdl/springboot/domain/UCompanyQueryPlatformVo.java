package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyQueryPlatformVo extends PageEntity{


    @ApiModelProperty(value = "parentId", example = "test", required = false, position = 1)
    private Integer parentId;

    @ApiModelProperty(value = "companyType", example = "test", required = false, position = 1)
    private String companyType;

    @ApiModelProperty(value = "商户id", example = "test", required = false, position = 1)
    private int companyId;

}
