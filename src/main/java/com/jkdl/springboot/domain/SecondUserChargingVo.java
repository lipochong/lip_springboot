package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class SecondUserChargingVo {

    @NotNull(message = "openid不能为空")
    @ApiModelProperty(value = "openid", example = "fdsaoifewq142543", required = true, position = 1)
    private String openid;

    @NotNull(message = "用户id不能为空")
    @ApiModelProperty(value = "userId", example = "1000", required = true, position = 2)
    private String userId;



}
