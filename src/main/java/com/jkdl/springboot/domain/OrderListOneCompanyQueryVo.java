package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString
public class OrderListOneCompanyQueryVo extends PageEntity{

    @ApiModelProperty(value = "二级代理商id", example = "test", required = false, position = 3)
    private int twoCompanyId;

}
