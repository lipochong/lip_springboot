package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;


@Data
@ToString
public class BusinessActivityMemberVo {

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @ApiModelProperty(value = "userId", example = "1", required = true, position = 2)
    private Integer userId;

    @ApiModelProperty(value = "wxId", example = "1", required = true, position = 2)
    private Integer wxId;

    @ApiModelProperty(value = "businessActivityId", example = "1", required = true, position = 2)
    private Integer businessActivityId;

    @ApiModelProperty(value = "qyid", example = "1", required = true, position = 2)
    private Integer qyid;

    @ApiModelProperty(value = "qyUserId", example = "1", required = true, position = 2)
    private Integer qyUserId;

    @ApiModelProperty(value = "openid", example = "fddfg234234", required = true, position = 2)
    private String openid;

    @ApiModelProperty(value = "code", example = "fddfg234234", required = true, position = 2)
    private String code;

    @ApiModelProperty(value = "状态", example = "1", required = true, position = 2)
    private Integer status;

    @ApiModelProperty(value = "开始时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;


    @ApiModelProperty(value = "内容", example = "内容", required = true, position = 2)
    private String content;

    @ApiModelProperty(value = "优惠", example = "优惠", required = true, position = 2)
    private String discount;

    @ApiModelProperty(value = "金额", example = "2.22", required = true, position = 2)
    private String money;

    @ApiModelProperty(value = "标题", example = "标题", required = true, position = 2)
    private String title;

    @ApiModelProperty(value = "到期时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm", timezone = "GMT+8")
    private Date lasttime;

    @ApiModelProperty(value = "到期时间", example = "2018-12-12", required = false, position = 4)
    private String llasttime;

    @ApiModelProperty(value = "创建时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm", timezone = "GMT+8")
    private Date createtime;

    @ApiModelProperty(value = "创建时间", example = "2018-12-12", required = false, position = 4)
    private String lcreatetime;





}
