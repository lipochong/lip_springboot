package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class DongguanOrderQueryMonthVo {

    @ApiModelProperty(value = "数量", example = "test", required = false, position = 3)
    private int count;

    @ApiModelProperty(value = "订单时间", example = "test", required = false, position = 3)
    private Date orderDate;


}
