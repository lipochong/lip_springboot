package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UCompanyUpdateVo extends UCompanyAddVo{

    @ApiModelProperty(value = "商户id", example = "test", required = false, position = 3)
    @NotNull(message="商户不能为空")
    private Integer id;

}
