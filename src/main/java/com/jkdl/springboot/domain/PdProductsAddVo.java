package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class PdProductsAddVo{

    @ApiModelProperty(value = "产品名称", example = "test", required = false, position = 3)
    private String productName;

    @ApiModelProperty(value = "产品规格", example = "test", required = false, position = 3)
    private String specifications;

    @ApiModelProperty(value = "市场零售价", example = "test", required = false, position = 3)
    private Double money;

    @ApiModelProperty(value = "所属商家", example = "test", required = false, position = 3)
    private Integer  companyid;


    @ApiModelProperty(value = "设备批次", example = "test", required = false, position = 3)
    private Integer  batchnumber;

    @ApiModelProperty(value = "主要特点/备注", example = "test", required = false, position = 3)
    private String  productstyle;


    @ApiModelProperty(value = "新增端口：1,2,3,4,5", example = "test", required = false, position = 3)
    private Integer  selectPort;

    @ApiModelProperty(value = "新增数量", example = "test", required = false, position = 3)
    private Integer  productsNumber;

   /* @ApiModelProperty(value = "创建开始时间", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "创建结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;


    @ApiModelProperty(value = "修改开始时间", example = "2018-10-12", required = false, position = 3)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDatelast;

    @ApiModelProperty(value = "修改结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDatelast;*/


}
