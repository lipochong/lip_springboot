package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class OdrOrderdetailReportdatailVo {
    //-代理商渠道
    @ApiModelProperty(value = "代理商渠道", example = "test", required = false, position = 3)
    private Integer parentid;
    //直属代理商
    @ApiModelProperty(value = "直属代理商", example = "test", required = false, position = 3)
    private Integer grandParentId;
    //商户名称
    @ApiModelProperty(value = "商户名称", example = "test", required = false, position = 3)
    private String companyname;
    //商户ID
    @ApiModelProperty(value = "商户ID", example = "test", required = false, position = 3)
    private Integer id;
    //订单状态
    @ApiModelProperty(value = "订单状态", example = "test", required = false, position = 3)
    private Integer status;
    //开始时间
    @ApiModelProperty(value = "开始时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    //结束时间
    @ApiModelProperty(value = "结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;


    @ApiModelProperty(value = "", example = "test", required = false, position = 3)
    private Integer selectListSecondSelect;

    //按条件查询
    @ApiModelProperty(value = "订单按条件查询", example = "test", required = false, position = 3)
    private String condition;

    //按价格查订单
    @ApiModelProperty(value = "订单按价格查询", example = "test", required = false, position = 3)
    private Integer Price;
}
