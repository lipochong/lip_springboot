package com.jkdl.springboot.domain.res;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ResBaseAuthoritiesVo {

    private Integer id;

    private String displayName;

    private Integer parentid;

    private String iconcls;

    private String expanded;

    private String request;

    private Integer sortno;

    private String remark;

    private String icon;

    private String authtype;

    private Integer authno;

}
