package com.jkdl.springboot.domain.res;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jkdl.springboot.domain.PageEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
public class ResBaseUCompanyVo{

    private Integer id;

    private String companyname;

    private String code;

    private String email;

    private String mobile;

    private String phone;

    private Date createtime;

    private Double citypostage;

    private Double nationpostage;

    private String province;

    private String city;

    private String area;

    private Integer parentid;

    private String companytype;

    private Double rebateparent;

    private String companyimg;

    private String name;

    private Integer cuid;

    private Integer chargingtime;

    private Integer devicenumber;



}
