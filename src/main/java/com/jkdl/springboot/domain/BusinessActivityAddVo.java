package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;


@Data
@ToString
public class BusinessActivityAddVo {

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @ApiModelProperty(value = "userId", example = "1", required = true, position = 2)
    private Integer userId;

    @ApiModelProperty(value = "qyid", example = "1", required = true, position = 2)
    private Integer qyid;

    @ApiModelProperty(value = "qy_user_id", example = "1", required = true, position = 2)
    private Integer qyUserId;

    @ApiModelProperty(value = "unionid", example = "sdfg23dsgs5345fg", required = true, position = 2)
    private String unionid;

    @ApiModelProperty(value = "companyname", example = "1", required = true, position = 2)
    private String companyname;

    @ApiModelProperty(value = "imageUrl", example = "1", required = true, position = 2)
    private String imageUrl;

    @ApiModelProperty(value = "openid", example = "fddfg234234", required = true, position = 2)
    private String openid;

    @NotNull(message = "标题不能为空")
    @ApiModelProperty(value = "标题", example = "标题", required = true, position = 2)
    private String title;

    @NotNull(message = "内容不能为空")
    @ApiModelProperty(value = "内容", example = "内容", required = true, position = 2)
    private String content;

    @ApiModelProperty(value = "优惠", example = "优惠", required = true, position = 2)
    private String discount;

    @ApiModelProperty(value = "金额", example = "2.22", required = true, position = 2)
    private String money;

    @ApiModelProperty(value = "count", example = "12", required = true, position = 2)
    private Integer count;

    @ApiModelProperty(value = "joincount", example = "12", required = true, position = 2)
    private Integer joincount;

    @ApiModelProperty(value = "joincount", example = "12", required = true, position = 2)
    private Integer usecount;

    @ApiModelProperty(value = "类型", example = "1", required = true, position = 2)
    private Integer type;

    @ApiModelProperty(value = "状态", example = "1", required = true, position = 2)
    private Integer status;

    @ApiModelProperty(value = "开始时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    @ApiModelProperty(value = "结束时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;

    @ApiModelProperty(value = "到期时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm", timezone = "GMT+8")
    private Date lasttime;

    @ApiModelProperty(value = "创建时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm", timezone = "GMT+8")
    private Date createtime;

}
