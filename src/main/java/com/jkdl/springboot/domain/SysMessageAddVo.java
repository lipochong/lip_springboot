package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class SysMessageAddVo {


    @ApiModelProperty(value = "id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "文章标题", example = "test", required = false, position = 3)
    private String systitle;

    @ApiModelProperty(value = "文章内容", example = "test", required = false, position = 3)
    private  String syscontent;

    @ApiModelProperty(value = "文章作者", example = "test", required = false, position = 3)
    private String sysauthor;

    @ApiModelProperty(value = "文章链接", example = "test", required = false, position = 3)
    private String sysurl;

    @ApiModelProperty(value = "备注", example = "test", required = false, position = 3)
    private String sysnote;

    @ApiModelProperty(value = "结束时间", example = "test", required = false, position = 3)
    private Date endtime;


}
