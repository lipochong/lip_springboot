package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class UCompanyCodeVo {

    @ApiModelProperty(value = "登录名", example = "test", required = false, position = 3)

    private String code;

}
