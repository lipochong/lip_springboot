package com.jkdl.springboot.domain.membershipcard;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;


@Data
@ToString
public class MembershipCardVo {

    @ApiModelProperty(value = "id", example = "1", required = true, position = 2)
    private Integer id;

    @ApiModelProperty(value = "companyId", example = "1", required = true, position = 2)
    private Integer companyId;

    @ApiModelProperty(value = "status", example = "1", required = true, position = 2)
    private Integer status;

}
