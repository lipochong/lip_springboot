package com.jkdl.springboot.domain.membershipcard;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;


@Data
@ToString
public class MembershipCardLstVo {

    @ApiModelProperty(value = "lst", example = "1", required = true, position = 2)
    private List<?> lst;

    @ApiModelProperty(value = "lstMoney", example = "1", required = true, position = 2)
    private List<?> lstMoney;

}
