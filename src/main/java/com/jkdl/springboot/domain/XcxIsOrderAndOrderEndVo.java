package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class XcxIsOrderAndOrderEndVo {

    @ApiModelProperty(value = "用户id", example = "100100", required = true, position = 1)
    @NotNull
    private int userId;

    @ApiModelProperty(value = "设备id", example = "100100", required = true, position = 2)
    @NotNull
    private int productId;




}
