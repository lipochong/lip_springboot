package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyUpdateGrandParentIdVo{


    @ApiModelProperty(value = "parentId", example = "test", required = false, position = 1)
    private Integer parentId;


    @ApiModelProperty(value = "grandParentId", example = "test", required = false, position = 2)
    private Integer grandParentId;

}
