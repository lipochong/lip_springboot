package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class XcxCreateOrderVo{

    @ApiModelProperty(value = "设备id", example = "100100", required = true, position = 1)
    @NotNull
    private Integer productId;

    @ApiModelProperty(value = "用户id", example = "100100", required = true, position = 1)
    @NotNull
    private Integer userId;



}
