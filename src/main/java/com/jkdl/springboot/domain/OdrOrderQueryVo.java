package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class OdrOrderQueryVo extends PageEntity{

    @ApiModelProperty(value = "订单号", example = "test", required = false, position = 3)
    private String orderid;

   /* @ApiModelProperty(value = "创建时间", example = "test", required = false, position = 3)
    private String createdate;

    @ApiModelProperty(value = "订单状态", example = "test", required = false, position = 3)
    private String status;

    @ApiModelProperty(value = "支付金额", example = "test", required = false, position = 3)
    private String payamount;*/


}
