package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class DongguanOVo {

    @ApiModelProperty(value = "商户id", example = "test", required = false, position = 3)
    private int id;

    @ApiModelProperty(value = "商户companyName", example = "test", required = false, position = 3)
    private String companyName;

}
