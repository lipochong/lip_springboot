package com.jkdl.springboot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString
public class OdrOrderListQueryVo{

    @ApiModelProperty(value = "订单id", example = "test", required = false, position = 3)
    private int id;

    @ApiModelProperty(value = "订单号", example = "test", required = false, position = 3)
    private int cuid;

    @ApiModelProperty(value = "企业id", example = "test", required = false, position = 3)
    private int qyid;

    @ApiModelProperty(value = "订单号", example = "test", required = false, position = 3)
    private String orderid;

    @ApiModelProperty(value = "订单时间", example = "2018-12-12", required = false, position = 4)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date createdate;

    @ApiModelProperty(value = "订单状态", example = "test", required = false, position = 3)
    private int status;

    @ApiModelProperty(value = "订单金额", example = "test", required = false, position = 3)
    private BigDecimal amount;




}
