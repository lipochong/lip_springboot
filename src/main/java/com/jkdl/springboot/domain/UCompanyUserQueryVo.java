package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UCompanyUserQueryVo {

    @ApiModelProperty(value = "商家端用户id", example = "test", required = false, position = 3)
    private Integer id;

    @ApiModelProperty(value = "手机号", example = "test", required = false, position = 3)
    private String phone;


}
