package com.jkdl.springboot.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString
public class OrderListVo{

    @ApiModelProperty(value = "订单id", example = "test", required = false, position = 3)
    private int id;

    @ApiModelProperty(value = "订单号", example = "test", required = false, position = 3)
    private String orderid;

    @ApiModelProperty(value = "创建时间", example = "test", required = false, position = 3)
    private Date createdate;

    @ApiModelProperty(value = "订单状态", example = "test", required = false, position = 3)
    private int status;

    @ApiModelProperty(value = "订单金额", example = "test", required = false, position = 3)
    private BigDecimal amount;

    @ApiModelProperty(value = "设备编号", example = "test", required = false, position = 3)
    private int productid;

    @ApiModelProperty(value = "房间号", example = "test", required = false, position = 3)
    private String detail;

    @ApiModelProperty(value = "设备数量", example = "test", required = false, position = 3)
    private int devicenumber;

    @ApiModelProperty(value = "公司名称", example = "test", required = false, position = 3)
    private String companyname;

    @ApiModelProperty(value = "上级代理商", example = "test", required = false, position = 3)
    private String twoCompanyName;

    @ApiModelProperty(value = "一级代理商", example = "test", required = false, position = 3)
    private String oneCompanyName;

    @ApiModelProperty(value = "openid", example = "test", required = false, position = 3)
    private String openid;

}
