package com.jkdl.springboot.controller.xcxBusiness;

import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.UCompanyMapper;
import com.jkdl.springboot.domain.USecondUserAddVo;
import com.jkdl.springboot.domain.UsersIdVo;
import com.jkdl.springboot.domain.UsersPhoneVo;
import com.jkdl.springboot.domain.UsersWxVo;
import com.jkdl.springboot.service.UCompanyService;
import com.jkdl.springboot.service.UCompanyUserService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Api(description = "16C商家端微信小程序用户相关api-用户授权登录")
@RestController
@RequestMapping("/xcxBusiness/wxUser")
public class XcxBusinessLoginController {

    @Autowired
    private UCompanyService uCompanyService;

    @Autowired
    private UCompanyUserService uCompanyUserService;

    /**
     * 根据code获取openid注册用户信息
     */
    @ApiOperation(value = "判断微信用户是否已经注册", produces = "application/json", notes = "判断微信用户是否已经注册")
    @ApiImplicitParam(paramType = "path", name = "code", value = "用户code", required = true, dataType = "String", example = "vcxf45s465ds4afs")
    @GetMapping("/exist/{code}")
    public JsonResult checkoutUserExist(@PathVariable("code") String code) {
        Map<String, Object> resultMap = uCompanyService.checkoutUCompanyExist(code);
        return JsonResult.ok(resultMap);
    }

    /**
     * 注册登录
     */
    @ApiOperation(value = "判断微信用户是否已经注册", produces = "application/json", notes = "判断微信用户是否已经注册")
    @ApiImplicitParam(paramType = "path", name = "usersWxVo", value = "用户usersWxVo", required = true, dataType = "String", example = "usersWxVo")
    @PostMapping("/addUCompanyOpenid")
    public JsonResult addUCompanyOpenid(@RequestBody UsersWxVo usersWxVo){
        return JsonResult.ok(uCompanyService.addUCompanyOpenid(usersWxVo));
    }

    /**
     * 商家端下面用户登录
     */
    @ApiOperation(value = "微信商家端下面用户登录", produces = "application/json", notes = "微信商家端下面用户登录")
    @ApiImplicitParam(paramType = "path", name = "usersWxVo", value = "用户usersWxVo", required = true, dataType = "String", example = "usersWxVo")
    @PostMapping("/addUCompanyUserOpenid")
    public JsonResult addUCompanyUserOpenid(@RequestBody UsersWxVo usersWxVo){
        return JsonResult.ok(uCompanyUserService.addUCompanyUserOpenid(usersWxVo));
    }

    /**
     * 退出登录
     */
    @ApiOperation(value = "微信用户退出登录", produces = "application/json", notes = "微信用户退出登录")
    @ApiImplicitParam(paramType = "path", name = "usersIdVo", value = "usersIdVo", required = true, dataType = "String", example = "usersIdVo")
    @PostMapping("/quit")
    public JsonResult quitCompanyOpenid(@RequestBody UsersIdVo usersIdVo){
        return JsonResult.ok(uCompanyService.quitXcxLogin(usersIdVo));
    }

    /**
     * 商家端下级用户退出登录
     */
    @ApiOperation(value = "商家端下级用户退出登录", produces = "application/json", notes = "商家端下级用户退出登录")
    @ApiImplicitParam(paramType = "path", name = "usersIdVo", value = "usersIdVo", required = true, dataType = "String", example = "usersIdVo")
    @PostMapping("/quitUser")
    public JsonResult quitCompanyUserOpenid(@RequestBody UsersIdVo usersIdVo){
        return JsonResult.ok(uCompanyUserService.quitXcxUserLogin(usersIdVo));
    }

    /**
     * 用户注册
     */
    @ApiOperation(value = "用户注册", produces = "application/json", notes = "用户注册")
    @ApiImplicitParam(paramType = "path", name = "UsersWxVo", value = "UsersWxVo", required = true, dataType = "String", example = "UsersWxVo")
    @PostMapping("/register")
    public JsonResult registerBusinessUser(@RequestBody UsersPhoneVo usersPhoneVo) throws Exception {
        return JsonResult.ok(uCompanyUserService.addCompanyUser(usersPhoneVo));
    }

    @Value("${appidDir}")
    private Integer appidDir;

    @Value("${appkeyDir}")
    private String appkeyDir;

    /**
     * 获取验证码
     */
    @ApiOperation(value = "获取验证码", produces = "application/json", notes = "获取验证码")
    @ApiImplicitParam(paramType = "path", name = "UsersWxVo", value = "UsersWxVo", required = true, dataType = "String", example = "UsersWxVo")
    @PostMapping("/verificationCode")
    public JsonResult verificationCode(@RequestBody UsersPhoneVo usersPhoneVo){
        return JsonResult.ok(uCompanyUserService.verificationCode(usersPhoneVo, appidDir, appkeyDir));
    }

//    @RequestMapping(value = "admin/updimg", method = RequestMethod.POST)
//    @ResponseBody
//    public JsonResult updimg(@RequestParam("uploadImg") MultipartFile file, HttpServletResponse response, HttpServletRequest request){
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        String imgName;
//        //imgName= adminService.updimg(file,request);
//        //http://yhhvip.oss-cn-shenzhen.aliyuncs.com/
//        return JsonResult.ok("");
//    }

}
