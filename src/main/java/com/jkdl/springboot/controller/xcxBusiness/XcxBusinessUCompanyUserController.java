package com.jkdl.springboot.controller.xcxBusiness;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.UCompanyUserCodeVo;
import com.jkdl.springboot.domain.UsersWxVo;
import com.jkdl.springboot.service.BusinessActivityService;
import com.jkdl.springboot.service.UCompanyService;
import com.jkdl.springboot.service.UCompanyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/xcxBusiness/uCompanyUser")
public class XcxBusinessUCompanyUserController {

    @Autowired
    private UCompanyUserService uCompanyUserService;

    @Value("${appidDir}")
    private Integer appidDir;

    @Value("${appkeyDir}")
    private String appkeyDir;

    @ApiOperation(value = "获取验证码", produces = "application/json", notes = "获取验证码")
    @ApiImplicitParam(paramType = "path", name = "id", value = "id", required = true, dataType = "String", example = "1")
    @GetMapping("/getUCompanyUser/{phone}")
    public JsonResult getUCompany(@PathVariable("phone") String phone){
        return JsonResult.ok(uCompanyUserService.getCode(phone, appidDir, appkeyDir));
    }

    @ApiOperation(value = "设置密码", produces = "application/json", notes = "设置密码")
    @ApiImplicitParam(paramType = "path", name = "id", value = "id", required = true, dataType = "String", example = "1")
    @PostMapping("/updateUserPassword")
    public JsonResult getUCompany(@RequestBody UCompanyUserCodeVo uCompanyUserCodeVo) throws Exception{
        return JsonResult.ok(uCompanyUserService.updatePassword(uCompanyUserCodeVo));
    }

}
