package com.jkdl.springboot.controller.xcxBusiness;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.BAMerberPageVo;
import com.jkdl.springboot.service.BusinessActivityMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "16C微信小程序用户相关api  232")
@RestController
@RequestMapping("/xcxBusiness/activityMember")
public class XcxBusinessActivityMemberController {

    @Autowired
    private BusinessActivityMemberService businessActivityMemberService;


    @ApiOperation(value = "核销记录", produces = "application/json", notes = "核销记录")
    @ApiImplicitParam(paramType = "path", name = "qyid", value = "qyid", required = true, dataType = "String", example = "vcxf45s465ds4afs")
    @GetMapping("/getActivityMember/{qyid}")
    public JsonResult addActivity(@PathVariable("qyid") Integer qyid){
        return JsonResult.ok(businessActivityMemberService.getActivityMemberWriteOff(qyid));
    }

    @ApiOperation(value = "分页核销记录", produces = "application/json", notes = "核销记录")
    @ApiImplicitParam(paramType = "path", name = "bAMerberPageVo", value = "bAMerberPageVo", required = true, dataType = "String", example = "vcxf45s465ds4afs")
    @PostMapping("/getActivityMember")
    public JsonResult getActivity( @RequestBody BAMerberPageVo bAMerberPageVo){
        return JsonResult.ok(businessActivityMemberService.getActivityMemberWriteOff(bAMerberPageVo));
    }

}
