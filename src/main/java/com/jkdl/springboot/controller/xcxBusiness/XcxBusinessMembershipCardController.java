package com.jkdl.springboot.controller.xcxBusiness;

import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.entity.MembershipCard;
import com.jkdl.springboot.service.MembershipCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/xcxBusiness/membershipCard")
public class XcxBusinessMembershipCardController {

    @Autowired
    private MembershipCardService membershipCardService;

    @ApiOperation(value = "商家发布会员卡", produces = "application/json", notes = "商家发布会员卡")
    @ApiImplicitParam(name = "userVo", value = "请求参数实体", required = true, dataType = "membershipCard")
    @PostMapping("/addMembershipCard")
    public JsonResult addUser(@RequestBody MembershipCard membershipCard) throws Exception {
        BeanValidators.validate(membershipCard);
        return JsonResult.ok(membershipCardService.addMembershipCard(membershipCard));
    }

//    /**
//     * 登录获取微信unionid  平台唯一标志
//     */
//    @ApiOperation(value = "授权获取unionid", produces = "application/json", notes = "授权获取unionid")
//    @PostMapping("/loginAuth")
//    public JsonResult loginAuth(@RequestBody UsersWxVo usersWxVo) throws Exception{
//        BeanValidators.validate(usersWxVo);
//        int userId = uSecondUserService.loginAuth(usersWxVo);
//        if (0!=userId) {
//            return JsonResult.ok(userId);
//        }
//        return JsonResult.build("添加失败");
//
//    }

}
