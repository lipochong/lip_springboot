package com.jkdl.springboot.controller.xcxBusiness;

import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.BusinessActivityAddVo;
import com.jkdl.springboot.domain.BusinessActivityMemberVo;
import com.jkdl.springboot.domain.UsersWxVo;
import com.jkdl.springboot.entity.BusinessActivityMember;
import com.jkdl.springboot.service.BusinessActivityMemberService;
import com.jkdl.springboot.service.BusinessActivityService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Api(description = "16C微信小程序用户相关api  232")
@RestController
@RequestMapping("/xcxBusiness/activity")
public class XcxBusinessActivityController {

    @Autowired
    private USecondUserService uSecondUserService;

    @Autowired
    private BusinessActivityService businessActivityService;

    @Autowired
    private BusinessActivityMemberService businessActivityMemberService;


    @ApiOperation(value = "添加活动", produces = "application/json", notes = "添加活动")
    @ApiImplicitParam(paramType = "path", name = "code", value = "用户code", required = true, dataType = "String", example = "vcxf45s465ds4afs")
    @PostMapping("/addActivity")
    public JsonResult addActivity(@RequestBody BusinessActivityAddVo businessActivityAddVo){
        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityService.addActivity(businessActivityAddVo));
    }

    @ApiOperation(value = "活动列表", produces = "application/json", notes = "活动列表")
    @ApiImplicitParam(paramType = "path", name = "code", value = "用户code", required = true, dataType = "String", example = "vcxf45s465ds4afs")
    @PostMapping("/activityList")
    public JsonResult activityList(@RequestBody BusinessActivityAddVo businessActivityAddVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityService.activityList(businessActivityAddVo));
    }

    @ApiOperation(value = "核销列表", produces = "application/json", notes = "核销列表")
    @ApiImplicitParam(paramType = "path", name = "businessActivityAddVo", value = "businessActivityAddVo", required = true, dataType = "BusinessActivityAddVo", example = "businessActivityAddVo")
    @PostMapping("/activityMemberList")
    public JsonResult activityMemberList(@RequestBody BusinessActivityMemberVo businessActivityMemberVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityMemberService.activityMemberList(businessActivityMemberVo));
    }

    @ApiOperation(value = "核销", produces = "application/json", notes = "核销")
    @ApiImplicitParam(paramType = "path", name = "businessActivityAddVo", value = "businessActivityAddVo", required = true, dataType = "BusinessActivityAddVo", example = "businessActivityAddVo")
    @PostMapping("/useActivityMember")
    public JsonResult useActivityMember(@RequestBody BusinessActivityMemberVo businessActivityMemberVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityMemberService.useActivityMember(businessActivityMemberVo));
    }

    @Value("${uploadDir}")
    private String uploadDir;

    @ApiOperation(value = "上传图片", produces = "application/json", notes = "上传图片")
    @ApiImplicitParam(paramType = "path", name = "id", value = "id", required = true, dataType = "int", example = "1")
    @PostMapping("/uploadImg")
    public JsonResult uploadImg(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request) throws RuntimeException{
//        BeanValidators.validate(businessActivityAddVo);
        if (file.isEmpty()) {
            return JsonResult.build("文件不能为空");
        }
        Map<String, Object> map =  businessActivityService.uploadImg(file, request, uploadDir);

        return JsonResult.ok(map);

    }

    @ApiOperation(value = "根据活动Id查出业绩排行榜", produces = "application/json", notes = "根据活动Id查出业绩排行榜")
    @ApiImplicitParam(paramType = "path", name = "businessActivityAddVo", value = "businessActivityAddVo", required = true, dataType = "BusinessActivityAddVo", example = "businessActivityAddVo")
    @GetMapping("/activityMemberId/{id}")
    public JsonResult activityMemberId(@PathVariable("id") Integer id){
        return JsonResult.ok(businessActivityMemberService.getActivityMemberId(id));
    }
}
