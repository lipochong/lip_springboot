package com.jkdl.springboot.controller.xcxBusiness;

import com.github.pagehelper.PageInfo;
import com.google.gson.JsonObject;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.OdrOrderdetailWithBLOBs;
import com.jkdl.springboot.service.OdrOrderService;
import com.jkdl.springboot.service.OdrOrderdetailService;
import com.jkdl.springboot.service.PdProductsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Api(description = "16C商家端小程序订单相关api")
@RestController
@RequestMapping("/xcxBusiness/order")
public class XcxBusinessOrderController {

    @Autowired
    private OdrOrderdetailService orderdetailService;

    @Autowired
    private OdrOrderService orderService;

    @Autowired
    private PdProductsService pdProductsService;

    /**
     * 获取小程序商家每月订单列表
     */
    @ApiOperation(value="获取小程序商家每月订单列表", protocols = "application/json", notes = "获取小程序商家每月订单列表")
    @PostMapping("/orderMonthList")
    public JsonResult listAllLotUser(@RequestBody OdrOrderMonthListVo odrOrderMonthListVo) throws Exception{
//        BeanValidators.validate(odrOrderMonthListVo);

        return JsonResult.ok(orderdetailService.listAllLotUser(odrOrderMonthListVo));
    }

}
