package com.jkdl.springboot.controller.xcxBusiness;

import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.BusinessActivityAddVo;
import com.jkdl.springboot.service.BusinessActivityService;
import com.jkdl.springboot.service.UCompanyService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/xcxBusiness/uCompany")
public class XcxBusinessUCompanyController {

    @Autowired
    private UCompanyService uCompanyService;

    @Autowired
    private BusinessActivityService businessActivityService;

    @ApiOperation(value = "根据商户id获取商户信息", produces = "application/json", notes = "根据商户id获取商户信息")
    @ApiImplicitParam(paramType = "path", name = "id", value = "id", required = true, dataType = "String", example = "1")
    @GetMapping("/getUCompany/{id}")
    public JsonResult getUCompany(@PathVariable("id") String id){

        return JsonResult.ok(uCompanyService.getUCompany(Integer.parseInt(id)));
    }

}
