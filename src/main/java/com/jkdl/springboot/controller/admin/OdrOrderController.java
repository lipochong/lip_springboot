package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.OdrOrderQueryVo;
import com.jkdl.springboot.domain.OrderListQueryVo;
import com.jkdl.springboot.domain.OrderListVo;
import com.jkdl.springboot.entity.OdrOrder;
import com.jkdl.springboot.service.OdrOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单111
 */
@Api(description = "16C后台管理系统订单管理API")
@RestController
@RequestMapping("/admin/odrOrder")
public class OdrOrderController {

    @Autowired
    private OdrOrderService orderService;

    /**
     * 订单列表
     */
    @ApiOperation(value="订单列表", protocols = "application/json", notes = "订单列表")
    @PostMapping("/list")
    public JsonResult listAllLotOdrOrder(@RequestBody OdrOrderQueryVo odrOrderQueryVo){
        PageInfo<OdrOrder>  odrOrderPageInfo=orderService.selectOdrOrderList(odrOrderQueryVo);
        return JsonResult.ok(odrOrderPageInfo);
    }

    @ApiOperation(value="订单列表", notes = "订单列表")
    @RequestMapping("/orderList")
    public JsonResult orderList(@RequestBody OrderListQueryVo orderListQueryVo){

        return JsonResult.ok(orderService.orderList(orderListQueryVo));
    }

    @ApiOperation(value="修改订单状态", notes = "修改订单状态")
    @RequestMapping("/updateOrderStatus")
    public JsonResult updateOrderStatus(@RequestBody OrderListVo orderListVo){

        return JsonResult.ok(orderService.updateOrderStatus(orderListVo));
    }


}
