package com.jkdl.springboot.controller.admin;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.BusinessActivityListVo;
import com.jkdl.springboot.domain.BusinessActivityMemberListVo;
import com.jkdl.springboot.service.BusinessActivityMemberService;
import com.jkdl.springboot.service.BusinessActivityService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/admin/businessActivityMember")
public class BusinessActivityMemberController {

    @Autowired
    private USecondUserService uSecondUserService;

    @Autowired
    private BusinessActivityMemberService businessActivityMemberService;

    @ApiOperation(value = "参与活动列表分页", produces = "application/json", notes = "参与活动列表分页")
    @ApiImplicitParam(paramType = "path", name = "businessActivityMemberListVo", value = "businessActivityMemberListVo", required = true, dataType = "businessActivityMemberListVo", example = "businessActivityMemberListVo")
    @PostMapping("/getActivityMemberList")
    public JsonResult getActivityMemberList(@RequestBody BusinessActivityMemberListVo businessActivityMemberListVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityMemberService.selectBusinessActivityMemberList(businessActivityMemberListVo));
    }


}
