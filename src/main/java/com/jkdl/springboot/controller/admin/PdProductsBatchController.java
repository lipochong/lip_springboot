package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.PdProductsBatchAddVo;
import com.jkdl.springboot.domain.PdProductsBatchQueryVo;
import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryVo;
import com.jkdl.springboot.entity.PdProductsBatch;
import com.jkdl.springboot.entity.PdProductsBatchVoiceKey;
import com.jkdl.springboot.service.PdProductsBatchService;
import com.jkdl.springboot.service.PdProductsBatchVoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 设备批次
 */
@Api(description = "16C后台管理系统设备批次信息API")
@RestController
@RequestMapping("/admin/pdProductsBatch")
public class PdProductsBatchController {

    @Autowired
    private PdProductsBatchService pdProductsBatchService;

    /**
     * 设备批次列表
     */
    @ApiOperation(value="设备批次", protocols = "application/json", notes = "设备批次")
    @PostMapping("/list")
    public JsonResult listAllPdProductsBatch(@RequestBody PdProductsBatchQueryVo pdProductsBatchQueryVo){
        PageInfo<PdProductsBatch>  pdProductsBatchPageInfo=pdProductsBatchService.selectPdProductsBatchList(pdProductsBatchQueryVo);
        return JsonResult.ok(pdProductsBatchPageInfo);
    }

    /**
     * 批次添加
     */
    @ApiOperation(value="设备批次添加", protocols = "application/json", notes = "设备批次添加")
    @PostMapping("/addPdProductsBatch")
    public JsonResult addPdProductsBatch(@RequestBody PdProductsBatchAddVo pdProductsBatchAddVo){
        BeanValidators.validate(pdProductsBatchAddVo);
        int addPdProductsBatch=pdProductsBatchService.addPdProductsBatch(pdProductsBatchAddVo);
        return JsonResult.ok(addPdProductsBatch);
    }

}
