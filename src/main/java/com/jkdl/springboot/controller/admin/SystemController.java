package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.USers;
import com.jkdl.springboot.service.SystemUserRoleAuthService;
import com.jkdl.springboot.service.USersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Api(description = "后台用户权限管理api")
@RestController
@RequestMapping("/admin/system")
public class SystemController {

    @Autowired
    private USersService uSersService;

    @Autowired
    private SystemUserRoleAuthService systemUserRoleAuthService;

    /**
     * 用户登录
     */
    @ApiOperation(value = "用户登录", protocols = "application/json", notes = "用户登录")
    @ApiImplicitParam(name = "systemLoginVo", value = "请求参数实体", required = true, dataType = "SystemLoginVo")
    @PostMapping("/login")
    public JsonResult SystemLogin(@RequestBody SystemLoginVo systemLoginVo, HttpSession session) {
        BeanValidators.validate(systemLoginVo);

        int id = systemUserRoleAuthService.systemLogin(systemLoginVo);
        if(id==0){
            return JsonResult.build("登录失败");
        }
        SystemLoginSuccessVo systemLoginSuccessVo = new SystemLoginSuccessVo();
        systemLoginSuccessVo.setId(id);
        systemLoginSuccessVo.setLoginName(systemLoginVo.getLoginName());
        session.setAttribute("token",id);
        session.setAttribute("loginName",systemLoginVo.getLoginName());
        return JsonResult.ok(systemLoginSuccessVo);
    }

    /**
     * 用户退出  清理session
     */
    @ApiOperation(value = "用户退出", protocols = "application/json", notes = "用户退出")
    @ApiImplicitParam(name = "systemLoginVo", value = "请求参数实体", required = true, dataType = "SystemLoginVo")
    @PostMapping("/loginOut")
    public JsonResult SystemLogin(HttpSession session) {

        session.removeAttribute("token");
        session.removeAttribute("loginName");

        return JsonResult.ok("退出成功");
    }

    /**
     * 获取用户权限
     */
    @ApiOperation(value = "根据商户code获取商户id 获取用户权限列表", protocols = "application/json", notes = "获取用户权限")
    @ApiImplicitParam(name = "systemLoginVo", value = "请求参数实体", required = true, dataType = "SystemLoginVo")
    @PostMapping("/usersAuthList")
    public JsonResult getUsersAuthList(HttpSession session, @RequestBody SystemLoginSuccessVo systemLoginSuccessVo) {
        String token = "";
        String loginName = "";
        if(null==session.getAttribute("token")||"".equals(session.getAttribute("token"))){
            token = systemLoginSuccessVo.getId().toString();
            loginName = systemLoginSuccessVo.getLoginName();
        }else{
            token = session.getAttribute("token").toString();
            loginName = session.getAttribute("loginName").toString();
        }

        return systemUserRoleAuthService.getUsersAuthListByCode(loginName,Integer.parseInt(token));

        //return systemUserRoleAuthService.getUsersAuthList(Integer.parseInt(token));
    }
}
