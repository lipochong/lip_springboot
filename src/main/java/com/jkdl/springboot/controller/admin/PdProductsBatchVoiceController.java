package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryVo;
import com.jkdl.springboot.entity.PdProductsBatchVoiceKey;
import com.jkdl.springboot.service.PdProductsBatchVoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 批次对应合成音
 */
@Api(description = "16C后台管理系统批次对应合成音信息API")
@RestController
@RequestMapping("/admin/pdProductsBatchVoice")
public class PdProductsBatchVoiceController {

    @Autowired
    private PdProductsBatchVoiceService pdProductsBatchVoiceService;

    /**
     * 批次对应合成音列表
     */
    @ApiOperation(value="批次对应合成音", protocols = "application/json", notes = "批次对应合成音")
    @PostMapping("/list")
    public JsonResult listAllPdProductsBatchVoice(@RequestBody PdProductsBatchVoiceQueryVo pdProductsBatchVoiceQueryVo){
        PageInfo<PdProductsBatchVoiceKey>  pdProductsBatchVoiceKeyPageInfo=pdProductsBatchVoiceService.selectPdProductsBatchVoiceList(pdProductsBatchVoiceQueryVo);
        return JsonResult.ok(pdProductsBatchVoiceKeyPageInfo);
    }

}
