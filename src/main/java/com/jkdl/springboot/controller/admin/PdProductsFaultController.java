package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.PdProductsFaultAddVo;
import com.jkdl.springboot.domain.PdProductsFaultQueryVo;
import com.jkdl.springboot.entity.PdProductsFault;
import com.jkdl.springboot.service.PdProductsFaultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 故障信息
 */
@Api(description = "16C后台管理系统故障信息API")
@RestController
@RequestMapping("/admin/pdProductsFault")
public class PdProductsFaultController {

    @Autowired
    private PdProductsFaultService pdProductsFaultService;

    /**
     * 故障信息列表
     */
    @ApiOperation(value="故障信息", protocols = "application/json", notes = "故障信息")
    @PostMapping("/list")
    public JsonResult listAllPdProductsFault(@RequestBody PdProductsFaultQueryVo pdProductsFaultQueryVo){
        PageInfo<PdProductsFault>  pdProductsFaultPageInfo=pdProductsFaultService.selectPdProductsFaultList(pdProductsFaultQueryVo);
        return JsonResult.ok(pdProductsFaultPageInfo);
    }
    /**
     * 添加故障信息
     */
    @ApiOperation(value="添加故障信息", protocols = "application/json", notes = "添加故障信息")
    @PostMapping("/addPdProductsFault")
    public JsonResult addPdProductsFault(@RequestBody PdProductsFaultAddVo pdProductsFaultAddVo){

        BeanValidators.validate(pdProductsFaultAddVo);
        int pdProductsFault=pdProductsFaultService.addPdProductsFault(pdProductsFaultAddVo);
        return JsonResult.ok(pdProductsFault);
    }
}
