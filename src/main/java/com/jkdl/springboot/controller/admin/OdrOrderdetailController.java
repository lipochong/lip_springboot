package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.OdrOrderdetailQueryCompanyidVo;
import com.jkdl.springboot.domain.OdrOrderdetailQueryVo;
import com.jkdl.springboot.domain.OdrOrderdetailReportdatailVo;
import com.jkdl.springboot.entity.OdrOrderdetail;
import com.jkdl.springboot.service.OdrOrderdetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;


/**
 * 订单详细
 */
@Api(description = "16C后台管理系统订单详情管理API")
@RestController
@RequestMapping("/admin/odrOrderdetail")
public class OdrOrderdetailController {

    @Autowired
    private OdrOrderdetailService orderdetailService;

    /**
     * 订单列表
     */
    @ApiOperation(value="订单详细列表", protocols = "application/json", notes = "订单详细列表")
    @PostMapping("/list")
    public JsonResult listAllLotOdrOrderdetail(@RequestBody OdrOrderdetailQueryVo odrOrderdetailQueryVo){
        PageInfo<OdrOrderdetail>  odrOrderdetailPageInfo=orderdetailService.selectOdrOrderdetailList(odrOrderdetailQueryVo);
        return JsonResult.ok(odrOrderdetailPageInfo);
    }



    /**
     * 订单导出Excel
     */
    @ApiOperation(value="订单详细列表导出", notes = "订单详细列表导出")
    @RequestMapping("/exportOrderDetails")
    public  JsonResult exportOrderDetails(
            @RequestParam(name = "agentnamess") String agentnamess,
            @RequestParam(name = "companyname") String companyname,
            @RequestParam(name = "cuname") String cuname,
            @RequestParam(name = "openids") String openids,
            @RequestParam(name = "orderid") String orderid,
            @RequestParam(name = "payamount") String payamount,
            @RequestParam(name = "productid") Integer productid,
            @RequestParam(name = "startDate") String startDate,
            @RequestParam(name = "endDate") String endDate,
            @RequestParam(name = "status") String status,
            HttpServletResponse response) throws ParseException {
        HSSFWorkbook hssfWorkbook=orderdetailService.exportOrderDetails( agentnamess,companyname,cuname,openids,orderid,
                 payamount,productid,startDate,endDate,status,response);
        return  JsonResult.ok(hssfWorkbook);
    }


    @ApiOperation(value="订单统计通过companyID", notes = "订单统计通过companyID")
    @RequestMapping("/orderCountByCompanyid")
    public JsonResult orderCountByCompanyid(@RequestBody OdrOrderdetailQueryCompanyidVo odrOrderdetailQueryCompanyidVo){
        BeanValidators.validate(odrOrderdetailQueryCompanyidVo);
        int orderCountByCompanyid=orderdetailService.selectOdrOrderdetailCountByConpanyid(odrOrderdetailQueryCompanyidVo);
        return JsonResult.ok(orderCountByCompanyid);
    }


    @ApiOperation(value="订单总金额通过companyID", notes = "订单总金额通过companyID")
    @RequestMapping("/orderCountnationPostageByConpanyid")
    public JsonResult orderCountnationPostageByConpanyid(@RequestBody OdrOrderdetailQueryCompanyidVo odrOrderdetailQueryCompanyidVo){
        BeanValidators.validate(odrOrderdetailQueryCompanyidVo);
        int orderCountByCompanyid=orderdetailService.selectOdrOrderdetailCountnationPostageByConpanyid(odrOrderdetailQueryCompanyidVo);
        return JsonResult.ok(orderCountByCompanyid);
    }


    @ApiOperation(value="设备统计通过companyID", notes = "设备统计通过companyID")
    @RequestMapping("/orderCountPdProductByCompanyid")
    public JsonResult orderCountPdProductByCompanyid(@RequestBody OdrOrderdetailQueryCompanyidVo odrOrderdetailQueryCompanyidVo){
        BeanValidators.validate(odrOrderdetailQueryCompanyidVo);
        int orderCountPdProductByCompanyid=orderdetailService.selectOrderdetailCountPdProductByCompanyid(odrOrderdetailQueryCompanyidVo);
        return JsonResult.ok(orderCountPdProductByCompanyid);
    }

    @ApiOperation(value="订单报表查询", notes = "订单报表查询")
    @RequestMapping("/orderReportdatail")
    public JsonResult orderReportdatail(@RequestBody OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo){
        return JsonResult.ok( orderdetailService.orderReportdatail(odrOrderdetailReportdatailVo));
    }
}
