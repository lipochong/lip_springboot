package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.PdProductsAddVo;
import com.jkdl.springboot.domain.PdProductsQueryVo;
import com.jkdl.springboot.domain.PdProductsUpdateAuditstatusVo;
import com.jkdl.springboot.domain.PdProductsUpdateVo;
import com.jkdl.springboot.entity.PdProducts;
import com.jkdl.springboot.entity.PdProductsWithBLOBs;
import com.jkdl.springboot.service.PdProductsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Api(description = "16C后台管理系统设备管理API")
@RestController
@RequestMapping("/admin/pdProducts")
public class PdProductsController {

    @Autowired
    private PdProductsService pdProductsService;

    /**
     * 获取设备list
     * Auditstatus0：未分配，1：已分配可使用，2:维护中
     * companyid
     */
    @ApiOperation(value="获取所有设备列表", protocols = "application/json", notes = "获取所有设备列表")
    @PostMapping("/list")
    public JsonResult listAllPdProducts(@RequestBody PdProductsQueryVo pdProductsQueryVo){
        PageInfo<PdProductsWithBLOBs>  pdProductsPageInfo=pdProductsService.selectPdProductsList(pdProductsQueryVo);
        return  JsonResult.ok(pdProductsPageInfo);
    }

    /**
     * 新增设备
     * @param pdProductsAddVo
     * @return
     */
    @ApiOperation(value="新增设备", protocols = "application/json", notes = "新增设备")
    @PostMapping("/addPdProducts")
    public JsonResult addPdProducts(@RequestBody PdProductsAddVo pdProductsAddVo){
        BeanValidators.validate(pdProductsAddVo);
        int addPdProducts = pdProductsService.addPdProducts(pdProductsAddVo);
        return   JsonResult.ok(addPdProducts);
    }

    /**
     * 修改设备
     * @param pdProductsUpdateVo
     * @return
     */
    @ApiOperation(value="修改设备", protocols = "application/json", notes = "修改设备")
    @PutMapping("/updatePdProducts")
    public JsonResult updatePdProducts(@RequestBody PdProductsUpdateVo pdProductsUpdateVo){
        BeanValidators.validate(pdProductsUpdateVo);
        int updatePdProducts=pdProductsService.updatePdProducts(pdProductsUpdateVo);
        return  JsonResult.ok(updatePdProducts);
    }

    @ApiOperation(value="修改状态Auditstatus0：未分配，1：已分配可使用，2维护中", protocols = "application/json", notes = "修改状态Auditstatus0：未分配，1：已分配可使用，2维护中")
    @PutMapping("/updatePdProductsAuditstatus")
    public JsonResult updatePdProductsAuditstatus(@RequestBody PdProductsUpdateAuditstatusVo pdProductsUpdateAuditstatusVo){
        BeanValidators.validate(pdProductsUpdateAuditstatusVo);
        int updatePdProductsAuditstatus=pdProductsService.updatePdProductsAuditstatus(pdProductsUpdateAuditstatusVo);
        return  JsonResult.ok(updatePdProductsAuditstatus);
    }

    @ApiOperation(value="导入修改设备", protocols = "application/json", notes = "导入修改设备")
    @PostMapping("/importPdProducts")
    public JsonResult importPdProducts(@RequestParam("file") MultipartFile file) {
        String importPdProducts = "";
        String fileName = file.getOriginalFilename();
        try {
           importPdProducts = pdProductsService.pdProductsImport(fileName, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  JsonResult.ok(importPdProducts);
    }

    @ApiOperation(value="强制导入修改设备", protocols = "application/json", notes = "强制导入修改设备")
    @PostMapping("/directPdProductsImport")
    public JsonResult directPdProductsImport(@RequestParam("file") MultipartFile file) {
        String directPdProductsImport = "";
        String fileName = file.getOriginalFilename();
        try {
            directPdProductsImport = pdProductsService.directPdProductsImport(fileName, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  JsonResult.ok(directPdProductsImport);
    }


}
