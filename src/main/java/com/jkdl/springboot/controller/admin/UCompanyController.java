package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.UCompany;
import com.jkdl.springboot.service.SysCityService;
import com.jkdl.springboot.service.SysDistrictService;
import com.jkdl.springboot.service.SysProvinceService;
import com.jkdl.springboot.service.UCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 公司
 */
@Api(description = "16C后台管理系统用户管理API")
@RestController
@RequestMapping("/admin/uCompany")

public class UCompanyController {

    @Autowired
    private UCompanyService uCompanyService;

    @Autowired
    private SysProvinceService sysProvinceService;

    @Autowired
    private SysCityService sysCityService;

    @Autowired
    private SysDistrictService sysDistrictService;

    /**
     * 测试添加grandParentId
     */
    @ApiOperation(value = "测试添加grandParentId", protocols = "application/json", notes = "测试添加grandParentId")
    @PostMapping("/addGrandParentId")
    public JsonResult selectUCompanyListPage() {

        int count = uCompanyService.addGrandParentId();

        return JsonResult.ok(count);
    }

    /**
     * 获取所有公司列表
     */
    @ApiOperation(value = "获取所有公司列表", protocols = "application/json", notes = "获取所有平台")
    @PostMapping("/allUCompanyList")
    public JsonResult allUCompanyList(@RequestBody UCompanyQueryVo uCompanyQueryVo) {

        List<UCompany> uCompanyListInfo = uCompanyService.selectAllUCompanyList(uCompanyQueryVo);

        return JsonResult.ok(uCompanyListInfo);
    }

    /**
     * 获取所有公司列表分页
     */
    @ApiOperation(value = "获取所有公司列表分页", protocols = "application/json", notes = "获取所有平台")
    @PostMapping("/allUCompanyPage")
    public JsonResult allUCompanyPage(@RequestBody UCompanyQueryVo uCompanyQueryVo) {
        BeanValidators.validate(uCompanyQueryVo);

        return JsonResult.ok(uCompanyService.selectUCompanyList(uCompanyQueryVo));
    }

    /**
     * 获取代理商
     */
    @ApiOperation(value = "根据平台（一级代理）获取代理商", protocols = "application/json", notes = "根据平台（一级代理）获取代理商")
    @PostMapping("/getAgentList")
    public JsonResult getAgentList(@RequestBody UCompanyQueryPlatformVo uCompanyQueryPlatformVo) {

        List<UCompany> uCompanyListInfo = uCompanyService.getAgentList(uCompanyQueryPlatformVo);

        return JsonResult.ok(uCompanyListInfo);
    }

    /**
     * 添加公司（同时添加用户）
     */
    @ApiOperation(value = "添加公司（同时添加用户）", protocols = "application/json", notes = "添加公司（同时添加用户）")
    @PostMapping("/addCompany")
    public JsonResult addUCompanyAndUsers(@RequestBody UCompanyAddVo uCompanyAddVo) throws Exception{
        BeanValidators.validate(uCompanyAddVo);
        UCompanyCodeVo vcode = new UCompanyCodeVo();
        vcode.setCode(uCompanyAddVo.getCode());
        //登录名重复
        int count = uCompanyService.queryUCompanyByCode(vcode);
        if (count >= 1) {
            return JsonResult.build("登录名重复");
        }

        int uCompany = uCompanyService.addUCompany(uCompanyAddVo);

        return JsonResult.ok(uCompany);
    }

    /**
     * 修改公司信息
     */
    @ApiOperation(value = "修改公司信息", protocols = "application/json", notes = "修改公司信息")
    @PutMapping("/updateCompany")
    public JsonResult updateUCompanyAndUsers(@RequestBody UCompanyUpdateVo uCompanyUpdateVo) {
        BeanValidators.validate(uCompanyUpdateVo);

        return uCompanyService.updateUCompany(uCompanyUpdateVo);
    }

    /**
     * 删除/恢复公司信息
     */
    @ApiOperation(value = "删除/恢复公司信息", protocols = "application/json", notes = "删除/恢复公司信息")
    @DeleteMapping("/deleteCompany")
    public JsonResult deleteUCompanyAndUsers(@RequestBody UCompanyDeleteVo uCompanyDeleteVo) {
        BeanValidators.validate(uCompanyDeleteVo);

        int uCompany = uCompanyService.deleteUCompany(uCompanyDeleteVo);

        if(uCompany==1){
            return JsonResult.ok("操作成功");
        }

        return JsonResult.build("操作失败");
    }

    /**
     * 获取所有直属代理商和区域代理商名称和id  供客服录入商户查询
     */
    @ApiOperation(value = "获取所有代理商", protocols = "application/json", notes = "获取所有代理商")
    @PostMapping("/getAllAgent")
    public JsonResult getAllAgent(@RequestBody UCompanyQueryPlatformVo uCompanyQueryPlatformVo) {

        return JsonResult.ok(uCompanyService.getAgentList(uCompanyQueryPlatformVo));
    }

    /**
     * 获取所有省
     */
    @ApiOperation(value = "获取所有省", protocols = "application/json", notes = "获取所有代理商")
    @PostMapping("/getProvince")
    public JsonResult getProvinceDate() {
        return JsonResult.ok(sysProvinceService.getAllProvince());
    }

    /**
     * 根据省id获取所有市
     */
    @ApiOperation(value = "根据省id获取所有市", protocols = "application/json", notes = "根据省id获取所有市")
    @PostMapping("/getCity")
    public JsonResult getCityDate(@RequestBody SysCityQueryVo sysCityQueryVo) {
        return JsonResult.ok(sysCityService.getAllCityByProvinceId(sysCityQueryVo));
    }

    /**
     * 根据市id获取所有区
     */
    @ApiOperation(value = "根据省id获取所有市", protocols = "application/json", notes = "根据省id获取所有市")
    @PostMapping("/getDistrict")
    public JsonResult getDistrictDate(@RequestBody SysDistrictQueryVo sysCityQueryVo) {
        return JsonResult.ok(sysDistrictService.getAllDistrictByCityId(sysCityQueryVo));
    }


    /**
     * 查询东莞所有商户一个月订单成功数据
     */
    @ApiOperation(value = "根据省id获取所有市", protocols = "application/json", notes = "根据省id获取所有市")
    @PostMapping("/getDongguanOrder")
    public JsonResult getDongguanOrder(@RequestBody DongguanOVo dongguanOVo) {
        return JsonResult.ok(uCompanyService.getDongguanOrder(dongguanOVo));
    }

    /**
     *  查询结算数据
     */
    @ApiOperation(value = "结算管理", protocols = "application/json", notes = "结算管理")
    @PostMapping("/getJiesuan")
    public JsonResult getAgencyCompanyList(@RequestBody CloseAnAccountVo closeAnAccountVo) {
        //BeanValidators.validate(closeAnAccountVo);
        return JsonResult.ok(uCompanyService.getJiesuanList(closeAnAccountVo));
    }

    /**
     * 结算导出Excel
     */
    @ApiOperation(value="结算导出Excel", notes = "结算导出Excel")
    @PostMapping("/exportJiesuan")
    public JsonResult exportJiesuan(@RequestBody CloseAnAccountVo closeAnAccountVo, HttpServletResponse response)throws ParseException {
        return JsonResult.ok(uCompanyService.exportJiesuan(closeAnAccountVo,response));
    }

    /**
     * 结算导出Excel2
     */
    @ApiOperation(value="结算导出Excel", notes = "结算导出Excel")
    @RequestMapping("/exportBalance")
    public JsonResult exportBalance(@RequestParam(name = "parentid") String parentid,
                                    @RequestParam(name = "grandParentId") String grandParentId,
                                    @RequestParam(name = "companyname") String companyname,
                                    @RequestParam(name = "id") String id,
                                    @RequestParam(name = "nationpostage") String nationpostage,
                                    @RequestParam(name = "status") String status,
                                    @RequestParam(name = "startDate") String startDate,
                                    @RequestParam(name = "endDate") String endDate,
                                    @RequestParam(name = "selectListSecondSelect") String selectListSecondSelect,

                                    HttpServletResponse response)throws ParseException {

        CloseAnAccountVo closeAnAccountVo = new CloseAnAccountVo();
        closeAnAccountVo.setCompanyname(companyname);
        if(null!=parentid && !"".equals(parentid)){
            closeAnAccountVo.setParentid(Integer.parseInt(parentid));
        }

        if(null!=grandParentId && !"".equals(grandParentId)){
            closeAnAccountVo.setGrandParentId(Integer.parseInt(grandParentId));
        }

        if(null!=id && !"".equals(id)){
            closeAnAccountVo.setId(Integer.parseInt(id));
        }

        closeAnAccountVo.setNationpostage(nationpostage);

        if(null!=status && !"".equals(status)){
            closeAnAccountVo.setStatus(Integer.parseInt(status));
        }

        if(null!=selectListSecondSelect && !"".equals(selectListSecondSelect)){
            closeAnAccountVo.setSelectListSecondSelect(Integer.parseInt(selectListSecondSelect));
        }

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date startTime = new Date();
        String starTimes=formatter.format(startTime);
        // System.out.print(starTimes+"------现在时间--");
        if(null!=startDate&&""!=startDate&&null!=startDate){
            startTime = format.parse(startDate);
        }else{
            // startTime = format.parse("2017-12-12");
            startTime = format.parse(starTimes);
        }

        Date endTime = new Date();

        if(null!=endDate&&""!=endDate&&null!=endDate){
            endTime = format.parse(endDate);
        }

        closeAnAccountVo.setStartDate(startTime);

        closeAnAccountVo.setEndDate(endTime);

        return JsonResult.ok(uCompanyService.exportJiesuan(closeAnAccountVo,response));
    }
}
