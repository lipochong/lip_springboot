package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.USers;
import com.jkdl.springboot.entity.USersWithBLOBs;
import com.jkdl.springboot.service.USecondUserService;
import com.jkdl.springboot.service.USersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "16C后台管理系统用户管理API")
@RestController
@RequestMapping("/admin/uSers")
public class UsersController {

    @Autowired
    private USersService uSersService;

    @Autowired
    private USecondUserService uSecondUserService;

    /**
     * 获取所有用户列表
     */
    @ApiOperation(value="获取所有用户列表", protocols = "application/json", notes = "获取所有用户列表")
    @PostMapping("/list")
    public JsonResult listAllLotUser(@RequestBody UsersQueryVo usersQueryVo){

        System.out.print("获取所有用户列表登录名称");

        PageInfo<USers> uSersPageInfo = uSersService.selectUSersList(usersQueryVo);

        System.out.print(uSersPageInfo.getList().size());

        return JsonResult.ok(uSersPageInfo);
    }

    /**
     * 添加用户
     */
    @ApiOperation(value="添加用户", protocols = "application/json", notes = "添加用户")
    @PostMapping("/addUsers")
    public JsonResult addUsers(@RequestBody UsersAddVo usersAddVo){

        BeanValidators.validate(usersAddVo);
        int userss=uSersService.addUsers(usersAddVo);
        return JsonResult.ok(userss);
    }

    /**
     * 修改用户
     */
    @ApiOperation(value="修改用户", protocols = "application/json", notes = "修改用户")
    @PutMapping("/updateUsers")
    public JsonResult updateUsers(@RequestBody UsersUpdateVo usersUpdateVo){
        System.out.print("获取所有用户列表登录名称");
        BeanValidators.validate(usersUpdateVo);
        int userss=uSersService.updateUsers(usersUpdateVo);
        return JsonResult.ok(userss);
    }

    /**
     * 删除用户
     */
    @ApiOperation(value="删除用户", protocols = "application/json", notes = "删除用户")
    @DeleteMapping("/deleteUsers")
    public JsonResult deleteUsers(@RequestBody UsersDeleteVo usersDeleteVo){

        BeanValidators.validate(usersDeleteVo);
        int userss=uSersService.deleteUsers(usersDeleteVo);
        return JsonResult.ok(userss);
    }

    /**
     * 获取所有微信用户数
     */
//    @ApiOperation(value="获取所有微信用户数", protocols = "application/json", notes = "获取所有微信用户数")
//    @PostMapping("/getAllWxUsersCount")
//    public JsonResult selectAllSecondUserCount(@RequestBody SelectAllWxUsersCountVo selectAllWxUsersCountVo){
//
//        AllSecondUserCountVo userss=uSecondUserService.selectAllSecondUserCount(selectAllWxUsersCountVo);
//        return JsonResult.ok(userss);
//    }
}
