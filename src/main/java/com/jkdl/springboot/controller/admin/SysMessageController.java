package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.SysMessageAddVo;
import com.jkdl.springboot.domain.SysMessageDeleteVo;
import com.jkdl.springboot.domain.SysMessageQueryVo;
import com.jkdl.springboot.entity.SysMessage;
import com.jkdl.springboot.service.SysMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "后台系统消息管理api")
@RestController
@RequestMapping("/admin/sysmessage")
public class SysMessageController {
    @Autowired
    private SysMessageService sysMessageService;



    /**
     * 系统分页列表
     * @param sysMessageQueryVo
     * @return
     */
    @ApiOperation(value="系统消息分页", protocols = "application/json", notes = "系统消息分页")
    @PostMapping("/sysMessageList")
    public JsonResult sysMessageList(@RequestBody SysMessageQueryVo sysMessageQueryVo){
        PageInfo<SysMessage> sysMessagePageInfo=sysMessageService.sysMessageList(sysMessageQueryVo);
        return JsonResult.ok(sysMessagePageInfo);
    }

    /**
     * 添加系统消息
     */
    @ApiOperation(value="添加系统消息", protocols = "application/json", notes = "添加系统消息")
    @PostMapping("/addSysMessage")
    public JsonResult addSysMessage(@RequestBody SysMessageAddVo sysMessageAddVo){

        BeanValidators.validate(sysMessageAddVo);
        int addSysMessage=sysMessageService.addSysMessage(sysMessageAddVo);
        return JsonResult.ok(addSysMessage);
    }

    /**
     * 修改系统消息
     */
    @ApiOperation(value="修改系统消息", protocols = "application/json", notes = "修改系统消息")
    @PutMapping("/updateSysMessage")
    public JsonResult updateSysMessage(@RequestBody SysMessageAddVo sysMessageAddVo){

        BeanValidators.validate(sysMessageAddVo);
        int updateSysMessage=sysMessageService.updateSysMessage(sysMessageAddVo);
        return JsonResult.ok(updateSysMessage);
    }
    /**
     * 删除系统消息：修改bdelete为1
     */
    @ApiOperation(value="删除系统消息：修改bdelete为1", protocols = "application/json", notes = "删除系统消息：修改bdelete为1")
    @PutMapping("/deleteSysMessage")
    public JsonResult deleteSysMessage(@RequestBody SysMessageDeleteVo sysMessageDeleteVo){

        BeanValidators.validate(sysMessageDeleteVo);
        int deleteSysMessage=sysMessageService.deleteSysMessage(sysMessageDeleteVo);
        return JsonResult.ok(deleteSysMessage);
    }


}
