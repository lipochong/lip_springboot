package com.jkdl.springboot.controller.admin;

import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.BusinessActivityAddVo;
import com.jkdl.springboot.domain.BusinessActivityListVo;
import com.jkdl.springboot.service.BusinessActivityService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/admin/businessActivity")
public class BusinessActivityController {

    @Autowired
    private USecondUserService uSecondUserService;

    @Autowired
    private BusinessActivityService businessActivityService;

    @ApiOperation(value = "活动列表分页", produces = "application/json", notes = "活动列表分页")
    @ApiImplicitParam(paramType = "path", name = "businessActivityListVo", value = "businessActivityListVo", required = true, dataType = "businessActivityListVo", example = "businessActivityListVo")
    @PostMapping("/getActivityList")
    public JsonResult getActivityList(@RequestBody BusinessActivityListVo businessActivityListVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityService.selectBusinessActivityList(businessActivityListVo));
    }

    @ApiOperation(value = "审核活动", produces = "application/json", notes = "审核活动")
    @ApiImplicitParam(paramType = "path", name = "businessActivityListVo", value = "businessActivityListVo", required = true, dataType = "businessActivityListVo", example = "businessActivityListVo")
    @PostMapping("/updateActivity")
    public JsonResult updateActivity(@RequestBody BusinessActivityAddVo businessActivityAddVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityService.updateActivityStatus(businessActivityAddVo));
    }

}
