package com.jkdl.springboot.controller.admin;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.OdrOrderQueryVo;
import com.jkdl.springboot.domain.OrderListQueryVo;
import com.jkdl.springboot.domain.OrderListVo;
import com.jkdl.springboot.domain.USecondUserListQueryVo;
import com.jkdl.springboot.entity.OdrOrder;
import com.jkdl.springboot.service.OdrOrderService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信用户
 */
@Api(description = "16C后台管理系统订单管理API")
@RestController
@RequestMapping("/admin/uSecondUser")
public class USecondUserController {

    @Autowired
    private USecondUserService uSecondUserService;


    @ApiOperation(value="订单列表", notes = "订单列表")
    @RequestMapping("/getUSecondUserList")
    public JsonResult uSecondUserList(@RequestBody USecondUserListQueryVo uSecondUserListQueryVo){

        return JsonResult.ok(uSecondUserService.uSecondUserList(uSecondUserListQueryVo));
    }

    @ApiOperation(value="订单列表", notes = "订单列表")
    @RequestMapping("/updateSmlock")
    public JsonResult updateUSeconsUserSmlock(@RequestBody USecondUserListQueryVo uSecondUserListQueryVo){

        return JsonResult.ok(uSecondUserService.updateUSeconsUserSmlock(uSecondUserListQueryVo));
    }



}
