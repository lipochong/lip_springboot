package com.jkdl.springboot.controller.xcx;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.membershipcard.MembershipCardVo;
import com.jkdl.springboot.entity.MembershipCardUser;
import com.jkdl.springboot.service.MembershipCardService;
import com.jkdl.springboot.service.MembershipCardUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/xcx/membershipCard")
public class XcxMembershipCardController {

    @Autowired
    private MembershipCardService membershipCardService;

    @Autowired
    private MembershipCardUserService membershipCardUserService;

    @ApiOperation(value = "查询发布会员卡", produces = "application/json", notes = "查询发布会员卡")
    @ApiImplicitParam(name = "membershipCardVo", value = "请求参数实体", required = true, dataType = "membershipCard")
    @PostMapping("/getMembershipCard")
    public JsonResult addUser(@RequestBody MembershipCardVo membershipCardVo) {
        return JsonResult.ok(membershipCardService.getMembershipCardLst(membershipCardVo));
    }

    @ApiOperation(value = "用户办理会员卡", produces = "application/json", notes = "用户办理会员卡")
    @ApiImplicitParam(name = "membershipCardUser", value = "请求参数实体", required = true, dataType = "membershipCard")
    @PostMapping("/addMembershipCardUser")
    public JsonResult addUser(@RequestBody MembershipCardUser membershipCardUser) {
        return JsonResult.ok(membershipCardUserService.addMembershipCardUser(membershipCardUser));
    }

}
