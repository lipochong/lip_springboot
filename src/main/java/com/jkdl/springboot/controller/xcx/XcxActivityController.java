package com.jkdl.springboot.controller.xcx;

import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.DateTools;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.BusinessActivityAddVo;
import com.jkdl.springboot.service.BusinessActivityService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/xcx/activity")
public class XcxActivityController {

    @Autowired
    private USecondUserService uSecondUserService;

    @Autowired
    private BusinessActivityService businessActivityService;

    @ApiOperation(value = "活动列表", produces = "application/json", notes = "活动列表")
    @ApiImplicitParam(paramType = "path", name = "businessActivityAddVo", value = "businessActivityAddVo", required = true, dataType = "BusinessActivityAddVo", example = "vcxf45s465ds4afs")
    @PostMapping("/activityList")
    public JsonResult activityList(@RequestBody BusinessActivityAddVo businessActivityAddVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityService.activityList(businessActivityAddVo));
    }

    @ApiOperation(value = "活动详情", produces = "application/json", notes = "活动详情")
    @ApiImplicitParam(paramType = "path", name = "businessActivityAddVo", value = "businessActivityAddVo", required = true, dataType = "businessActivityAddVo", example = "1")
    @PostMapping("/activityDetails")
    public JsonResult activityDetails(@RequestBody BusinessActivityAddVo businessActivityAddVo) {
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityService.activityDetails(businessActivityAddVo));
    }


}
