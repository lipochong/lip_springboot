package com.jkdl.springboot.controller.xcx;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.UCompanyMapper;
import com.jkdl.springboot.domain.BusinessActivityAddVo;
import com.jkdl.springboot.domain.BusinessActivityMemberShareVo;
import com.jkdl.springboot.domain.BusinessActivityMemberVo;
import com.jkdl.springboot.service.BusinessActivityMemberService;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/xcx/activityMember")
public class XcxActivityMemberController {

    @Autowired
    private USecondUserService uSecondUserService;

    @Autowired
    private BusinessActivityMemberService businessActivityMemberService;

    @Autowired
    private UCompanyMapper uCompanyMapper;

    @ApiOperation(value = "获取用户活动详情", produces = "application/json", notes = "获取用户活动详情")
    @ApiImplicitParam(paramType = "path", name = "businessActivityMemberVo", value = "businessActivityMemberVo", required = true, dataType = "businessActivityMemberVo", example = "businessActivityMemberVo")
    @PostMapping("/getUcompanyInfo")
    public JsonResult getUcompanyInfo(@RequestBody BusinessActivityMemberVo businessActivityMemberVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(uCompanyMapper.selectByPrimaryKey(businessActivityMemberVo.getQyid()));

    }

    @ApiOperation(value = "参加活动", produces = "application/json", notes = "参加活动")
    @ApiImplicitParam(paramType = "path", name = "id", value = "id", required = true, dataType = "int", example = "1")
    @PostMapping("/addActivityMember")
    public JsonResult addActivityMember(@RequestBody BusinessActivityAddVo businessActivityAddVo){
//        BeanValidators.validate(businessActivityAddVo);
        return businessActivityMemberService.joinActivity(businessActivityAddVo);

    }

    @ApiOperation(value = "分享好友", produces = "application/json", notes = "分享好友")
    @ApiImplicitParam(paramType = "path", name = "id", value = "id", required = true, dataType = "int", example = "1")
    @PostMapping("/addActivityMemberUser")
    public JsonResult addActivityMemberUser(@RequestBody BusinessActivityMemberShareVo businessActivityMemberShareVo){
        return businessActivityMemberService.joinActivityShareUser(businessActivityMemberShareVo);

    }


    @ApiOperation(value = "商户详情", produces = "application/json", notes = "商户详情")
    @ApiImplicitParam(paramType = "path", name = "businessActivityMemberVo", value = "businessActivityMemberVo", required = true, dataType = "businessActivityMemberVo", example = "businessActivityMemberVo")
    @PostMapping("/getActivityMemberDetails")
    public JsonResult getActivityMemberDetails(@RequestBody BusinessActivityMemberVo businessActivityMemberVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityMemberService.getActivityMemberDetails(businessActivityMemberVo));

    }

    /**
     * 用户活动列表
     */
    @ApiOperation(value = "用户活动列表", produces = "application/json", notes = "用户活动列表")
    @ApiImplicitParam(paramType = "path", name = "businessActivityMemberVo", value = "businessActivityMemberVo", required = true, dataType = "businessActivityMemberVo", example = "businessActivityMemberVo")
    @PostMapping("/getActivityMemberList")
    public JsonResult getActivityMemberList(@RequestBody BusinessActivityMemberVo businessActivityMemberVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityMemberService.getActivityMemberList(businessActivityMemberVo));

    }

    @ApiOperation(value = "查看商户详情", produces = "application/json", notes = "查看商户详情")
    @ApiImplicitParam(paramType = "path", name = "businessActivityMemberVo", value = "businessActivityMemberVo", required = true, dataType = "businessActivityMemberVo", example = "businessActivityMemberVo")
    @PostMapping("/getQueryActivityMemberDetails")
    public JsonResult getQueryActivityMemberDetails(@RequestBody BusinessActivityMemberVo businessActivityMemberVo){
//        BeanValidators.validate(businessActivityAddVo);
        return JsonResult.ok(businessActivityMemberService.getQueryActivityMemberDetails(businessActivityMemberVo));

    }

}
