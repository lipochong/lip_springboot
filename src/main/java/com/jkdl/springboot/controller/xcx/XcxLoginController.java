package com.jkdl.springboot.controller.xcx;

import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.USecondUserAddVo;
import com.jkdl.springboot.domain.UsersWxVo;
import com.jkdl.springboot.service.USecondUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(description = "16C微信小程序用户相关api")
@RestController
@RequestMapping("/xcx/wxUser")
public class XcxLoginController {

    @Autowired
    private USecondUserService uSecondUserService;

    /**
     * 根据code获取openid注册用户信息
     */
    @ApiOperation(value = "判断微信用户是否已经注册", produces = "application/json", notes = "判断微信用户是否已经注册")
    @ApiImplicitParam(paramType = "path", name = "code", value = "用户code", required = true, dataType = "String", example = "vcxf45s465ds4afs")
    @GetMapping("/exist/{code}")
    public JsonResult checkoutUserExist(@PathVariable("code") String code) {
        Map<String, Object> resultMap = uSecondUserService.checkoutUserExist(code);
        return JsonResult.ok(resultMap);
    }

    @ApiOperation(value = "添加微信用户信息", produces = "application/json", notes = "添加微信用户信息")
    @ApiImplicitParam(name = "userVo", value = "请求参数实体", required = true, dataType = "USecondUserAddVo")
    @PostMapping("/addUser")
    public JsonResult addUser(@RequestBody USecondUserAddVo uSecondUserVo) throws Exception {
        BeanValidators.validate(uSecondUserVo);
        Integer wxUserId = uSecondUserService.addUser(uSecondUserVo);
        if (null != wxUserId) {
            return JsonResult.ok(wxUserId);
        }
        return JsonResult.build("添加失败");
    }

//    @ApiOperation(value = "判断用户是否免支付", produces = "application/json", notes = "添加微信用户信息")
//    @ApiImplicitParam(name = "userVo", value = "请求参数实体", required = true, dataType = "USecondUserAddVo")
//    @PostMapping("/addUser")
//    public JsonResult addUser(@RequestBody USecondUserAddVo uSecondUserVo) throws Exception {
//        BeanValidators.validate(uSecondUserVo);
//        Integer wxUserId = uSecondUserService.addUser(uSecondUserVo);
//        if (null != wxUserId) {
//            return JsonResult.ok(wxUserId);
//        }
//        return JsonResult.build("添加失败");
//    }

    /**
     * 登录获取微信unionid  平台唯一标志
     */
    @ApiOperation(value = "授权获取unionid", produces = "application/json", notes = "授权获取unionid")
    @PostMapping("/loginAuth")
    public JsonResult loginAuth(@RequestBody UsersWxVo usersWxVo) throws Exception{
        BeanValidators.validate(usersWxVo);
        int userId = uSecondUserService.loginAuth(usersWxVo);
        if (0!=userId) {
            return JsonResult.ok(userId);
        }
        return JsonResult.build("添加失败");

    }

}
