package com.jkdl.springboot.controller.xcx;

import com.github.pagehelper.PageInfo;
import com.google.gson.JsonObject;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.PdProductsVoiceMapper;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.OdrOrderdetailWithBLOBs;
import com.jkdl.springboot.service.OdrOrderService;
import com.jkdl.springboot.service.OdrOrderdetailService;
import com.jkdl.springboot.service.PdProductsBatchVoiceService;
import com.jkdl.springboot.service.PdProductsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Api(description = "16C小程序订单设备相关api")
@RestController
@RequestMapping("/xcx/product")
public class XcxProductController {

    @Autowired
    private OdrOrderdetailService orderdetailService;

    @Autowired
    private OdrOrderService orderService;

    @Autowired
    private PdProductsService pdProductsService;

    @Autowired
    private PdProductsBatchVoiceService pdProductsBatchVoiceService;

    /**
     * 获取充电播放语音地址
     */
    @ApiOperation(value="获取充电播放语音地址", protocols = "application/json", notes = "获取充电播放语音地址")
    @PostMapping("/getAudioVoice")
    public JsonResult getAudioVoice(@RequestBody PdVoiceQueryByBatchNumberVo pdVoiceQueryByBatchNumberVo){

        BeanValidators.validate(pdVoiceQueryByBatchNumberVo);

        return JsonResult.ok(pdProductsBatchVoiceService.getAudioVoice(pdVoiceQueryByBatchNumberVo));
    }

    /**
     * 查询当前有没有正在充电订单
     */
//    @ApiOperation(value = "查询当前有没有正在充电订单", produces = "application/json", notes = "查询当前有没有正在充电订单")
//    @ApiImplicitParam(name = "userVo", value = "请求参数实体", required = true, dataType = "SecondUserChargingVo")
//    @PostMapping("/isChargingOrder")
//    public JsonResult addUser(@RequestBody SecondUserChargingVo secondUserChargingVo) throws Exception {
//        BeanValidators.validate(secondUserChargingVo);
//        Integer wxUserId = orderdetailService.addUser(uSecondUserVo);
//        if (null != wxUserId) {
//            return JsonResult.ok(wxUserId);
//        }
//        return JsonResult.build("添加失败");
//    }

    /**
     * 创建预支付订单之前判断设备情况
     * @param
     * @return
     */
    @ApiOperation(value = "创建预支付订单之前判断设备情况", produces = "application/json", notes = "创建预支付订单之前判断设备情况")
    @ApiImplicitParam(name = "pdProductSelectByIdVo", value = "请求参数实体", required = true, dataType = "PdProductSelectByIdVo")
    @PostMapping("/getProductById")
    public JsonResult createOrder(@RequestBody PdProductSelectByIdVo pdProductSelectByIdVo) {

        BeanValidators.validate(pdProductSelectByIdVo);

        return pdProductsService.selectProductsById(pdProductSelectByIdVo);
    }

    /**
     * 创建预支付订单
     * @param xcxCreateOrderVo
     * @return
     */
    @ApiOperation(value = "创建预支付订单", produces = "application/json", notes = "创建与支付订单")
    @ApiImplicitParam(name = "xcxCreateOrderVo", value = "请求参数实体", required = true, dataType = "XcxCreateOrder")
    @PostMapping("/createOrder")
    public JsonResult createOrder(@RequestBody XcxCreateOrderVo xcxCreateOrderVo) {

        BeanValidators.validate(xcxCreateOrderVo);

        Map<String, Object> map = new HashMap<>();

        try {
            map = orderService.createOrder(xcxCreateOrderVo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return JsonResult.ok(map);

    }

    /**
     * 小程序支付订单
     * @param xcxPayOrderVo
     * @return
     */
//    @ApiOperation(value = "小程序支付订单", produces = "application/json", notes = "小程序支付订单")
    @ApiImplicitParam(name = "xcxPayOrderVo", value = "请求参数实体", required = true, dataType = "XcxPayOrderVo")
    @RequestMapping("/payOrder")
    public JsonResult createOrder(@RequestBody XcxPayOrderVo xcxPayOrderVo) {

        BeanValidators.validate(xcxPayOrderVo);

        String outTradeNo = null;
        JsonObject returnData = null;
        try {
            outTradeNo = orderService.payOrder(xcxPayOrderVo);

            //returnData = new JsonParser().parse(outTradeNo).getAsJsonObject();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return JsonResult.ok(outTradeNo);
    }

    /**
     * 根据设备id获取设备信息及设备充电时长
     * @param pdProductSelectByIdVo
     * @return
     */
    @ApiImplicitParam(name = "pdProductSelectByIdVo", value = "请求参数实体", required = true, dataType = "PdProductSelectByIdVo")
    @RequestMapping("/getProductByProductId")
    public JsonResult getProductByProductId(@RequestBody PdProductSelectByIdVo pdProductSelectByIdVo) {

        BeanValidators.validate(pdProductSelectByIdVo);

        return pdProductsService.getProductByProductId(pdProductSelectByIdVo);
    }

    /**
     * 根据设备id获取客服电话
     * @param pdProductSelectByIdVo
     * @return
     */
    @ApiImplicitParam(name = "pdProductSelectByIdVo", value = "请求参数实体", required = true, dataType = "PdProductSelectByIdVo")
    @RequestMapping("/getCustomerPhoneByProductId")
    public JsonResult getCustomerPhoneByProductId(@RequestBody PdProductSelectByIdVo pdProductSelectByIdVo) {

        BeanValidators.validate(pdProductSelectByIdVo);

        return pdProductsService.getProductByProductId(pdProductSelectByIdVo);
    }


}
