package com.jkdl.springboot.controller.xcx;

import com.github.pagehelper.PageInfo;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jkdl.springboot.common.util.BeanValidators;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.OdrOrderdetail;
import com.jkdl.springboot.entity.OdrOrderdetailWithBLOBs;
import com.jkdl.springboot.entity.PdProducts;
import com.jkdl.springboot.entity.USers;
import com.jkdl.springboot.service.OdrOrderService;
import com.jkdl.springboot.service.OdrOrderdetailService;
import com.jkdl.springboot.service.PdProductsService;
import com.jkdl.springboot.service.USersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
@Slf4j
@Api(description = "16C小程序订单相关api")
@RestController
@RequestMapping("/xcx/order")
public class XcxOrderController {

    @Autowired
    private OdrOrderdetailService orderdetailService;

    @Autowired
    private OdrOrderService orderService;

    @Autowired
    private PdProductsService pdProductsService;

    /**
     * 获取小程序个人订单列表
     */
    @ApiOperation(value="获取小程序个人订单", protocols = "application/json", notes = "获取小程序个人订单")
    @PostMapping("/list")
    public JsonResult listAllLotUser(@RequestBody OdrOrderdetailQueryOpenidVo odrOrderdetailQueryOpenidVo){

        PageInfo<OdrOrderdetailWithBLOBs>  odrOrderdetailWithBLOBsPageInfo=orderdetailService.selectOdrOrderdetailByOpenidpage(odrOrderdetailQueryOpenidVo);
        return JsonResult.ok(odrOrderdetailWithBLOBsPageInfo);
    }

    /**
     * 查询当前有没有正在充电订单
     */
//    @ApiOperation(value = "查询当前有没有正在充电订单", produces = "application/json", notes = "查询当前有没有正在充电订单")
//    @ApiImplicitParam(name = "userVo", value = "请求参数实体", required = true, dataType = "SecondUserChargingVo")
//    @PostMapping("/isChargingOrder")
//    public JsonResult addUser(@RequestBody SecondUserChargingVo secondUserChargingVo) throws Exception {
//        BeanValidators.validate(secondUserChargingVo);
//        Integer wxUserId = orderdetailService.addUser(uSecondUserVo);
//        if (null != wxUserId) {
//            return JsonResult.ok(wxUserId);
//        }
//        return JsonResult.build("添加失败");
//    }

    /**
     * 创建预支付订单之前判断设备情况
     * @param
     * @return
     */
    @ApiOperation(value = "创建预支付订单之前判断设备情况", produces = "application/json", notes = "创建预支付订单之前判断设备情况")
    @ApiImplicitParam(name = "pdProductSelectByIdVo", value = "请求参数实体", required = true, dataType = "PdProductSelectByIdVo")
    @PostMapping("/getProductById")
    public JsonResult createOrder(@RequestBody PdProductSelectByIdVo pdProductSelectByIdVo) {

        BeanValidators.validate(pdProductSelectByIdVo);

        return pdProductsService.selectProductsById(pdProductSelectByIdVo);
    }

    /**
     * 创建预支付订单
     * @param xcxCreateOrderVo
     * @return
     */
    @ApiOperation(value = "创建预支付订单", produces = "application/json", notes = "创建与支付订单")
    @ApiImplicitParam(name = "xcxCreateOrderVo", value = "请求参数实体", required = true, dataType = "XcxCreateOrder")
    @PostMapping("/createOrder")
    public JsonResult createOrder(@RequestBody XcxCreateOrderVo xcxCreateOrderVo) {

        BeanValidators.validate(xcxCreateOrderVo);

        Map<String, Object> map = new HashMap<>();

        try {
            map = orderService.createOrder(xcxCreateOrderVo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return JsonResult.ok(map);

    }

    /**
     * 小程序支付订单
     * @param xcxPayOrderVo
     * @return
     */
//    @ApiOperation(value = "小程序支付订单", produces = "application/json", notes = "小程序支付订单")
    @ApiImplicitParam(name = "xcxPayOrderVo", value = "请求参数实体", required = true, dataType = "XcxPayOrderVo")
    @RequestMapping("/payOrder")
    public JsonResult createOrder(@RequestBody XcxPayOrderVo xcxPayOrderVo) {

        BeanValidators.validate(xcxPayOrderVo);

        String outTradeNo = null;
        JsonObject returnData = null;
        try {
            outTradeNo = orderService.payOrder(xcxPayOrderVo);

            //returnData = new JsonParser().parse(outTradeNo).getAsJsonObject();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return JsonResult.ok(outTradeNo);
    }

    /**
     * 小程序支付成功之后的回调
     */
    //@ApiImplicitParam(name = "xcxPayOrderVo", value = "请求参数实体", required = true, dataType = "XcxPayOrderVo")
    @PostMapping("/weixinNotify")
    public JsonResult createOrder(@RequestBody String resultStr) {

        log.info("----微信支付回调----");
        return JsonResult.ok(orderService.weixinNotify(resultStr));
    }

    /**
     * 判断订单是否存在且是否已过期
     * @param xcxIsOrderAndOrderEndVo
     * @return
     */
    @ApiOperation(value = "判断订单是否存在且是否已过期", produces = "application/json", notes = "判断订单是否存在且是否已过期")
    @ApiImplicitParam(name = "xcxIsOrderAndOrderEndVo", value = "请求参数实体", required = true, dataType = "XcxIsOrderAndOrderEndVo")
    @PostMapping("/isOrderAndEnd")
    public JsonResult isOrderAndEnd(@RequestBody XcxIsOrderAndOrderEndVo xcxIsOrderAndOrderEndVo) {

        BeanValidators.validate(xcxIsOrderAndOrderEndVo);

        return orderdetailService.isOrderAndEnd(xcxIsOrderAndOrderEndVo);
    }
}
