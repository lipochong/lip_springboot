package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UCompanyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UCompanyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCompanynameIsNull() {
            addCriterion("companyname is null");
            return (Criteria) this;
        }

        public Criteria andCompanynameIsNotNull() {
            addCriterion("companyname is not null");
            return (Criteria) this;
        }

        public Criteria andCompanynameEqualTo(String value) {
            addCriterion("companyname =", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameNotEqualTo(String value) {
            addCriterion("companyname <>", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameGreaterThan(String value) {
            addCriterion("companyname >", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameGreaterThanOrEqualTo(String value) {
            addCriterion("companyname >=", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameLessThan(String value) {
            addCriterion("companyname <", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameLessThanOrEqualTo(String value) {
            addCriterion("companyname <=", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameLike(String value) {
            addCriterion("companyname like", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameNotLike(String value) {
            addCriterion("companyname not like", value, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameIn(List<String> values) {
            addCriterion("companyname in", values, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameNotIn(List<String> values) {
            addCriterion("companyname not in", values, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameBetween(String value1, String value2) {
            addCriterion("companyname between", value1, value2, "companyname");
            return (Criteria) this;
        }

        public Criteria andCompanynameNotBetween(String value1, String value2) {
            addCriterion("companyname not between", value1, value2, "companyname");
            return (Criteria) this;
        }

        public Criteria andCodeIsNull() {
            addCriterion("code is null");
            return (Criteria) this;
        }

        public Criteria andCodeIsNotNull() {
            addCriterion("code is not null");
            return (Criteria) this;
        }

        public Criteria andCodeEqualTo(String value) {
            addCriterion("code =", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("code <>", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThan(String value) {
            addCriterion("code >", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("code >=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThan(String value) {
            addCriterion("code <", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("code <=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLike(String value) {
            addCriterion("code like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotLike(String value) {
            addCriterion("code not like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeIn(List<String> values) {
            addCriterion("code in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("code not in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("code between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("code not between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andLicenseIsNull() {
            addCriterion("license is null");
            return (Criteria) this;
        }

        public Criteria andLicenseIsNotNull() {
            addCriterion("license is not null");
            return (Criteria) this;
        }

        public Criteria andLicenseEqualTo(String value) {
            addCriterion("license =", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseNotEqualTo(String value) {
            addCriterion("license <>", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseGreaterThan(String value) {
            addCriterion("license >", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseGreaterThanOrEqualTo(String value) {
            addCriterion("license >=", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseLessThan(String value) {
            addCriterion("license <", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseLessThanOrEqualTo(String value) {
            addCriterion("license <=", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseLike(String value) {
            addCriterion("license like", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseNotLike(String value) {
            addCriterion("license not like", value, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseIn(List<String> values) {
            addCriterion("license in", values, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseNotIn(List<String> values) {
            addCriterion("license not in", values, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseBetween(String value1, String value2) {
            addCriterion("license between", value1, value2, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseNotBetween(String value1, String value2) {
            addCriterion("license not between", value1, value2, "license");
            return (Criteria) this;
        }

        public Criteria andLicenseimgIsNull() {
            addCriterion("licenseimg is null");
            return (Criteria) this;
        }

        public Criteria andLicenseimgIsNotNull() {
            addCriterion("licenseimg is not null");
            return (Criteria) this;
        }

        public Criteria andLicenseimgEqualTo(String value) {
            addCriterion("licenseimg =", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgNotEqualTo(String value) {
            addCriterion("licenseimg <>", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgGreaterThan(String value) {
            addCriterion("licenseimg >", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgGreaterThanOrEqualTo(String value) {
            addCriterion("licenseimg >=", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgLessThan(String value) {
            addCriterion("licenseimg <", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgLessThanOrEqualTo(String value) {
            addCriterion("licenseimg <=", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgLike(String value) {
            addCriterion("licenseimg like", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgNotLike(String value) {
            addCriterion("licenseimg not like", value, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgIn(List<String> values) {
            addCriterion("licenseimg in", values, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgNotIn(List<String> values) {
            addCriterion("licenseimg not in", values, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgBetween(String value1, String value2) {
            addCriterion("licenseimg between", value1, value2, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andLicenseimgNotBetween(String value1, String value2) {
            addCriterion("licenseimg not between", value1, value2, "licenseimg");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNull() {
            addCriterion("website is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNotNull() {
            addCriterion("website is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteEqualTo(String value) {
            addCriterion("website =", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotEqualTo(String value) {
            addCriterion("website <>", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThan(String value) {
            addCriterion("website >", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThanOrEqualTo(String value) {
            addCriterion("website >=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThan(String value) {
            addCriterion("website <", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThanOrEqualTo(String value) {
            addCriterion("website <=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLike(String value) {
            addCriterion("website like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotLike(String value) {
            addCriterion("website not like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteIn(List<String> values) {
            addCriterion("website in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotIn(List<String> values) {
            addCriterion("website not in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteBetween(String value1, String value2) {
            addCriterion("website between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotBetween(String value1, String value2) {
            addCriterion("website not between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andLogoIsNull() {
            addCriterion("logo is null");
            return (Criteria) this;
        }

        public Criteria andLogoIsNotNull() {
            addCriterion("logo is not null");
            return (Criteria) this;
        }

        public Criteria andLogoEqualTo(String value) {
            addCriterion("logo =", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotEqualTo(String value) {
            addCriterion("logo <>", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoGreaterThan(String value) {
            addCriterion("logo >", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoGreaterThanOrEqualTo(String value) {
            addCriterion("logo >=", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLessThan(String value) {
            addCriterion("logo <", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLessThanOrEqualTo(String value) {
            addCriterion("logo <=", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLike(String value) {
            addCriterion("logo like", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotLike(String value) {
            addCriterion("logo not like", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoIn(List<String> values) {
            addCriterion("logo in", values, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotIn(List<String> values) {
            addCriterion("logo not in", values, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoBetween(String value1, String value2) {
            addCriterion("logo between", value1, value2, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotBetween(String value1, String value2) {
            addCriterion("logo not between", value1, value2, "logo");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andQrcodepathIsNull() {
            addCriterion("qrcodepath is null");
            return (Criteria) this;
        }

        public Criteria andQrcodepathIsNotNull() {
            addCriterion("qrcodepath is not null");
            return (Criteria) this;
        }

        public Criteria andQrcodepathEqualTo(String value) {
            addCriterion("qrcodepath =", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotEqualTo(String value) {
            addCriterion("qrcodepath <>", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathGreaterThan(String value) {
            addCriterion("qrcodepath >", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathGreaterThanOrEqualTo(String value) {
            addCriterion("qrcodepath >=", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathLessThan(String value) {
            addCriterion("qrcodepath <", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathLessThanOrEqualTo(String value) {
            addCriterion("qrcodepath <=", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathLike(String value) {
            addCriterion("qrcodepath like", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotLike(String value) {
            addCriterion("qrcodepath not like", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathIn(List<String> values) {
            addCriterion("qrcodepath in", values, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotIn(List<String> values) {
            addCriterion("qrcodepath not in", values, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathBetween(String value1, String value2) {
            addCriterion("qrcodepath between", value1, value2, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotBetween(String value1, String value2) {
            addCriterion("qrcodepath not between", value1, value2, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andCitypostageIsNull() {
            addCriterion("cityPostage is null");
            return (Criteria) this;
        }

        public Criteria andCitypostageIsNotNull() {
            addCriterion("cityPostage is not null");
            return (Criteria) this;
        }

        public Criteria andCitypostageEqualTo(Double value) {
            addCriterion("cityPostage =", value, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageNotEqualTo(Double value) {
            addCriterion("cityPostage <>", value, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageGreaterThan(Double value) {
            addCriterion("cityPostage >", value, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageGreaterThanOrEqualTo(Double value) {
            addCriterion("cityPostage >=", value, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageLessThan(Double value) {
            addCriterion("cityPostage <", value, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageLessThanOrEqualTo(Double value) {
            addCriterion("cityPostage <=", value, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageIn(List<Double> values) {
            addCriterion("cityPostage in", values, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageNotIn(List<Double> values) {
            addCriterion("cityPostage not in", values, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageBetween(Double value1, Double value2) {
            addCriterion("cityPostage between", value1, value2, "citypostage");
            return (Criteria) this;
        }

        public Criteria andCitypostageNotBetween(Double value1, Double value2) {
            addCriterion("cityPostage not between", value1, value2, "citypostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageIsNull() {
            addCriterion("provincePostage is null");
            return (Criteria) this;
        }

        public Criteria andProvincepostageIsNotNull() {
            addCriterion("provincePostage is not null");
            return (Criteria) this;
        }

        public Criteria andProvincepostageEqualTo(Double value) {
            addCriterion("provincePostage =", value, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageNotEqualTo(Double value) {
            addCriterion("provincePostage <>", value, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageGreaterThan(Double value) {
            addCriterion("provincePostage >", value, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageGreaterThanOrEqualTo(Double value) {
            addCriterion("provincePostage >=", value, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageLessThan(Double value) {
            addCriterion("provincePostage <", value, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageLessThanOrEqualTo(Double value) {
            addCriterion("provincePostage <=", value, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageIn(List<Double> values) {
            addCriterion("provincePostage in", values, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageNotIn(List<Double> values) {
            addCriterion("provincePostage not in", values, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageBetween(Double value1, Double value2) {
            addCriterion("provincePostage between", value1, value2, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andProvincepostageNotBetween(Double value1, Double value2) {
            addCriterion("provincePostage not between", value1, value2, "provincepostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageIsNull() {
            addCriterion("nationPostage is null");
            return (Criteria) this;
        }

        public Criteria andNationpostageIsNotNull() {
            addCriterion("nationPostage is not null");
            return (Criteria) this;
        }

        public Criteria andNationpostageEqualTo(Double value) {
            addCriterion("nationPostage =", value, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageNotEqualTo(Double value) {
            addCriterion("nationPostage <>", value, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageGreaterThan(Double value) {
            addCriterion("nationPostage >", value, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageGreaterThanOrEqualTo(Double value) {
            addCriterion("nationPostage >=", value, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageLessThan(Double value) {
            addCriterion("nationPostage <", value, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageLessThanOrEqualTo(Double value) {
            addCriterion("nationPostage <=", value, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageIn(List<Double> values) {
            addCriterion("nationPostage in", values, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageNotIn(List<Double> values) {
            addCriterion("nationPostage not in", values, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageBetween(Double value1, Double value2) {
            addCriterion("nationPostage between", value1, value2, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andNationpostageNotBetween(Double value1, Double value2) {
            addCriterion("nationPostage not between", value1, value2, "nationpostage");
            return (Criteria) this;
        }

        public Criteria andPreferentialIsNull() {
            addCriterion("preferential is null");
            return (Criteria) this;
        }

        public Criteria andPreferentialIsNotNull() {
            addCriterion("preferential is not null");
            return (Criteria) this;
        }

        public Criteria andPreferentialEqualTo(Double value) {
            addCriterion("preferential =", value, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialNotEqualTo(Double value) {
            addCriterion("preferential <>", value, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialGreaterThan(Double value) {
            addCriterion("preferential >", value, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialGreaterThanOrEqualTo(Double value) {
            addCriterion("preferential >=", value, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialLessThan(Double value) {
            addCriterion("preferential <", value, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialLessThanOrEqualTo(Double value) {
            addCriterion("preferential <=", value, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialIn(List<Double> values) {
            addCriterion("preferential in", values, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialNotIn(List<Double> values) {
            addCriterion("preferential not in", values, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialBetween(Double value1, Double value2) {
            addCriterion("preferential between", value1, value2, "preferential");
            return (Criteria) this;
        }

        public Criteria andPreferentialNotBetween(Double value1, Double value2) {
            addCriterion("preferential not between", value1, value2, "preferential");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(String value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(String value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(String value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(String value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(String value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(String value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLike(String value) {
            addCriterion("area like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotLike(String value) {
            addCriterion("area not like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<String> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<String> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(String value1, String value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(String value1, String value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andParentidIsNull() {
            addCriterion("parentid is null");
            return (Criteria) this;
        }

        public Criteria andParentidIsNotNull() {
            addCriterion("parentid is not null");
            return (Criteria) this;
        }

        public Criteria andParentidEqualTo(Integer value) {
            addCriterion("parentid =", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidNotEqualTo(Integer value) {
            addCriterion("parentid <>", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidGreaterThan(Integer value) {
            addCriterion("parentid >", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidGreaterThanOrEqualTo(Integer value) {
            addCriterion("parentid >=", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidLessThan(Integer value) {
            addCriterion("parentid <", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidLessThanOrEqualTo(Integer value) {
            addCriterion("parentid <=", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidIn(List<Integer> values) {
            addCriterion("parentid in", values, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidNotIn(List<Integer> values) {
            addCriterion("parentid not in", values, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidBetween(Integer value1, Integer value2) {
            addCriterion("parentid between", value1, value2, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidNotBetween(Integer value1, Integer value2) {
            addCriterion("parentid not between", value1, value2, "parentid");
            return (Criteria) this;
        }

        public Criteria andCompanytypeIsNull() {
            addCriterion("companytype is null");
            return (Criteria) this;
        }

        public Criteria andCompanytypeIsNotNull() {
            addCriterion("companytype is not null");
            return (Criteria) this;
        }

        public Criteria andCompanytypeEqualTo(String value) {
            addCriterion("companytype =", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeNotEqualTo(String value) {
            addCriterion("companytype <>", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeGreaterThan(String value) {
            addCriterion("companytype >", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeGreaterThanOrEqualTo(String value) {
            addCriterion("companytype >=", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeLessThan(String value) {
            addCriterion("companytype <", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeLessThanOrEqualTo(String value) {
            addCriterion("companytype <=", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeLike(String value) {
            addCriterion("companytype like", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeNotLike(String value) {
            addCriterion("companytype not like", value, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeIn(List<String> values) {
            addCriterion("companytype in", values, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeNotIn(List<String> values) {
            addCriterion("companytype not in", values, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeBetween(String value1, String value2) {
            addCriterion("companytype between", value1, value2, "companytype");
            return (Criteria) this;
        }

        public Criteria andCompanytypeNotBetween(String value1, String value2) {
            addCriterion("companytype not between", value1, value2, "companytype");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlIsNull() {
            addCriterion("tuiguangurl is null");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlIsNotNull() {
            addCriterion("tuiguangurl is not null");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlEqualTo(String value) {
            addCriterion("tuiguangurl =", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlNotEqualTo(String value) {
            addCriterion("tuiguangurl <>", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlGreaterThan(String value) {
            addCriterion("tuiguangurl >", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlGreaterThanOrEqualTo(String value) {
            addCriterion("tuiguangurl >=", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlLessThan(String value) {
            addCriterion("tuiguangurl <", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlLessThanOrEqualTo(String value) {
            addCriterion("tuiguangurl <=", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlLike(String value) {
            addCriterion("tuiguangurl like", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlNotLike(String value) {
            addCriterion("tuiguangurl not like", value, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlIn(List<String> values) {
            addCriterion("tuiguangurl in", values, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlNotIn(List<String> values) {
            addCriterion("tuiguangurl not in", values, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlBetween(String value1, String value2) {
            addCriterion("tuiguangurl between", value1, value2, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andTuiguangurlNotBetween(String value1, String value2) {
            addCriterion("tuiguangurl not between", value1, value2, "tuiguangurl");
            return (Criteria) this;
        }

        public Criteria andFrtypeIsNull() {
            addCriterion("frtype is null");
            return (Criteria) this;
        }

        public Criteria andFrtypeIsNotNull() {
            addCriterion("frtype is not null");
            return (Criteria) this;
        }

        public Criteria andFrtypeEqualTo(Integer value) {
            addCriterion("frtype =", value, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeNotEqualTo(Integer value) {
            addCriterion("frtype <>", value, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeGreaterThan(Integer value) {
            addCriterion("frtype >", value, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("frtype >=", value, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeLessThan(Integer value) {
            addCriterion("frtype <", value, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeLessThanOrEqualTo(Integer value) {
            addCriterion("frtype <=", value, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeIn(List<Integer> values) {
            addCriterion("frtype in", values, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeNotIn(List<Integer> values) {
            addCriterion("frtype not in", values, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeBetween(Integer value1, Integer value2) {
            addCriterion("frtype between", value1, value2, "frtype");
            return (Criteria) this;
        }

        public Criteria andFrtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("frtype not between", value1, value2, "frtype");
            return (Criteria) this;
        }

        public Criteria andRebateparentIsNull() {
            addCriterion("rebateparent is null");
            return (Criteria) this;
        }

        public Criteria andRebateparentIsNotNull() {
            addCriterion("rebateparent is not null");
            return (Criteria) this;
        }

        public Criteria andRebateparentEqualTo(Double value) {
            addCriterion("rebateparent =", value, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentNotEqualTo(Double value) {
            addCriterion("rebateparent <>", value, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentGreaterThan(Double value) {
            addCriterion("rebateparent >", value, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentGreaterThanOrEqualTo(Double value) {
            addCriterion("rebateparent >=", value, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentLessThan(Double value) {
            addCriterion("rebateparent <", value, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentLessThanOrEqualTo(Double value) {
            addCriterion("rebateparent <=", value, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentIn(List<Double> values) {
            addCriterion("rebateparent in", values, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentNotIn(List<Double> values) {
            addCriterion("rebateparent not in", values, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentBetween(Double value1, Double value2) {
            addCriterion("rebateparent between", value1, value2, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateparentNotBetween(Double value1, Double value2) {
            addCriterion("rebateparent not between", value1, value2, "rebateparent");
            return (Criteria) this;
        }

        public Criteria andRebateproportionIsNull() {
            addCriterion("rebateproportion is null");
            return (Criteria) this;
        }

        public Criteria andRebateproportionIsNotNull() {
            addCriterion("rebateproportion is not null");
            return (Criteria) this;
        }

        public Criteria andRebateproportionEqualTo(Double value) {
            addCriterion("rebateproportion =", value, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionNotEqualTo(Double value) {
            addCriterion("rebateproportion <>", value, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionGreaterThan(Double value) {
            addCriterion("rebateproportion >", value, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionGreaterThanOrEqualTo(Double value) {
            addCriterion("rebateproportion >=", value, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionLessThan(Double value) {
            addCriterion("rebateproportion <", value, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionLessThanOrEqualTo(Double value) {
            addCriterion("rebateproportion <=", value, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionIn(List<Double> values) {
            addCriterion("rebateproportion in", values, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionNotIn(List<Double> values) {
            addCriterion("rebateproportion not in", values, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionBetween(Double value1, Double value2) {
            addCriterion("rebateproportion between", value1, value2, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebateproportionNotBetween(Double value1, Double value2) {
            addCriterion("rebateproportion not between", value1, value2, "rebateproportion");
            return (Criteria) this;
        }

        public Criteria andRebatequotaIsNull() {
            addCriterion("rebatequota is null");
            return (Criteria) this;
        }

        public Criteria andRebatequotaIsNotNull() {
            addCriterion("rebatequota is not null");
            return (Criteria) this;
        }

        public Criteria andRebatequotaEqualTo(Double value) {
            addCriterion("rebatequota =", value, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaNotEqualTo(Double value) {
            addCriterion("rebatequota <>", value, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaGreaterThan(Double value) {
            addCriterion("rebatequota >", value, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaGreaterThanOrEqualTo(Double value) {
            addCriterion("rebatequota >=", value, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaLessThan(Double value) {
            addCriterion("rebatequota <", value, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaLessThanOrEqualTo(Double value) {
            addCriterion("rebatequota <=", value, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaIn(List<Double> values) {
            addCriterion("rebatequota in", values, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaNotIn(List<Double> values) {
            addCriterion("rebatequota not in", values, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaBetween(Double value1, Double value2) {
            addCriterion("rebatequota between", value1, value2, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatequotaNotBetween(Double value1, Double value2) {
            addCriterion("rebatequota not between", value1, value2, "rebatequota");
            return (Criteria) this;
        }

        public Criteria andRebatelineIsNull() {
            addCriterion("rebateline is null");
            return (Criteria) this;
        }

        public Criteria andRebatelineIsNotNull() {
            addCriterion("rebateline is not null");
            return (Criteria) this;
        }

        public Criteria andRebatelineEqualTo(String value) {
            addCriterion("rebateline =", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineNotEqualTo(String value) {
            addCriterion("rebateline <>", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineGreaterThan(String value) {
            addCriterion("rebateline >", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineGreaterThanOrEqualTo(String value) {
            addCriterion("rebateline >=", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineLessThan(String value) {
            addCriterion("rebateline <", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineLessThanOrEqualTo(String value) {
            addCriterion("rebateline <=", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineLike(String value) {
            addCriterion("rebateline like", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineNotLike(String value) {
            addCriterion("rebateline not like", value, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineIn(List<String> values) {
            addCriterion("rebateline in", values, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineNotIn(List<String> values) {
            addCriterion("rebateline not in", values, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineBetween(String value1, String value2) {
            addCriterion("rebateline between", value1, value2, "rebateline");
            return (Criteria) this;
        }

        public Criteria andRebatelineNotBetween(String value1, String value2) {
            addCriterion("rebateline not between", value1, value2, "rebateline");
            return (Criteria) this;
        }

        public Criteria andStoreIsNull() {
            addCriterion("store is null");
            return (Criteria) this;
        }

        public Criteria andStoreIsNotNull() {
            addCriterion("store is not null");
            return (Criteria) this;
        }

        public Criteria andStoreEqualTo(Integer value) {
            addCriterion("store =", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNotEqualTo(Integer value) {
            addCriterion("store <>", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreGreaterThan(Integer value) {
            addCriterion("store >", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("store >=", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreLessThan(Integer value) {
            addCriterion("store <", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreLessThanOrEqualTo(Integer value) {
            addCriterion("store <=", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreIn(List<Integer> values) {
            addCriterion("store in", values, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNotIn(List<Integer> values) {
            addCriterion("store not in", values, "store");
            return (Criteria) this;
        }

        public Criteria andStoreBetween(Integer value1, Integer value2) {
            addCriterion("store between", value1, value2, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNotBetween(Integer value1, Integer value2) {
            addCriterion("store not between", value1, value2, "store");
            return (Criteria) this;
        }

        public Criteria andStyleIsNull() {
            addCriterion("style is null");
            return (Criteria) this;
        }

        public Criteria andStyleIsNotNull() {
            addCriterion("style is not null");
            return (Criteria) this;
        }

        public Criteria andStyleEqualTo(Integer value) {
            addCriterion("style =", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleNotEqualTo(Integer value) {
            addCriterion("style <>", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleGreaterThan(Integer value) {
            addCriterion("style >", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleGreaterThanOrEqualTo(Integer value) {
            addCriterion("style >=", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleLessThan(Integer value) {
            addCriterion("style <", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleLessThanOrEqualTo(Integer value) {
            addCriterion("style <=", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleIn(List<Integer> values) {
            addCriterion("style in", values, "style");
            return (Criteria) this;
        }

        public Criteria andStyleNotIn(List<Integer> values) {
            addCriterion("style not in", values, "style");
            return (Criteria) this;
        }

        public Criteria andStyleBetween(Integer value1, Integer value2) {
            addCriterion("style between", value1, value2, "style");
            return (Criteria) this;
        }

        public Criteria andStyleNotBetween(Integer value1, Integer value2) {
            addCriterion("style not between", value1, value2, "style");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountIsNull() {
            addCriterion("appointmentcount is null");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountIsNotNull() {
            addCriterion("appointmentcount is not null");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountEqualTo(Integer value) {
            addCriterion("appointmentcount =", value, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountNotEqualTo(Integer value) {
            addCriterion("appointmentcount <>", value, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountGreaterThan(Integer value) {
            addCriterion("appointmentcount >", value, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("appointmentcount >=", value, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountLessThan(Integer value) {
            addCriterion("appointmentcount <", value, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountLessThanOrEqualTo(Integer value) {
            addCriterion("appointmentcount <=", value, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountIn(List<Integer> values) {
            addCriterion("appointmentcount in", values, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountNotIn(List<Integer> values) {
            addCriterion("appointmentcount not in", values, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountBetween(Integer value1, Integer value2) {
            addCriterion("appointmentcount between", value1, value2, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andAppointmentcountNotBetween(Integer value1, Integer value2) {
            addCriterion("appointmentcount not between", value1, value2, "appointmentcount");
            return (Criteria) this;
        }

        public Criteria andCompanyimgIsNull() {
            addCriterion("companyimg is null");
            return (Criteria) this;
        }

        public Criteria andCompanyimgIsNotNull() {
            addCriterion("companyimg is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyimgEqualTo(String value) {
            addCriterion("companyimg =", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgNotEqualTo(String value) {
            addCriterion("companyimg <>", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgGreaterThan(String value) {
            addCriterion("companyimg >", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgGreaterThanOrEqualTo(String value) {
            addCriterion("companyimg >=", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgLessThan(String value) {
            addCriterion("companyimg <", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgLessThanOrEqualTo(String value) {
            addCriterion("companyimg <=", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgLike(String value) {
            addCriterion("companyimg like", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgNotLike(String value) {
            addCriterion("companyimg not like", value, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgIn(List<String> values) {
            addCriterion("companyimg in", values, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgNotIn(List<String> values) {
            addCriterion("companyimg not in", values, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgBetween(String value1, String value2) {
            addCriterion("companyimg between", value1, value2, "companyimg");
            return (Criteria) this;
        }

        public Criteria andCompanyimgNotBetween(String value1, String value2) {
            addCriterion("companyimg not between", value1, value2, "companyimg");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersIsNull() {
            addCriterion("companyimgothers is null");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersIsNotNull() {
            addCriterion("companyimgothers is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersEqualTo(String value) {
            addCriterion("companyimgothers =", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersNotEqualTo(String value) {
            addCriterion("companyimgothers <>", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersGreaterThan(String value) {
            addCriterion("companyimgothers >", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersGreaterThanOrEqualTo(String value) {
            addCriterion("companyimgothers >=", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersLessThan(String value) {
            addCriterion("companyimgothers <", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersLessThanOrEqualTo(String value) {
            addCriterion("companyimgothers <=", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersLike(String value) {
            addCriterion("companyimgothers like", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersNotLike(String value) {
            addCriterion("companyimgothers not like", value, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersIn(List<String> values) {
            addCriterion("companyimgothers in", values, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersNotIn(List<String> values) {
            addCriterion("companyimgothers not in", values, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersBetween(String value1, String value2) {
            addCriterion("companyimgothers between", value1, value2, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andCompanyimgothersNotBetween(String value1, String value2) {
            addCriterion("companyimgothers not between", value1, value2, "companyimgothers");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andCuidIsNull() {
            addCriterion("cuid is null");
            return (Criteria) this;
        }

        public Criteria andCuidIsNotNull() {
            addCriterion("cuid is not null");
            return (Criteria) this;
        }

        public Criteria andCuidEqualTo(Integer value) {
            addCriterion("cuid =", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidNotEqualTo(Integer value) {
            addCriterion("cuid <>", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidGreaterThan(Integer value) {
            addCriterion("cuid >", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cuid >=", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidLessThan(Integer value) {
            addCriterion("cuid <", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidLessThanOrEqualTo(Integer value) {
            addCriterion("cuid <=", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidIn(List<Integer> values) {
            addCriterion("cuid in", values, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidNotIn(List<Integer> values) {
            addCriterion("cuid not in", values, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidBetween(Integer value1, Integer value2) {
            addCriterion("cuid between", value1, value2, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidNotBetween(Integer value1, Integer value2) {
            addCriterion("cuid not between", value1, value2, "cuid");
            return (Criteria) this;
        }

        public Criteria andTxtypeIsNull() {
            addCriterion("txtype is null");
            return (Criteria) this;
        }

        public Criteria andTxtypeIsNotNull() {
            addCriterion("txtype is not null");
            return (Criteria) this;
        }

        public Criteria andTxtypeEqualTo(String value) {
            addCriterion("txtype =", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeNotEqualTo(String value) {
            addCriterion("txtype <>", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeGreaterThan(String value) {
            addCriterion("txtype >", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeGreaterThanOrEqualTo(String value) {
            addCriterion("txtype >=", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeLessThan(String value) {
            addCriterion("txtype <", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeLessThanOrEqualTo(String value) {
            addCriterion("txtype <=", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeLike(String value) {
            addCriterion("txtype like", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeNotLike(String value) {
            addCriterion("txtype not like", value, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeIn(List<String> values) {
            addCriterion("txtype in", values, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeNotIn(List<String> values) {
            addCriterion("txtype not in", values, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeBetween(String value1, String value2) {
            addCriterion("txtype between", value1, value2, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxtypeNotBetween(String value1, String value2) {
            addCriterion("txtype not between", value1, value2, "txtype");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysIsNull() {
            addCriterion("txafterdays is null");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysIsNotNull() {
            addCriterion("txafterdays is not null");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysEqualTo(Integer value) {
            addCriterion("txafterdays =", value, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysNotEqualTo(Integer value) {
            addCriterion("txafterdays <>", value, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysGreaterThan(Integer value) {
            addCriterion("txafterdays >", value, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysGreaterThanOrEqualTo(Integer value) {
            addCriterion("txafterdays >=", value, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysLessThan(Integer value) {
            addCriterion("txafterdays <", value, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysLessThanOrEqualTo(Integer value) {
            addCriterion("txafterdays <=", value, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysIn(List<Integer> values) {
            addCriterion("txafterdays in", values, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysNotIn(List<Integer> values) {
            addCriterion("txafterdays not in", values, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysBetween(Integer value1, Integer value2) {
            addCriterion("txafterdays between", value1, value2, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andTxafterdaysNotBetween(Integer value1, Integer value2) {
            addCriterion("txafterdays not between", value1, value2, "txafterdays");
            return (Criteria) this;
        }

        public Criteria andChargingtimeIsNull() {
            addCriterion("chargingtime is null");
            return (Criteria) this;
        }

        public Criteria andChargingtimeIsNotNull() {
            addCriterion("chargingtime is not null");
            return (Criteria) this;
        }

        public Criteria andChargingtimeEqualTo(Integer value) {
            addCriterion("chargingtime =", value, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeNotEqualTo(Integer value) {
            addCriterion("chargingtime <>", value, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeGreaterThan(Integer value) {
            addCriterion("chargingtime >", value, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("chargingtime >=", value, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeLessThan(Integer value) {
            addCriterion("chargingtime <", value, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeLessThanOrEqualTo(Integer value) {
            addCriterion("chargingtime <=", value, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeIn(List<Integer> values) {
            addCriterion("chargingtime in", values, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeNotIn(List<Integer> values) {
            addCriterion("chargingtime not in", values, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeBetween(Integer value1, Integer value2) {
            addCriterion("chargingtime between", value1, value2, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andChargingtimeNotBetween(Integer value1, Integer value2) {
            addCriterion("chargingtime not between", value1, value2, "chargingtime");
            return (Criteria) this;
        }

        public Criteria andDevicenumberIsNull() {
            addCriterion("devicenumber is null");
            return (Criteria) this;
        }

        public Criteria andDevicenumberIsNotNull() {
            addCriterion("devicenumber is not null");
            return (Criteria) this;
        }

        public Criteria andDevicenumberEqualTo(Integer value) {
            addCriterion("devicenumber =", value, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberNotEqualTo(Integer value) {
            addCriterion("devicenumber <>", value, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberGreaterThan(Integer value) {
            addCriterion("devicenumber >", value, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("devicenumber >=", value, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberLessThan(Integer value) {
            addCriterion("devicenumber <", value, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberLessThanOrEqualTo(Integer value) {
            addCriterion("devicenumber <=", value, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberIn(List<Integer> values) {
            addCriterion("devicenumber in", values, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberNotIn(List<Integer> values) {
            addCriterion("devicenumber not in", values, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberBetween(Integer value1, Integer value2) {
            addCriterion("devicenumber between", value1, value2, "devicenumber");
            return (Criteria) this;
        }

        public Criteria andDevicenumberNotBetween(Integer value1, Integer value2) {
            addCriterion("devicenumber not between", value1, value2, "devicenumber");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}