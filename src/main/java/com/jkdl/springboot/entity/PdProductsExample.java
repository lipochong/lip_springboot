package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PdProductsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PdProductsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andLeveloneIsNull() {
            addCriterion("levelone is null");
            return (Criteria) this;
        }

        public Criteria andLeveloneIsNotNull() {
            addCriterion("levelone is not null");
            return (Criteria) this;
        }

        public Criteria andLeveloneEqualTo(Integer value) {
            addCriterion("levelone =", value, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneNotEqualTo(Integer value) {
            addCriterion("levelone <>", value, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneGreaterThan(Integer value) {
            addCriterion("levelone >", value, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("levelone >=", value, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneLessThan(Integer value) {
            addCriterion("levelone <", value, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneLessThanOrEqualTo(Integer value) {
            addCriterion("levelone <=", value, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneIn(List<Integer> values) {
            addCriterion("levelone in", values, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneNotIn(List<Integer> values) {
            addCriterion("levelone not in", values, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneBetween(Integer value1, Integer value2) {
            addCriterion("levelone between", value1, value2, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveloneNotBetween(Integer value1, Integer value2) {
            addCriterion("levelone not between", value1, value2, "levelone");
            return (Criteria) this;
        }

        public Criteria andLeveltwoIsNull() {
            addCriterion("leveltwo is null");
            return (Criteria) this;
        }

        public Criteria andLeveltwoIsNotNull() {
            addCriterion("leveltwo is not null");
            return (Criteria) this;
        }

        public Criteria andLeveltwoEqualTo(Integer value) {
            addCriterion("leveltwo =", value, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoNotEqualTo(Integer value) {
            addCriterion("leveltwo <>", value, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoGreaterThan(Integer value) {
            addCriterion("leveltwo >", value, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoGreaterThanOrEqualTo(Integer value) {
            addCriterion("leveltwo >=", value, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoLessThan(Integer value) {
            addCriterion("leveltwo <", value, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoLessThanOrEqualTo(Integer value) {
            addCriterion("leveltwo <=", value, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoIn(List<Integer> values) {
            addCriterion("leveltwo in", values, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoNotIn(List<Integer> values) {
            addCriterion("leveltwo not in", values, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoBetween(Integer value1, Integer value2) {
            addCriterion("leveltwo between", value1, value2, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andLeveltwoNotBetween(Integer value1, Integer value2) {
            addCriterion("leveltwo not between", value1, value2, "leveltwo");
            return (Criteria) this;
        }

        public Criteria andCompanyidIsNull() {
            addCriterion("companyid is null");
            return (Criteria) this;
        }

        public Criteria andCompanyidIsNotNull() {
            addCriterion("companyid is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyidEqualTo(Integer value) {
            addCriterion("companyid =", value, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidNotEqualTo(Integer value) {
            addCriterion("companyid <>", value, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidGreaterThan(Integer value) {
            addCriterion("companyid >", value, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidGreaterThanOrEqualTo(Integer value) {
            addCriterion("companyid >=", value, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidLessThan(Integer value) {
            addCriterion("companyid <", value, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidLessThanOrEqualTo(Integer value) {
            addCriterion("companyid <=", value, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidIn(List<Integer> values) {
            addCriterion("companyid in", values, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidNotIn(List<Integer> values) {
            addCriterion("companyid not in", values, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidBetween(Integer value1, Integer value2) {
            addCriterion("companyid between", value1, value2, "companyid");
            return (Criteria) this;
        }

        public Criteria andCompanyidNotBetween(Integer value1, Integer value2) {
            addCriterion("companyid not between", value1, value2, "companyid");
            return (Criteria) this;
        }

        public Criteria andProductnameIsNull() {
            addCriterion("productName is null");
            return (Criteria) this;
        }

        public Criteria andProductnameIsNotNull() {
            addCriterion("productName is not null");
            return (Criteria) this;
        }

        public Criteria andProductnameEqualTo(String value) {
            addCriterion("productName =", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotEqualTo(String value) {
            addCriterion("productName <>", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameGreaterThan(String value) {
            addCriterion("productName >", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameGreaterThanOrEqualTo(String value) {
            addCriterion("productName >=", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLessThan(String value) {
            addCriterion("productName <", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLessThanOrEqualTo(String value) {
            addCriterion("productName <=", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLike(String value) {
            addCriterion("productName like", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotLike(String value) {
            addCriterion("productName not like", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameIn(List<String> values) {
            addCriterion("productName in", values, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotIn(List<String> values) {
            addCriterion("productName not in", values, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameBetween(String value1, String value2) {
            addCriterion("productName between", value1, value2, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotBetween(String value1, String value2) {
            addCriterion("productName not between", value1, value2, "productname");
            return (Criteria) this;
        }

        public Criteria andStyleIsNull() {
            addCriterion("style is null");
            return (Criteria) this;
        }

        public Criteria andStyleIsNotNull() {
            addCriterion("style is not null");
            return (Criteria) this;
        }

        public Criteria andStyleEqualTo(String value) {
            addCriterion("style =", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleNotEqualTo(String value) {
            addCriterion("style <>", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleGreaterThan(String value) {
            addCriterion("style >", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleGreaterThanOrEqualTo(String value) {
            addCriterion("style >=", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleLessThan(String value) {
            addCriterion("style <", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleLessThanOrEqualTo(String value) {
            addCriterion("style <=", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleLike(String value) {
            addCriterion("style like", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleNotLike(String value) {
            addCriterion("style not like", value, "style");
            return (Criteria) this;
        }

        public Criteria andStyleIn(List<String> values) {
            addCriterion("style in", values, "style");
            return (Criteria) this;
        }

        public Criteria andStyleNotIn(List<String> values) {
            addCriterion("style not in", values, "style");
            return (Criteria) this;
        }

        public Criteria andStyleBetween(String value1, String value2) {
            addCriterion("style between", value1, value2, "style");
            return (Criteria) this;
        }

        public Criteria andStyleNotBetween(String value1, String value2) {
            addCriterion("style not between", value1, value2, "style");
            return (Criteria) this;
        }

        public Criteria andMeasurementIsNull() {
            addCriterion("measurement is null");
            return (Criteria) this;
        }

        public Criteria andMeasurementIsNotNull() {
            addCriterion("measurement is not null");
            return (Criteria) this;
        }

        public Criteria andMeasurementEqualTo(Integer value) {
            addCriterion("measurement =", value, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementNotEqualTo(Integer value) {
            addCriterion("measurement <>", value, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementGreaterThan(Integer value) {
            addCriterion("measurement >", value, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementGreaterThanOrEqualTo(Integer value) {
            addCriterion("measurement >=", value, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementLessThan(Integer value) {
            addCriterion("measurement <", value, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementLessThanOrEqualTo(Integer value) {
            addCriterion("measurement <=", value, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementIn(List<Integer> values) {
            addCriterion("measurement in", values, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementNotIn(List<Integer> values) {
            addCriterion("measurement not in", values, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementBetween(Integer value1, Integer value2) {
            addCriterion("measurement between", value1, value2, "measurement");
            return (Criteria) this;
        }

        public Criteria andMeasurementNotBetween(Integer value1, Integer value2) {
            addCriterion("measurement not between", value1, value2, "measurement");
            return (Criteria) this;
        }

        public Criteria andSpecificationsIsNull() {
            addCriterion("specifications is null");
            return (Criteria) this;
        }

        public Criteria andSpecificationsIsNotNull() {
            addCriterion("specifications is not null");
            return (Criteria) this;
        }

        public Criteria andSpecificationsEqualTo(String value) {
            addCriterion("specifications =", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsNotEqualTo(String value) {
            addCriterion("specifications <>", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsGreaterThan(String value) {
            addCriterion("specifications >", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsGreaterThanOrEqualTo(String value) {
            addCriterion("specifications >=", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsLessThan(String value) {
            addCriterion("specifications <", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsLessThanOrEqualTo(String value) {
            addCriterion("specifications <=", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsLike(String value) {
            addCriterion("specifications like", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsNotLike(String value) {
            addCriterion("specifications not like", value, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsIn(List<String> values) {
            addCriterion("specifications in", values, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsNotIn(List<String> values) {
            addCriterion("specifications not in", values, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsBetween(String value1, String value2) {
            addCriterion("specifications between", value1, value2, "specifications");
            return (Criteria) this;
        }

        public Criteria andSpecificationsNotBetween(String value1, String value2) {
            addCriterion("specifications not between", value1, value2, "specifications");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNull() {
            addCriterion("money is null");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNotNull() {
            addCriterion("money is not null");
            return (Criteria) this;
        }

        public Criteria andMoneyEqualTo(Double value) {
            addCriterion("money =", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotEqualTo(Double value) {
            addCriterion("money <>", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThan(Double value) {
            addCriterion("money >", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThanOrEqualTo(Double value) {
            addCriterion("money >=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThan(Double value) {
            addCriterion("money <", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThanOrEqualTo(Double value) {
            addCriterion("money <=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyIn(List<Double> values) {
            addCriterion("money in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotIn(List<Double> values) {
            addCriterion("money not in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyBetween(Double value1, Double value2) {
            addCriterion("money between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotBetween(Double value1, Double value2) {
            addCriterion("money not between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andUrlIsNull() {
            addCriterion("url is null");
            return (Criteria) this;
        }

        public Criteria andUrlIsNotNull() {
            addCriterion("url is not null");
            return (Criteria) this;
        }

        public Criteria andUrlEqualTo(String value) {
            addCriterion("url =", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotEqualTo(String value) {
            addCriterion("url <>", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThan(String value) {
            addCriterion("url >", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThanOrEqualTo(String value) {
            addCriterion("url >=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThan(String value) {
            addCriterion("url <", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThanOrEqualTo(String value) {
            addCriterion("url <=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLike(String value) {
            addCriterion("url like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotLike(String value) {
            addCriterion("url not like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlIn(List<String> values) {
            addCriterion("url in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotIn(List<String> values) {
            addCriterion("url not in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlBetween(String value1, String value2) {
            addCriterion("url between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotBetween(String value1, String value2) {
            addCriterion("url not between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andIconIsNull() {
            addCriterion("icon is null");
            return (Criteria) this;
        }

        public Criteria andIconIsNotNull() {
            addCriterion("icon is not null");
            return (Criteria) this;
        }

        public Criteria andIconEqualTo(String value) {
            addCriterion("icon =", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotEqualTo(String value) {
            addCriterion("icon <>", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconGreaterThan(String value) {
            addCriterion("icon >", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconGreaterThanOrEqualTo(String value) {
            addCriterion("icon >=", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLessThan(String value) {
            addCriterion("icon <", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLessThanOrEqualTo(String value) {
            addCriterion("icon <=", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLike(String value) {
            addCriterion("icon like", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotLike(String value) {
            addCriterion("icon not like", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconIn(List<String> values) {
            addCriterion("icon in", values, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotIn(List<String> values) {
            addCriterion("icon not in", values, "icon");
            return (Criteria) this;
        }

        public Criteria andIconBetween(String value1, String value2) {
            addCriterion("icon between", value1, value2, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotBetween(String value1, String value2) {
            addCriterion("icon not between", value1, value2, "icon");
            return (Criteria) this;
        }

        public Criteria andBannerIsNull() {
            addCriterion("banner is null");
            return (Criteria) this;
        }

        public Criteria andBannerIsNotNull() {
            addCriterion("banner is not null");
            return (Criteria) this;
        }

        public Criteria andBannerEqualTo(String value) {
            addCriterion("banner =", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerNotEqualTo(String value) {
            addCriterion("banner <>", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerGreaterThan(String value) {
            addCriterion("banner >", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerGreaterThanOrEqualTo(String value) {
            addCriterion("banner >=", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerLessThan(String value) {
            addCriterion("banner <", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerLessThanOrEqualTo(String value) {
            addCriterion("banner <=", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerLike(String value) {
            addCriterion("banner like", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerNotLike(String value) {
            addCriterion("banner not like", value, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerIn(List<String> values) {
            addCriterion("banner in", values, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerNotIn(List<String> values) {
            addCriterion("banner not in", values, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerBetween(String value1, String value2) {
            addCriterion("banner between", value1, value2, "banner");
            return (Criteria) this;
        }

        public Criteria andBannerNotBetween(String value1, String value2) {
            addCriterion("banner not between", value1, value2, "banner");
            return (Criteria) this;
        }

        public Criteria andProductactiveIsNull() {
            addCriterion("productactive is null");
            return (Criteria) this;
        }

        public Criteria andProductactiveIsNotNull() {
            addCriterion("productactive is not null");
            return (Criteria) this;
        }

        public Criteria andProductactiveEqualTo(Integer value) {
            addCriterion("productactive =", value, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveNotEqualTo(Integer value) {
            addCriterion("productactive <>", value, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveGreaterThan(Integer value) {
            addCriterion("productactive >", value, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveGreaterThanOrEqualTo(Integer value) {
            addCriterion("productactive >=", value, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveLessThan(Integer value) {
            addCriterion("productactive <", value, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveLessThanOrEqualTo(Integer value) {
            addCriterion("productactive <=", value, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveIn(List<Integer> values) {
            addCriterion("productactive in", values, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveNotIn(List<Integer> values) {
            addCriterion("productactive not in", values, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveBetween(Integer value1, Integer value2) {
            addCriterion("productactive between", value1, value2, "productactive");
            return (Criteria) this;
        }

        public Criteria andProductactiveNotBetween(Integer value1, Integer value2) {
            addCriterion("productactive not between", value1, value2, "productactive");
            return (Criteria) this;
        }

        public Criteria andVisitcountIsNull() {
            addCriterion("visitcount is null");
            return (Criteria) this;
        }

        public Criteria andVisitcountIsNotNull() {
            addCriterion("visitcount is not null");
            return (Criteria) this;
        }

        public Criteria andVisitcountEqualTo(Integer value) {
            addCriterion("visitcount =", value, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountNotEqualTo(Integer value) {
            addCriterion("visitcount <>", value, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountGreaterThan(Integer value) {
            addCriterion("visitcount >", value, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("visitcount >=", value, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountLessThan(Integer value) {
            addCriterion("visitcount <", value, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountLessThanOrEqualTo(Integer value) {
            addCriterion("visitcount <=", value, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountIn(List<Integer> values) {
            addCriterion("visitcount in", values, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountNotIn(List<Integer> values) {
            addCriterion("visitcount not in", values, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountBetween(Integer value1, Integer value2) {
            addCriterion("visitcount between", value1, value2, "visitcount");
            return (Criteria) this;
        }

        public Criteria andVisitcountNotBetween(Integer value1, Integer value2) {
            addCriterion("visitcount not between", value1, value2, "visitcount");
            return (Criteria) this;
        }

        public Criteria andDetailIsNull() {
            addCriterion("detail is null");
            return (Criteria) this;
        }

        public Criteria andDetailIsNotNull() {
            addCriterion("detail is not null");
            return (Criteria) this;
        }

        public Criteria andDetailEqualTo(String value) {
            addCriterion("detail =", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailNotEqualTo(String value) {
            addCriterion("detail <>", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailGreaterThan(String value) {
            addCriterion("detail >", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailGreaterThanOrEqualTo(String value) {
            addCriterion("detail >=", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailLessThan(String value) {
            addCriterion("detail <", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailLessThanOrEqualTo(String value) {
            addCriterion("detail <=", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailLike(String value) {
            addCriterion("detail like", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailNotLike(String value) {
            addCriterion("detail not like", value, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailIn(List<String> values) {
            addCriterion("detail in", values, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailNotIn(List<String> values) {
            addCriterion("detail not in", values, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailBetween(String value1, String value2) {
            addCriterion("detail between", value1, value2, "detail");
            return (Criteria) this;
        }

        public Criteria andDetailNotBetween(String value1, String value2) {
            addCriterion("detail not between", value1, value2, "detail");
            return (Criteria) this;
        }

        public Criteria andIntruductionIsNull() {
            addCriterion("intruduction is null");
            return (Criteria) this;
        }

        public Criteria andIntruductionIsNotNull() {
            addCriterion("intruduction is not null");
            return (Criteria) this;
        }

        public Criteria andIntruductionEqualTo(String value) {
            addCriterion("intruduction =", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionNotEqualTo(String value) {
            addCriterion("intruduction <>", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionGreaterThan(String value) {
            addCriterion("intruduction >", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionGreaterThanOrEqualTo(String value) {
            addCriterion("intruduction >=", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionLessThan(String value) {
            addCriterion("intruduction <", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionLessThanOrEqualTo(String value) {
            addCriterion("intruduction <=", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionLike(String value) {
            addCriterion("intruduction like", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionNotLike(String value) {
            addCriterion("intruduction not like", value, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionIn(List<String> values) {
            addCriterion("intruduction in", values, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionNotIn(List<String> values) {
            addCriterion("intruduction not in", values, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionBetween(String value1, String value2) {
            addCriterion("intruduction between", value1, value2, "intruduction");
            return (Criteria) this;
        }

        public Criteria andIntruductionNotBetween(String value1, String value2) {
            addCriterion("intruduction not between", value1, value2, "intruduction");
            return (Criteria) this;
        }

        public Criteria andAttachmentIsNull() {
            addCriterion("attachment is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentIsNotNull() {
            addCriterion("attachment is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentEqualTo(String value) {
            addCriterion("attachment =", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentNotEqualTo(String value) {
            addCriterion("attachment <>", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentGreaterThan(String value) {
            addCriterion("attachment >", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentGreaterThanOrEqualTo(String value) {
            addCriterion("attachment >=", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentLessThan(String value) {
            addCriterion("attachment <", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentLessThanOrEqualTo(String value) {
            addCriterion("attachment <=", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentLike(String value) {
            addCriterion("attachment like", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentNotLike(String value) {
            addCriterion("attachment not like", value, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentIn(List<String> values) {
            addCriterion("attachment in", values, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentNotIn(List<String> values) {
            addCriterion("attachment not in", values, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentBetween(String value1, String value2) {
            addCriterion("attachment between", value1, value2, "attachment");
            return (Criteria) this;
        }

        public Criteria andAttachmentNotBetween(String value1, String value2) {
            addCriterion("attachment not between", value1, value2, "attachment");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeIsNull() {
            addCriterion("isshowinhome is null");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeIsNotNull() {
            addCriterion("isshowinhome is not null");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeEqualTo(Integer value) {
            addCriterion("isshowinhome =", value, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeNotEqualTo(Integer value) {
            addCriterion("isshowinhome <>", value, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeGreaterThan(Integer value) {
            addCriterion("isshowinhome >", value, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeGreaterThanOrEqualTo(Integer value) {
            addCriterion("isshowinhome >=", value, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeLessThan(Integer value) {
            addCriterion("isshowinhome <", value, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeLessThanOrEqualTo(Integer value) {
            addCriterion("isshowinhome <=", value, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeIn(List<Integer> values) {
            addCriterion("isshowinhome in", values, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeNotIn(List<Integer> values) {
            addCriterion("isshowinhome not in", values, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeBetween(Integer value1, Integer value2) {
            addCriterion("isshowinhome between", value1, value2, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andIsshowinhomeNotBetween(Integer value1, Integer value2) {
            addCriterion("isshowinhome not between", value1, value2, "isshowinhome");
            return (Criteria) this;
        }

        public Criteria andAuditstatusIsNull() {
            addCriterion("auditstatus is null");
            return (Criteria) this;
        }

        public Criteria andAuditstatusIsNotNull() {
            addCriterion("auditstatus is not null");
            return (Criteria) this;
        }

        public Criteria andAuditstatusEqualTo(Integer value) {
            addCriterion("auditstatus =", value, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusNotEqualTo(Integer value) {
            addCriterion("auditstatus <>", value, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusGreaterThan(Integer value) {
            addCriterion("auditstatus >", value, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("auditstatus >=", value, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusLessThan(Integer value) {
            addCriterion("auditstatus <", value, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("auditstatus <=", value, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusIn(List<Integer> values) {
            addCriterion("auditstatus in", values, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusNotIn(List<Integer> values) {
            addCriterion("auditstatus not in", values, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusBetween(Integer value1, Integer value2) {
            addCriterion("auditstatus between", value1, value2, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andAuditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("auditstatus not between", value1, value2, "auditstatus");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andCreateuserIsNull() {
            addCriterion("createuser is null");
            return (Criteria) this;
        }

        public Criteria andCreateuserIsNotNull() {
            addCriterion("createuser is not null");
            return (Criteria) this;
        }

        public Criteria andCreateuserEqualTo(Integer value) {
            addCriterion("createuser =", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotEqualTo(Integer value) {
            addCriterion("createuser <>", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserGreaterThan(Integer value) {
            addCriterion("createuser >", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserGreaterThanOrEqualTo(Integer value) {
            addCriterion("createuser >=", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserLessThan(Integer value) {
            addCriterion("createuser <", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserLessThanOrEqualTo(Integer value) {
            addCriterion("createuser <=", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserIn(List<Integer> values) {
            addCriterion("createuser in", values, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotIn(List<Integer> values) {
            addCriterion("createuser not in", values, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserBetween(Integer value1, Integer value2) {
            addCriterion("createuser between", value1, value2, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotBetween(Integer value1, Integer value2) {
            addCriterion("createuser not between", value1, value2, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andLasttimeIsNull() {
            addCriterion("lasttime is null");
            return (Criteria) this;
        }

        public Criteria andLasttimeIsNotNull() {
            addCriterion("lasttime is not null");
            return (Criteria) this;
        }

        public Criteria andLasttimeEqualTo(Date value) {
            addCriterion("lasttime =", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeNotEqualTo(Date value) {
            addCriterion("lasttime <>", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeGreaterThan(Date value) {
            addCriterion("lasttime >", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("lasttime >=", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeLessThan(Date value) {
            addCriterion("lasttime <", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeLessThanOrEqualTo(Date value) {
            addCriterion("lasttime <=", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeIn(List<Date> values) {
            addCriterion("lasttime in", values, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeNotIn(List<Date> values) {
            addCriterion("lasttime not in", values, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeBetween(Date value1, Date value2) {
            addCriterion("lasttime between", value1, value2, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeNotBetween(Date value1, Date value2) {
            addCriterion("lasttime not between", value1, value2, "lasttime");
            return (Criteria) this;
        }

        public Criteria andJpgIsNull() {
            addCriterion("jpg is null");
            return (Criteria) this;
        }

        public Criteria andJpgIsNotNull() {
            addCriterion("jpg is not null");
            return (Criteria) this;
        }

        public Criteria andJpgEqualTo(String value) {
            addCriterion("jpg =", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgNotEqualTo(String value) {
            addCriterion("jpg <>", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgGreaterThan(String value) {
            addCriterion("jpg >", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgGreaterThanOrEqualTo(String value) {
            addCriterion("jpg >=", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgLessThan(String value) {
            addCriterion("jpg <", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgLessThanOrEqualTo(String value) {
            addCriterion("jpg <=", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgLike(String value) {
            addCriterion("jpg like", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgNotLike(String value) {
            addCriterion("jpg not like", value, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgIn(List<String> values) {
            addCriterion("jpg in", values, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgNotIn(List<String> values) {
            addCriterion("jpg not in", values, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgBetween(String value1, String value2) {
            addCriterion("jpg between", value1, value2, "jpg");
            return (Criteria) this;
        }

        public Criteria andJpgNotBetween(String value1, String value2) {
            addCriterion("jpg not between", value1, value2, "jpg");
            return (Criteria) this;
        }

        public Criteria andLongitudeIsNull() {
            addCriterion("longitude is null");
            return (Criteria) this;
        }

        public Criteria andLongitudeIsNotNull() {
            addCriterion("longitude is not null");
            return (Criteria) this;
        }

        public Criteria andLongitudeEqualTo(Double value) {
            addCriterion("longitude =", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotEqualTo(Double value) {
            addCriterion("longitude <>", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeGreaterThan(Double value) {
            addCriterion("longitude >", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeGreaterThanOrEqualTo(Double value) {
            addCriterion("longitude >=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLessThan(Double value) {
            addCriterion("longitude <", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLessThanOrEqualTo(Double value) {
            addCriterion("longitude <=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeIn(List<Double> values) {
            addCriterion("longitude in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotIn(List<Double> values) {
            addCriterion("longitude not in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeBetween(Double value1, Double value2) {
            addCriterion("longitude between", value1, value2, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotBetween(Double value1, Double value2) {
            addCriterion("longitude not between", value1, value2, "longitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeIsNull() {
            addCriterion("latitude is null");
            return (Criteria) this;
        }

        public Criteria andLatitudeIsNotNull() {
            addCriterion("latitude is not null");
            return (Criteria) this;
        }

        public Criteria andLatitudeEqualTo(Double value) {
            addCriterion("latitude =", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotEqualTo(Double value) {
            addCriterion("latitude <>", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThan(Double value) {
            addCriterion("latitude >", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThanOrEqualTo(Double value) {
            addCriterion("latitude >=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThan(Double value) {
            addCriterion("latitude <", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThanOrEqualTo(Double value) {
            addCriterion("latitude <=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeIn(List<Double> values) {
            addCriterion("latitude in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotIn(List<Double> values) {
            addCriterion("latitude not in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeBetween(Double value1, Double value2) {
            addCriterion("latitude between", value1, value2, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotBetween(Double value1, Double value2) {
            addCriterion("latitude not between", value1, value2, "latitude");
            return (Criteria) this;
        }

        public Criteria andQrcodepathIsNull() {
            addCriterion("qrcodepath is null");
            return (Criteria) this;
        }

        public Criteria andQrcodepathIsNotNull() {
            addCriterion("qrcodepath is not null");
            return (Criteria) this;
        }

        public Criteria andQrcodepathEqualTo(String value) {
            addCriterion("qrcodepath =", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotEqualTo(String value) {
            addCriterion("qrcodepath <>", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathGreaterThan(String value) {
            addCriterion("qrcodepath >", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathGreaterThanOrEqualTo(String value) {
            addCriterion("qrcodepath >=", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathLessThan(String value) {
            addCriterion("qrcodepath <", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathLessThanOrEqualTo(String value) {
            addCriterion("qrcodepath <=", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathLike(String value) {
            addCriterion("qrcodepath like", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotLike(String value) {
            addCriterion("qrcodepath not like", value, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathIn(List<String> values) {
            addCriterion("qrcodepath in", values, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotIn(List<String> values) {
            addCriterion("qrcodepath not in", values, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathBetween(String value1, String value2) {
            addCriterion("qrcodepath between", value1, value2, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andQrcodepathNotBetween(String value1, String value2) {
            addCriterion("qrcodepath not between", value1, value2, "qrcodepath");
            return (Criteria) this;
        }

        public Criteria andMd5keyIsNull() {
            addCriterion("md5key is null");
            return (Criteria) this;
        }

        public Criteria andMd5keyIsNotNull() {
            addCriterion("md5key is not null");
            return (Criteria) this;
        }

        public Criteria andMd5keyEqualTo(String value) {
            addCriterion("md5key =", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyNotEqualTo(String value) {
            addCriterion("md5key <>", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyGreaterThan(String value) {
            addCriterion("md5key >", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyGreaterThanOrEqualTo(String value) {
            addCriterion("md5key >=", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyLessThan(String value) {
            addCriterion("md5key <", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyLessThanOrEqualTo(String value) {
            addCriterion("md5key <=", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyLike(String value) {
            addCriterion("md5key like", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyNotLike(String value) {
            addCriterion("md5key not like", value, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyIn(List<String> values) {
            addCriterion("md5key in", values, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyNotIn(List<String> values) {
            addCriterion("md5key not in", values, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyBetween(String value1, String value2) {
            addCriterion("md5key between", value1, value2, "md5key");
            return (Criteria) this;
        }

        public Criteria andMd5keyNotBetween(String value1, String value2) {
            addCriterion("md5key not between", value1, value2, "md5key");
            return (Criteria) this;
        }

        public Criteria andDevtypeIsNull() {
            addCriterion("devtype is null");
            return (Criteria) this;
        }

        public Criteria andDevtypeIsNotNull() {
            addCriterion("devtype is not null");
            return (Criteria) this;
        }

        public Criteria andDevtypeEqualTo(String value) {
            addCriterion("devtype =", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotEqualTo(String value) {
            addCriterion("devtype <>", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeGreaterThan(String value) {
            addCriterion("devtype >", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeGreaterThanOrEqualTo(String value) {
            addCriterion("devtype >=", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLessThan(String value) {
            addCriterion("devtype <", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLessThanOrEqualTo(String value) {
            addCriterion("devtype <=", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLike(String value) {
            addCriterion("devtype like", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotLike(String value) {
            addCriterion("devtype not like", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeIn(List<String> values) {
            addCriterion("devtype in", values, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotIn(List<String> values) {
            addCriterion("devtype not in", values, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeBetween(String value1, String value2) {
            addCriterion("devtype between", value1, value2, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotBetween(String value1, String value2) {
            addCriterion("devtype not between", value1, value2, "devtype");
            return (Criteria) this;
        }

        public Criteria andBatchnumberIsNull() {
            addCriterion("batchnumber is null");
            return (Criteria) this;
        }

        public Criteria andBatchnumberIsNotNull() {
            addCriterion("batchnumber is not null");
            return (Criteria) this;
        }

        public Criteria andBatchnumberEqualTo(Integer value) {
            addCriterion("batchnumber =", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberNotEqualTo(Integer value) {
            addCriterion("batchnumber <>", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberGreaterThan(Integer value) {
            addCriterion("batchnumber >", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("batchnumber >=", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberLessThan(Integer value) {
            addCriterion("batchnumber <", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberLessThanOrEqualTo(Integer value) {
            addCriterion("batchnumber <=", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberIn(List<Integer> values) {
            addCriterion("batchnumber in", values, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberNotIn(List<Integer> values) {
            addCriterion("batchnumber not in", values, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberBetween(Integer value1, Integer value2) {
            addCriterion("batchnumber between", value1, value2, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberNotBetween(Integer value1, Integer value2) {
            addCriterion("batchnumber not between", value1, value2, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andMe5keyIsNull() {
            addCriterion("me5key is null");
            return (Criteria) this;
        }

        public Criteria andMe5keyIsNotNull() {
            addCriterion("me5key is not null");
            return (Criteria) this;
        }

        public Criteria andMe5keyEqualTo(String value) {
            addCriterion("me5key =", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyNotEqualTo(String value) {
            addCriterion("me5key <>", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyGreaterThan(String value) {
            addCriterion("me5key >", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyGreaterThanOrEqualTo(String value) {
            addCriterion("me5key >=", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyLessThan(String value) {
            addCriterion("me5key <", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyLessThanOrEqualTo(String value) {
            addCriterion("me5key <=", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyLike(String value) {
            addCriterion("me5key like", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyNotLike(String value) {
            addCriterion("me5key not like", value, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyIn(List<String> values) {
            addCriterion("me5key in", values, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyNotIn(List<String> values) {
            addCriterion("me5key not in", values, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyBetween(String value1, String value2) {
            addCriterion("me5key between", value1, value2, "me5key");
            return (Criteria) this;
        }

        public Criteria andMe5keyNotBetween(String value1, String value2) {
            addCriterion("me5key not between", value1, value2, "me5key");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidIsNull() {
            addCriterion("currentvoiceid is null");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidIsNotNull() {
            addCriterion("currentvoiceid is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidEqualTo(Integer value) {
            addCriterion("currentvoiceid =", value, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidNotEqualTo(Integer value) {
            addCriterion("currentvoiceid <>", value, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidGreaterThan(Integer value) {
            addCriterion("currentvoiceid >", value, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidGreaterThanOrEqualTo(Integer value) {
            addCriterion("currentvoiceid >=", value, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidLessThan(Integer value) {
            addCriterion("currentvoiceid <", value, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidLessThanOrEqualTo(Integer value) {
            addCriterion("currentvoiceid <=", value, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidIn(List<Integer> values) {
            addCriterion("currentvoiceid in", values, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidNotIn(List<Integer> values) {
            addCriterion("currentvoiceid not in", values, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidBetween(Integer value1, Integer value2) {
            addCriterion("currentvoiceid between", value1, value2, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andCurrentvoiceidNotBetween(Integer value1, Integer value2) {
            addCriterion("currentvoiceid not between", value1, value2, "currentvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidIsNull() {
            addCriterion("resetvoiceid is null");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidIsNotNull() {
            addCriterion("resetvoiceid is not null");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidEqualTo(Integer value) {
            addCriterion("resetvoiceid =", value, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidNotEqualTo(Integer value) {
            addCriterion("resetvoiceid <>", value, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidGreaterThan(Integer value) {
            addCriterion("resetvoiceid >", value, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidGreaterThanOrEqualTo(Integer value) {
            addCriterion("resetvoiceid >=", value, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidLessThan(Integer value) {
            addCriterion("resetvoiceid <", value, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidLessThanOrEqualTo(Integer value) {
            addCriterion("resetvoiceid <=", value, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidIn(List<Integer> values) {
            addCriterion("resetvoiceid in", values, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidNotIn(List<Integer> values) {
            addCriterion("resetvoiceid not in", values, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidBetween(Integer value1, Integer value2) {
            addCriterion("resetvoiceid between", value1, value2, "resetvoiceid");
            return (Criteria) this;
        }

        public Criteria andResetvoiceidNotBetween(Integer value1, Integer value2) {
            addCriterion("resetvoiceid not between", value1, value2, "resetvoiceid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}