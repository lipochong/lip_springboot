package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SysMessageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SysMessageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSystitleIsNull() {
            addCriterion("systitle is null");
            return (Criteria) this;
        }

        public Criteria andSystitleIsNotNull() {
            addCriterion("systitle is not null");
            return (Criteria) this;
        }

        public Criteria andSystitleEqualTo(String value) {
            addCriterion("systitle =", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleNotEqualTo(String value) {
            addCriterion("systitle <>", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleGreaterThan(String value) {
            addCriterion("systitle >", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleGreaterThanOrEqualTo(String value) {
            addCriterion("systitle >=", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleLessThan(String value) {
            addCriterion("systitle <", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleLessThanOrEqualTo(String value) {
            addCriterion("systitle <=", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleLike(String value) {
            addCriterion("systitle like", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleNotLike(String value) {
            addCriterion("systitle not like", value, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleIn(List<String> values) {
            addCriterion("systitle in", values, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleNotIn(List<String> values) {
            addCriterion("systitle not in", values, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleBetween(String value1, String value2) {
            addCriterion("systitle between", value1, value2, "systitle");
            return (Criteria) this;
        }

        public Criteria andSystitleNotBetween(String value1, String value2) {
            addCriterion("systitle not between", value1, value2, "systitle");
            return (Criteria) this;
        }

        public Criteria andSyscontentIsNull() {
            addCriterion("syscontent is null");
            return (Criteria) this;
        }

        public Criteria andSyscontentIsNotNull() {
            addCriterion("syscontent is not null");
            return (Criteria) this;
        }

        public Criteria andSyscontentEqualTo(String value) {
            addCriterion("syscontent =", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentNotEqualTo(String value) {
            addCriterion("syscontent <>", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentGreaterThan(String value) {
            addCriterion("syscontent >", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentGreaterThanOrEqualTo(String value) {
            addCriterion("syscontent >=", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentLessThan(String value) {
            addCriterion("syscontent <", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentLessThanOrEqualTo(String value) {
            addCriterion("syscontent <=", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentLike(String value) {
            addCriterion("syscontent like", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentNotLike(String value) {
            addCriterion("syscontent not like", value, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentIn(List<String> values) {
            addCriterion("syscontent in", values, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentNotIn(List<String> values) {
            addCriterion("syscontent not in", values, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentBetween(String value1, String value2) {
            addCriterion("syscontent between", value1, value2, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSyscontentNotBetween(String value1, String value2) {
            addCriterion("syscontent not between", value1, value2, "syscontent");
            return (Criteria) this;
        }

        public Criteria andSysauthorIsNull() {
            addCriterion("sysauthor is null");
            return (Criteria) this;
        }

        public Criteria andSysauthorIsNotNull() {
            addCriterion("sysauthor is not null");
            return (Criteria) this;
        }

        public Criteria andSysauthorEqualTo(String value) {
            addCriterion("sysauthor =", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorNotEqualTo(String value) {
            addCriterion("sysauthor <>", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorGreaterThan(String value) {
            addCriterion("sysauthor >", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorGreaterThanOrEqualTo(String value) {
            addCriterion("sysauthor >=", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorLessThan(String value) {
            addCriterion("sysauthor <", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorLessThanOrEqualTo(String value) {
            addCriterion("sysauthor <=", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorLike(String value) {
            addCriterion("sysauthor like", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorNotLike(String value) {
            addCriterion("sysauthor not like", value, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorIn(List<String> values) {
            addCriterion("sysauthor in", values, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorNotIn(List<String> values) {
            addCriterion("sysauthor not in", values, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorBetween(String value1, String value2) {
            addCriterion("sysauthor between", value1, value2, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysauthorNotBetween(String value1, String value2) {
            addCriterion("sysauthor not between", value1, value2, "sysauthor");
            return (Criteria) this;
        }

        public Criteria andSysurlIsNull() {
            addCriterion("sysurl is null");
            return (Criteria) this;
        }

        public Criteria andSysurlIsNotNull() {
            addCriterion("sysurl is not null");
            return (Criteria) this;
        }

        public Criteria andSysurlEqualTo(String value) {
            addCriterion("sysurl =", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlNotEqualTo(String value) {
            addCriterion("sysurl <>", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlGreaterThan(String value) {
            addCriterion("sysurl >", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlGreaterThanOrEqualTo(String value) {
            addCriterion("sysurl >=", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlLessThan(String value) {
            addCriterion("sysurl <", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlLessThanOrEqualTo(String value) {
            addCriterion("sysurl <=", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlLike(String value) {
            addCriterion("sysurl like", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlNotLike(String value) {
            addCriterion("sysurl not like", value, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlIn(List<String> values) {
            addCriterion("sysurl in", values, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlNotIn(List<String> values) {
            addCriterion("sysurl not in", values, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlBetween(String value1, String value2) {
            addCriterion("sysurl between", value1, value2, "sysurl");
            return (Criteria) this;
        }

        public Criteria andSysurlNotBetween(String value1, String value2) {
            addCriterion("sysurl not between", value1, value2, "sysurl");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andSysnoteIsNull() {
            addCriterion("sysnote is null");
            return (Criteria) this;
        }

        public Criteria andSysnoteIsNotNull() {
            addCriterion("sysnote is not null");
            return (Criteria) this;
        }

        public Criteria andSysnoteEqualTo(String value) {
            addCriterion("sysnote =", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteNotEqualTo(String value) {
            addCriterion("sysnote <>", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteGreaterThan(String value) {
            addCriterion("sysnote >", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteGreaterThanOrEqualTo(String value) {
            addCriterion("sysnote >=", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteLessThan(String value) {
            addCriterion("sysnote <", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteLessThanOrEqualTo(String value) {
            addCriterion("sysnote <=", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteLike(String value) {
            addCriterion("sysnote like", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteNotLike(String value) {
            addCriterion("sysnote not like", value, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteIn(List<String> values) {
            addCriterion("sysnote in", values, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteNotIn(List<String> values) {
            addCriterion("sysnote not in", values, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteBetween(String value1, String value2) {
            addCriterion("sysnote between", value1, value2, "sysnote");
            return (Criteria) this;
        }

        public Criteria andSysnoteNotBetween(String value1, String value2) {
            addCriterion("sysnote not between", value1, value2, "sysnote");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNull() {
            addCriterion("endtime is null");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNotNull() {
            addCriterion("endtime is not null");
            return (Criteria) this;
        }

        public Criteria andEndtimeEqualTo(Date value) {
            addCriterion("endtime =", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotEqualTo(Date value) {
            addCriterion("endtime <>", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThan(Date value) {
            addCriterion("endtime >", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("endtime >=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThan(Date value) {
            addCriterion("endtime <", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThanOrEqualTo(Date value) {
            addCriterion("endtime <=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIn(List<Date> values) {
            addCriterion("endtime in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotIn(List<Date> values) {
            addCriterion("endtime not in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeBetween(Date value1, Date value2) {
            addCriterion("endtime between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotBetween(Date value1, Date value2) {
            addCriterion("endtime not between", value1, value2, "endtime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}