package com.jkdl.springboot.entity;

import java.util.Date;

public class UCompany {
    private Integer id;

    private String companyname;

    private String code;

    private String license;

    private String licenseimg;

    private String email;

    private String website;

    private String mobile;

    private String phone;

    private String logo;

    private Date createtime;

    private String qrcodepath;

    private Double citypostage;

    private Double provincepostage;

    private Double nationpostage;

    private Double preferential;

    private String province;

    private String city;

    private String area;

    private Integer parentid;

    private Integer grandParentId;

    private String companytype;

    private String tuiguangurl;

    private Integer frtype;

    private Double rebateparent;

    private Double rebateproportion;

    private Double rebatequota;

    private String rebateline;

    private Integer store;

    private Integer style;

    private Integer appointmentcount;

    private String companyimg;

    private Integer bdelete;

    private String companyimgothers;

    private String name;

    private Integer cuid;

    private String txtype;

    private Integer txafterdays;

    private Integer chargingtime;

    private Integer devicenumber;


    private String provinceName;

    private String cityName;

    private String districtName;

    private String openid;

    private String unionid;

    private String nickname;

    private String avatarUrl;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }


    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getLicenseimg() {
        return licenseimg;
    }

    public void setLicenseimg(String licenseimg) {
        this.licenseimg = licenseimg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getQrcodepath() {
        return qrcodepath;
    }

    public void setQrcodepath(String qrcodepath) {
        this.qrcodepath = qrcodepath;
    }

    public Double getCitypostage() {
        return citypostage;
    }

    public void setCitypostage(Double citypostage) {
        this.citypostage = citypostage;
    }

    public Double getProvincepostage() {
        return provincepostage;
    }

    public void setProvincepostage(Double provincepostage) {
        this.provincepostage = provincepostage;
    }

    public Double getNationpostage() {
        return nationpostage;
    }

    public void setNationpostage(Double nationpostage) {
        this.nationpostage = nationpostage;
    }

    public Double getPreferential() {
        return preferential;
    }

    public void setPreferential(Double preferential) {
        this.preferential = preferential;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public Integer getGrandParentId() {
        return grandParentId;
    }

    public void setGrandParentId(Integer grandParentId) {
        this.grandParentId = grandParentId;
    }

    public String getCompanytype() {
        return companytype;
    }

    public void setCompanytype(String companytype) {
        this.companytype = companytype;
    }

    public String getTuiguangurl() {
        return tuiguangurl;
    }

    public void setTuiguangurl(String tuiguangurl) {
        this.tuiguangurl = tuiguangurl;
    }

    public Integer getFrtype() {
        return frtype;
    }

    public void setFrtype(Integer frtype) {
        this.frtype = frtype;
    }

    public Double getRebateparent() {
        return rebateparent;
    }

    public void setRebateparent(Double rebateparent) {
        this.rebateparent = rebateparent;
    }

    public Double getRebateproportion() {
        return rebateproportion;
    }

    public void setRebateproportion(Double rebateproportion) {
        this.rebateproportion = rebateproportion;
    }

    public Double getRebatequota() {
        return rebatequota;
    }

    public void setRebatequota(Double rebatequota) {
        this.rebatequota = rebatequota;
    }

    public String getRebateline() {
        return rebateline;
    }

    public void setRebateline(String rebateline) {
        this.rebateline = rebateline;
    }

    public Integer getStore() {
        return store;
    }

    public void setStore(Integer store) {
        this.store = store;
    }

    public Integer getStyle() {
        return style;
    }

    public void setStyle(Integer style) {
        this.style = style;
    }

    public Integer getAppointmentcount() {
        return appointmentcount;
    }

    public void setAppointmentcount(Integer appointmentcount) {
        this.appointmentcount = appointmentcount;
    }

    public String getCompanyimg() {
        return companyimg;
    }

    public void setCompanyimg(String companyimg) {
        this.companyimg = companyimg;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }

    public String getCompanyimgothers() {
        return companyimgothers;
    }

    public void setCompanyimgothers(String companyimgothers) {
        this.companyimgothers = companyimgothers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCuid() {
        return cuid;
    }

    public void setCuid(Integer cuid) {
        this.cuid = cuid;
    }

    public String getTxtype() {
        return txtype;
    }

    public void setTxtype(String txtype) {
        this.txtype = txtype;
    }

    public Integer getTxafterdays() {
        return txafterdays;
    }

    public void setTxafterdays(Integer txafterdays) {
        this.txafterdays = txafterdays;
    }

    public Integer getChargingtime() {
        return chargingtime;
    }

    public void setChargingtime(Integer chargingtime) {
        this.chargingtime = chargingtime;
    }

    public Integer getDevicenumber() {
        return devicenumber;
    }

    public void setDevicenumber(Integer devicenumber) {
        this.devicenumber = devicenumber;
    }
}