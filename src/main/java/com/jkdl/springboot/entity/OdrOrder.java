package com.jkdl.springboot.entity;

import java.math.BigDecimal;
import java.util.Date;

public class OdrOrder {
    private Date maxcreatedate;

    public Date getMaxcreatedate() {
        return maxcreatedate;
    }

    public void setMaxcreatedate(Date maxcreatedate) {
        this.maxcreatedate = maxcreatedate;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    private Integer orderCount;

    private Integer id;

    private Integer cuid;

    private Integer qyid;

    private String orderid;

    private String oname;

    private String cuname;

    private Integer paytype;

    private String ordercode;

    private BigDecimal rebate;

    private Date createdate;

    private Integer status;

    private String refundstatus;

    private BigDecimal amount;

    private BigDecimal fee;

    private BigDecimal ptotal;

    private Integer quantity;

    private String paystatus;

    private BigDecimal updateamount;

    private BigDecimal payamount;

    private String expresscode;

    private String expressname;

    private String otherrequirement;

    private String remark;

    private String expressno;

    private String expresscompanyname;

    private String lowstocks;

    private String confirmsendproductremark;

    private String closedcomment;

    private Integer score;

    private String tradeno;

    private String ordertype;

    private String invoicehead;

    private Integer bdelete;

    private Integer txstatus;

    private Integer txid;

    private Integer erjistatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCuid() {
        return cuid;
    }

    public void setCuid(Integer cuid) {
        this.cuid = cuid;
    }

    public Integer getQyid() {
        return qyid;
    }

    public void setQyid(Integer qyid) {
        this.qyid = qyid;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public String getCuname() {
        return cuname;
    }

    public void setCuname(String cuname) {
        this.cuname = cuname;
    }

    public Integer getPaytype() {
        return paytype;
    }

    public void setPaytype(Integer paytype) {
        this.paytype = paytype;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public BigDecimal getRebate() {
        return rebate;
    }

    public void setRebate(BigDecimal rebate) {
        this.rebate = rebate;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRefundstatus() {
        return refundstatus;
    }

    public void setRefundstatus(String refundstatus) {
        this.refundstatus = refundstatus;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getPtotal() {
        return ptotal;
    }

    public void setPtotal(BigDecimal ptotal) {
        this.ptotal = ptotal;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPaystatus() {
        return paystatus;
    }

    public void setPaystatus(String paystatus) {
        this.paystatus = paystatus;
    }

    public BigDecimal getUpdateamount() {
        return updateamount;
    }

    public void setUpdateamount(BigDecimal updateamount) {
        this.updateamount = updateamount;
    }

    public BigDecimal getPayamount() {
        return payamount;
    }

    public void setPayamount(BigDecimal payamount) {
        this.payamount = payamount;
    }

    public String getExpresscode() {
        return expresscode;
    }

    public void setExpresscode(String expresscode) {
        this.expresscode = expresscode;
    }

    public String getExpressname() {
        return expressname;
    }

    public void setExpressname(String expressname) {
        this.expressname = expressname;
    }

    public String getOtherrequirement() {
        return otherrequirement;
    }

    public void setOtherrequirement(String otherrequirement) {
        this.otherrequirement = otherrequirement;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getExpressno() {
        return expressno;
    }

    public void setExpressno(String expressno) {
        this.expressno = expressno;
    }

    public String getExpresscompanyname() {
        return expresscompanyname;
    }

    public void setExpresscompanyname(String expresscompanyname) {
        this.expresscompanyname = expresscompanyname;
    }

    public String getLowstocks() {
        return lowstocks;
    }

    public void setLowstocks(String lowstocks) {
        this.lowstocks = lowstocks;
    }

    public String getConfirmsendproductremark() {
        return confirmsendproductremark;
    }

    public void setConfirmsendproductremark(String confirmsendproductremark) {
        this.confirmsendproductremark = confirmsendproductremark;
    }

    public String getClosedcomment() {
        return closedcomment;
    }

    public void setClosedcomment(String closedcomment) {
        this.closedcomment = closedcomment;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getTradeno() {
        return tradeno;
    }

    public void setTradeno(String tradeno) {
        this.tradeno = tradeno;
    }

    public String getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(String ordertype) {
        this.ordertype = ordertype;
    }

    public String getInvoicehead() {
        return invoicehead;
    }

    public void setInvoicehead(String invoicehead) {
        this.invoicehead = invoicehead;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }

    public Integer getTxstatus() {
        return txstatus;
    }

    public void setTxstatus(Integer txstatus) {
        this.txstatus = txstatus;
    }

    public Integer getTxid() {
        return txid;
    }

    public void setTxid(Integer txid) {
        this.txid = txid;
    }

    public Integer getErjistatus() {
        return erjistatus;
    }

    public void setErjistatus(Integer erjistatus) {
        this.erjistatus = erjistatus;
    }
}