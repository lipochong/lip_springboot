package com.jkdl.springboot.entity;

public class PdProductsBatchVoiceKey {
    private Integer id;

    private Integer bacthid;

    private Integer synthid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBacthid() {
        return bacthid;
    }

    public void setBacthid(Integer bacthid) {
        this.bacthid = bacthid;
    }

    public Integer getSynthid() {
        return synthid;
    }

    public void setSynthid(Integer synthid) {
        this.synthid = synthid;
    }
}