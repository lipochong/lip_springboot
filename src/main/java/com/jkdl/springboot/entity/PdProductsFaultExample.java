package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PdProductsFaultExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PdProductsFaultExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSeconduseridIsNull() {
            addCriterion("seconduserid is null");
            return (Criteria) this;
        }

        public Criteria andSeconduseridIsNotNull() {
            addCriterion("seconduserid is not null");
            return (Criteria) this;
        }

        public Criteria andSeconduseridEqualTo(Integer value) {
            addCriterion("seconduserid =", value, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridNotEqualTo(Integer value) {
            addCriterion("seconduserid <>", value, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridGreaterThan(Integer value) {
            addCriterion("seconduserid >", value, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridGreaterThanOrEqualTo(Integer value) {
            addCriterion("seconduserid >=", value, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridLessThan(Integer value) {
            addCriterion("seconduserid <", value, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridLessThanOrEqualTo(Integer value) {
            addCriterion("seconduserid <=", value, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridIn(List<Integer> values) {
            addCriterion("seconduserid in", values, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridNotIn(List<Integer> values) {
            addCriterion("seconduserid not in", values, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridBetween(Integer value1, Integer value2) {
            addCriterion("seconduserid between", value1, value2, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andSeconduseridNotBetween(Integer value1, Integer value2) {
            addCriterion("seconduserid not between", value1, value2, "seconduserid");
            return (Criteria) this;
        }

        public Criteria andLinknameIsNull() {
            addCriterion("linkname is null");
            return (Criteria) this;
        }

        public Criteria andLinknameIsNotNull() {
            addCriterion("linkname is not null");
            return (Criteria) this;
        }

        public Criteria andLinknameEqualTo(String value) {
            addCriterion("linkname =", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotEqualTo(String value) {
            addCriterion("linkname <>", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameGreaterThan(String value) {
            addCriterion("linkname >", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameGreaterThanOrEqualTo(String value) {
            addCriterion("linkname >=", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameLessThan(String value) {
            addCriterion("linkname <", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameLessThanOrEqualTo(String value) {
            addCriterion("linkname <=", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameLike(String value) {
            addCriterion("linkname like", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotLike(String value) {
            addCriterion("linkname not like", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameIn(List<String> values) {
            addCriterion("linkname in", values, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotIn(List<String> values) {
            addCriterion("linkname not in", values, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameBetween(String value1, String value2) {
            addCriterion("linkname between", value1, value2, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotBetween(String value1, String value2) {
            addCriterion("linkname not between", value1, value2, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinkphoneIsNull() {
            addCriterion("linkphone is null");
            return (Criteria) this;
        }

        public Criteria andLinkphoneIsNotNull() {
            addCriterion("linkphone is not null");
            return (Criteria) this;
        }

        public Criteria andLinkphoneEqualTo(String value) {
            addCriterion("linkphone =", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneNotEqualTo(String value) {
            addCriterion("linkphone <>", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneGreaterThan(String value) {
            addCriterion("linkphone >", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneGreaterThanOrEqualTo(String value) {
            addCriterion("linkphone >=", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneLessThan(String value) {
            addCriterion("linkphone <", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneLessThanOrEqualTo(String value) {
            addCriterion("linkphone <=", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneLike(String value) {
            addCriterion("linkphone like", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneNotLike(String value) {
            addCriterion("linkphone not like", value, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneIn(List<String> values) {
            addCriterion("linkphone in", values, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneNotIn(List<String> values) {
            addCriterion("linkphone not in", values, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneBetween(String value1, String value2) {
            addCriterion("linkphone between", value1, value2, "linkphone");
            return (Criteria) this;
        }

        public Criteria andLinkphoneNotBetween(String value1, String value2) {
            addCriterion("linkphone not between", value1, value2, "linkphone");
            return (Criteria) this;
        }

        public Criteria andFaultreasonIsNull() {
            addCriterion("faultreason is null");
            return (Criteria) this;
        }

        public Criteria andFaultreasonIsNotNull() {
            addCriterion("faultreason is not null");
            return (Criteria) this;
        }

        public Criteria andFaultreasonEqualTo(Integer value) {
            addCriterion("faultreason =", value, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonNotEqualTo(Integer value) {
            addCriterion("faultreason <>", value, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonGreaterThan(Integer value) {
            addCriterion("faultreason >", value, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonGreaterThanOrEqualTo(Integer value) {
            addCriterion("faultreason >=", value, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonLessThan(Integer value) {
            addCriterion("faultreason <", value, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonLessThanOrEqualTo(Integer value) {
            addCriterion("faultreason <=", value, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonIn(List<Integer> values) {
            addCriterion("faultreason in", values, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonNotIn(List<Integer> values) {
            addCriterion("faultreason not in", values, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonBetween(Integer value1, Integer value2) {
            addCriterion("faultreason between", value1, value2, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasonNotBetween(Integer value1, Integer value2) {
            addCriterion("faultreason not between", value1, value2, "faultreason");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextIsNull() {
            addCriterion("faultreasontext is null");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextIsNotNull() {
            addCriterion("faultreasontext is not null");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextEqualTo(String value) {
            addCriterion("faultreasontext =", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextNotEqualTo(String value) {
            addCriterion("faultreasontext <>", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextGreaterThan(String value) {
            addCriterion("faultreasontext >", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextGreaterThanOrEqualTo(String value) {
            addCriterion("faultreasontext >=", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextLessThan(String value) {
            addCriterion("faultreasontext <", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextLessThanOrEqualTo(String value) {
            addCriterion("faultreasontext <=", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextLike(String value) {
            addCriterion("faultreasontext like", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextNotLike(String value) {
            addCriterion("faultreasontext not like", value, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextIn(List<String> values) {
            addCriterion("faultreasontext in", values, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextNotIn(List<String> values) {
            addCriterion("faultreasontext not in", values, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextBetween(String value1, String value2) {
            addCriterion("faultreasontext between", value1, value2, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andFaultreasontextNotBetween(String value1, String value2) {
            addCriterion("faultreasontext not between", value1, value2, "faultreasontext");
            return (Criteria) this;
        }

        public Criteria andProductidIsNull() {
            addCriterion("productid is null");
            return (Criteria) this;
        }

        public Criteria andProductidIsNotNull() {
            addCriterion("productid is not null");
            return (Criteria) this;
        }

        public Criteria andProductidEqualTo(String value) {
            addCriterion("productid =", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidNotEqualTo(String value) {
            addCriterion("productid <>", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidGreaterThan(String value) {
            addCriterion("productid >", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidGreaterThanOrEqualTo(String value) {
            addCriterion("productid >=", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidLessThan(String value) {
            addCriterion("productid <", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidLessThanOrEqualTo(String value) {
            addCriterion("productid <=", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidLike(String value) {
            addCriterion("productid like", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidNotLike(String value) {
            addCriterion("productid not like", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidIn(List<String> values) {
            addCriterion("productid in", values, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidNotIn(List<String> values) {
            addCriterion("productid not in", values, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidBetween(String value1, String value2) {
            addCriterion("productid between", value1, value2, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidNotBetween(String value1, String value2) {
            addCriterion("productid not between", value1, value2, "productid");
            return (Criteria) this;
        }

        public Criteria andProductportidIsNull() {
            addCriterion("productportid is null");
            return (Criteria) this;
        }

        public Criteria andProductportidIsNotNull() {
            addCriterion("productportid is not null");
            return (Criteria) this;
        }

        public Criteria andProductportidEqualTo(Integer value) {
            addCriterion("productportid =", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidNotEqualTo(Integer value) {
            addCriterion("productportid <>", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidGreaterThan(Integer value) {
            addCriterion("productportid >", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidGreaterThanOrEqualTo(Integer value) {
            addCriterion("productportid >=", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidLessThan(Integer value) {
            addCriterion("productportid <", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidLessThanOrEqualTo(Integer value) {
            addCriterion("productportid <=", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidIn(List<Integer> values) {
            addCriterion("productportid in", values, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidNotIn(List<Integer> values) {
            addCriterion("productportid not in", values, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidBetween(Integer value1, Integer value2) {
            addCriterion("productportid between", value1, value2, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidNotBetween(Integer value1, Integer value2) {
            addCriterion("productportid not between", value1, value2, "productportid");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andQyidIsNull() {
            addCriterion("qyid is null");
            return (Criteria) this;
        }

        public Criteria andQyidIsNotNull() {
            addCriterion("qyid is not null");
            return (Criteria) this;
        }

        public Criteria andQyidEqualTo(Integer value) {
            addCriterion("qyid =", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidNotEqualTo(Integer value) {
            addCriterion("qyid <>", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidGreaterThan(Integer value) {
            addCriterion("qyid >", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidGreaterThanOrEqualTo(Integer value) {
            addCriterion("qyid >=", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidLessThan(Integer value) {
            addCriterion("qyid <", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidLessThanOrEqualTo(Integer value) {
            addCriterion("qyid <=", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidIn(List<Integer> values) {
            addCriterion("qyid in", values, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidNotIn(List<Integer> values) {
            addCriterion("qyid not in", values, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidBetween(Integer value1, Integer value2) {
            addCriterion("qyid between", value1, value2, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidNotBetween(Integer value1, Integer value2) {
            addCriterion("qyid not between", value1, value2, "qyid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}