package com.jkdl.springboot.entity;

import java.math.BigDecimal;
import java.util.Date;

public class OdrOrderdetail {
    private Integer id;

    private String orderid;

    private Integer productid;

    private Integer productportid;

    private String productdetail;

    private Integer specid;

    private Long price;

    private Integer number;

    private String productname;

    private BigDecimal fee;

    private BigDecimal total0;

    private Integer iscomment;

    private String lowstocks;

    private Integer score;

    private String specinfo;

    private String giftid;

    private String remark;

    private String proimg;

    private String catalogstatus;

    private Integer bdelete;

    private Date cdstarttime;

    private Date cdendtime;

    private Integer ordstatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public Integer getProductportid() {
        return productportid;
    }

    public void setProductportid(Integer productportid) {
        this.productportid = productportid;
    }

    public String getProductdetail() {
        return productdetail;
    }

    public void setProductdetail(String productdetail) {
        this.productdetail = productdetail;
    }

    public Integer getSpecid() {
        return specid;
    }

    public void setSpecid(Integer specid) {
        this.specid = specid;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getTotal0() {
        return total0;
    }

    public void setTotal0(BigDecimal total0) {
        this.total0 = total0;
    }

    public Integer getIscomment() {
        return iscomment;
    }

    public void setIscomment(Integer iscomment) {
        this.iscomment = iscomment;
    }

    public String getLowstocks() {
        return lowstocks;
    }

    public void setLowstocks(String lowstocks) {
        this.lowstocks = lowstocks;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getSpecinfo() {
        return specinfo;
    }

    public void setSpecinfo(String specinfo) {
        this.specinfo = specinfo;
    }

    public String getGiftid() {
        return giftid;
    }

    public void setGiftid(String giftid) {
        this.giftid = giftid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getProimg() {
        return proimg;
    }

    public void setProimg(String proimg) {
        this.proimg = proimg;
    }

    public String getCatalogstatus() {
        return catalogstatus;
    }

    public void setCatalogstatus(String catalogstatus) {
        this.catalogstatus = catalogstatus;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }

    public Date getCdstarttime() {
        return cdstarttime;
    }

    public void setCdstarttime(Date cdstarttime) {
        this.cdstarttime = cdstarttime;
    }

    public Date getCdendtime() {
        return cdendtime;
    }

    public void setCdendtime(Date cdendtime) {
        this.cdendtime = cdendtime;
    }

    public Integer getOrdstatus() {
        return ordstatus;
    }

    public void setOrdstatus(Integer ordstatus) {
        this.ordstatus = ordstatus;
    }
}