package com.jkdl.springboot.entity;

import java.util.Date;

public class PdProductsFault {
    private Integer id;

    private Integer seconduserid;

    private String linkname;

    private String linkphone;

    private Integer faultreason;

    private String faultreasontext;

    private String productid;

    private Integer productportid;

    private Date createtime;

    private Integer bdelete;

    private Integer qyid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSeconduserid() {
        return seconduserid;
    }

    public void setSeconduserid(Integer seconduserid) {
        this.seconduserid = seconduserid;
    }

    public String getLinkname() {
        return linkname;
    }

    public void setLinkname(String linkname) {
        this.linkname = linkname;
    }

    public String getLinkphone() {
        return linkphone;
    }

    public void setLinkphone(String linkphone) {
        this.linkphone = linkphone;
    }

    public Integer getFaultreason() {
        return faultreason;
    }

    public void setFaultreason(Integer faultreason) {
        this.faultreason = faultreason;
    }

    public String getFaultreasontext() {
        return faultreasontext;
    }

    public void setFaultreasontext(String faultreasontext) {
        this.faultreasontext = faultreasontext;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public Integer getProductportid() {
        return productportid;
    }

    public void setProductportid(Integer productportid) {
        this.productportid = productportid;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }

    public Integer getQyid() {
        return qyid;
    }

    public void setQyid(Integer qyid) {
        this.qyid = qyid;
    }
}