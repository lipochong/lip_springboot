package com.jkdl.springboot.entity;

public class OdrOrderdetailWithBLOBs extends OdrOrderdetail {
    private String  companyname;
    private String  provincename;
    private String  cityname;
    private String  districtname;
    private String  openids;
    private String  payamount;
    private String  detail;
    private String  devicenumber;
    private String  agentnamess;
    private String  cuname;
    private String  status;
    private String  createdate;

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getProvincename() {
        return provincename;
    }

    public void setProvincename(String provincename) {
        this.provincename = provincename;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getDistrictname() {
        return districtname;
    }

    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }

    public String getOpenids() {
        return openids;
    }

    public void setOpenids(String openids) {
        this.openids = openids;
    }

    public String getPayamount() {
        return payamount;
    }

    public void setPayamount(String payamount) {
        this.payamount = payamount;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDevicenumber() {
        return devicenumber;
    }

    public void setDevicenumber(String devicenumber) {
        this.devicenumber = devicenumber;
    }

    public String getAgentnamess() {
        return agentnamess;
    }

    public void setAgentnamess(String agentnamess) {
        this.agentnamess = agentnamess;
    }

    public String getCuname() {
        return cuname;
    }

    public void setCuname(String cuname) {
        this.cuname = cuname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
