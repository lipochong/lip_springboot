package com.jkdl.springboot.entity;

import java.util.Date;

public class SettleAccounts {
    //开始时间
    private Date beginDate;
    //设备对应ID
    private  Integer companyid;
    //结束时间
    private Date endDate;
    //总设备数量
    private Integer machineCount;

    //1元订单
    private Integer one;
    //2元订单
    private Integer too;
    //3元订单
    private Integer three;

    //4元订单
    private Integer four;

    //5元订单
    private Integer five;

    //0.01元订单
    private Integer zeroOne;

    //0.01元订单
    private Integer zeroZeroOne;

    //0.03
    private Integer zeroThree;


    //充电金额
    private Double nationpostage;
    //公司名称

    public double getAftersaleService() {
        return aftersaleService;
    }

    public void setAftersaleService(double aftersaleService) {
        this.aftersaleService = aftersaleService;
    }

    private String companyname;
    //商户创建时间
    private Date createtime;

    //交易笔数
    private  int transactionNumber;
    //总金额
    private double totalSum;
    //月平均
    private double monthAvg;
    //平台服务费
    private double serviceCost;
    //客诉服务费
    private  double aftersaleService;
    //每台月营业额
    private double countavg;

    public double getCountavg() {
        return countavg;
    }

    public void setCountavg(double countavg) {
        this.countavg = countavg;
    }

    public int getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(int transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    public double getMonthAvg() {
        return monthAvg;
    }

    public void setMonthAvg(double monthAvg) {
        this.monthAvg = monthAvg;
    }

    public double getServiceCost() {
        return serviceCost;
    }

    public void setServiceCost(double serviceCost) {
        this.serviceCost = serviceCost;
    }



    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public Integer getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Integer companyid) {
        this.companyid = companyid;
    }

    public Integer getMachineCount() {
        return machineCount;
    }

    public void setMachineCount(Integer machineCount) {
        this.machineCount = machineCount;
    }


    public Integer getZeroOne() {
        return zeroOne;
    }

    public void setZeroOne(Integer zeroOne) {
        this.zeroOne = zeroOne;
    }

    public Integer getOne() {
        return one;
    }

    public void setOne(Integer one) {
        this.one = one;
    }

    public Integer getToo() {
        return too;
    }

    public void setToo(Integer too) {
        this.too = too;
    }

    public Integer getThree() {
        return three;
    }

    public void setThree(Integer three) {
        this.three = three;
    }

    public Integer getFour() {
        return four;
    }

    public void setFour(Integer four) {
        this.four = four;
    }

    public Integer getFive() {
        return five;
    }

    public void setFive(Integer five) {
        this.five = five;
    }

    public Double getNationpostage() {
        return nationpostage;
    }

    public void setNationpostage(Double nationpostage) {
        this.nationpostage = nationpostage;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}
