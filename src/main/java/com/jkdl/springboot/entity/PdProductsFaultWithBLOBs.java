package com.jkdl.springboot.entity;

public class PdProductsFaultWithBLOBs extends PdProductsFault {
    private String  companyname;
    private String  detail;

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
