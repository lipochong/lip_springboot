package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.List;

public class PdProductsVoiceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PdProductsVoiceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSynthsrcIsNull() {
            addCriterion("synthsrc is null");
            return (Criteria) this;
        }

        public Criteria andSynthsrcIsNotNull() {
            addCriterion("synthsrc is not null");
            return (Criteria) this;
        }

        public Criteria andSynthsrcEqualTo(String value) {
            addCriterion("synthsrc =", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcNotEqualTo(String value) {
            addCriterion("synthsrc <>", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcGreaterThan(String value) {
            addCriterion("synthsrc >", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcGreaterThanOrEqualTo(String value) {
            addCriterion("synthsrc >=", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcLessThan(String value) {
            addCriterion("synthsrc <", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcLessThanOrEqualTo(String value) {
            addCriterion("synthsrc <=", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcLike(String value) {
            addCriterion("synthsrc like", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcNotLike(String value) {
            addCriterion("synthsrc not like", value, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcIn(List<String> values) {
            addCriterion("synthsrc in", values, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcNotIn(List<String> values) {
            addCriterion("synthsrc not in", values, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcBetween(String value1, String value2) {
            addCriterion("synthsrc between", value1, value2, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andSynthsrcNotBetween(String value1, String value2) {
            addCriterion("synthsrc not between", value1, value2, "synthsrc");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameIsNull() {
            addCriterion("ultrasonicname is null");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameIsNotNull() {
            addCriterion("ultrasonicname is not null");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameEqualTo(String value) {
            addCriterion("ultrasonicname =", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameNotEqualTo(String value) {
            addCriterion("ultrasonicname <>", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameGreaterThan(String value) {
            addCriterion("ultrasonicname >", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameGreaterThanOrEqualTo(String value) {
            addCriterion("ultrasonicname >=", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameLessThan(String value) {
            addCriterion("ultrasonicname <", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameLessThanOrEqualTo(String value) {
            addCriterion("ultrasonicname <=", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameLike(String value) {
            addCriterion("ultrasonicname like", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameNotLike(String value) {
            addCriterion("ultrasonicname not like", value, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameIn(List<String> values) {
            addCriterion("ultrasonicname in", values, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameNotIn(List<String> values) {
            addCriterion("ultrasonicname not in", values, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameBetween(String value1, String value2) {
            addCriterion("ultrasonicname between", value1, value2, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andUltrasonicnameNotBetween(String value1, String value2) {
            addCriterion("ultrasonicname not between", value1, value2, "ultrasonicname");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}