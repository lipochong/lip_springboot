package com.jkdl.springboot.entity;

public class PdProductsBatchWithBLOBs extends PdProductsBatch{
    private Integer synthid;

    public Integer getSynthid() {
        return synthid;
    }

    public void setSynthid(Integer synthid) {
        this.synthid = synthid;
    }
}