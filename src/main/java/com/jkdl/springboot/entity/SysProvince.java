package com.jkdl.springboot.entity;

import java.util.Date;

public class SysProvince {
    private Integer id;

    private String provincename;

    private Date datecreated;

    private Date dateupdated;

    private String bdelete;

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProvincename() {
        return provincename;
    }

    public void setProvincename(String provincename) {
        this.provincename = provincename;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    public Date getDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(Date dateupdated) {
        this.dateupdated = dateupdated;
    }

    public String getBdelete() {
        return bdelete;
    }

    public void setBdelete(String bdelete) {
        this.bdelete = bdelete;
    }
}