package com.jkdl.springboot.entity;

import java.util.Date;

public class PdProductsBatch {
    private Double id;

    private Double parentid;

    private Double level;

    private String devtype;

    private String producttypename;

    private String icon;

    private Double sort;

    private String voiceids;

    private Double bdelete;

    private Double createuser;

    private Date createtime;

    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }

    public Double getParentid() {
        return parentid;
    }

    public void setParentid(Double parentid) {
        this.parentid = parentid;
    }

    public Double getLevel() {
        return level;
    }

    public void setLevel(Double level) {
        this.level = level;
    }

    public String getDevtype() {
        return devtype;
    }

    public void setDevtype(String devtype) {
        this.devtype = devtype;
    }

    public String getProducttypename() {
        return producttypename;
    }

    public void setProducttypename(String producttypename) {
        this.producttypename = producttypename;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Double getSort() {
        return sort;
    }

    public void setSort(Double sort) {
        this.sort = sort;
    }

    public String getVoiceids() {
        return voiceids;
    }

    public void setVoiceids(String voiceids) {
        this.voiceids = voiceids;
    }

    public Double getBdelete() {
        return bdelete;
    }

    public void setBdelete(Double bdelete) {
        this.bdelete = bdelete;
    }

    public Double getCreateuser() {
        return createuser;
    }

    public void setCreateuser(Double createuser) {
        this.createuser = createuser;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}