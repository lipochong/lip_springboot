package com.jkdl.springboot.entity;

public class OdrOrderGroup {
    //    日期/时间段
    private String name;
    //数量
    private Integer value;
    //自身数量
    private int counttoo;

    public int getCounttoo() {
        return counttoo;
    }

    public void setCounttoo(int counttoo) {
        this.counttoo = counttoo;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
