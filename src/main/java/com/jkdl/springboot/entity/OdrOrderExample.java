package com.jkdl.springboot.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OdrOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OdrOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCuidIsNull() {
            addCriterion("cuid is null");
            return (Criteria) this;
        }

        public Criteria andCuidIsNotNull() {
            addCriterion("cuid is not null");
            return (Criteria) this;
        }

        public Criteria andCuidEqualTo(Integer value) {
            addCriterion("cuid =", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidNotEqualTo(Integer value) {
            addCriterion("cuid <>", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidGreaterThan(Integer value) {
            addCriterion("cuid >", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cuid >=", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidLessThan(Integer value) {
            addCriterion("cuid <", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidLessThanOrEqualTo(Integer value) {
            addCriterion("cuid <=", value, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidIn(List<Integer> values) {
            addCriterion("cuid in", values, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidNotIn(List<Integer> values) {
            addCriterion("cuid not in", values, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidBetween(Integer value1, Integer value2) {
            addCriterion("cuid between", value1, value2, "cuid");
            return (Criteria) this;
        }

        public Criteria andCuidNotBetween(Integer value1, Integer value2) {
            addCriterion("cuid not between", value1, value2, "cuid");
            return (Criteria) this;
        }

        public Criteria andQyidIsNull() {
            addCriterion("qyid is null");
            return (Criteria) this;
        }

        public Criteria andQyidIsNotNull() {
            addCriterion("qyid is not null");
            return (Criteria) this;
        }

        public Criteria andQyidEqualTo(Integer value) {
            addCriterion("qyid =", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidNotEqualTo(Integer value) {
            addCriterion("qyid <>", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidGreaterThan(Integer value) {
            addCriterion("qyid >", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidGreaterThanOrEqualTo(Integer value) {
            addCriterion("qyid >=", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidLessThan(Integer value) {
            addCriterion("qyid <", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidLessThanOrEqualTo(Integer value) {
            addCriterion("qyid <=", value, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidIn(List<Integer> values) {
            addCriterion("qyid in", values, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidNotIn(List<Integer> values) {
            addCriterion("qyid not in", values, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidBetween(Integer value1, Integer value2) {
            addCriterion("qyid between", value1, value2, "qyid");
            return (Criteria) this;
        }

        public Criteria andQyidNotBetween(Integer value1, Integer value2) {
            addCriterion("qyid not between", value1, value2, "qyid");
            return (Criteria) this;
        }

        public Criteria andOrderidIsNull() {
            addCriterion("orderid is null");
            return (Criteria) this;
        }

        public Criteria andOrderidIsNotNull() {
            addCriterion("orderid is not null");
            return (Criteria) this;
        }

        public Criteria andOrderidEqualTo(String value) {
            addCriterion("orderid =", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotEqualTo(String value) {
            addCriterion("orderid <>", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidGreaterThan(String value) {
            addCriterion("orderid >", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidGreaterThanOrEqualTo(String value) {
            addCriterion("orderid >=", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidLessThan(String value) {
            addCriterion("orderid <", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidLessThanOrEqualTo(String value) {
            addCriterion("orderid <=", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidLike(String value) {
            addCriterion("orderid like", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotLike(String value) {
            addCriterion("orderid not like", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidIn(List<String> values) {
            addCriterion("orderid in", values, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotIn(List<String> values) {
            addCriterion("orderid not in", values, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidBetween(String value1, String value2) {
            addCriterion("orderid between", value1, value2, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotBetween(String value1, String value2) {
            addCriterion("orderid not between", value1, value2, "orderid");
            return (Criteria) this;
        }

        public Criteria andOnameIsNull() {
            addCriterion("oname is null");
            return (Criteria) this;
        }

        public Criteria andOnameIsNotNull() {
            addCriterion("oname is not null");
            return (Criteria) this;
        }

        public Criteria andOnameEqualTo(String value) {
            addCriterion("oname =", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameNotEqualTo(String value) {
            addCriterion("oname <>", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameGreaterThan(String value) {
            addCriterion("oname >", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameGreaterThanOrEqualTo(String value) {
            addCriterion("oname >=", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameLessThan(String value) {
            addCriterion("oname <", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameLessThanOrEqualTo(String value) {
            addCriterion("oname <=", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameLike(String value) {
            addCriterion("oname like", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameNotLike(String value) {
            addCriterion("oname not like", value, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameIn(List<String> values) {
            addCriterion("oname in", values, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameNotIn(List<String> values) {
            addCriterion("oname not in", values, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameBetween(String value1, String value2) {
            addCriterion("oname between", value1, value2, "oname");
            return (Criteria) this;
        }

        public Criteria andOnameNotBetween(String value1, String value2) {
            addCriterion("oname not between", value1, value2, "oname");
            return (Criteria) this;
        }

        public Criteria andCunameIsNull() {
            addCriterion("cuname is null");
            return (Criteria) this;
        }

        public Criteria andCunameIsNotNull() {
            addCriterion("cuname is not null");
            return (Criteria) this;
        }

        public Criteria andCunameEqualTo(String value) {
            addCriterion("cuname =", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameNotEqualTo(String value) {
            addCriterion("cuname <>", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameGreaterThan(String value) {
            addCriterion("cuname >", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameGreaterThanOrEqualTo(String value) {
            addCriterion("cuname >=", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameLessThan(String value) {
            addCriterion("cuname <", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameLessThanOrEqualTo(String value) {
            addCriterion("cuname <=", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameLike(String value) {
            addCriterion("cuname like", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameNotLike(String value) {
            addCriterion("cuname not like", value, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameIn(List<String> values) {
            addCriterion("cuname in", values, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameNotIn(List<String> values) {
            addCriterion("cuname not in", values, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameBetween(String value1, String value2) {
            addCriterion("cuname between", value1, value2, "cuname");
            return (Criteria) this;
        }

        public Criteria andCunameNotBetween(String value1, String value2) {
            addCriterion("cuname not between", value1, value2, "cuname");
            return (Criteria) this;
        }

        public Criteria andPaytypeIsNull() {
            addCriterion("paytype is null");
            return (Criteria) this;
        }

        public Criteria andPaytypeIsNotNull() {
            addCriterion("paytype is not null");
            return (Criteria) this;
        }

        public Criteria andPaytypeEqualTo(Integer value) {
            addCriterion("paytype =", value, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeNotEqualTo(Integer value) {
            addCriterion("paytype <>", value, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeGreaterThan(Integer value) {
            addCriterion("paytype >", value, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("paytype >=", value, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeLessThan(Integer value) {
            addCriterion("paytype <", value, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeLessThanOrEqualTo(Integer value) {
            addCriterion("paytype <=", value, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeIn(List<Integer> values) {
            addCriterion("paytype in", values, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeNotIn(List<Integer> values) {
            addCriterion("paytype not in", values, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeBetween(Integer value1, Integer value2) {
            addCriterion("paytype between", value1, value2, "paytype");
            return (Criteria) this;
        }

        public Criteria andPaytypeNotBetween(Integer value1, Integer value2) {
            addCriterion("paytype not between", value1, value2, "paytype");
            return (Criteria) this;
        }

        public Criteria andOrdercodeIsNull() {
            addCriterion("ordercode is null");
            return (Criteria) this;
        }

        public Criteria andOrdercodeIsNotNull() {
            addCriterion("ordercode is not null");
            return (Criteria) this;
        }

        public Criteria andOrdercodeEqualTo(String value) {
            addCriterion("ordercode =", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeNotEqualTo(String value) {
            addCriterion("ordercode <>", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeGreaterThan(String value) {
            addCriterion("ordercode >", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeGreaterThanOrEqualTo(String value) {
            addCriterion("ordercode >=", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeLessThan(String value) {
            addCriterion("ordercode <", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeLessThanOrEqualTo(String value) {
            addCriterion("ordercode <=", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeLike(String value) {
            addCriterion("ordercode like", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeNotLike(String value) {
            addCriterion("ordercode not like", value, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeIn(List<String> values) {
            addCriterion("ordercode in", values, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeNotIn(List<String> values) {
            addCriterion("ordercode not in", values, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeBetween(String value1, String value2) {
            addCriterion("ordercode between", value1, value2, "ordercode");
            return (Criteria) this;
        }

        public Criteria andOrdercodeNotBetween(String value1, String value2) {
            addCriterion("ordercode not between", value1, value2, "ordercode");
            return (Criteria) this;
        }

        public Criteria andRebateIsNull() {
            addCriterion("rebate is null");
            return (Criteria) this;
        }

        public Criteria andRebateIsNotNull() {
            addCriterion("rebate is not null");
            return (Criteria) this;
        }

        public Criteria andRebateEqualTo(BigDecimal value) {
            addCriterion("rebate =", value, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateNotEqualTo(BigDecimal value) {
            addCriterion("rebate <>", value, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateGreaterThan(BigDecimal value) {
            addCriterion("rebate >", value, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("rebate >=", value, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateLessThan(BigDecimal value) {
            addCriterion("rebate <", value, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("rebate <=", value, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateIn(List<BigDecimal> values) {
            addCriterion("rebate in", values, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateNotIn(List<BigDecimal> values) {
            addCriterion("rebate not in", values, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rebate between", value1, value2, "rebate");
            return (Criteria) this;
        }

        public Criteria andRebateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rebate not between", value1, value2, "rebate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("createdate is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("createdate is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("createdate =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("createdate <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("createdate >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("createdate >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("createdate <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("createdate <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("createdate in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("createdate not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("createdate between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("createdate not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andRefundstatusIsNull() {
            addCriterion("refundstatus is null");
            return (Criteria) this;
        }

        public Criteria andRefundstatusIsNotNull() {
            addCriterion("refundstatus is not null");
            return (Criteria) this;
        }

        public Criteria andRefundstatusEqualTo(String value) {
            addCriterion("refundstatus =", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotEqualTo(String value) {
            addCriterion("refundstatus <>", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusGreaterThan(String value) {
            addCriterion("refundstatus >", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusGreaterThanOrEqualTo(String value) {
            addCriterion("refundstatus >=", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusLessThan(String value) {
            addCriterion("refundstatus <", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusLessThanOrEqualTo(String value) {
            addCriterion("refundstatus <=", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusLike(String value) {
            addCriterion("refundstatus like", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotLike(String value) {
            addCriterion("refundstatus not like", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusIn(List<String> values) {
            addCriterion("refundstatus in", values, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotIn(List<String> values) {
            addCriterion("refundstatus not in", values, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusBetween(String value1, String value2) {
            addCriterion("refundstatus between", value1, value2, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotBetween(String value1, String value2) {
            addCriterion("refundstatus not between", value1, value2, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andAmountIsNull() {
            addCriterion("amount is null");
            return (Criteria) this;
        }

        public Criteria andAmountIsNotNull() {
            addCriterion("amount is not null");
            return (Criteria) this;
        }

        public Criteria andAmountEqualTo(BigDecimal value) {
            addCriterion("amount =", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotEqualTo(BigDecimal value) {
            addCriterion("amount <>", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThan(BigDecimal value) {
            addCriterion("amount >", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("amount >=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThan(BigDecimal value) {
            addCriterion("amount <", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("amount <=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountIn(List<BigDecimal> values) {
            addCriterion("amount in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotIn(List<BigDecimal> values) {
            addCriterion("amount not in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("amount between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("amount not between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andFeeIsNull() {
            addCriterion("fee is null");
            return (Criteria) this;
        }

        public Criteria andFeeIsNotNull() {
            addCriterion("fee is not null");
            return (Criteria) this;
        }

        public Criteria andFeeEqualTo(BigDecimal value) {
            addCriterion("fee =", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotEqualTo(BigDecimal value) {
            addCriterion("fee <>", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThan(BigDecimal value) {
            addCriterion("fee >", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fee >=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThan(BigDecimal value) {
            addCriterion("fee <", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fee <=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeIn(List<BigDecimal> values) {
            addCriterion("fee in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotIn(List<BigDecimal> values) {
            addCriterion("fee not in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee not between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andPtotalIsNull() {
            addCriterion("ptotal is null");
            return (Criteria) this;
        }

        public Criteria andPtotalIsNotNull() {
            addCriterion("ptotal is not null");
            return (Criteria) this;
        }

        public Criteria andPtotalEqualTo(BigDecimal value) {
            addCriterion("ptotal =", value, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalNotEqualTo(BigDecimal value) {
            addCriterion("ptotal <>", value, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalGreaterThan(BigDecimal value) {
            addCriterion("ptotal >", value, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ptotal >=", value, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalLessThan(BigDecimal value) {
            addCriterion("ptotal <", value, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ptotal <=", value, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalIn(List<BigDecimal> values) {
            addCriterion("ptotal in", values, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalNotIn(List<BigDecimal> values) {
            addCriterion("ptotal not in", values, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ptotal between", value1, value2, "ptotal");
            return (Criteria) this;
        }

        public Criteria andPtotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ptotal not between", value1, value2, "ptotal");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNull() {
            addCriterion("quantity is null");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNotNull() {
            addCriterion("quantity is not null");
            return (Criteria) this;
        }

        public Criteria andQuantityEqualTo(Integer value) {
            addCriterion("quantity =", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotEqualTo(Integer value) {
            addCriterion("quantity <>", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThan(Integer value) {
            addCriterion("quantity >", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("quantity >=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThan(Integer value) {
            addCriterion("quantity <", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("quantity <=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityIn(List<Integer> values) {
            addCriterion("quantity in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotIn(List<Integer> values) {
            addCriterion("quantity not in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityBetween(Integer value1, Integer value2) {
            addCriterion("quantity between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("quantity not between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andPaystatusIsNull() {
            addCriterion("paystatus is null");
            return (Criteria) this;
        }

        public Criteria andPaystatusIsNotNull() {
            addCriterion("paystatus is not null");
            return (Criteria) this;
        }

        public Criteria andPaystatusEqualTo(String value) {
            addCriterion("paystatus =", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusNotEqualTo(String value) {
            addCriterion("paystatus <>", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusGreaterThan(String value) {
            addCriterion("paystatus >", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusGreaterThanOrEqualTo(String value) {
            addCriterion("paystatus >=", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusLessThan(String value) {
            addCriterion("paystatus <", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusLessThanOrEqualTo(String value) {
            addCriterion("paystatus <=", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusLike(String value) {
            addCriterion("paystatus like", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusNotLike(String value) {
            addCriterion("paystatus not like", value, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusIn(List<String> values) {
            addCriterion("paystatus in", values, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusNotIn(List<String> values) {
            addCriterion("paystatus not in", values, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusBetween(String value1, String value2) {
            addCriterion("paystatus between", value1, value2, "paystatus");
            return (Criteria) this;
        }

        public Criteria andPaystatusNotBetween(String value1, String value2) {
            addCriterion("paystatus not between", value1, value2, "paystatus");
            return (Criteria) this;
        }

        public Criteria andUpdateamountIsNull() {
            addCriterion("updateamount is null");
            return (Criteria) this;
        }

        public Criteria andUpdateamountIsNotNull() {
            addCriterion("updateamount is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateamountEqualTo(BigDecimal value) {
            addCriterion("updateamount =", value, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountNotEqualTo(BigDecimal value) {
            addCriterion("updateamount <>", value, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountGreaterThan(BigDecimal value) {
            addCriterion("updateamount >", value, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("updateamount >=", value, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountLessThan(BigDecimal value) {
            addCriterion("updateamount <", value, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("updateamount <=", value, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountIn(List<BigDecimal> values) {
            addCriterion("updateamount in", values, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountNotIn(List<BigDecimal> values) {
            addCriterion("updateamount not in", values, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("updateamount between", value1, value2, "updateamount");
            return (Criteria) this;
        }

        public Criteria andUpdateamountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("updateamount not between", value1, value2, "updateamount");
            return (Criteria) this;
        }

        public Criteria andPayamountIsNull() {
            addCriterion("payamount is null");
            return (Criteria) this;
        }

        public Criteria andPayamountIsNotNull() {
            addCriterion("payamount is not null");
            return (Criteria) this;
        }

        public Criteria andPayamountEqualTo(BigDecimal value) {
            addCriterion("payamount =", value, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountNotEqualTo(BigDecimal value) {
            addCriterion("payamount <>", value, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountGreaterThan(BigDecimal value) {
            addCriterion("payamount >", value, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("payamount >=", value, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountLessThan(BigDecimal value) {
            addCriterion("payamount <", value, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("payamount <=", value, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountIn(List<BigDecimal> values) {
            addCriterion("payamount in", values, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountNotIn(List<BigDecimal> values) {
            addCriterion("payamount not in", values, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("payamount between", value1, value2, "payamount");
            return (Criteria) this;
        }

        public Criteria andPayamountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("payamount not between", value1, value2, "payamount");
            return (Criteria) this;
        }

        public Criteria andExpresscodeIsNull() {
            addCriterion("expresscode is null");
            return (Criteria) this;
        }

        public Criteria andExpresscodeIsNotNull() {
            addCriterion("expresscode is not null");
            return (Criteria) this;
        }

        public Criteria andExpresscodeEqualTo(String value) {
            addCriterion("expresscode =", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeNotEqualTo(String value) {
            addCriterion("expresscode <>", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeGreaterThan(String value) {
            addCriterion("expresscode >", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeGreaterThanOrEqualTo(String value) {
            addCriterion("expresscode >=", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeLessThan(String value) {
            addCriterion("expresscode <", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeLessThanOrEqualTo(String value) {
            addCriterion("expresscode <=", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeLike(String value) {
            addCriterion("expresscode like", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeNotLike(String value) {
            addCriterion("expresscode not like", value, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeIn(List<String> values) {
            addCriterion("expresscode in", values, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeNotIn(List<String> values) {
            addCriterion("expresscode not in", values, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeBetween(String value1, String value2) {
            addCriterion("expresscode between", value1, value2, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpresscodeNotBetween(String value1, String value2) {
            addCriterion("expresscode not between", value1, value2, "expresscode");
            return (Criteria) this;
        }

        public Criteria andExpressnameIsNull() {
            addCriterion("expressname is null");
            return (Criteria) this;
        }

        public Criteria andExpressnameIsNotNull() {
            addCriterion("expressname is not null");
            return (Criteria) this;
        }

        public Criteria andExpressnameEqualTo(String value) {
            addCriterion("expressname =", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameNotEqualTo(String value) {
            addCriterion("expressname <>", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameGreaterThan(String value) {
            addCriterion("expressname >", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameGreaterThanOrEqualTo(String value) {
            addCriterion("expressname >=", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameLessThan(String value) {
            addCriterion("expressname <", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameLessThanOrEqualTo(String value) {
            addCriterion("expressname <=", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameLike(String value) {
            addCriterion("expressname like", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameNotLike(String value) {
            addCriterion("expressname not like", value, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameIn(List<String> values) {
            addCriterion("expressname in", values, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameNotIn(List<String> values) {
            addCriterion("expressname not in", values, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameBetween(String value1, String value2) {
            addCriterion("expressname between", value1, value2, "expressname");
            return (Criteria) this;
        }

        public Criteria andExpressnameNotBetween(String value1, String value2) {
            addCriterion("expressname not between", value1, value2, "expressname");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementIsNull() {
            addCriterion("otherrequirement is null");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementIsNotNull() {
            addCriterion("otherrequirement is not null");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementEqualTo(String value) {
            addCriterion("otherrequirement =", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementNotEqualTo(String value) {
            addCriterion("otherrequirement <>", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementGreaterThan(String value) {
            addCriterion("otherrequirement >", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementGreaterThanOrEqualTo(String value) {
            addCriterion("otherrequirement >=", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementLessThan(String value) {
            addCriterion("otherrequirement <", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementLessThanOrEqualTo(String value) {
            addCriterion("otherrequirement <=", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementLike(String value) {
            addCriterion("otherrequirement like", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementNotLike(String value) {
            addCriterion("otherrequirement not like", value, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementIn(List<String> values) {
            addCriterion("otherrequirement in", values, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementNotIn(List<String> values) {
            addCriterion("otherrequirement not in", values, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementBetween(String value1, String value2) {
            addCriterion("otherrequirement between", value1, value2, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andOtherrequirementNotBetween(String value1, String value2) {
            addCriterion("otherrequirement not between", value1, value2, "otherrequirement");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andExpressnoIsNull() {
            addCriterion("expressno is null");
            return (Criteria) this;
        }

        public Criteria andExpressnoIsNotNull() {
            addCriterion("expressno is not null");
            return (Criteria) this;
        }

        public Criteria andExpressnoEqualTo(String value) {
            addCriterion("expressno =", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoNotEqualTo(String value) {
            addCriterion("expressno <>", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoGreaterThan(String value) {
            addCriterion("expressno >", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoGreaterThanOrEqualTo(String value) {
            addCriterion("expressno >=", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoLessThan(String value) {
            addCriterion("expressno <", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoLessThanOrEqualTo(String value) {
            addCriterion("expressno <=", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoLike(String value) {
            addCriterion("expressno like", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoNotLike(String value) {
            addCriterion("expressno not like", value, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoIn(List<String> values) {
            addCriterion("expressno in", values, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoNotIn(List<String> values) {
            addCriterion("expressno not in", values, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoBetween(String value1, String value2) {
            addCriterion("expressno between", value1, value2, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpressnoNotBetween(String value1, String value2) {
            addCriterion("expressno not between", value1, value2, "expressno");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameIsNull() {
            addCriterion("expresscompanyname is null");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameIsNotNull() {
            addCriterion("expresscompanyname is not null");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameEqualTo(String value) {
            addCriterion("expresscompanyname =", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameNotEqualTo(String value) {
            addCriterion("expresscompanyname <>", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameGreaterThan(String value) {
            addCriterion("expresscompanyname >", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameGreaterThanOrEqualTo(String value) {
            addCriterion("expresscompanyname >=", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameLessThan(String value) {
            addCriterion("expresscompanyname <", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameLessThanOrEqualTo(String value) {
            addCriterion("expresscompanyname <=", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameLike(String value) {
            addCriterion("expresscompanyname like", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameNotLike(String value) {
            addCriterion("expresscompanyname not like", value, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameIn(List<String> values) {
            addCriterion("expresscompanyname in", values, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameNotIn(List<String> values) {
            addCriterion("expresscompanyname not in", values, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameBetween(String value1, String value2) {
            addCriterion("expresscompanyname between", value1, value2, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andExpresscompanynameNotBetween(String value1, String value2) {
            addCriterion("expresscompanyname not between", value1, value2, "expresscompanyname");
            return (Criteria) this;
        }

        public Criteria andLowstocksIsNull() {
            addCriterion("lowstocks is null");
            return (Criteria) this;
        }

        public Criteria andLowstocksIsNotNull() {
            addCriterion("lowstocks is not null");
            return (Criteria) this;
        }

        public Criteria andLowstocksEqualTo(String value) {
            addCriterion("lowstocks =", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotEqualTo(String value) {
            addCriterion("lowstocks <>", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksGreaterThan(String value) {
            addCriterion("lowstocks >", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksGreaterThanOrEqualTo(String value) {
            addCriterion("lowstocks >=", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksLessThan(String value) {
            addCriterion("lowstocks <", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksLessThanOrEqualTo(String value) {
            addCriterion("lowstocks <=", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksLike(String value) {
            addCriterion("lowstocks like", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotLike(String value) {
            addCriterion("lowstocks not like", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksIn(List<String> values) {
            addCriterion("lowstocks in", values, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotIn(List<String> values) {
            addCriterion("lowstocks not in", values, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksBetween(String value1, String value2) {
            addCriterion("lowstocks between", value1, value2, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotBetween(String value1, String value2) {
            addCriterion("lowstocks not between", value1, value2, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkIsNull() {
            addCriterion("confirmsendproductremark is null");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkIsNotNull() {
            addCriterion("confirmsendproductremark is not null");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkEqualTo(String value) {
            addCriterion("confirmsendproductremark =", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkNotEqualTo(String value) {
            addCriterion("confirmsendproductremark <>", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkGreaterThan(String value) {
            addCriterion("confirmsendproductremark >", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkGreaterThanOrEqualTo(String value) {
            addCriterion("confirmsendproductremark >=", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkLessThan(String value) {
            addCriterion("confirmsendproductremark <", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkLessThanOrEqualTo(String value) {
            addCriterion("confirmsendproductremark <=", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkLike(String value) {
            addCriterion("confirmsendproductremark like", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkNotLike(String value) {
            addCriterion("confirmsendproductremark not like", value, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkIn(List<String> values) {
            addCriterion("confirmsendproductremark in", values, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkNotIn(List<String> values) {
            addCriterion("confirmsendproductremark not in", values, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkBetween(String value1, String value2) {
            addCriterion("confirmsendproductremark between", value1, value2, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andConfirmsendproductremarkNotBetween(String value1, String value2) {
            addCriterion("confirmsendproductremark not between", value1, value2, "confirmsendproductremark");
            return (Criteria) this;
        }

        public Criteria andClosedcommentIsNull() {
            addCriterion("closedcomment is null");
            return (Criteria) this;
        }

        public Criteria andClosedcommentIsNotNull() {
            addCriterion("closedcomment is not null");
            return (Criteria) this;
        }

        public Criteria andClosedcommentEqualTo(String value) {
            addCriterion("closedcomment =", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentNotEqualTo(String value) {
            addCriterion("closedcomment <>", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentGreaterThan(String value) {
            addCriterion("closedcomment >", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentGreaterThanOrEqualTo(String value) {
            addCriterion("closedcomment >=", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentLessThan(String value) {
            addCriterion("closedcomment <", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentLessThanOrEqualTo(String value) {
            addCriterion("closedcomment <=", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentLike(String value) {
            addCriterion("closedcomment like", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentNotLike(String value) {
            addCriterion("closedcomment not like", value, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentIn(List<String> values) {
            addCriterion("closedcomment in", values, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentNotIn(List<String> values) {
            addCriterion("closedcomment not in", values, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentBetween(String value1, String value2) {
            addCriterion("closedcomment between", value1, value2, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andClosedcommentNotBetween(String value1, String value2) {
            addCriterion("closedcomment not between", value1, value2, "closedcomment");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Integer value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Integer value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Integer value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Integer value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Integer value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Integer> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Integer> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Integer value1, Integer value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andTradenoIsNull() {
            addCriterion("tradeno is null");
            return (Criteria) this;
        }

        public Criteria andTradenoIsNotNull() {
            addCriterion("tradeno is not null");
            return (Criteria) this;
        }

        public Criteria andTradenoEqualTo(String value) {
            addCriterion("tradeno =", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoNotEqualTo(String value) {
            addCriterion("tradeno <>", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoGreaterThan(String value) {
            addCriterion("tradeno >", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoGreaterThanOrEqualTo(String value) {
            addCriterion("tradeno >=", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoLessThan(String value) {
            addCriterion("tradeno <", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoLessThanOrEqualTo(String value) {
            addCriterion("tradeno <=", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoLike(String value) {
            addCriterion("tradeno like", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoNotLike(String value) {
            addCriterion("tradeno not like", value, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoIn(List<String> values) {
            addCriterion("tradeno in", values, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoNotIn(List<String> values) {
            addCriterion("tradeno not in", values, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoBetween(String value1, String value2) {
            addCriterion("tradeno between", value1, value2, "tradeno");
            return (Criteria) this;
        }

        public Criteria andTradenoNotBetween(String value1, String value2) {
            addCriterion("tradeno not between", value1, value2, "tradeno");
            return (Criteria) this;
        }

        public Criteria andOrdertypeIsNull() {
            addCriterion("ordertype is null");
            return (Criteria) this;
        }

        public Criteria andOrdertypeIsNotNull() {
            addCriterion("ordertype is not null");
            return (Criteria) this;
        }

        public Criteria andOrdertypeEqualTo(String value) {
            addCriterion("ordertype =", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeNotEqualTo(String value) {
            addCriterion("ordertype <>", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeGreaterThan(String value) {
            addCriterion("ordertype >", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeGreaterThanOrEqualTo(String value) {
            addCriterion("ordertype >=", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeLessThan(String value) {
            addCriterion("ordertype <", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeLessThanOrEqualTo(String value) {
            addCriterion("ordertype <=", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeLike(String value) {
            addCriterion("ordertype like", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeNotLike(String value) {
            addCriterion("ordertype not like", value, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeIn(List<String> values) {
            addCriterion("ordertype in", values, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeNotIn(List<String> values) {
            addCriterion("ordertype not in", values, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeBetween(String value1, String value2) {
            addCriterion("ordertype between", value1, value2, "ordertype");
            return (Criteria) this;
        }

        public Criteria andOrdertypeNotBetween(String value1, String value2) {
            addCriterion("ordertype not between", value1, value2, "ordertype");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadIsNull() {
            addCriterion("invoicehead is null");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadIsNotNull() {
            addCriterion("invoicehead is not null");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadEqualTo(String value) {
            addCriterion("invoicehead =", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadNotEqualTo(String value) {
            addCriterion("invoicehead <>", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadGreaterThan(String value) {
            addCriterion("invoicehead >", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadGreaterThanOrEqualTo(String value) {
            addCriterion("invoicehead >=", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadLessThan(String value) {
            addCriterion("invoicehead <", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadLessThanOrEqualTo(String value) {
            addCriterion("invoicehead <=", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadLike(String value) {
            addCriterion("invoicehead like", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadNotLike(String value) {
            addCriterion("invoicehead not like", value, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadIn(List<String> values) {
            addCriterion("invoicehead in", values, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadNotIn(List<String> values) {
            addCriterion("invoicehead not in", values, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadBetween(String value1, String value2) {
            addCriterion("invoicehead between", value1, value2, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andInvoiceheadNotBetween(String value1, String value2) {
            addCriterion("invoicehead not between", value1, value2, "invoicehead");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andTxstatusIsNull() {
            addCriterion("txStatus is null");
            return (Criteria) this;
        }

        public Criteria andTxstatusIsNotNull() {
            addCriterion("txStatus is not null");
            return (Criteria) this;
        }

        public Criteria andTxstatusEqualTo(Integer value) {
            addCriterion("txStatus =", value, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusNotEqualTo(Integer value) {
            addCriterion("txStatus <>", value, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusGreaterThan(Integer value) {
            addCriterion("txStatus >", value, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("txStatus >=", value, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusLessThan(Integer value) {
            addCriterion("txStatus <", value, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusLessThanOrEqualTo(Integer value) {
            addCriterion("txStatus <=", value, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusIn(List<Integer> values) {
            addCriterion("txStatus in", values, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusNotIn(List<Integer> values) {
            addCriterion("txStatus not in", values, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusBetween(Integer value1, Integer value2) {
            addCriterion("txStatus between", value1, value2, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("txStatus not between", value1, value2, "txstatus");
            return (Criteria) this;
        }

        public Criteria andTxidIsNull() {
            addCriterion("txid is null");
            return (Criteria) this;
        }

        public Criteria andTxidIsNotNull() {
            addCriterion("txid is not null");
            return (Criteria) this;
        }

        public Criteria andTxidEqualTo(Integer value) {
            addCriterion("txid =", value, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidNotEqualTo(Integer value) {
            addCriterion("txid <>", value, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidGreaterThan(Integer value) {
            addCriterion("txid >", value, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidGreaterThanOrEqualTo(Integer value) {
            addCriterion("txid >=", value, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidLessThan(Integer value) {
            addCriterion("txid <", value, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidLessThanOrEqualTo(Integer value) {
            addCriterion("txid <=", value, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidIn(List<Integer> values) {
            addCriterion("txid in", values, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidNotIn(List<Integer> values) {
            addCriterion("txid not in", values, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidBetween(Integer value1, Integer value2) {
            addCriterion("txid between", value1, value2, "txid");
            return (Criteria) this;
        }

        public Criteria andTxidNotBetween(Integer value1, Integer value2) {
            addCriterion("txid not between", value1, value2, "txid");
            return (Criteria) this;
        }

        public Criteria andErjistatusIsNull() {
            addCriterion("erjistatus is null");
            return (Criteria) this;
        }

        public Criteria andErjistatusIsNotNull() {
            addCriterion("erjistatus is not null");
            return (Criteria) this;
        }

        public Criteria andErjistatusEqualTo(Integer value) {
            addCriterion("erjistatus =", value, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusNotEqualTo(Integer value) {
            addCriterion("erjistatus <>", value, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusGreaterThan(Integer value) {
            addCriterion("erjistatus >", value, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("erjistatus >=", value, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusLessThan(Integer value) {
            addCriterion("erjistatus <", value, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusLessThanOrEqualTo(Integer value) {
            addCriterion("erjistatus <=", value, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusIn(List<Integer> values) {
            addCriterion("erjistatus in", values, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusNotIn(List<Integer> values) {
            addCriterion("erjistatus not in", values, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusBetween(Integer value1, Integer value2) {
            addCriterion("erjistatus between", value1, value2, "erjistatus");
            return (Criteria) this;
        }

        public Criteria andErjistatusNotBetween(Integer value1, Integer value2) {
            addCriterion("erjistatus not between", value1, value2, "erjistatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}