package com.jkdl.springboot.entity;

import java.util.Date;

public class SysMessage {
    private Integer id;

    private String systitle;

    private String syscontent;

    private String sysauthor;

    private String sysurl;

    private Date createtime;

    private Integer bdelete;

    private String sysnote;

    private Date endtime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSystitle() {
        return systitle;
    }

    public void setSystitle(String systitle) {
        this.systitle = systitle;
    }

    public String getSyscontent() {
        return syscontent;
    }

    public void setSyscontent(String syscontent) {
        this.syscontent = syscontent;
    }

    public String getSysauthor() {
        return sysauthor;
    }

    public void setSysauthor(String sysauthor) {
        this.sysauthor = sysauthor;
    }

    public String getSysurl() {
        return sysurl;
    }

    public void setSysurl(String sysurl) {
        this.sysurl = sysurl;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }

    public String getSysnote() {
        return sysnote;
    }

    public void setSysnote(String sysnote) {
        this.sysnote = sysnote;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }
}