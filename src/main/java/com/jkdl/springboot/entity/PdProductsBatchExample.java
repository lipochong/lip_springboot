package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PdProductsBatchExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PdProductsBatchExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("Id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("Id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Double value) {
            addCriterion("Id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Double value) {
            addCriterion("Id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Double value) {
            addCriterion("Id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Double value) {
            addCriterion("Id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Double value) {
            addCriterion("Id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Double value) {
            addCriterion("Id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Double> values) {
            addCriterion("Id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Double> values) {
            addCriterion("Id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Double value1, Double value2) {
            addCriterion("Id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Double value1, Double value2) {
            addCriterion("Id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andParentidIsNull() {
            addCriterion("parentId is null");
            return (Criteria) this;
        }

        public Criteria andParentidIsNotNull() {
            addCriterion("parentId is not null");
            return (Criteria) this;
        }

        public Criteria andParentidEqualTo(Double value) {
            addCriterion("parentId =", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidNotEqualTo(Double value) {
            addCriterion("parentId <>", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidGreaterThan(Double value) {
            addCriterion("parentId >", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidGreaterThanOrEqualTo(Double value) {
            addCriterion("parentId >=", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidLessThan(Double value) {
            addCriterion("parentId <", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidLessThanOrEqualTo(Double value) {
            addCriterion("parentId <=", value, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidIn(List<Double> values) {
            addCriterion("parentId in", values, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidNotIn(List<Double> values) {
            addCriterion("parentId not in", values, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidBetween(Double value1, Double value2) {
            addCriterion("parentId between", value1, value2, "parentid");
            return (Criteria) this;
        }

        public Criteria andParentidNotBetween(Double value1, Double value2) {
            addCriterion("parentId not between", value1, value2, "parentid");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("level is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("level is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(Double value) {
            addCriterion("level =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(Double value) {
            addCriterion("level <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(Double value) {
            addCriterion("level >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(Double value) {
            addCriterion("level >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(Double value) {
            addCriterion("level <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(Double value) {
            addCriterion("level <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<Double> values) {
            addCriterion("level in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<Double> values) {
            addCriterion("level not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(Double value1, Double value2) {
            addCriterion("level between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(Double value1, Double value2) {
            addCriterion("level not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andDevtypeIsNull() {
            addCriterion("devtype is null");
            return (Criteria) this;
        }

        public Criteria andDevtypeIsNotNull() {
            addCriterion("devtype is not null");
            return (Criteria) this;
        }

        public Criteria andDevtypeEqualTo(String value) {
            addCriterion("devtype =", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotEqualTo(String value) {
            addCriterion("devtype <>", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeGreaterThan(String value) {
            addCriterion("devtype >", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeGreaterThanOrEqualTo(String value) {
            addCriterion("devtype >=", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLessThan(String value) {
            addCriterion("devtype <", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLessThanOrEqualTo(String value) {
            addCriterion("devtype <=", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLike(String value) {
            addCriterion("devtype like", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotLike(String value) {
            addCriterion("devtype not like", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeIn(List<String> values) {
            addCriterion("devtype in", values, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotIn(List<String> values) {
            addCriterion("devtype not in", values, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeBetween(String value1, String value2) {
            addCriterion("devtype between", value1, value2, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotBetween(String value1, String value2) {
            addCriterion("devtype not between", value1, value2, "devtype");
            return (Criteria) this;
        }

        public Criteria andProducttypenameIsNull() {
            addCriterion("producttypename is null");
            return (Criteria) this;
        }

        public Criteria andProducttypenameIsNotNull() {
            addCriterion("producttypename is not null");
            return (Criteria) this;
        }

        public Criteria andProducttypenameEqualTo(String value) {
            addCriterion("producttypename =", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameNotEqualTo(String value) {
            addCriterion("producttypename <>", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameGreaterThan(String value) {
            addCriterion("producttypename >", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameGreaterThanOrEqualTo(String value) {
            addCriterion("producttypename >=", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameLessThan(String value) {
            addCriterion("producttypename <", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameLessThanOrEqualTo(String value) {
            addCriterion("producttypename <=", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameLike(String value) {
            addCriterion("producttypename like", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameNotLike(String value) {
            addCriterion("producttypename not like", value, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameIn(List<String> values) {
            addCriterion("producttypename in", values, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameNotIn(List<String> values) {
            addCriterion("producttypename not in", values, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameBetween(String value1, String value2) {
            addCriterion("producttypename between", value1, value2, "producttypename");
            return (Criteria) this;
        }

        public Criteria andProducttypenameNotBetween(String value1, String value2) {
            addCriterion("producttypename not between", value1, value2, "producttypename");
            return (Criteria) this;
        }

        public Criteria andIconIsNull() {
            addCriterion("icon is null");
            return (Criteria) this;
        }

        public Criteria andIconIsNotNull() {
            addCriterion("icon is not null");
            return (Criteria) this;
        }

        public Criteria andIconEqualTo(String value) {
            addCriterion("icon =", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotEqualTo(String value) {
            addCriterion("icon <>", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconGreaterThan(String value) {
            addCriterion("icon >", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconGreaterThanOrEqualTo(String value) {
            addCriterion("icon >=", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLessThan(String value) {
            addCriterion("icon <", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLessThanOrEqualTo(String value) {
            addCriterion("icon <=", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconLike(String value) {
            addCriterion("icon like", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotLike(String value) {
            addCriterion("icon not like", value, "icon");
            return (Criteria) this;
        }

        public Criteria andIconIn(List<String> values) {
            addCriterion("icon in", values, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotIn(List<String> values) {
            addCriterion("icon not in", values, "icon");
            return (Criteria) this;
        }

        public Criteria andIconBetween(String value1, String value2) {
            addCriterion("icon between", value1, value2, "icon");
            return (Criteria) this;
        }

        public Criteria andIconNotBetween(String value1, String value2) {
            addCriterion("icon not between", value1, value2, "icon");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Double value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Double value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Double value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Double value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Double value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Double value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Double> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Double> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Double value1, Double value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Double value1, Double value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andVoiceidsIsNull() {
            addCriterion("voiceids is null");
            return (Criteria) this;
        }

        public Criteria andVoiceidsIsNotNull() {
            addCriterion("voiceids is not null");
            return (Criteria) this;
        }

        public Criteria andVoiceidsEqualTo(String value) {
            addCriterion("voiceids =", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsNotEqualTo(String value) {
            addCriterion("voiceids <>", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsGreaterThan(String value) {
            addCriterion("voiceids >", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsGreaterThanOrEqualTo(String value) {
            addCriterion("voiceids >=", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsLessThan(String value) {
            addCriterion("voiceids <", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsLessThanOrEqualTo(String value) {
            addCriterion("voiceids <=", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsLike(String value) {
            addCriterion("voiceids like", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsNotLike(String value) {
            addCriterion("voiceids not like", value, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsIn(List<String> values) {
            addCriterion("voiceids in", values, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsNotIn(List<String> values) {
            addCriterion("voiceids not in", values, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsBetween(String value1, String value2) {
            addCriterion("voiceids between", value1, value2, "voiceids");
            return (Criteria) this;
        }

        public Criteria andVoiceidsNotBetween(String value1, String value2) {
            addCriterion("voiceids not between", value1, value2, "voiceids");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Double value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Double value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Double value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Double value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Double value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Double value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Double> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Double> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Double value1, Double value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Double value1, Double value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andCreateuserIsNull() {
            addCriterion("createuser is null");
            return (Criteria) this;
        }

        public Criteria andCreateuserIsNotNull() {
            addCriterion("createuser is not null");
            return (Criteria) this;
        }

        public Criteria andCreateuserEqualTo(Double value) {
            addCriterion("createuser =", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotEqualTo(Double value) {
            addCriterion("createuser <>", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserGreaterThan(Double value) {
            addCriterion("createuser >", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserGreaterThanOrEqualTo(Double value) {
            addCriterion("createuser >=", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserLessThan(Double value) {
            addCriterion("createuser <", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserLessThanOrEqualTo(Double value) {
            addCriterion("createuser <=", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserIn(List<Double> values) {
            addCriterion("createuser in", values, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotIn(List<Double> values) {
            addCriterion("createuser not in", values, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserBetween(Double value1, Double value2) {
            addCriterion("createuser between", value1, value2, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotBetween(Double value1, Double value2) {
            addCriterion("createuser not between", value1, value2, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}