package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PdProductsPortExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PdProductsPortExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPortidIsNull() {
            addCriterion("portid is null");
            return (Criteria) this;
        }

        public Criteria andPortidIsNotNull() {
            addCriterion("portid is not null");
            return (Criteria) this;
        }

        public Criteria andPortidEqualTo(Integer value) {
            addCriterion("portid =", value, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidNotEqualTo(Integer value) {
            addCriterion("portid <>", value, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidGreaterThan(Integer value) {
            addCriterion("portid >", value, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidGreaterThanOrEqualTo(Integer value) {
            addCriterion("portid >=", value, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidLessThan(Integer value) {
            addCriterion("portid <", value, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidLessThanOrEqualTo(Integer value) {
            addCriterion("portid <=", value, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidIn(List<Integer> values) {
            addCriterion("portid in", values, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidNotIn(List<Integer> values) {
            addCriterion("portid not in", values, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidBetween(Integer value1, Integer value2) {
            addCriterion("portid between", value1, value2, "portid");
            return (Criteria) this;
        }

        public Criteria andPortidNotBetween(Integer value1, Integer value2) {
            addCriterion("portid not between", value1, value2, "portid");
            return (Criteria) this;
        }

        public Criteria andProductsidIsNull() {
            addCriterion("productsid is null");
            return (Criteria) this;
        }

        public Criteria andProductsidIsNotNull() {
            addCriterion("productsid is not null");
            return (Criteria) this;
        }

        public Criteria andProductsidEqualTo(Integer value) {
            addCriterion("productsid =", value, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidNotEqualTo(Integer value) {
            addCriterion("productsid <>", value, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidGreaterThan(Integer value) {
            addCriterion("productsid >", value, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidGreaterThanOrEqualTo(Integer value) {
            addCriterion("productsid >=", value, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidLessThan(Integer value) {
            addCriterion("productsid <", value, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidLessThanOrEqualTo(Integer value) {
            addCriterion("productsid <=", value, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidIn(List<Integer> values) {
            addCriterion("productsid in", values, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidNotIn(List<Integer> values) {
            addCriterion("productsid not in", values, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidBetween(Integer value1, Integer value2) {
            addCriterion("productsid between", value1, value2, "productsid");
            return (Criteria) this;
        }

        public Criteria andProductsidNotBetween(Integer value1, Integer value2) {
            addCriterion("productsid not between", value1, value2, "productsid");
            return (Criteria) this;
        }

        public Criteria andStatusWhIsNull() {
            addCriterion("status_wh is null");
            return (Criteria) this;
        }

        public Criteria andStatusWhIsNotNull() {
            addCriterion("status_wh is not null");
            return (Criteria) this;
        }

        public Criteria andStatusWhEqualTo(Integer value) {
            addCriterion("status_wh =", value, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhNotEqualTo(Integer value) {
            addCriterion("status_wh <>", value, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhGreaterThan(Integer value) {
            addCriterion("status_wh >", value, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_wh >=", value, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhLessThan(Integer value) {
            addCriterion("status_wh <", value, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhLessThanOrEqualTo(Integer value) {
            addCriterion("status_wh <=", value, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhIn(List<Integer> values) {
            addCriterion("status_wh in", values, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhNotIn(List<Integer> values) {
            addCriterion("status_wh not in", values, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhBetween(Integer value1, Integer value2) {
            addCriterion("status_wh between", value1, value2, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusWhNotBetween(Integer value1, Integer value2) {
            addCriterion("status_wh not between", value1, value2, "statusWh");
            return (Criteria) this;
        }

        public Criteria andStatusCfIsNull() {
            addCriterion("status_cf is null");
            return (Criteria) this;
        }

        public Criteria andStatusCfIsNotNull() {
            addCriterion("status_cf is not null");
            return (Criteria) this;
        }

        public Criteria andStatusCfEqualTo(Integer value) {
            addCriterion("status_cf =", value, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfNotEqualTo(Integer value) {
            addCriterion("status_cf <>", value, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfGreaterThan(Integer value) {
            addCriterion("status_cf >", value, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_cf >=", value, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfLessThan(Integer value) {
            addCriterion("status_cf <", value, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfLessThanOrEqualTo(Integer value) {
            addCriterion("status_cf <=", value, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfIn(List<Integer> values) {
            addCriterion("status_cf in", values, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfNotIn(List<Integer> values) {
            addCriterion("status_cf not in", values, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfBetween(Integer value1, Integer value2) {
            addCriterion("status_cf between", value1, value2, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCfNotBetween(Integer value1, Integer value2) {
            addCriterion("status_cf not between", value1, value2, "statusCf");
            return (Criteria) this;
        }

        public Criteria andStatusCbIsNull() {
            addCriterion("status_cb is null");
            return (Criteria) this;
        }

        public Criteria andStatusCbIsNotNull() {
            addCriterion("status_cb is not null");
            return (Criteria) this;
        }

        public Criteria andStatusCbEqualTo(Integer value) {
            addCriterion("status_cb =", value, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbNotEqualTo(Integer value) {
            addCriterion("status_cb <>", value, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbGreaterThan(Integer value) {
            addCriterion("status_cb >", value, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_cb >=", value, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbLessThan(Integer value) {
            addCriterion("status_cb <", value, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbLessThanOrEqualTo(Integer value) {
            addCriterion("status_cb <=", value, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbIn(List<Integer> values) {
            addCriterion("status_cb in", values, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbNotIn(List<Integer> values) {
            addCriterion("status_cb not in", values, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbBetween(Integer value1, Integer value2) {
            addCriterion("status_cb between", value1, value2, "statusCb");
            return (Criteria) this;
        }

        public Criteria andStatusCbNotBetween(Integer value1, Integer value2) {
            addCriterion("status_cb not between", value1, value2, "statusCb");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartIsNull() {
            addCriterion("cdtime_start is null");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartIsNotNull() {
            addCriterion("cdtime_start is not null");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartEqualTo(Date value) {
            addCriterion("cdtime_start =", value, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartNotEqualTo(Date value) {
            addCriterion("cdtime_start <>", value, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartGreaterThan(Date value) {
            addCriterion("cdtime_start >", value, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartGreaterThanOrEqualTo(Date value) {
            addCriterion("cdtime_start >=", value, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartLessThan(Date value) {
            addCriterion("cdtime_start <", value, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartLessThanOrEqualTo(Date value) {
            addCriterion("cdtime_start <=", value, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartIn(List<Date> values) {
            addCriterion("cdtime_start in", values, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartNotIn(List<Date> values) {
            addCriterion("cdtime_start not in", values, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartBetween(Date value1, Date value2) {
            addCriterion("cdtime_start between", value1, value2, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeStartNotBetween(Date value1, Date value2) {
            addCriterion("cdtime_start not between", value1, value2, "cdtimeStart");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndIsNull() {
            addCriterion("cdtime_end is null");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndIsNotNull() {
            addCriterion("cdtime_end is not null");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndEqualTo(Date value) {
            addCriterion("cdtime_end =", value, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndNotEqualTo(Date value) {
            addCriterion("cdtime_end <>", value, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndGreaterThan(Date value) {
            addCriterion("cdtime_end >", value, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndGreaterThanOrEqualTo(Date value) {
            addCriterion("cdtime_end >=", value, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndLessThan(Date value) {
            addCriterion("cdtime_end <", value, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndLessThanOrEqualTo(Date value) {
            addCriterion("cdtime_end <=", value, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndIn(List<Date> values) {
            addCriterion("cdtime_end in", values, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndNotIn(List<Date> values) {
            addCriterion("cdtime_end not in", values, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndBetween(Date value1, Date value2) {
            addCriterion("cdtime_end between", value1, value2, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimeEndNotBetween(Date value1, Date value2) {
            addCriterion("cdtime_end not between", value1, value2, "cdtimeEnd");
            return (Criteria) this;
        }

        public Criteria andCdtimesIsNull() {
            addCriterion("cdtimes is null");
            return (Criteria) this;
        }

        public Criteria andCdtimesIsNotNull() {
            addCriterion("cdtimes is not null");
            return (Criteria) this;
        }

        public Criteria andCdtimesEqualTo(Integer value) {
            addCriterion("cdtimes =", value, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesNotEqualTo(Integer value) {
            addCriterion("cdtimes <>", value, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesGreaterThan(Integer value) {
            addCriterion("cdtimes >", value, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("cdtimes >=", value, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesLessThan(Integer value) {
            addCriterion("cdtimes <", value, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesLessThanOrEqualTo(Integer value) {
            addCriterion("cdtimes <=", value, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesIn(List<Integer> values) {
            addCriterion("cdtimes in", values, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesNotIn(List<Integer> values) {
            addCriterion("cdtimes not in", values, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesBetween(Integer value1, Integer value2) {
            addCriterion("cdtimes between", value1, value2, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCdtimesNotBetween(Integer value1, Integer value2) {
            addCriterion("cdtimes not between", value1, value2, "cdtimes");
            return (Criteria) this;
        }

        public Criteria andCreateuserIsNull() {
            addCriterion("createuser is null");
            return (Criteria) this;
        }

        public Criteria andCreateuserIsNotNull() {
            addCriterion("createuser is not null");
            return (Criteria) this;
        }

        public Criteria andCreateuserEqualTo(Integer value) {
            addCriterion("createuser =", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotEqualTo(Integer value) {
            addCriterion("createuser <>", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserGreaterThan(Integer value) {
            addCriterion("createuser >", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserGreaterThanOrEqualTo(Integer value) {
            addCriterion("createuser >=", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserLessThan(Integer value) {
            addCriterion("createuser <", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserLessThanOrEqualTo(Integer value) {
            addCriterion("createuser <=", value, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserIn(List<Integer> values) {
            addCriterion("createuser in", values, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotIn(List<Integer> values) {
            addCriterion("createuser not in", values, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserBetween(Integer value1, Integer value2) {
            addCriterion("createuser between", value1, value2, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreateuserNotBetween(Integer value1, Integer value2) {
            addCriterion("createuser not between", value1, value2, "createuser");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}