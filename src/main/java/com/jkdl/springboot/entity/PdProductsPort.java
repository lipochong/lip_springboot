package com.jkdl.springboot.entity;

import java.util.Date;

public class PdProductsPort {
    private Integer id;

    private Integer portid;

    private Integer productsid;

    private Integer statusWh;

    private Integer statusCf;

    private Integer statusCb;

    private Date cdtimeStart;

    private Date cdtimeEnd;

    private Integer cdtimes;

    private Integer createuser;

    private Date createtime;

    private Integer bdelete;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPortid() {
        return portid;
    }

    public void setPortid(Integer portid) {
        this.portid = portid;
    }

    public Integer getProductsid() {
        return productsid;
    }

    public void setProductsid(Integer productsid) {
        this.productsid = productsid;
    }

    public Integer getStatusWh() {
        return statusWh;
    }

    public void setStatusWh(Integer statusWh) {
        this.statusWh = statusWh;
    }

    public Integer getStatusCf() {
        return statusCf;
    }

    public void setStatusCf(Integer statusCf) {
        this.statusCf = statusCf;
    }

    public Integer getStatusCb() {
        return statusCb;
    }

    public void setStatusCb(Integer statusCb) {
        this.statusCb = statusCb;
    }

    public Date getCdtimeStart() {
        return cdtimeStart;
    }

    public void setCdtimeStart(Date cdtimeStart) {
        this.cdtimeStart = cdtimeStart;
    }

    public Date getCdtimeEnd() {
        return cdtimeEnd;
    }

    public void setCdtimeEnd(Date cdtimeEnd) {
        this.cdtimeEnd = cdtimeEnd;
    }

    public Integer getCdtimes() {
        return cdtimes;
    }

    public void setCdtimes(Integer cdtimes) {
        this.cdtimes = cdtimes;
    }

    public Integer getCreateuser() {
        return createuser;
    }

    public void setCreateuser(Integer createuser) {
        this.createuser = createuser;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }
}