package com.jkdl.springboot.entity;

import java.util.Date;

public class PdProducts {
    //设备数量
    private Integer machineCount;

    public Integer getMachineCount() {
        return machineCount;
    }

    public void setMachineCount(Integer machineCount) {
        this.machineCount = machineCount;
    }

    private Integer id;

    private Integer levelone;

    private Integer leveltwo;

    private Integer companyid;

    private String productname;

    private String style;

    private Integer measurement;

    private String specifications;

    private Double money;

    private String url;

    private String icon;

    private String banner;

    private Integer productactive;

    private Integer visitcount;

    private String detail;

    private String intruduction;

    private String attachment;

    private Integer isshowinhome;

    private Integer auditstatus;

    private Integer sort;

    private Integer bdelete;

    private Integer createuser;

    private Date createtime;

    private Date lasttime;

    private String jpg;

    private Double longitude;

    private Double latitude;

    private String qrcodepath;

    private String md5key;

    private String devtype;

    private Integer batchnumber;

    private String me5key;

    private Integer currentvoiceid;

    private Integer resetvoiceid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLevelone() {
        return levelone;
    }

    public void setLevelone(Integer levelone) {
        this.levelone = levelone;
    }

    public Integer getLeveltwo() {
        return leveltwo;
    }

    public void setLeveltwo(Integer leveltwo) {
        this.leveltwo = leveltwo;
    }

    public Integer getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Integer companyid) {
        this.companyid = companyid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Integer getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Integer measurement) {
        this.measurement = measurement;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public Integer getProductactive() {
        return productactive;
    }

    public void setProductactive(Integer productactive) {
        this.productactive = productactive;
    }

    public Integer getVisitcount() {
        return visitcount;
    }

    public void setVisitcount(Integer visitcount) {
        this.visitcount = visitcount;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getIntruduction() {
        return intruduction;
    }

    public void setIntruduction(String intruduction) {
        this.intruduction = intruduction;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Integer getIsshowinhome() {
        return isshowinhome;
    }

    public void setIsshowinhome(Integer isshowinhome) {
        this.isshowinhome = isshowinhome;
    }

    public Integer getAuditstatus() {
        return auditstatus;
    }

    public void setAuditstatus(Integer auditstatus) {
        this.auditstatus = auditstatus;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }

    public Integer getCreateuser() {
        return createuser;
    }

    public void setCreateuser(Integer createuser) {
        this.createuser = createuser;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getLasttime() {
        return lasttime;
    }

    public void setLasttime(Date lasttime) {
        this.lasttime = lasttime;
    }

    public String getJpg() {
        return jpg;
    }

    public void setJpg(String jpg) {
        this.jpg = jpg;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getQrcodepath() {
        return qrcodepath;
    }

    public void setQrcodepath(String qrcodepath) {
        this.qrcodepath = qrcodepath;
    }

    public String getMd5key() {
        return md5key;
    }

    public void setMd5key(String md5key) {
        this.md5key = md5key;
    }

    public String getDevtype() {
        return devtype;
    }

    public void setDevtype(String devtype) {
        this.devtype = devtype;
    }

    public Integer getBatchnumber() {
        return batchnumber;
    }

    public void setBatchnumber(Integer batchnumber) {
        this.batchnumber = batchnumber;
    }

    public String getMe5key() {
        return me5key;
    }

    public void setMe5key(String me5key) {
        this.me5key = me5key;
    }

    public Integer getCurrentvoiceid() {
        return currentvoiceid;
    }

    public void setCurrentvoiceid(Integer currentvoiceid) {
        this.currentvoiceid = currentvoiceid;
    }

    public Integer getResetvoiceid() {
        return resetvoiceid;
    }

    public void setResetvoiceid(Integer resetvoiceid) {
        this.resetvoiceid = resetvoiceid;
    }
}