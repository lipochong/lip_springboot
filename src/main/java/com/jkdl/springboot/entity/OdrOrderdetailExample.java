package com.jkdl.springboot.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OdrOrderdetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OdrOrderdetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrderidIsNull() {
            addCriterion("orderid is null");
            return (Criteria) this;
        }

        public Criteria andOrderidIsNotNull() {
            addCriterion("orderid is not null");
            return (Criteria) this;
        }

        public Criteria andOrderidEqualTo(String value) {
            addCriterion("orderid =", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotEqualTo(String value) {
            addCriterion("orderid <>", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidGreaterThan(String value) {
            addCriterion("orderid >", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidGreaterThanOrEqualTo(String value) {
            addCriterion("orderid >=", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidLessThan(String value) {
            addCriterion("orderid <", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidLessThanOrEqualTo(String value) {
            addCriterion("orderid <=", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidLike(String value) {
            addCriterion("orderid like", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotLike(String value) {
            addCriterion("orderid not like", value, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidIn(List<String> values) {
            addCriterion("orderid in", values, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotIn(List<String> values) {
            addCriterion("orderid not in", values, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidBetween(String value1, String value2) {
            addCriterion("orderid between", value1, value2, "orderid");
            return (Criteria) this;
        }

        public Criteria andOrderidNotBetween(String value1, String value2) {
            addCriterion("orderid not between", value1, value2, "orderid");
            return (Criteria) this;
        }

        public Criteria andProductidIsNull() {
            addCriterion("productid is null");
            return (Criteria) this;
        }

        public Criteria andProductidIsNotNull() {
            addCriterion("productid is not null");
            return (Criteria) this;
        }

        public Criteria andProductidEqualTo(Integer value) {
            addCriterion("productid =", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidNotEqualTo(Integer value) {
            addCriterion("productid <>", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidGreaterThan(Integer value) {
            addCriterion("productid >", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidGreaterThanOrEqualTo(Integer value) {
            addCriterion("productid >=", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidLessThan(Integer value) {
            addCriterion("productid <", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidLessThanOrEqualTo(Integer value) {
            addCriterion("productid <=", value, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidIn(List<Integer> values) {
            addCriterion("productid in", values, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidNotIn(List<Integer> values) {
            addCriterion("productid not in", values, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidBetween(Integer value1, Integer value2) {
            addCriterion("productid between", value1, value2, "productid");
            return (Criteria) this;
        }

        public Criteria andProductidNotBetween(Integer value1, Integer value2) {
            addCriterion("productid not between", value1, value2, "productid");
            return (Criteria) this;
        }

        public Criteria andProductportidIsNull() {
            addCriterion("productportid is null");
            return (Criteria) this;
        }

        public Criteria andProductportidIsNotNull() {
            addCriterion("productportid is not null");
            return (Criteria) this;
        }

        public Criteria andProductportidEqualTo(Integer value) {
            addCriterion("productportid =", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidNotEqualTo(Integer value) {
            addCriterion("productportid <>", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidGreaterThan(Integer value) {
            addCriterion("productportid >", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidGreaterThanOrEqualTo(Integer value) {
            addCriterion("productportid >=", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidLessThan(Integer value) {
            addCriterion("productportid <", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidLessThanOrEqualTo(Integer value) {
            addCriterion("productportid <=", value, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidIn(List<Integer> values) {
            addCriterion("productportid in", values, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidNotIn(List<Integer> values) {
            addCriterion("productportid not in", values, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidBetween(Integer value1, Integer value2) {
            addCriterion("productportid between", value1, value2, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductportidNotBetween(Integer value1, Integer value2) {
            addCriterion("productportid not between", value1, value2, "productportid");
            return (Criteria) this;
        }

        public Criteria andProductdetailIsNull() {
            addCriterion("productdetail is null");
            return (Criteria) this;
        }

        public Criteria andProductdetailIsNotNull() {
            addCriterion("productdetail is not null");
            return (Criteria) this;
        }

        public Criteria andProductdetailEqualTo(String value) {
            addCriterion("productdetail =", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailNotEqualTo(String value) {
            addCriterion("productdetail <>", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailGreaterThan(String value) {
            addCriterion("productdetail >", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailGreaterThanOrEqualTo(String value) {
            addCriterion("productdetail >=", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailLessThan(String value) {
            addCriterion("productdetail <", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailLessThanOrEqualTo(String value) {
            addCriterion("productdetail <=", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailLike(String value) {
            addCriterion("productdetail like", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailNotLike(String value) {
            addCriterion("productdetail not like", value, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailIn(List<String> values) {
            addCriterion("productdetail in", values, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailNotIn(List<String> values) {
            addCriterion("productdetail not in", values, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailBetween(String value1, String value2) {
            addCriterion("productdetail between", value1, value2, "productdetail");
            return (Criteria) this;
        }

        public Criteria andProductdetailNotBetween(String value1, String value2) {
            addCriterion("productdetail not between", value1, value2, "productdetail");
            return (Criteria) this;
        }

        public Criteria andSpecidIsNull() {
            addCriterion("specid is null");
            return (Criteria) this;
        }

        public Criteria andSpecidIsNotNull() {
            addCriterion("specid is not null");
            return (Criteria) this;
        }

        public Criteria andSpecidEqualTo(Integer value) {
            addCriterion("specid =", value, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidNotEqualTo(Integer value) {
            addCriterion("specid <>", value, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidGreaterThan(Integer value) {
            addCriterion("specid >", value, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidGreaterThanOrEqualTo(Integer value) {
            addCriterion("specid >=", value, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidLessThan(Integer value) {
            addCriterion("specid <", value, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidLessThanOrEqualTo(Integer value) {
            addCriterion("specid <=", value, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidIn(List<Integer> values) {
            addCriterion("specid in", values, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidNotIn(List<Integer> values) {
            addCriterion("specid not in", values, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidBetween(Integer value1, Integer value2) {
            addCriterion("specid between", value1, value2, "specid");
            return (Criteria) this;
        }

        public Criteria andSpecidNotBetween(Integer value1, Integer value2) {
            addCriterion("specid not between", value1, value2, "specid");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Long value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Long value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Long value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Long value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Long value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Long value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Long> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Long> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Long value1, Long value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Long value1, Long value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andNumberIsNull() {
            addCriterion("number is null");
            return (Criteria) this;
        }

        public Criteria andNumberIsNotNull() {
            addCriterion("number is not null");
            return (Criteria) this;
        }

        public Criteria andNumberEqualTo(Integer value) {
            addCriterion("number =", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberNotEqualTo(Integer value) {
            addCriterion("number <>", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberGreaterThan(Integer value) {
            addCriterion("number >", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("number >=", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberLessThan(Integer value) {
            addCriterion("number <", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberLessThanOrEqualTo(Integer value) {
            addCriterion("number <=", value, "number");
            return (Criteria) this;
        }

        public Criteria andNumberIn(List<Integer> values) {
            addCriterion("number in", values, "number");
            return (Criteria) this;
        }

        public Criteria andNumberNotIn(List<Integer> values) {
            addCriterion("number not in", values, "number");
            return (Criteria) this;
        }

        public Criteria andNumberBetween(Integer value1, Integer value2) {
            addCriterion("number between", value1, value2, "number");
            return (Criteria) this;
        }

        public Criteria andNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("number not between", value1, value2, "number");
            return (Criteria) this;
        }

        public Criteria andProductnameIsNull() {
            addCriterion("productname is null");
            return (Criteria) this;
        }

        public Criteria andProductnameIsNotNull() {
            addCriterion("productname is not null");
            return (Criteria) this;
        }

        public Criteria andProductnameEqualTo(String value) {
            addCriterion("productname =", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotEqualTo(String value) {
            addCriterion("productname <>", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameGreaterThan(String value) {
            addCriterion("productname >", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameGreaterThanOrEqualTo(String value) {
            addCriterion("productname >=", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLessThan(String value) {
            addCriterion("productname <", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLessThanOrEqualTo(String value) {
            addCriterion("productname <=", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLike(String value) {
            addCriterion("productname like", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotLike(String value) {
            addCriterion("productname not like", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameIn(List<String> values) {
            addCriterion("productname in", values, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotIn(List<String> values) {
            addCriterion("productname not in", values, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameBetween(String value1, String value2) {
            addCriterion("productname between", value1, value2, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotBetween(String value1, String value2) {
            addCriterion("productname not between", value1, value2, "productname");
            return (Criteria) this;
        }

        public Criteria andFeeIsNull() {
            addCriterion("fee is null");
            return (Criteria) this;
        }

        public Criteria andFeeIsNotNull() {
            addCriterion("fee is not null");
            return (Criteria) this;
        }

        public Criteria andFeeEqualTo(BigDecimal value) {
            addCriterion("fee =", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotEqualTo(BigDecimal value) {
            addCriterion("fee <>", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThan(BigDecimal value) {
            addCriterion("fee >", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fee >=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThan(BigDecimal value) {
            addCriterion("fee <", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fee <=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeIn(List<BigDecimal> values) {
            addCriterion("fee in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotIn(List<BigDecimal> values) {
            addCriterion("fee not in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee not between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andTotal0IsNull() {
            addCriterion("total0 is null");
            return (Criteria) this;
        }

        public Criteria andTotal0IsNotNull() {
            addCriterion("total0 is not null");
            return (Criteria) this;
        }

        public Criteria andTotal0EqualTo(BigDecimal value) {
            addCriterion("total0 =", value, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0NotEqualTo(BigDecimal value) {
            addCriterion("total0 <>", value, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0GreaterThan(BigDecimal value) {
            addCriterion("total0 >", value, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total0 >=", value, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0LessThan(BigDecimal value) {
            addCriterion("total0 <", value, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0LessThanOrEqualTo(BigDecimal value) {
            addCriterion("total0 <=", value, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0In(List<BigDecimal> values) {
            addCriterion("total0 in", values, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0NotIn(List<BigDecimal> values) {
            addCriterion("total0 not in", values, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("total0 between", value1, value2, "total0");
            return (Criteria) this;
        }

        public Criteria andTotal0NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total0 not between", value1, value2, "total0");
            return (Criteria) this;
        }

        public Criteria andIscommentIsNull() {
            addCriterion("iscomment is null");
            return (Criteria) this;
        }

        public Criteria andIscommentIsNotNull() {
            addCriterion("iscomment is not null");
            return (Criteria) this;
        }

        public Criteria andIscommentEqualTo(Integer value) {
            addCriterion("iscomment =", value, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentNotEqualTo(Integer value) {
            addCriterion("iscomment <>", value, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentGreaterThan(Integer value) {
            addCriterion("iscomment >", value, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentGreaterThanOrEqualTo(Integer value) {
            addCriterion("iscomment >=", value, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentLessThan(Integer value) {
            addCriterion("iscomment <", value, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentLessThanOrEqualTo(Integer value) {
            addCriterion("iscomment <=", value, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentIn(List<Integer> values) {
            addCriterion("iscomment in", values, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentNotIn(List<Integer> values) {
            addCriterion("iscomment not in", values, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentBetween(Integer value1, Integer value2) {
            addCriterion("iscomment between", value1, value2, "iscomment");
            return (Criteria) this;
        }

        public Criteria andIscommentNotBetween(Integer value1, Integer value2) {
            addCriterion("iscomment not between", value1, value2, "iscomment");
            return (Criteria) this;
        }

        public Criteria andLowstocksIsNull() {
            addCriterion("lowstocks is null");
            return (Criteria) this;
        }

        public Criteria andLowstocksIsNotNull() {
            addCriterion("lowstocks is not null");
            return (Criteria) this;
        }

        public Criteria andLowstocksEqualTo(String value) {
            addCriterion("lowstocks =", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotEqualTo(String value) {
            addCriterion("lowstocks <>", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksGreaterThan(String value) {
            addCriterion("lowstocks >", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksGreaterThanOrEqualTo(String value) {
            addCriterion("lowstocks >=", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksLessThan(String value) {
            addCriterion("lowstocks <", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksLessThanOrEqualTo(String value) {
            addCriterion("lowstocks <=", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksLike(String value) {
            addCriterion("lowstocks like", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotLike(String value) {
            addCriterion("lowstocks not like", value, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksIn(List<String> values) {
            addCriterion("lowstocks in", values, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotIn(List<String> values) {
            addCriterion("lowstocks not in", values, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksBetween(String value1, String value2) {
            addCriterion("lowstocks between", value1, value2, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andLowstocksNotBetween(String value1, String value2) {
            addCriterion("lowstocks not between", value1, value2, "lowstocks");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Integer value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Integer value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Integer value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Integer value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Integer value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Integer> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Integer> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Integer value1, Integer value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andSpecinfoIsNull() {
            addCriterion("specinfo is null");
            return (Criteria) this;
        }

        public Criteria andSpecinfoIsNotNull() {
            addCriterion("specinfo is not null");
            return (Criteria) this;
        }

        public Criteria andSpecinfoEqualTo(String value) {
            addCriterion("specinfo =", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoNotEqualTo(String value) {
            addCriterion("specinfo <>", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoGreaterThan(String value) {
            addCriterion("specinfo >", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoGreaterThanOrEqualTo(String value) {
            addCriterion("specinfo >=", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoLessThan(String value) {
            addCriterion("specinfo <", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoLessThanOrEqualTo(String value) {
            addCriterion("specinfo <=", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoLike(String value) {
            addCriterion("specinfo like", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoNotLike(String value) {
            addCriterion("specinfo not like", value, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoIn(List<String> values) {
            addCriterion("specinfo in", values, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoNotIn(List<String> values) {
            addCriterion("specinfo not in", values, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoBetween(String value1, String value2) {
            addCriterion("specinfo between", value1, value2, "specinfo");
            return (Criteria) this;
        }

        public Criteria andSpecinfoNotBetween(String value1, String value2) {
            addCriterion("specinfo not between", value1, value2, "specinfo");
            return (Criteria) this;
        }

        public Criteria andGiftidIsNull() {
            addCriterion("giftid is null");
            return (Criteria) this;
        }

        public Criteria andGiftidIsNotNull() {
            addCriterion("giftid is not null");
            return (Criteria) this;
        }

        public Criteria andGiftidEqualTo(String value) {
            addCriterion("giftid =", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidNotEqualTo(String value) {
            addCriterion("giftid <>", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidGreaterThan(String value) {
            addCriterion("giftid >", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidGreaterThanOrEqualTo(String value) {
            addCriterion("giftid >=", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidLessThan(String value) {
            addCriterion("giftid <", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidLessThanOrEqualTo(String value) {
            addCriterion("giftid <=", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidLike(String value) {
            addCriterion("giftid like", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidNotLike(String value) {
            addCriterion("giftid not like", value, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidIn(List<String> values) {
            addCriterion("giftid in", values, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidNotIn(List<String> values) {
            addCriterion("giftid not in", values, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidBetween(String value1, String value2) {
            addCriterion("giftid between", value1, value2, "giftid");
            return (Criteria) this;
        }

        public Criteria andGiftidNotBetween(String value1, String value2) {
            addCriterion("giftid not between", value1, value2, "giftid");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andProimgIsNull() {
            addCriterion("proimg is null");
            return (Criteria) this;
        }

        public Criteria andProimgIsNotNull() {
            addCriterion("proimg is not null");
            return (Criteria) this;
        }

        public Criteria andProimgEqualTo(String value) {
            addCriterion("proimg =", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgNotEqualTo(String value) {
            addCriterion("proimg <>", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgGreaterThan(String value) {
            addCriterion("proimg >", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgGreaterThanOrEqualTo(String value) {
            addCriterion("proimg >=", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgLessThan(String value) {
            addCriterion("proimg <", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgLessThanOrEqualTo(String value) {
            addCriterion("proimg <=", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgLike(String value) {
            addCriterion("proimg like", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgNotLike(String value) {
            addCriterion("proimg not like", value, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgIn(List<String> values) {
            addCriterion("proimg in", values, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgNotIn(List<String> values) {
            addCriterion("proimg not in", values, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgBetween(String value1, String value2) {
            addCriterion("proimg between", value1, value2, "proimg");
            return (Criteria) this;
        }

        public Criteria andProimgNotBetween(String value1, String value2) {
            addCriterion("proimg not between", value1, value2, "proimg");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusIsNull() {
            addCriterion("catalogstatus is null");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusIsNotNull() {
            addCriterion("catalogstatus is not null");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusEqualTo(String value) {
            addCriterion("catalogstatus =", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusNotEqualTo(String value) {
            addCriterion("catalogstatus <>", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusGreaterThan(String value) {
            addCriterion("catalogstatus >", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusGreaterThanOrEqualTo(String value) {
            addCriterion("catalogstatus >=", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusLessThan(String value) {
            addCriterion("catalogstatus <", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusLessThanOrEqualTo(String value) {
            addCriterion("catalogstatus <=", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusLike(String value) {
            addCriterion("catalogstatus like", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusNotLike(String value) {
            addCriterion("catalogstatus not like", value, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusIn(List<String> values) {
            addCriterion("catalogstatus in", values, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusNotIn(List<String> values) {
            addCriterion("catalogstatus not in", values, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusBetween(String value1, String value2) {
            addCriterion("catalogstatus between", value1, value2, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andCatalogstatusNotBetween(String value1, String value2) {
            addCriterion("catalogstatus not between", value1, value2, "catalogstatus");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNull() {
            addCriterion("bdelete is null");
            return (Criteria) this;
        }

        public Criteria andBdeleteIsNotNull() {
            addCriterion("bdelete is not null");
            return (Criteria) this;
        }

        public Criteria andBdeleteEqualTo(Integer value) {
            addCriterion("bdelete =", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotEqualTo(Integer value) {
            addCriterion("bdelete <>", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThan(Integer value) {
            addCriterion("bdelete >", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("bdelete >=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThan(Integer value) {
            addCriterion("bdelete <", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("bdelete <=", value, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteIn(List<Integer> values) {
            addCriterion("bdelete in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotIn(List<Integer> values) {
            addCriterion("bdelete not in", values, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteBetween(Integer value1, Integer value2) {
            addCriterion("bdelete between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andBdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("bdelete not between", value1, value2, "bdelete");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeIsNull() {
            addCriterion("cdstarttime is null");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeIsNotNull() {
            addCriterion("cdstarttime is not null");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeEqualTo(Date value) {
            addCriterion("cdstarttime =", value, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeNotEqualTo(Date value) {
            addCriterion("cdstarttime <>", value, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeGreaterThan(Date value) {
            addCriterion("cdstarttime >", value, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cdstarttime >=", value, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeLessThan(Date value) {
            addCriterion("cdstarttime <", value, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeLessThanOrEqualTo(Date value) {
            addCriterion("cdstarttime <=", value, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeIn(List<Date> values) {
            addCriterion("cdstarttime in", values, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeNotIn(List<Date> values) {
            addCriterion("cdstarttime not in", values, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeBetween(Date value1, Date value2) {
            addCriterion("cdstarttime between", value1, value2, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdstarttimeNotBetween(Date value1, Date value2) {
            addCriterion("cdstarttime not between", value1, value2, "cdstarttime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeIsNull() {
            addCriterion("cdendtime is null");
            return (Criteria) this;
        }

        public Criteria andCdendtimeIsNotNull() {
            addCriterion("cdendtime is not null");
            return (Criteria) this;
        }

        public Criteria andCdendtimeEqualTo(Date value) {
            addCriterion("cdendtime =", value, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeNotEqualTo(Date value) {
            addCriterion("cdendtime <>", value, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeGreaterThan(Date value) {
            addCriterion("cdendtime >", value, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cdendtime >=", value, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeLessThan(Date value) {
            addCriterion("cdendtime <", value, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeLessThanOrEqualTo(Date value) {
            addCriterion("cdendtime <=", value, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeIn(List<Date> values) {
            addCriterion("cdendtime in", values, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeNotIn(List<Date> values) {
            addCriterion("cdendtime not in", values, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeBetween(Date value1, Date value2) {
            addCriterion("cdendtime between", value1, value2, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andCdendtimeNotBetween(Date value1, Date value2) {
            addCriterion("cdendtime not between", value1, value2, "cdendtime");
            return (Criteria) this;
        }

        public Criteria andOrdstatusIsNull() {
            addCriterion("ordstatus is null");
            return (Criteria) this;
        }

        public Criteria andOrdstatusIsNotNull() {
            addCriterion("ordstatus is not null");
            return (Criteria) this;
        }

        public Criteria andOrdstatusEqualTo(Integer value) {
            addCriterion("ordstatus =", value, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusNotEqualTo(Integer value) {
            addCriterion("ordstatus <>", value, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusGreaterThan(Integer value) {
            addCriterion("ordstatus >", value, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("ordstatus >=", value, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusLessThan(Integer value) {
            addCriterion("ordstatus <", value, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusLessThanOrEqualTo(Integer value) {
            addCriterion("ordstatus <=", value, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusIn(List<Integer> values) {
            addCriterion("ordstatus in", values, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusNotIn(List<Integer> values) {
            addCriterion("ordstatus not in", values, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusBetween(Integer value1, Integer value2) {
            addCriterion("ordstatus between", value1, value2, "ordstatus");
            return (Criteria) this;
        }

        public Criteria andOrdstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("ordstatus not between", value1, value2, "ordstatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}