package com.jkdl.springboot.entity;

public class PdProductsVoice {
    private Integer id;

    private String synthsrc;

    private String ultrasonicname;

    private Integer bdelete;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSynthsrc() {
        return synthsrc;
    }

    public void setSynthsrc(String synthsrc) {
        this.synthsrc = synthsrc;
    }

    public String getUltrasonicname() {
        return ultrasonicname;
    }

    public void setUltrasonicname(String ultrasonicname) {
        this.ultrasonicname = ultrasonicname;
    }

    public Integer getBdelete() {
        return bdelete;
    }

    public void setBdelete(Integer bdelete) {
        this.bdelete = bdelete;
    }
}