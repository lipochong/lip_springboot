package com.jkdl.springboot.entity;

import java.math.BigDecimal;
import java.util.Date;

public class MCardUserRecord {
    private Integer id;

    private Integer mcUserId;

    private Integer companyId;

    private Integer pyNo;

    private BigDecimal money;

    private Integer status;

    private String remarks;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMcUserId() {
        return mcUserId;
    }

    public void setMcUserId(Integer mcUserId) {
        this.mcUserId = mcUserId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getPyNo() {
        return pyNo;
    }

    public void setPyNo(Integer pyNo) {
        this.pyNo = pyNo;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}