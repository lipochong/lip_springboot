package com.jkdl.springboot.entity;

public class PdProductsWithBLOBs extends PdProducts {
    private String productbrief;

    private String productstyle;

    private String declaration;

    private String technical;


    private String companyname;

    public String getProductbrief() {
        return productbrief;
    }

    public void setProductbrief(String productbrief) {
        this.productbrief = productbrief;
    }

    public String getProductstyle() {
        return productstyle;
    }

    public void setProductstyle(String productstyle) {
        this.productstyle = productstyle;
    }

    public String getDeclaration() {
        return declaration;
    }

    public void setDeclaration(String declaration) {
        this.declaration = declaration;
    }

    public String getTechnical() {
        return technical;
    }

    public void setTechnical(String technical) {
        this.technical = technical;
    }



    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }
}