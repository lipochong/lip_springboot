package com.jkdl.springboot.entity;

import java.util.ArrayList;
import java.util.List;

public class PdProductsBatchVoiceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PdProductsBatchVoiceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBacthidIsNull() {
            addCriterion("bacthid is null");
            return (Criteria) this;
        }

        public Criteria andBacthidIsNotNull() {
            addCriterion("bacthid is not null");
            return (Criteria) this;
        }

        public Criteria andBacthidEqualTo(Integer value) {
            addCriterion("bacthid =", value, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidNotEqualTo(Integer value) {
            addCriterion("bacthid <>", value, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidGreaterThan(Integer value) {
            addCriterion("bacthid >", value, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidGreaterThanOrEqualTo(Integer value) {
            addCriterion("bacthid >=", value, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidLessThan(Integer value) {
            addCriterion("bacthid <", value, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidLessThanOrEqualTo(Integer value) {
            addCriterion("bacthid <=", value, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidIn(List<Integer> values) {
            addCriterion("bacthid in", values, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidNotIn(List<Integer> values) {
            addCriterion("bacthid not in", values, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidBetween(Integer value1, Integer value2) {
            addCriterion("bacthid between", value1, value2, "bacthid");
            return (Criteria) this;
        }

        public Criteria andBacthidNotBetween(Integer value1, Integer value2) {
            addCriterion("bacthid not between", value1, value2, "bacthid");
            return (Criteria) this;
        }

        public Criteria andSynthidIsNull() {
            addCriterion("synthid is null");
            return (Criteria) this;
        }

        public Criteria andSynthidIsNotNull() {
            addCriterion("synthid is not null");
            return (Criteria) this;
        }

        public Criteria andSynthidEqualTo(Integer value) {
            addCriterion("synthid =", value, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidNotEqualTo(Integer value) {
            addCriterion("synthid <>", value, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidGreaterThan(Integer value) {
            addCriterion("synthid >", value, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidGreaterThanOrEqualTo(Integer value) {
            addCriterion("synthid >=", value, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidLessThan(Integer value) {
            addCriterion("synthid <", value, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidLessThanOrEqualTo(Integer value) {
            addCriterion("synthid <=", value, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidIn(List<Integer> values) {
            addCriterion("synthid in", values, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidNotIn(List<Integer> values) {
            addCriterion("synthid not in", values, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidBetween(Integer value1, Integer value2) {
            addCriterion("synthid between", value1, value2, "synthid");
            return (Criteria) this;
        }

        public Criteria andSynthidNotBetween(Integer value1, Integer value2) {
            addCriterion("synthid not between", value1, value2, "synthid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}