package com.jkdl.springboot.entity;

public class PdProductsBatchVoiceWithBLOBs extends PdProductsBatchVoiceKey{
    private String  synthsrc;

    private String producttypename;

    public String getSynthsrc() {
        return synthsrc;
    }

    public void setSynthsrc(String synthsrc) {
        this.synthsrc = synthsrc;
    }

    public String getProducttypename() {
        return producttypename;
    }

    public void setProducttypename(String producttypename) {
        this.producttypename = producttypename;
    }
}