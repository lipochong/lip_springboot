package com.jkdl.springboot.entity;

import java.util.List;

public class OrderReportdatailExample {
    //二元使用率
    private double two;
    //三元使用率
    private double three;
    //罗湖区大厅使用率
    private double luohuhall;
    //罗湖区房间使用率
    private double luohuRoom;
    //设备总数量
    private Integer equipmentNumber;
    //总订单金额
    private double totalMoney;
    //  商户名称
    private String companyname;
    //使用设备数
    private  Integer usageQuantity;
    //设备使用率
    public double utilizationRate;

    public double getTwo() {
        return two;
    }

    public void setTwo(double two) {
        this.two = two;
    }

    public double getThree() {
        return three;
    }

    public void setThree(double three) {
        this.three = three;
    }

    public double getLuohuhall() {
        return luohuhall;
    }

    public void setLuohuhall(double luohuhall) {
        this.luohuhall = luohuhall;
    }

    public double getLuohuRoom() {
        return luohuRoom;
    }

    public void setLuohuRoom(double luohuRoom) {
        this.luohuRoom = luohuRoom;
    }

    public double getUtilizationRate() {
        return utilizationRate;
    }

    public List<OdrOrderGroup> getOdrOrderGroupList() {
        return odrOrderGroupList;
    }

    public void setOdrOrderGroupList(List<OdrOrderGroup> odrOrderGroupList) {
        this.odrOrderGroupList = odrOrderGroupList;
    }

    public void setUtilizationRate(double utilizationRate) {
        this.utilizationRate = utilizationRate;
    }

    //报表数据
    public List<OdrOrderGroup> odrOrderGroupList;

    public Integer getEquipmentNumber() {
        return equipmentNumber;
    }

    public void setEquipmentNumber(Integer equipmentNumber) {
        this.equipmentNumber = equipmentNumber;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public Integer getUsageQuantity() {
        return usageQuantity;
    }

    public void setUsageQuantity(Integer usageQuantity) {
        this.usageQuantity = usageQuantity;
    }





}
