package com.jkdl.springboot.exception;


public class BusinessException extends RuntimeException {
    /**
     * 异常编码
     */
    private String code;
    
    public BusinessException(String msg){
        super(msg);
        this.code = "-1";
    }

    public BusinessException(String code, String message){
        super(message);
        this.code = code;
    }

    public BusinessException(String code, Throwable throwable){
        super(throwable);
        this.code = code;
    }

    public BusinessException(String code, String message, Throwable throwable){
        super(message, throwable);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
