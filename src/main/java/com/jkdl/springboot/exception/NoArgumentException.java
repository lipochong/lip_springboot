package com.jkdl.springboot.exception;

public class NoArgumentException extends IllegalArgumentException {

    /**
     * 异常编码
     */
    private String code;

    public String getcode() {
        return code;
    }

    public void setcode(String code) {
        this.code = code;
    }

    public NoArgumentException(String msg){
        super(msg);
        this.code = "-1";
    }

    public NoArgumentException(String code, String message){
        super(message);
        this.code = code;
    }

    public NoArgumentException(String code, Throwable throwable){
        super(throwable);
        this.code = code;
    }

    public NoArgumentException(String code, String message, Throwable throwable){
        super(message, throwable);
        this.code = code;
    }
}
