package com.jkdl.springboot.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface BusinessActivityImgService {

    Map<String, Object> uploadImg(MultipartFile file, HttpServletRequest request);

}
