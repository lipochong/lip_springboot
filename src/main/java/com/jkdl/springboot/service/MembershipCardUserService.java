package com.jkdl.springboot.service;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.entity.MembershipCardUser;

public interface MembershipCardUserService {
    /**
     * 用户办理会员卡
     * @param membershipCardUser
     * @return
     */
    JsonResult addMembershipCardUser(MembershipCardUser membershipCardUser);

}
