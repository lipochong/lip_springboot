package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.BusinessActivityMember;

import java.util.List;

public interface BusinessActivityMemberService {

    JsonResult joinActivity(BusinessActivityAddVo businessActivityAddVo);

    BusinessActivityMemberVo activityMemberList(BusinessActivityMemberVo businessActivityMemberVo);

    /**
     * 获取活动列表分页
     */
    PageInfo<BusinessActivityMember> selectBusinessActivityMemberList(BusinessActivityMemberListVo businessActivityMemberListVo);

    BusinessActivityMember getActivityMemberDetails(BusinessActivityMemberVo businessActivityMemberVo);

    JsonResult useActivityMember(BusinessActivityMemberVo businessActivityMemberVo);

    List<BusinessActivityMember> getActivityMemberList(BusinessActivityMemberVo businessActivityMemberVo);

    /**
     * 根据活动Id查出业绩排行榜
     * @param id
     * @return
     */
    BusinessActivityMemberLeaderboardListVo getActivityMemberId(Integer id);

    /**
     * 根据qyId查核销记录
     * @param id
     * @return
     */
    List<BusinessActivityMemberWriteoffVo> getActivityMemberWriteOff(Integer id);

    /**
     * 根据qyId查核销记录
     * @param bAMerberPageVo
     * @return
     */
    PageInfo<BusinessActivityMemberWriteoffVo> getActivityMemberWriteOff(BAMerberPageVo bAMerberPageVo);

    /**
     * 分享好友领取
     * @param businessActivityMemberShareVo
     * @return
     */
    JsonResult joinActivityShareUser(BusinessActivityMemberShareVo businessActivityMemberShareVo);

    /**
     * 分享好友查看是否已领取
     * @param businessActivityMemberVo
     * @return
     */
    BusinessActivityMember getQueryActivityMemberDetails(BusinessActivityMemberVo businessActivityMemberVo);

}