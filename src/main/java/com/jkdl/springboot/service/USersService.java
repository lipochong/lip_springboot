package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.domain.UsersAddVo;
import com.jkdl.springboot.domain.UsersDeleteVo;
import com.jkdl.springboot.domain.UsersQueryVo;
import com.jkdl.springboot.domain.UsersUpdateVo;
import com.jkdl.springboot.entity.USers;

import java.util.List;

public interface USersService {

    /**
     * 添加用户信息
     */
    Long add(USers uSers) throws Exception;

    /**
     * 获取用户列表
     */
    List<USers> list(UsersQueryVo usersQueryVo);

    /**
     * 获取用户列表分页
     */
    PageInfo<USers> selectUSersList(UsersQueryVo usersQueryVo);
    /**
     * 新增用户
     */
    int addUsers(UsersAddVo usersAddVo);
    /**
     * 修改用户
     */
    int updateUsers(UsersUpdateVo usersUpdateVo);

    /**
     * 删除用户
     */
    int deleteUsers(UsersDeleteVo usersDeleteVo);
}
