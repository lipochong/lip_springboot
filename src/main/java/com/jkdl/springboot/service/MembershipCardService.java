package com.jkdl.springboot.service;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.membershipcard.MembershipCardLstVo;
import com.jkdl.springboot.domain.membershipcard.MembershipCardVo;
import com.jkdl.springboot.entity.MembershipCard;

public interface MembershipCardService {
    /**
     * 添加会员卡
     * @param membershipCard
     * @return
     */
    JsonResult addMembershipCard(MembershipCard membershipCard);

    /**
     * 获取发布会员卡
     * @param membershipCardVo
     * @return
     */
    MembershipCardLstVo getMembershipCardLst(MembershipCardVo membershipCardVo);

}
