package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.BusinessActivity;
import com.jkdl.springboot.entity.UCompany;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface BusinessActivityService {

    Map<String, Object> uploadImg(MultipartFile file, HttpServletRequest request, String uploadDir);

    /**
     * 添加活动
     * @param businessActivityAddVo
     * @return
     */
    JsonResult addActivity(BusinessActivityAddVo businessActivityAddVo);

    /**
     * 小程序活动列表
     * @param businessActivityAddVo
     * @return
     */
    List<BusinessActivity> activityList(BusinessActivityAddVo businessActivityAddVo);

    /**
     * 获取活动列表分页
     */
    PageInfo<BusinessActivity> selectBusinessActivityList(BusinessActivityListVo businessActivityListVo);

    /**
     * 活动详情
     */
    JsonResult activityDetails(BusinessActivityAddVo businessActivityAddVo);

    /**
     * 参加活动
     * @param businessActivityAddVo
     * @return
     */
    int joinActivity(BusinessActivityAddVo businessActivityAddVo);

    /**
     * 审核活动
     * @param businessActivityAddVo
     * @return
     */
    JsonResult updateActivityStatus(BusinessActivityAddVo businessActivityAddVo);
}
