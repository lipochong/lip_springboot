package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.PdProducts;
import com.jkdl.springboot.entity.OdrOrderdetail;
import com.jkdl.springboot.entity.PdProductsWithBLOBs;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PdProductsService {


    /**
     * 获取设备列表
     */
    List<PdProductsWithBLOBs> list(PdProductsQueryVo pdProductsQueryVo);

    /**
     * 获取设备列表分页
     */
    PageInfo<PdProductsWithBLOBs> selectPdProductsList(PdProductsQueryVo pdProductsQueryVo);

    /**
     * 新增设备
     */
    int addPdProducts(PdProductsAddVo pdProductsAddVo);

    /**
     * 新增端口
     */
    void addNewPort(PdProductsWithBLOBs pdProductsWithBLOBs, int selectPort);
    /**
     * 修改设备
     */

    int updatePdProducts(PdProductsUpdateVo pdProductsUpdateVo);

    /**
     * 修改状态Auditstatus0：未分配，1：已分配可使用，2维护中
     */
    int updatePdProductsAuditstatus(PdProductsUpdateAuditstatusVo pdProductsUpdateAuditstatusVo);


    /**
     * 判断导入excle
     * @param fileName
     * @param file
     * @return
     * @throws Exception
     */
    String pdProductsImport(String fileName, MultipartFile file) throws Exception;

    /**
     * 强制导入excle
     * @param fileName
     * @param file
     * @return
     * @throws Exception
     */
    String directPdProductsImport(String fileName, MultipartFile file) throws Exception;

    /**
     * 根据设备id获取设备情况 返回字段展示给小程序设备是否可以正常使用
     * @param productId
     * @return
     */
    JsonResult selectProductsById(PdProductSelectByIdVo productId);

    JsonResult getProductByProductId(PdProductSelectByIdVo productId);

    List<PdProducts> getmachineCount(CloseAnAccountVo closeAnAccountVo);


    /**
     * 根据商户名称查询设备总数
     */
    int getequipmentNumber(OdrOrderdetailQueryCompanyidListVo odrOrderdetailQueryCompanyidListVo);


    List<String> getproductAll();
    int getcompanypdProductCount(OdrOrderdetailQueryCompanyidListVo odrOrderdetailQueryCompanyidListVo);
}
