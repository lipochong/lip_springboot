package com.jkdl.springboot.service;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.OdrOrderdetail;
import com.jkdl.springboot.entity.OdrOrderdetailWithBLOBs;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

public interface OdrOrderdetailService {
    /**
     * 获取订单详细信息
     * @param odrOrderdetailQueryVo
     * @return
     */

    List<OdrOrderdetail> list(OdrOrderdetailQueryVo odrOrderdetailQueryVo);

    /**
     * 获取订单导出详细信息
     * @param odrOrderdetailExportQueryVo
     * @return
     */

    List<OdrOrderdetailWithBLOBs> listWithBLOBs(OdrOrderdetailExportQueryVo odrOrderdetailExportQueryVo);

    /**
     * 获取订单详细信息列表分页
     * @param odrOrderdetailQueryVo
     * @return
     */

    PageInfo<OdrOrderdetail> selectOdrOrderdetailList(OdrOrderdetailQueryVo odrOrderdetailQueryVo);

    /**
     * 订单导出Excel
     * @param agentnamess
     * @param companyname
     * @param cuname
     * @param openids
     * @param orderid
     * @param payamount
     * @param productid
     * @param startDate
     * @param endDate
     * @param status
     * @param response
     * @return
     */
      HSSFWorkbook exportOrderDetails(  @RequestParam(name = "agentnamess") String agentnamess,
                                        @RequestParam(name = "companyname") String companyname,
                                        @RequestParam(name = "cuname") String cuname,
                                        @RequestParam(name = "openids") String openids,
                                        @RequestParam(name = "orderid") String orderid,
                                        @RequestParam(name = "payamount") String payamount,
                                        @RequestParam(name = "productid") Integer productid,
                                        @RequestParam(name = "startDate") String startDate,
                                        @RequestParam(name = "endDate") String endDate,
                                        @RequestParam(name = "status") String status,
                                        HttpServletResponse response)throws ParseException ;

    /**
     * 小程序根据openid获取订单所有
     * @param odrOrderdetailQueryOpenidVo
     * @return
     */
    List<OdrOrderdetailWithBLOBs>  listByOpenid(OdrOrderdetailQueryOpenidVo odrOrderdetailQueryOpenidVo);


    /**List
     * 小程序根据openid获取订单列表
     * @param odrOrderdetailQueryOpenidVo
     * @return
     */
    PageInfo<OdrOrderdetailWithBLOBs>  selectOdrOrderdetailByOpenidpage(OdrOrderdetailQueryOpenidVo odrOrderdetailQueryOpenidVo);

    /**
     * 订单总数
     * @param odrOrderdetailQueryCompanyidVo
     * @return
     */
    int  selectOdrOrderdetailCountByConpanyid(OdrOrderdetailQueryCompanyidVo odrOrderdetailQueryCompanyidVo);

    /**
     * 订单金额总数
     * @param odrOrderdetailQueryCompanyidVo
     * @return
     */
    int  selectOdrOrderdetailCountnationPostageByConpanyid(OdrOrderdetailQueryCompanyidVo odrOrderdetailQueryCompanyidVo);

    /**
     * 设备总数bycompanyid 并修改该商家的设备数量
     * @param odrOrderdetailQueryCompanyidVo
     * @return
     */
    int selectOrderdetailCountPdProductByCompanyid(OdrOrderdetailQueryCompanyidVo odrOrderdetailQueryCompanyidVo);

    JsonResult isOrderAndEnd(XcxIsOrderAndOrderEndVo xcxIsOrderAndOrderEndVo);


    /**
     * 商家端  月订单
     */
    JsonResult listAllLotUser(OdrOrderMonthListVo odrOrderMonthListVo) throws Exception;

    /**
     * 报表数据
     */
    JsonResult orderReportdatail(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);


    /**
     *   根据条件查询设备使用数量
     */

    List<String> getproductNumber(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);

    /**
     *   用于查询罗湖区大厅设备使用率
     */

    List<String> getproductLuohuTingCount(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);
}
