package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.PdProducts;
import com.jkdl.springboot.entity.PdProductsPort;
import com.jkdl.springboot.entity.PdProductsWithBLOBs;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PdProductsPostService {


    /**
     * 获取设备端口情况
     */
    List<PdProductsPort> list(PdProductsQueryVo pdProductsQueryVo);




}
