package com.jkdl.springboot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.OdrOrderdetailMapper;
import com.jkdl.springboot.dao.PdProductsBatchVoiceMapper;
import com.jkdl.springboot.dao.PdProductsMapper;
import com.jkdl.springboot.dao.PdProductsVoiceMapper;
import com.jkdl.springboot.domain.OdrOrderdetailQueryByOrderIdVo;
import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryVo;
import com.jkdl.springboot.domain.PdVoiceQueryByBatchNumberVo;
import com.jkdl.springboot.entity.OdrOrderdetail;
import com.jkdl.springboot.entity.PdProducts;
import com.jkdl.springboot.entity.PdProductsBatchVoiceKey;
import com.jkdl.springboot.entity.PdProductsVoice;
import com.jkdl.springboot.service.PdProductsBatchVoiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class PdProductsBatchVoiceServiceImpl implements PdProductsBatchVoiceService {

    @Autowired
    private PdProductsBatchVoiceMapper pdProductsBatchVoiceMapper;

    @Autowired
    private PdProductsVoiceMapper pdProductsVoiceMapper;

    @Autowired
    private OdrOrderdetailMapper odrOrderdetailMapper;

    @Autowired
    private PdProductsMapper pdProductsMapper;

    @Override
    public List<PdProductsBatchVoiceKey> list(PdProductsBatchVoiceQueryVo pdProductsBatchVoiceQueryVo){
        return pdProductsBatchVoiceMapper.list(pdProductsBatchVoiceQueryVo);
    }
    @Override
    public PageInfo<PdProductsBatchVoiceKey> selectPdProductsBatchVoiceList(PdProductsBatchVoiceQueryVo pdProductsBatchVoiceQueryVo){
        PageHelper.startPage(pdProductsBatchVoiceQueryVo.getPageNum(),pdProductsBatchVoiceQueryVo.getPageSize());
        List<PdProductsBatchVoiceKey>  pdProductsBatchVoiceKeyList=pdProductsBatchVoiceMapper.list(pdProductsBatchVoiceQueryVo);
        PageInfo<PdProductsBatchVoiceKey> pdProductsBatchVoiceKeyPageInfo=new PageInfo<>(pdProductsBatchVoiceKeyList);
        return pdProductsBatchVoiceKeyPageInfo;
    }

    @Override
    public JsonResult getAudioVoice(PdVoiceQueryByBatchNumberVo pdVoiceQueryByBatchNumberVo) {

        //根据设备id找设备
        PdProducts pdProducts = pdProductsMapper.selectByPrimaryKey(pdVoiceQueryByBatchNumberVo.getProductId());

        //根据设备获取重置合成音id
        Integer resetvoiceid = pdProducts.getResetvoiceid();

        PdProductsVoice pdProductsVoice = pdProductsVoiceMapper.selectByPrimaryKey(resetvoiceid);

        //原音频地址
        //String httpsynthsrc = "https://www.16charge.com" + pdProductsVoice.getSynthsrc();
        //现音频地址
        String httpsynthsrc = "https://www.mwsong.com" + pdProductsVoice.getSynthsrc();

        log.info("----合成音音频地址synthsrc-----" + httpsynthsrc);

//        List<PdProductsVoice> productsVoices = pdProductsVoiceMapper.getSynthsrcByBatchNumber(pdVoiceQueryByBatchNumberVo.getBatchNumber());
//        String httpsynthsrc = "https://www.16charge.com" + productsVoices.get(0).getSynthsrc();
//        log.info("----合成音音频地址synthsrc-----" + httpsynthsrc);
//
        OdrOrderdetailQueryByOrderIdVo odrOrderdetailQueryByOrderIdVo = new OdrOrderdetailQueryByOrderIdVo();
        odrOrderdetailQueryByOrderIdVo.setOrderId(pdVoiceQueryByBatchNumberVo.getOrderId());
        OdrOrderdetail odrOrderdetail = odrOrderdetailMapper.selectOdrOrderdetailByOrderId(odrOrderdetailQueryByOrderIdVo);

        Map<String, Object> map = new HashMap<>();
        map.put("synthsrc",httpsynthsrc);
        map.put("odrOrderdetail", odrOrderdetail);

        return JsonResult.ok(map);
    }
}
