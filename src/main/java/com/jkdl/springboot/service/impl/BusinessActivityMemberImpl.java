package com.jkdl.springboot.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.DateTools;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.BusinessActivityMapper;
import com.jkdl.springboot.dao.BusinessActivityMemberMapper;
import com.jkdl.springboot.dao.USecondUsersMapper;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.BusinessActivity;
import com.jkdl.springboot.entity.BusinessActivityMember;
import com.jkdl.springboot.entity.USecondUsers;
import com.jkdl.springboot.service.BusinessActivityMemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class BusinessActivityMemberImpl implements BusinessActivityMemberService {

    @Autowired
    private BusinessActivityMemberMapper businessActivityMemberMapper;

    @Autowired
    private BusinessActivityMapper businessActivityMapper;

    @Autowired
    private USecondUsersMapper uSecondUsersMapper;

    @Override
    public JsonResult joinActivity(BusinessActivityAddVo businessActivityAddVo) {

        BusinessActivityMember businessActivityMember = new BusinessActivityMember();

        BusinessActivity businessActivity = businessActivityMapper.selectByPrimaryKey(businessActivityAddVo.getId());

        if(DateTools.calcYearTimes(businessActivity.getLasttime(), new Date()) > 1){
            businessActivity.setStatus(3);
            businessActivityMapper.updateByPrimaryKey(businessActivity);
            return JsonResult.build("活动已过期");
        }
        Integer joinCount = businessActivity.getJoincount() + 1;
        if(joinCount > businessActivity.getCount()){
            return JsonResult.build("优惠券领取已达上线");
        }
        businessActivity.setJoincount(joinCount);

        USecondUsers uSecondUsers = uSecondUsersMapper.selectByPrimaryKey(businessActivityAddVo.getUserId());
//        BusinessActivityMemberListVo businessActivityMemberListVo = new BusinessActivityMemberListVo();
//        List<BusinessActivityMember> list = businessActivityMemberMapper.selectBusinessActivityMemberList(businessActivityMemberListVo);
        int sumCount = businessActivityMemberMapper.getMemberCount(null);

        businessActivityMember.setBusinessactivityid(businessActivityAddVo.getId());
        businessActivityMember.setQyid(businessActivity.getQyid());
        int code = 10000 + sumCount + 1;
        businessActivityMember.setCode(String.valueOf(code));
        businessActivityMember.setCreatetime(new Date());
        businessActivityMember.setOpenid(uSecondUsers.getOpenid());
        businessActivityMember.setStatus(0);
        businessActivityMember.setUseconduserid(businessActivityAddVo.getUserId());
        businessActivityMember.setQyUserId(businessActivityAddVo.getQyUserId());

        int count = businessActivityMemberMapper.insert(businessActivityMember);

        if(count>0){
            businessActivityMapper.updateByPrimaryKey(businessActivity);
            return JsonResult.ok("参与活动成功");
        }else{
            return JsonResult.build("参与活动失败");
        }

    }

    @Override
    public BusinessActivityMemberVo activityMemberList(BusinessActivityMemberVo businessActivityMemberVo) {

        BusinessActivityMemberVo businessActivityMember = businessActivityMemberMapper.activityMemberList(businessActivityMemberVo);
        businessActivityMember.setLcreatetime(DateTools.format(businessActivityMember.getCreatetime(),"yyyy-MM-dd"));
        businessActivityMember.setLlasttime(DateTools.format(businessActivityMember.getLasttime(),"yyyy-MM-dd"));
        return businessActivityMember;
    }

    @Override
    public PageInfo<BusinessActivityMember> selectBusinessActivityMemberList(BusinessActivityMemberListVo businessActivityMemberListVo) {
        PageHelper.startPage(businessActivityMemberListVo.getPageNum(), businessActivityMemberListVo.getPageSize());
        List<BusinessActivityMember> businessActivityMemberList = businessActivityMemberMapper.selectBusinessActivityMemberList(businessActivityMemberListVo);

        PageInfo<BusinessActivityMember> businessActivityMemberPageInfo = new PageInfo<>(businessActivityMemberList);
        return businessActivityMemberPageInfo;
    }

    @Override
    public BusinessActivityMember getActivityMemberDetails(BusinessActivityMemberVo businessActivityMemberVo) {
        BusinessActivityMember businessActivityMember = businessActivityMemberMapper.getActivityMemberDetails(businessActivityMemberVo);
        if(businessActivityMember != null){
            if(businessActivityMember.getStatus() == 3){
                businessActivityMember.setStatus(1);
            }
            return businessActivityMember;
        }
        return businessActivityMember;
    }

    @Override
    public JsonResult useActivityMember(BusinessActivityMemberVo businessActivityMemberVo) {

        BusinessActivityMember businessActivityMember = businessActivityMemberMapper.selectByPrimaryKey(businessActivityMemberVo.getId());
        businessActivityMember.setStatus(1);
        businessActivityMember.setLasttime(new Date());
        int count = businessActivityMemberMapper.updateByPrimaryKey(businessActivityMember);
        if(count > 0){
            BusinessActivity businessActivity = businessActivityMapper.selectByPrimaryKey(businessActivityMember.getBusinessactivityid());
            businessActivity.setUsecount(businessActivity.getUsecount()+1);
            businessActivity.setUpdatetime(new Date());
            businessActivityMapper.updateByPrimaryKey(businessActivity);
            return JsonResult.ok("成功");
        }

        return JsonResult.build("失败");
    }

    @Override
    public List<BusinessActivityMember> getActivityMemberList(BusinessActivityMemberVo businessActivityMemberVo) {
        return businessActivityMemberMapper.getActivityMemberList(businessActivityMemberVo);
    }

    @Override
    public BusinessActivityMemberLeaderboardListVo getActivityMemberId(Integer id) {
        BusinessActivityMemberLeaderboardListVo baml = new BusinessActivityMemberLeaderboardListVo();
        BusinessActivity businessActivity = businessActivityMapper.selectByPrimaryKey(id);
        baml.setContent(businessActivity.getContent());
        baml.setCount(businessActivity.getCount()+"");
        baml.setCreateTime(DateTools.format(businessActivity.getCreatetime(),"yyyy-MM-dd"));
        baml.setLastTime(DateTools.format(businessActivity.getLasttime(),"yyyy-MM-dd"));
        List<BusinessActivityMemberIdListVo> lst = businessActivityMemberMapper.getBusinessActivityMemberList(id);
        int i = 0;
        Integer sum = 0;
        for(BusinessActivityMemberIdListVo obj : lst){
            if (obj.getUseCount() == sum) {
            }else{
                i = i + 1;
            }
            if (i == 1) {
                obj.setOrder("/images/icon/one.png");
            } else if (i == 2) {
                obj.setOrder("/images/icon/tow.png");
            } else if (i == 3) {
                obj.setOrder("/images/icon/three.png");
            } else {
                obj.setOrder(i + "");
            }
            sum = obj.getUseCount();
        }
        baml.setLst(lst);
        return baml;
    }

    @Override
    public  List<BusinessActivityMemberWriteoffVo> getActivityMemberWriteOff(Integer id) {
        List<BusinessActivityMemberWriteoffVo> lst = businessActivityMemberMapper.getBusinessActivityMemberWriteOffList(id);
        return lst;
    }

    @Override
    public  PageInfo<BusinessActivityMemberWriteoffVo> getActivityMemberWriteOff(BAMerberPageVo bAMerberPageVo) {
        PageHelper.startPage(bAMerberPageVo.getPageNum(), bAMerberPageVo.getPageSize());
        List<BusinessActivityMemberWriteoffVo> businessActivityMemberWriteoffVo = businessActivityMemberMapper.getBusinessActivityMemberWriteOffList(bAMerberPageVo.getId());

        PageInfo<BusinessActivityMemberWriteoffVo> businessActivityMemberWriteoffVoInfo = new PageInfo<>(businessActivityMemberWriteoffVo);
        return businessActivityMemberWriteoffVoInfo;
    }

    @Override
    public JsonResult joinActivityShareUser(BusinessActivityMemberShareVo businessActivityMemberShareVo) {

        BusinessActivityMember businessActivityMember = new BusinessActivityMember();

        BusinessActivity businessActivity = businessActivityMapper.selectByPrimaryKey(businessActivityMemberShareVo.getId());

        if(DateTools.calcYearTimes(businessActivity.getLasttime(), new Date()) > 1){
            businessActivity.setStatus(3);
            businessActivityMapper.updateByPrimaryKey(businessActivity);
            return JsonResult.build("活动已过期");
        }

        BusinessActivityMember bamEntity = businessActivityMemberMapper.selectByPrimaryKey(businessActivityMemberShareVo.getBamId());
        if(bamEntity.getStatus() == 3){
            return JsonResult.build("已被领取");
        }

        USecondUsers uSecondUsers = uSecondUsersMapper.selectByPrimaryKey(businessActivityMemberShareVo.getUserId());
        int sumCount = businessActivityMemberMapper.getMemberCount(null);

        businessActivityMember.setBusinessactivityid(businessActivityMemberShareVo.getId());
        businessActivityMember.setQyid(businessActivity.getQyid());
        int code = 10000 + sumCount + 1;
        businessActivityMember.setCode(String.valueOf(code));
        businessActivityMember.setCreatetime(new Date());
        businessActivityMember.setOpenid(uSecondUsers.getOpenid());
        businessActivityMember.setStatus(0);
        businessActivityMember.setUseconduserid(businessActivityMemberShareVo.getUserId());
        businessActivityMember.setQyUserId(businessActivityMemberShareVo.getQyUserId());

        int count = businessActivityMemberMapper.insert(businessActivityMember);

        if(count>0){
            if(businessActivityMemberShareVo.getBamId() != null){
                BusinessActivityMember bam = new BusinessActivityMember();
                bam.setStatus(3);
                bam.setId(businessActivityMemberShareVo.getBamId());
                businessActivityMemberMapper.updateByPrimaryKeySelective(bam);
            }
            return JsonResult.ok("参与活动成功");
        }else{
            return JsonResult.build("参与活动失败");
        }

    }

    @Override
    public BusinessActivityMember getQueryActivityMemberDetails(BusinessActivityMemberVo businessActivityMemberVo) {
        BusinessActivityMember bam = businessActivityMemberMapper.selectByPrimaryKey(businessActivityMemberVo.getId());
        if(bam.getStatus() == 3 ){
            bam.setStatus(4);
            return bam;
        }
        if(bam.getStatus() == 1){
            bam.setStatus(3);
            return bam;
        }
        businessActivityMemberVo.setId(null);
        BusinessActivityMember businessActivityMember = businessActivityMemberMapper.getActivityMemberDetails(businessActivityMemberVo);
        if(businessActivityMember != null){
            if(businessActivityMember.getStatus() == 3){
                businessActivityMember.setStatus(5);
            }
            return businessActivityMember;
        }
        return businessActivityMember;
    }
}
