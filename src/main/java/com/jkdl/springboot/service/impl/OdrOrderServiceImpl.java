package com.jkdl.springboot.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import com.jkdl.springboot.common.util.DateTools;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.common.util.OrderMasterUtil;
import com.jkdl.springboot.common.util.WxUtil.HttpUtil;
import com.jkdl.springboot.common.util.WxUtil.PayCommonUtil;
import com.jkdl.springboot.common.util.WxUtil.WxConfigUtil;
import com.jkdl.springboot.common.util.WxUtil.XMLUtil;
import com.jkdl.springboot.dao.*;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.*;
import com.jkdl.springboot.enums.OrderStatus;
import com.jkdl.springboot.service.OdrOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class OdrOrderServiceImpl implements OdrOrderService{

    @Autowired
    private OdrOrderMapper odrOrderMapper;

    @Autowired
    private OdrOrderdetailMapper odrOrderdetailMapper;

    @Autowired
    private PdProductsMapper pdProductsMapper;

    @Autowired
    private UCompanyMapper uCompanyMapper;

    @Autowired
    private USecondUsersMapper uSecondUsersMapper;

    @Override
    public List<OdrOrder> list(OdrOrderQueryVo odrOrderQueryVo){
        return odrOrderMapper.list(odrOrderQueryVo);
    }
    @Override
    public PageInfo<OdrOrder> selectOdrOrderList(OdrOrderQueryVo odrOrderQueryVo){
        PageHelper.startPage(odrOrderQueryVo.getPageNum(),odrOrderQueryVo.getPageSize());
        List<OdrOrder> odrOrderList=odrOrderMapper.list(odrOrderQueryVo);
        PageInfo<OdrOrder> odrOrderListInfo=new PageInfo<>(odrOrderList);
        return odrOrderListInfo;
    }

    @Override
    public synchronized Map<String,Object> createOrder(XcxCreateOrderVo xcxCreateOrderVo) {

        Integer productId = xcxCreateOrderVo.getProductId();

        //设备信息
        PdProducts pdProducts = pdProductsMapper.selectByPrimaryKey(productId);

        //商户信息
        UCompany company = uCompanyMapper.selectByPrimaryKey(pdProducts.getCompanyid());

        //用户信息
        USecondUsers uSecondUsers = uSecondUsersMapper.selectByPrimaryKey(xcxCreateOrderVo.getUserId());

        OdrOrder odrOrder = createOrder(productId, company, uSecondUsers);

        Map<String, Object> map = new HashMap<>();

        map.put("orderId", odrOrder.getOrderid());
        map.put("id", odrOrder.getId());
        //设备批次
        map.put("batchNumber", pdProducts.getBatchnumber());
        map.put("smlock", uSecondUsers.getSmlock());

        return map;
    }

    @Override
    public String payOrder(XcxPayOrderVo xcxPayOrderVo) throws JDOMException, IOException {
        //订单金额
        String total_fee = "3";
        //异步回调路径
        //String notify_url=null;
        //订单编号
        String did = null;
        //订单标题
        String title = null;
        BigDecimal total_fees = new BigDecimal("0.00");

        OdrOrder odrOrder = null;
        //得到小程序传过来的价格，注意这里的价格必须为整数，1代表1分，所以传过来的值必须*100;
        try{
           if (!StringUtils.isEmpty(xcxPayOrderVo.getOrderId())){
                //notify_url=WxConfigUtil.NOTIFY_URL_ONE;
                did=xcxPayOrderVo.getOrderId();
                title = xcxPayOrderVo.getOrderId();
                log.info("----根据id获取订单----"+xcxPayOrderVo.getId()+"------");
                odrOrder = odrOrderMapper.selectByPrimaryKey(xcxPayOrderVo.getId());



                total_fees = odrOrder.getAmount().multiply(new BigDecimal("100"));
            }else{
                throw new  RuntimeException("===============订单号为空=========outTradeNo:"+xcxPayOrderVo.getOpenId()+"=================orderId================="+xcxPayOrderVo.getOpenId());
            }
            total_fee = total_fees.toString();
            //订单金额
            if (total_fee.indexOf(".") > 0) {
                total_fee = total_fee.substring(0, total_fee.indexOf("."));
            }
        }catch (Exception e){
            throw new  RuntimeException("===============支付失败=========outTradeNo:"+xcxPayOrderVo.getOpenId()+"=================orderId================="+xcxPayOrderVo.getOpenId());
        }

        //时间戳
        String times = System.currentTimeMillis() + "";
        SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();
        packageParams.put("appid", WxConfigUtil.APPID);
        packageParams.put("mch_id", WxConfigUtil.MCHID);
        packageParams.put("nonce_str", times);//时间戳
        packageParams.put("body", "16c充电-消费");//支付主体
        packageParams.put("out_trade_no", did);//编号
        packageParams.put("total_fee", Integer.parseInt(total_fee));//价格
        // packageParams.put("spbill_create_ip", getIp2(request));这里之前加了ip，但是总是获取sign失败，原因不明，之后就注释掉了
        packageParams.put("notify_url", WxConfigUtil.NOTIFY_URL);//支付返回地址，
        packageParams.put("trade_type", "JSAPI");//这个api有，固定的
        packageParams.put("openid", xcxPayOrderVo.getOpenId() );//openid

        //获取sign
        String sign = PayCommonUtil.createSign("UTF-8", packageParams, WxConfigUtil.KEY);//最后这个是自己设置的32位密钥
        packageParams.put("sign", sign);
        //转成XML
        String requestXML = PayCommonUtil.getRequestXml(packageParams);
        System.out.println(requestXML);
        //得到含有prepay_id的XML
        String resXml = HttpUtil.postData("https://api.mch.weixin.qq.com/pay/unifiedorder", requestXML);
        System.out.println(resXml);
        //解析XML存入Map
        Map map = XMLUtil.doXMLParse(resXml);
        System.out.println(map);
        String return_code = (String) map.get("return_code");
        //得到prepay_id
        String prepay_id = (String) map.get("prepay_id");

        SortedMap<Object, Object> packageP = new TreeMap<Object, Object>();
        packageP.put("appId", WxConfigUtil.APPID);//！！！注意，这里是appId,上面是appid，真怀疑写这个东西的人。。。
        packageP.put("nonceStr", times);//时间戳
        packageP.put("package", "prepay_id=" + prepay_id);//必须把package写成 "prepay_id="+prepay_id这种形式
        packageP.put("signType", "MD5");//paySign加密
        packageP.put("timeStamp", (System.currentTimeMillis() / 1000) + "");
        //得到paySign
        String paySign = PayCommonUtil.createSign("UTF-8", packageP, WxConfigUtil.KEY);
        packageP.put("paySign", paySign);
        //将packageP数据返回给小程序
        Gson gson = new Gson();
        String json = gson.toJson(packageP);
        return json;
    }

    public OdrOrder createOrder(int productId, UCompany company, USecondUsers uSecondUsers) {

        Date date = new Date();
        // 1、保存订单信息 Order
        OdrOrder order = new OdrOrder();
        order.setOname("充电设备 " + date + uSecondUsers.getId());
        order.setCuid(uSecondUsers.getId());
        order.setQyid(company.getId());
        order.setCuname(uSecondUsers.getNickname());
        // 获取订单号
        String orderno = OrderMasterUtil.createOrderId(date, uSecondUsers.getQyid());
        order.setOrderid(orderno);
        order.setOrdertype("0");
        order.setCreatedate(date);

        order.setPaytype(1);

        //免支付订单  设置订单状态为3
        if(null!=uSecondUsers.getSmlock()&&!"".equals(uSecondUsers.getSmlock())){
            int smlock = uSecondUsers.getSmlock();
            if(1 == smlock){
                order.setStatus(OrderStatus.PayFreeOrder.getCode());
            }else{
                order.setStatus(OrderStatus.PayNo.getCode());
            }
        }else{
            order.setStatus(OrderStatus.PayNo.getCode());
        }

        order.setPaystatus("0");
        order.setScore(0);
        order.setExpressname("11");
        order.setExpressno("11");
        order.setTxstatus(10);
        order.setErjistatus(10);
        Integer totalamount = new Integer(1);
        Long payamount = new Long(2);
        //Double amount = 2.0;
        order.setAmount(new BigDecimal(company.getNationpostage()));
        order.setPayamount(new BigDecimal(company.getNationpostage()));
        order.setQuantity(totalamount);
        order.setPtotal(new BigDecimal(company.getNationpostage()));

        order.setBdelete(0);

//			order.setAmount(amount);
//			order.setPayamount(amount);
//			order.setQuantity(totalamount);
//			order.setPtotal(amount);

        // 保存订单详细信息 Orderdetail
        List<OdrOrderdetail> orderDetailList = new ArrayList<OdrOrderdetail>();
        //totalamount = totalamount + 1;
        OdrOrderdetail orderdetail = new OdrOrderdetail();
        orderdetail.setProductid(productId);
        orderdetail.setProductname("充电设备" + productId + "创建订单成功");
        //订单编号
        orderdetail.setOrderid(orderno);
        orderdetail.setPrice(company.getNationpostage().longValue());
        orderdetail.setNumber(1);
        orderdetail.setLowstocks("y");
        orderdetail.setIscomment(0);
        orderdetail.setScore(0);
        orderdetail.setCdstarttime(date);
        orderdetail.setCatalogstatus("p001");// 不知道是什么字段，反正少了就报错
        orderdetail.setOrdstatus(0);
        orderdetail.setBdelete(0);

        //目前测试
        orderdetail.setCdendtime(DateTools.getDateByMinuteAdd(date, company.getChargingtime()));

        odrOrderMapper.insert(order);
        odrOrderdetailMapper.insert(orderdetail);
        return order;


        //支付成功之后添加充电结束时间
//        if ("银泉宾馆".equals(company.getCompanyname()) || "六点半酒店".equals(company.getCompanyname())
//                || "金华酒店".equals(company.getCompanyname())) {
//            orderdetail.setCdendtime(DateTools.getDateByMinuteAdd(date, 720));
//            orderDetailList.add(orderdetail);
//            odrOrderMapper.insert(order);
////            odrOrderdetailMapper.saveList(orderDetailList);
//            odrOrderdetailMapper.insert(orderdetail);
//            return order;
//        } else {
//            orderdetail.setCdendtime(DateTools.getDateByMinuteAdd(date, company.getChargingtime()));
//            orderDetailList.add(orderdetail);
//            odrOrderMapper.insert(order);
//            odrOrderdetailMapper.insert(orderdetail);
////            orderdetailService.saveList(orderDetailList);
//            return order;
//        }
    }

    //异步请求回应微信支付是否成功
    @Transactional
    public String weixinNotify(String resultStr) {
        log.info("=========weixinPaynotify   start====");

        String return_code=null;
        String result_code=null;
        try {
            //字节流
            Map<String, String> resultMap = XMLUtil.doXMLParse(resultStr);
            log.info("resultMap:" + JSON.toJSONString(resultMap));
            String transaction_id = resultMap.get("transaction_id"); //微信交易流水号
            result_code = resultMap.get("result_code");
            String out_trade_no = resultMap.get("out_trade_no");
            return_code = resultMap.get("return_code");
            log.info("===========================微信异步回调 来了========================"+out_trade_no);
            // 签名验证(在网上复制而来,不知其用处故先注释)
            //GenericValue userLogin = delegator.findOne("UserLogin", UtilMisc.toMap("userLoginId", "admin"), false);

            //支付成功  修改订单状态和子订单状态  添加充电结束时间
            if ("SUCCESS".equals(return_code)&&"SUCCESS".equals(result_code)) {

                log.info("小程序订单列表支付回调接收到了out_trade_no=========="+out_trade_no);

                OdrOrderdetailQueryByOrderIdVo odrOrderdetailQueryByOrderIdVo = new OdrOrderdetailQueryByOrderIdVo();
                odrOrderdetailQueryByOrderIdVo.setOrderId(out_trade_no);

                //获取主订单
                OdrOrder orders = new OdrOrder();
                orders = odrOrderMapper.selectOdOrderByOrderId(odrOrderdetailQueryByOrderIdVo);
                orders.setStatus(OrderStatus.PaySuccess.getCode());
                odrOrderMapper.updateByPrimaryKeySelective(orders);
                //获取订单详情

                OdrOrderdetail odrOrderdetail = odrOrderdetailMapper.selectOdrOrderdetailByOrderId(odrOrderdetailQueryByOrderIdVo);
                //获取时间
                UCompany uCompany = uCompanyMapper.selectByPrimaryKey(orders.getQyid());
                Date date = new Date();
                odrOrderdetail.setCdendtime(DateTools.getDateByMinuteAdd(date, uCompany.getChargingtime()));

                odrOrderdetailMapper.updateByPrimaryKeySelective(odrOrderdetail);


            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("微信异步回调失败!");
        }
        return return_code;
    }

    @Override
    public JsonResult orderList(OrderListQueryVo orderListQueryVo) {

        log.info("-------查询订单开始---------");
        //分页
        int pageStart = (orderListQueryVo.getPageNum()-1)*orderListQueryVo.getPageSize() ;
        orderListQueryVo.setPageStart(pageStart);

        //平台id
        //int platformId = orderListQueryVo.getPlatformId();
        //一级代理商i
        int oneCompanyId = orderListQueryVo.getOneCompanyId();
        //二级代理商id
        int twoCompanyId = orderListQueryVo.getTwoCompanyId();

        //二级不为空  设置一级为空  只查询二级  二级为空  不做处理  下面会判断一级是否为空
        if(!"".equals(twoCompanyId)&&0!=twoCompanyId){
            oneCompanyId = 0;
        }

        String companyName = orderListQueryVo.getCompanyname();

        //设备编号不为空
        int productId = orderListQueryVo.getProductid();

        log.info("-------查询订单开始商户名称判断---------");
        //商户名称不为空
        if(!"".equals(companyName)&&null!=companyName){
            //如果商户名称不为空  则先查询匹配的商户  然后再查询商户id  然后再根据条件查询订单

            UCompanyCompanynameVo uCompanyCompanynameVo = new UCompanyCompanynameVo();
            uCompanyCompanynameVo.setCompanyname(companyName);

            List<UCompany> uCompanyList = uCompanyMapper.selectUcompanyByCompanyName(uCompanyCompanynameVo);

            List<String> companyQyidsList = new ArrayList<String>();

            for(UCompany uCompany : uCompanyList){
                companyQyidsList.add(uCompany.getId().toString());
            }

            orderListQueryVo.setCompanynameQyIds(companyQyidsList);

            //设置联动的id都为0  不走后面的判断
            oneCompanyId = 0;
            twoCompanyId = 0;

            productId = 0;

        }

        if(!"".equals(productId)&&productId!=0){
            //如果设备编号不为空  则先查询匹配的商户  然后再查询商户id  然后再根据条件查询订单

            UCompanyProductIdVo uCompanyProductIdVo = new UCompanyProductIdVo();
            uCompanyProductIdVo.setProductId(productId);
            uCompanyProductIdVo.setPageNum(pageStart);
            uCompanyProductIdVo.setPageSize(orderListQueryVo.getPageSize());

            List<OdrOrderdetail> orderdetailList = odrOrderdetailMapper.selectOdrOrderdetailByProductId(uCompanyProductIdVo);

            List<String> orderidList = new ArrayList<String>();

            for(OdrOrderdetail odrOrderdetail : orderdetailList){
                orderidList.add(String.valueOf(odrOrderdetail.getOrderid()));
            }

            orderListQueryVo.setOrderids(orderidList);

            //设置联动的id都为0  不走后面的判断
            oneCompanyId = 0;
            twoCompanyId = 0;

        }

        log.info("-------查询订单开始一级代理商判断---------");

        //一级代理商不为空  查询该代理商下的所有代理商   再查询订单
        if(!"".equals(oneCompanyId)&&0!=oneCompanyId){

            UCompanyQueryPlatformVo uCompanyQueryPlatformVo = new UCompanyQueryPlatformVo();
            //查询该代理商下的二级代理商
            uCompanyQueryPlatformVo.setParentId(oneCompanyId);
            uCompanyQueryPlatformVo.setCompanyType("20");

            List<UCompany> uCompanyList = uCompanyMapper.selectUCompanyListByParendIdAndCompanyType(uCompanyQueryPlatformVo);

            StringBuffer qyids = new StringBuffer();

            List<String> oneQyidsList = new ArrayList<String>();

            //获取所有二级代理商下的商户
            for(UCompany uCompany : uCompanyList){
                int id = uCompany.getId();

                //查询该代理商下的商户
                uCompanyQueryPlatformVo.setParentId(id);
                uCompanyQueryPlatformVo.setCompanyType("50");

                List<UCompany> uCompanyList2 = uCompanyMapper.selectUCompanyListByParendIdAndCompanyType(uCompanyQueryPlatformVo);
                for(UCompany uCompany1 : uCompanyList2){
//                    qyids.append(uCompany1.getId());
//                    qyids.append(",");
                    oneQyidsList.add(uCompany1.getId().toString());
                }

                //添加二级代理商id
                oneQyidsList.add(uCompany.getId().toString());
            }

            //添加一级代理商

            oneQyidsList.add(String.valueOf(oneCompanyId));

            //分页查询订单  根据企业ids  查询前30条

            orderListQueryVo.setOneCompanyQyIds(oneQyidsList);

        }

        log.info("-------查询订单开始二级代理商判断---------");
        //二级代理商不为空  查询该代理商下的所有订单
        if(!"".equals(twoCompanyId) && twoCompanyId !=0){
            List<String> twoQyidsList = new ArrayList<String>();
            //查询该代理商下的商户
            UCompanyQueryPlatformVo uCompanyQueryPlatformVo = new UCompanyQueryPlatformVo();

            uCompanyQueryPlatformVo.setCompanyType("50");
            uCompanyQueryPlatformVo.setParentId(twoCompanyId);

            List<UCompany> uCompanyList = uCompanyMapper.selectUCompanyListByParendIdAndCompanyType(uCompanyQueryPlatformVo);
            for(UCompany uCompany1 : uCompanyList){
//                    qyids.append(uCompany1.getId());
//                    qyids.append(",");
                twoQyidsList.add(uCompany1.getId().toString());
            }

            //添加二级id
            twoQyidsList.add(String.valueOf(twoCompanyId));
            orderListQueryVo.setTwoCompanyQyIds(twoQyidsList);
        }

        log.info("-------查询订单开始  条件查询---------");
        List<OdrOrderListQueryVo> orderList = odrOrderMapper.orderList(orderListQueryVo);
        List<OrderListVo> orderListVos = new ArrayList<>();
        for(OdrOrderListQueryVo odrOrder : orderList){

            OrderListVo orderListVo = new OrderListVo();
            orderListVo.setAmount(odrOrder.getAmount());
            orderListVo.setOrderid(odrOrder.getOrderid());
            orderListVo.setCreatedate(odrOrder.getCreatedate());
            orderListVo.setStatus(odrOrder.getStatus());
            orderListVo.setId(odrOrder.getId());

            //订单id查询订单详情
            String orderid = odrOrder.getOrderid();
            OdrOrderdetailQueryByOrderIdVo odrOrderdetailQueryByOrderIdVo = new OdrOrderdetailQueryByOrderIdVo();
            odrOrderdetailQueryByOrderIdVo.setOrderId(orderid);
            OdrOrderdetail odrOrderdetail = odrOrderdetailMapper.selectOdrOrderdetailByOrderId(odrOrderdetailQueryByOrderIdVo);

            //设备编号
            orderListVo.setProductid(odrOrderdetail.getProductid());

            //房间号
            int productid = odrOrderdetail.getProductid();
            PdProducts pdProducts = pdProductsMapper.selectByPrimaryKey(productid);
            String detail = pdProducts.getDetail();
            //房间号
            orderListVo.setDetail(detail);

            //用户openid
            USecondUsers uSecondUsers = uSecondUsersMapper.selectByPrimaryKey(odrOrder.getCuid());
            if(null!=uSecondUsers){
                if(null!=uSecondUsers.getOpenid()){
                    orderListVo.setOpenid(uSecondUsers.getOpenid());
                }
            }

            //企业id
            int qyid = odrOrder.getQyid();
            UCompany uCompany = uCompanyMapper.selectByPrimaryKey(qyid);

            if(null == uCompany.getCompanyname() || "".equals(uCompany.getCompanyname())){
                orderListVo.setCompanyname("");
            }else{
                //公司名称
                orderListVo.setCompanyname(uCompany.getCompanyname());
            }

            //上级名称
            if(null != uCompany.getParentid()){
                if(0 == uCompany.getParentid()){
                    //上级名称
                    orderListVo.setOneCompanyName("极客动力平台");
                    //上上级名称
                    orderListVo.setTwoCompanyName("极客动力平台");
                }else{
                    //上级名称
                    UCompany uCompany1 = uCompanyMapper.selectByPrimaryKey(uCompany.getParentid());
                    orderListVo.setOneCompanyName(uCompany1.getCompanyname());

                    //上上级名称
                    if(null != uCompany1.getParentid()){
                        if(0 == uCompany1.getParentid()){
                            //上上级名称
                            orderListVo.setTwoCompanyName("极客动力平台");
                        }else{
                            //上上级名称
                            UCompany uCompany2 = uCompanyMapper.selectByPrimaryKey(uCompany1.getParentid());
                            orderListVo.setTwoCompanyName(uCompany2.getCompanyname());
                        }
                    }else{
                        orderListVo.setTwoCompanyName("极客动力平台");
                    }

                }


            }else{
                orderListVo.setOneCompanyName("极客动力平台");
                orderListVo.setTwoCompanyName("极客动力平台");
            }

            //设备数量
            orderListVo.setDevicenumber(uCompany.getDevicenumber());

            orderListVos.add(orderListVo);

        }

        log.info("查询出来的订单数量:"+ orderListVos.size());

        PageInfo<OrderListVo> uCompanyListInfo = new PageInfo<>(orderListVos);

        //总条数
        long total = odrOrderMapper.orderListTotal(orderListQueryVo);
        log.info("总订单数量:"+ total);

        uCompanyListInfo.setTotal(total);

        log.info("-------查询订单结束---------");

        return JsonResult.ok(uCompanyListInfo);
    }

    @Override
    public JsonResult updateOrderStatus(OrderListVo orderListVo) {
        if(0==orderListVo.getId()||"".equals(orderListVo.getId())){
            return JsonResult.build("参数错误");
        }
        OdrOrder odrOrder = odrOrderMapper.selectByPrimaryKey(orderListVo.getId());
        if(null==odrOrder.getOrderid()){
            return JsonResult.build("订单不存在");
        }

        //是已支付订单  修改为退款
        if(10 == orderListVo.getStatus()){
            if(10 == odrOrder.getStatus()){
                odrOrder.setStatus(OrderStatus.PayRefundOrder.getCode());
            }
        }

        //是退款订单  修改为已支付
        if(4 == orderListVo.getStatus()){
            if(4 == odrOrder.getStatus()){
                odrOrder.setStatus(OrderStatus.PaySuccess.getCode());
            }
        }

        return JsonResult.ok(odrOrderMapper.updateByPrimaryKey(odrOrder));
    }

    @Override
    public List<OdrOrder> orderInfoList(CloseAnAccountVo closeAnAccountVo){
        return odrOrderMapper.orderInfoList(closeAnAccountVo);
    }
    @Override
    public List<OdrOrder> orderDateInfoList(){
        return odrOrderMapper.orderDateInfoList();
    }

    //查询使用总金额
    @Override
    public OrderReportdatailExample gettotalMoney(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo){
        return odrOrderMapper.gettotalMoney(odrOrderdetailReportdatailVo);
    };
    //查询折线图数据
    @Override
    public List<OdrOrderGroup> getreportFormsList(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo){
        return  odrOrderMapper.getreportFormsList(odrOrderdetailReportdatailVo);

    };
}
