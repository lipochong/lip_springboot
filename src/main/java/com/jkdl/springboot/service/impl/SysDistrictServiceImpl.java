package com.jkdl.springboot.service.impl;


import com.jkdl.springboot.dao.SysCityMapper;
import com.jkdl.springboot.dao.SysDistrictMapper;
import com.jkdl.springboot.domain.SysDistrictQueryVo;
import com.jkdl.springboot.entity.SysCity;
import com.jkdl.springboot.entity.SysDistrict;
import com.jkdl.springboot.service.SysCityService;
import com.jkdl.springboot.service.SysDistrictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SysDistrictServiceImpl implements SysDistrictService{

    @Autowired
    private SysDistrictMapper sysDistrictMapper;

    @Override
    public List<SysDistrict> getAllDistrictByCityId(SysDistrictQueryVo sysDistrictQueryVo) {
        return sysDistrictMapper.getAllDistrictByCityId(sysDistrictQueryVo);
    }
}
