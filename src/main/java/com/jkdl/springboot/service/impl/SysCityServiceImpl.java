package com.jkdl.springboot.service.impl;


import com.jkdl.springboot.dao.SysCityMapper;
import com.jkdl.springboot.dao.SysProvinceMapper;
import com.jkdl.springboot.domain.SysCityQueryVo;
import com.jkdl.springboot.entity.SysCity;
import com.jkdl.springboot.entity.SysProvince;
import com.jkdl.springboot.service.SysCityService;
import com.jkdl.springboot.service.SysProvinceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SysCityServiceImpl implements SysCityService{

    @Autowired
    private SysCityMapper sysCityMapper;

    @Override
    public List<SysCity> getAllCityByProvinceId(SysCityQueryVo sysCityQueryVo) {
        return sysCityMapper.getAllCityByProvinceId(sysCityQueryVo);
    }
}
