package com.jkdl.springboot.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.USersMapper;
import com.jkdl.springboot.domain.UsersAddVo;
import com.jkdl.springboot.domain.UsersDeleteVo;
import com.jkdl.springboot.domain.UsersQueryVo;
import com.jkdl.springboot.domain.UsersUpdateVo;
import com.jkdl.springboot.entity.USers;
import com.jkdl.springboot.entity.USersWithBLOBs;
import com.jkdl.springboot.service.USersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class USersServiceImpl implements USersService{


    @Autowired
    private USersMapper uSersMapper;

    @Override
    public Long add(USers uSers) throws Exception {
        return null;
    }

    @Override
    public  List<USers> list(UsersQueryVo usersQueryVo) {
        return uSersMapper.list(usersQueryVo);
    }

    @Override
    public PageInfo<USers> selectUSersList(UsersQueryVo usersQueryVo) {
        PageHelper.startPage(usersQueryVo.getPageNum(), usersQueryVo.getPageSize());
        List<USers> uSersList = uSersMapper.list(usersQueryVo);
        PageInfo<USers> uSersListInfo = new PageInfo<>(uSersList);
        return uSersListInfo;
    }
    @Override
    public int addUsers(UsersAddVo usersAddVo){
        USers users=new USers();
        BeanUtils.copyProperties(usersAddVo, users);
        users.setUsertype("2");
        return uSersMapper.insertSelective(users);

    }

    @Override
    public  int updateUsers(UsersUpdateVo usersUpdateVo){
        USers users=uSersMapper.selectByPrimaryKey(usersUpdateVo.getId());
        BeanUtils.copyProperties(usersUpdateVo, users);
        return uSersMapper.updateByPrimaryKeySelective(users);
    }

    @Override
    public int deleteUsers(UsersDeleteVo usersDeleteVo){
      //  USers users=uSersMapper.selectByPrimaryKey(usersDeleteVo.getId());
        return uSersMapper.deleteByPrimaryKey(usersDeleteVo.getId());
    }
}
