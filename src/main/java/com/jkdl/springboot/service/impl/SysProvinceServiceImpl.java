package com.jkdl.springboot.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.DateTools;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.*;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.*;
import com.jkdl.springboot.service.OdrOrderdetailService;
import com.jkdl.springboot.service.SysProvinceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class SysProvinceServiceImpl implements SysProvinceService{

    @Autowired
    private SysProvinceMapper sysProvinceMapper;

    @Override
    public List<SysProvince> getAllProvince() {
        return sysProvinceMapper.getAllProvince();
    }
}
