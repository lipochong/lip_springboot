package com.jkdl.springboot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.dao.PdProductsBatchMapper;
import com.jkdl.springboot.dao.PdProductsBatchVoiceMapper;
import com.jkdl.springboot.dao.PdProductsVoiceMapper;
import com.jkdl.springboot.domain.PdProductsBatchAddVo;
import com.jkdl.springboot.domain.PdProductsBatchQueryVo;
import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryVo;
import com.jkdl.springboot.entity.PdProductsBatch;
import com.jkdl.springboot.entity.PdProductsBatchVoiceKey;
import com.jkdl.springboot.entity.PdProductsVoice;
import com.jkdl.springboot.service.PdProductsBatchService;
import com.jkdl.springboot.service.PdProductsBatchVoiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class PdProductsBatchServiceImpl implements PdProductsBatchService {

    @Autowired
    private PdProductsBatchMapper pdProductsBatchMapper;

    @Autowired
    private PdProductsVoiceMapper pdProductsVoiceMapper;

    @Autowired
    private  PdProductsBatchVoiceMapper pdProductsBatchVoiceMapper;

    @Override
    public List<PdProductsBatch> list(PdProductsBatchQueryVo pdProductsBatchQueryVo){
        return pdProductsBatchMapper.list(pdProductsBatchQueryVo);
    }
    @Override
    public PageInfo<PdProductsBatch> selectPdProductsBatchList(PdProductsBatchQueryVo pdProductsBatchQueryVo){
        PageHelper.startPage(pdProductsBatchQueryVo.getPageNum(),pdProductsBatchQueryVo.getPageSize());
        List<PdProductsBatch>  pdProductsBatchList=pdProductsBatchMapper.list(pdProductsBatchQueryVo);
        PageInfo<PdProductsBatch> pdProductsBatchPageInfo=new PageInfo<>(pdProductsBatchList);
        return pdProductsBatchPageInfo;
    }

    @Override
    public int addPdProductsBatch(PdProductsBatchAddVo pdProductsBatchAddVo){
        PdProductsBatch productsBatch=new PdProductsBatch();
        if(null==pdProductsBatchAddVo.getId()){
            productsBatch.setParentid(0.0);
            productsBatch.setLevel(0.0);
            productsBatch.setBdelete(0.0);
            productsBatch.setCreatetime(new Date());
            BeanUtils.copyProperties(pdProductsBatchAddVo, productsBatch);
            //语言版有声音编号新增，而 旗舰版没有
            if(pdProductsBatchAddVo.getDevtype().equals("sy")){

                pdProductsBatchMapper.insertSelective(productsBatch);
                savevoice(productsBatch,pdProductsBatchAddVo);
            }else {
                pdProductsBatchMapper.insertSelective(productsBatch);
            }
        }

        return  pdProductsBatchMapper.insertSelective(productsBatch);
    }

    @Override
    public void savevoice(PdProductsBatch pdProductsBatch,PdProductsBatchAddVo pdProductsBatchAddVo){
        List<PdProductsVoice>  volist=new ArrayList<PdProductsVoice>();
        volist=pdProductsVoiceMapper.findVoiceList();
        int size=volist.size();
        Long[] listFname = new Long[size];
        for (int i = 0; i < size; i++) {
            PdProductsVoice p=volist.get(i);
            if(null!=p){
                Integer vids = p.getId();
                Long vid=vids.longValue();
                listFname[i] = vid;
            }
        }
        Arrays.sort(listFname);
        Long[] listvoice = new Long[501]; // 先假定每个批次10个合成音
        int z  =pdProductsBatchAddVo.getSynthid(); // 后台输入框的值----声音

        if (z < 0) {
            z = -z;
        }
        if (z > listFname.length - 500) {
            // 判断是否大于合成音总数
            z = z - 500;
        }

        int s = z + 500;

        for (int i = z; i < s; i++, z++) {
            int l = 0;
            listvoice[l] = listFname[i] - 1;
            PdProductsBatchVoiceKey ps = new PdProductsBatchVoiceKey();
            ps.setBacthid(Integer.parseInt(pdProductsBatch.getId().toString()));
            Integer listvocies= listvoice[l].intValue();
            ps.setSynthid(listvocies);
            pdProductsBatchVoiceMapper.insertSelective(ps);
            l++;
        }
    }
}
