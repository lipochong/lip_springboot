package com.jkdl.springboot.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.DateTools;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.BusinessActivityMapper;
import com.jkdl.springboot.dao.UCompanyMapper;
import com.jkdl.springboot.domain.BusinessActivityAddVo;
import com.jkdl.springboot.domain.BusinessActivityListVo;
import com.jkdl.springboot.entity.BusinessActivity;
import com.jkdl.springboot.entity.UCompany;
import com.jkdl.springboot.exception.BusinessException;
import com.jkdl.springboot.service.BusinessActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class BusinessActivityImpl implements BusinessActivityService {

    @Autowired
    private BusinessActivityMapper businessActivityMapper;

    @Autowired
    private UCompanyMapper uCompanyMapper;

    @Override
    public Map<String, Object> uploadImg(MultipartFile file, HttpServletRequest request, String uploadDir) {


        // 获取文件名
        String fileName = file.getOriginalFilename();

        // 获取文件的后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));

        // 文件上传后的路径
        String filePath = uploadDir;
        // 解决中文问题，liunx下中文路径，图片显示问题
        fileName = DateTools.getMillis(new Date()) + suffixName;
        File dest = new File(filePath + fileName);
        // 检测是否存在目录
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String,Object> map = new HashMap<>();

        map.put("fileName",fileName);
        map.put("fileUrl", "https://www.mwsong.com/xcxfile/");
        return map;

    }

    @Override
    public JsonResult addActivity(BusinessActivityAddVo businessActivityAddVo) {

        BusinessActivity businessActivity = new BusinessActivity();

        BeanUtils.copyProperties(businessActivityAddVo, businessActivity);

        businessActivity.setStatus(0);
        businessActivity.setCreatetime(new Date());

        int insert = businessActivityMapper.insert(businessActivity);

        if(insert>0){
            return JsonResult.ok("添加成功");
        }else{
            return JsonResult.build("添加失败");
        }

    }

    @Override
    public List<BusinessActivity> activityList(BusinessActivityAddVo businessActivityAddVo) {
        List<BusinessActivity> list = businessActivityMapper.getActivityList(businessActivityAddVo);
        return list;
    }

    @Override
    public PageInfo<BusinessActivity> selectBusinessActivityList(BusinessActivityListVo businessActivityListVo) {

        PageHelper.startPage(businessActivityListVo.getPageNum(), businessActivityListVo.getPageSize());
        List<BusinessActivity> businessActivityList = businessActivityMapper.selectAllBusinessActivityList(businessActivityListVo);

        PageInfo<BusinessActivity> businessActivityPageInfo = new PageInfo<>(businessActivityList);
        return businessActivityPageInfo;
    }

    @Override
    public JsonResult activityDetails(BusinessActivityAddVo businessActivityAddVo) {
        BusinessActivity businessActivity = businessActivityMapper.selectByPrimaryKey(businessActivityAddVo.getId());
        if(null == businessActivity || null == businessActivity.getTitle()){
            return JsonResult.build("活动不存在");
        }
        businessActivity.setCompanyname(uCompanyMapper.selectByPrimaryKey(businessActivity.getQyid()).getCompanyname());
        return JsonResult.ok(businessActivity);
    }

    @Override
    public int joinActivity(BusinessActivityAddVo businessActivityAddVo) {


        return 0;
    }

    @Override
    public JsonResult updateActivityStatus(BusinessActivityAddVo businessActivityAddVo) {
        if(null == businessActivityAddVo.getId()){
            return JsonResult.build("活动id不能为空");
        }
        BusinessActivity businessActivity = businessActivityMapper.selectByPrimaryKey(businessActivityAddVo.getId());
        if(null == businessActivity){
            return JsonResult.build("活动不存在");
        }
        if(DateTools.calcYearTimes(businessActivity.getLasttime(), new Date()) > 1){
            businessActivity.setStatus(3);
            businessActivityMapper.updateByPrimaryKey(businessActivity);
            return JsonResult.build("活动已过期");
        }
        businessActivity.setStatus(businessActivityAddVo.getStatus());
        int count = businessActivityMapper.updateByPrimaryKey(businessActivity);
        if(count > 0){
            return JsonResult.ok("审核成功");
        }
        return JsonResult.build("审核失败");

    }
}
