package com.jkdl.springboot.service.impl;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.common.util.Md5Util;
import com.jkdl.springboot.dao.*;
import com.jkdl.springboot.domain.AuthoritiesQueryIdParentIdVo;
import com.jkdl.springboot.domain.SystemLoginVo;
import com.jkdl.springboot.domain.UCompanyQueryInfoVo;
import com.jkdl.springboot.domain.UsersIdVo;
import com.jkdl.springboot.domain.res.ResBaseAuthoritiesVo;
import com.jkdl.springboot.entity.*;
import com.jkdl.springboot.service.SystemUserRoleAuthService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SystemUserRoleAuthServiceImpl implements SystemUserRoleAuthService{

    @Autowired
    private USersMapper uSersMapper;

    @Autowired
    private AuthoritiesMapper authoritiesMapper;

    @Autowired
    private UCompanyMapper uCompanyMapper;

    @Autowired
    private UCompanyUserMapper uCompanyUserMapper;

    @Override
    public int systemLogin(SystemLoginVo systemLoginVo){
        //判断用户名密码  密码需要md5加密
        int id = 0;
        try {

            systemLoginVo.setPassword(Md5Util.encoderByMd5(systemLoginVo.getPassword()));
            UsersIdVo usersIdVo  = uSersMapper.systemLogin(systemLoginVo);
            if(usersIdVo!=null){
                id = usersIdVo.getId();
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(id!=0){

        }
        return id;

    }

    @Override
    public JsonResult getUsersAuthList(int id) {

        int roleId = 1;
        //根据id获取userType
        USers uSers = uSersMapper.selectByPrimaryKey(id);
        String userType = uSers.getUsertype();
        String loginName = uSers.getLoginName();
        //如果是0  1.如果是admin则roleId是1  否则roleId是7
        //如果是2  则roleId则是9
        if("0".equals(userType)){
            if("admin".equals(loginName)){
                roleId = 1;

            }else{
                roleId = 7;
            }
        }else if("2".equals(userType)){
            roleId = 9;
        }

        List<ResBaseAuthoritiesVo> list = new ArrayList<>();
        list = authoritiesMapper.selectAuthListByRoleId(roleId);
        List<Object> allList = new ArrayList<>();
        for(ResBaseAuthoritiesVo authorities : list){
            Map<String,Object> map = new HashMap<>();
            //每一个管理下面的二级菜单
            List<ResBaseAuthoritiesVo> secondList = new ArrayList<>();
            int authId = authorities.getId();
            int parentId = 0;
            AuthoritiesQueryIdParentIdVo authoritiesQueryIdParentIdVo = new AuthoritiesQueryIdParentIdVo();
            authoritiesQueryIdParentIdVo.setId(authId);
            authoritiesQueryIdParentIdVo.setParentId(parentId);
            secondList = authoritiesMapper.selectAuthListById(authoritiesQueryIdParentIdVo);
            map.put("oneName", authorities);
            map.put("secondList", secondList);
            allList.add(map);
        }
        return JsonResult.ok(allList);
    }

    /**
     * 根据登录名获取公司信息
     * @param loginName
     * @return
     */
    @Override
    public JsonResult getUsersAuthListByCode(String loginName,int id) {

        log.info("-----用户登录  从session获取loginName id-----");
        log.info(loginName);
        log.info("",id);

        USers uSers = uSersMapper.selectByPrimaryKey(id);
        int companyId = uSers.getCompanyid();
        UCompany uCompany = uCompanyMapper.selectByPrimaryKey(companyId);

        log.info("-----查询出来的用户信息-----",uSers);

        UCompanyQueryInfoVo uCompanyQueryInfoVo = new UCompanyQueryInfoVo();
        uCompanyQueryInfoVo.setCompanyId(companyId);

        UCompanyWithBLOBs uCompanyWithBLOBs = uCompanyMapper.selectUcompanyInfo(uCompanyQueryInfoVo);

        log.info("-----查询出来的用户信息-2----",uCompanyWithBLOBs);


        return JsonResult.ok(uCompanyWithBLOBs);
    }

    @Override
    public int systemUserLogin(SystemLoginVo systemLoginVo){
        //判断用户名密码  密码需要md5加密
        int id = 0;
        UsersIdVo usersIdVo  = uCompanyUserMapper.systemUserLogin(systemLoginVo);
        if(usersIdVo!=null){
            id = usersIdVo.getId();
        }

        if(id!=0){

        }
        return id;

    }
}
