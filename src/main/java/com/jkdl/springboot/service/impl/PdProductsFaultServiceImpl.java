package com.jkdl.springboot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.dao.PdProductsFaultMapper;
import com.jkdl.springboot.domain.PdProductsFaultAddVo;
import com.jkdl.springboot.domain.PdProductsFaultQueryVo;
import com.jkdl.springboot.entity.PdProductsFault;
import com.jkdl.springboot.service.PdProductsFaultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class PdProductsFaultServiceImpl implements PdProductsFaultService {

    @Autowired
    private PdProductsFaultMapper pdProductsFaultMapper;

    @Override
    public List<PdProductsFault> list(PdProductsFaultQueryVo pdProductsFaultQueryVo){
        return pdProductsFaultMapper.list(pdProductsFaultQueryVo);
    }
    @Override
    public PageInfo<PdProductsFault> selectPdProductsFaultList(PdProductsFaultQueryVo pdProductsFaultQueryVo){
        PageHelper.startPage(pdProductsFaultQueryVo.getPageNum(),pdProductsFaultQueryVo.getPageSize());
        List<PdProductsFault>  pdProductsFaultList=pdProductsFaultMapper.list(pdProductsFaultQueryVo);
        PageInfo<PdProductsFault> pdProductsFaultPageInfo=new PageInfo<>(pdProductsFaultList);
        return pdProductsFaultPageInfo;
    }

    @Override
    public int addPdProductsFault(PdProductsFaultAddVo pdProductsFaultAddVo){

        PdProductsFault pdProductsFault=new PdProductsFault();
        pdProductsFault.setBdelete(0);
        pdProductsFault.setProductportid(1);
        pdProductsFault.setCreatetime(new Date());
        BeanUtils.copyProperties(pdProductsFaultAddVo,pdProductsFault);
        return pdProductsFaultMapper.insertSelective(pdProductsFault);
    }
}
