package com.jkdl.springboot.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.dao.SysMessageMapper;
import com.jkdl.springboot.domain.SysMessageAddVo;
import com.jkdl.springboot.domain.SysMessageDeleteVo;
import com.jkdl.springboot.domain.SysMessageQueryVo;
import com.jkdl.springboot.entity.SysMessage;
import com.jkdl.springboot.service.SysMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;


@Slf4j
@Service
public class SysMessageImpl implements SysMessageService {

    @Autowired
    private SysMessageMapper sysMessageMapper;




    @Override
    public List<SysMessage> list(SysMessageQueryVo sysMessageQueryVo){
        return  sysMessageMapper.list(sysMessageQueryVo);
    }

    /**
     * 消息分页
     * @param sysMessageQueryVo
     * @return
     */
    @Override
    public PageInfo<SysMessage> sysMessageList(SysMessageQueryVo sysMessageQueryVo){
        PageHelper.startPage(sysMessageQueryVo.getPageNum(),sysMessageQueryVo.getPageSize());
        List<SysMessage> sysMessageList=sysMessageMapper.list(sysMessageQueryVo);
        PageInfo<SysMessage> sysMessagePageInfo=new PageInfo<>(sysMessageList);
        return  sysMessagePageInfo;
    }

    /**
     * 添加系统消息
     * @param sysMessageAddVo
     * @return
     */
    @Override
    public Integer addSysMessage(SysMessageAddVo sysMessageAddVo) {
        SysMessage sysMessage=new SysMessage();
        BeanUtils.copyProperties(sysMessageAddVo,sysMessage);
        sysMessage.setCreatetime(new Date());
        sysMessage.setBdelete(0);
        return sysMessageMapper.insertSelective(sysMessage);
    }

    /**
     * 修改系统消息
     * @param sysMessageAddVo
     * @return
     */
    @Override
    public Integer updateSysMessage(SysMessageAddVo sysMessageAddVo) {
        SysMessage sysMessage=sysMessageMapper.selectByPrimaryKey(sysMessageAddVo.getId());
        BeanUtils.copyProperties(sysMessageAddVo,sysMessage);
        return sysMessageMapper.updateByPrimaryKeySelective(sysMessage);
    }


    /**
     * 删除系统消息：修改bdelete为1
     * @param sysMessageDeleteVo
     * @return
     */
    @Override
    public Integer deleteSysMessage(SysMessageDeleteVo sysMessageDeleteVo) {
        SysMessage sysMessage=sysMessageMapper.selectByPrimaryKey(sysMessageDeleteVo.getId());
        BeanUtils.copyProperties(sysMessageDeleteVo,sysMessage);
        return sysMessageMapper.updateByPrimaryKeySelective(sysMessage);
    }

}
