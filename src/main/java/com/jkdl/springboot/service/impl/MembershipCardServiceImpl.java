package com.jkdl.springboot.service.impl;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.dao.MembershipCardMapper;
import com.jkdl.springboot.domain.membershipcard.MembershipCardLstVo;
import com.jkdl.springboot.domain.membershipcard.MembershipCardVo;
import com.jkdl.springboot.entity.MembershipCard;
import com.jkdl.springboot.service.MembershipCardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class MembershipCardServiceImpl implements MembershipCardService {

    @Autowired
    private MembershipCardMapper membershipCardMapper;

    @Override
    public JsonResult addMembershipCard(MembershipCard membershipCard) {
        membershipCard.setStatus(0);
        membershipCard.setCreateTime(new Date());

        int insert = membershipCardMapper.insert(membershipCard);

        if(insert > 0){
            return JsonResult.ok("添加成功");
        }else{
            return JsonResult.build("添加失败");
        }

    }

    @Override
    public MembershipCardLstVo getMembershipCardLst(MembershipCardVo membershipCardVo) {
        membershipCardVo.setStatus(1);
        List<MembershipCard> lst = membershipCardMapper.getMembershipCardList(membershipCardVo);
        List<Integer> lstMoney = membershipCardMapper.getMembershipCardListMoney(membershipCardVo.getCompanyId());
        MembershipCardLstVo mcLst = new MembershipCardLstVo();
        mcLst.setLst(lst);
        mcLst.setLstMoney(lstMoney);
        return mcLst;
    }
}
