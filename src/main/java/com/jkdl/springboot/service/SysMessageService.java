package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.domain.SysMessageAddVo;
import com.jkdl.springboot.domain.SysMessageDeleteVo;
import com.jkdl.springboot.domain.SysMessageQueryVo;
import com.jkdl.springboot.domain.USecondUserAddVo;
import com.jkdl.springboot.entity.SysMessage;

import java.util.List;
import java.util.Map;

public interface SysMessageService {

    List<SysMessage> list(SysMessageQueryVo sysMessageQueryVo);

    /**
     * 消息分页
     * @param sysMessageQueryVo
     * @return
     */
    PageInfo<SysMessage> sysMessageList(SysMessageQueryVo sysMessageQueryVo);

    /**
     * 添加系统消息
     */
    Integer addSysMessage(SysMessageAddVo sysMessageAddVo) ;


    /**
     * 修改系统消息
     */
    Integer updateSysMessage(SysMessageAddVo sysMessageAddVo) ;

    /**
     * 删除系统消息：修改bdelete为1
     */
    Integer deleteSysMessage(SysMessageDeleteVo sysMessageDeleteVo);



}
