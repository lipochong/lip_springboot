package com.jkdl.springboot.service;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.UCompanyUserCodeVo;
import com.jkdl.springboot.domain.UsersIdVo;
import com.jkdl.springboot.domain.UsersPhoneVo;
import com.jkdl.springboot.domain.UsersWxVo;
import com.jkdl.springboot.entity.UCompanyUser;

public interface UCompanyUserService {

    /**
     * 添加
     * @param uCompanyUser
     * @return
     */
    int  add(UCompanyUser uCompanyUser);

    /**
     * 添加用户
     * @param usersPhoneVo
     * @return
     */
    JsonResult addCompanyUser(UsersPhoneVo usersPhoneVo) throws Exception;

    /**
     * 获取验证码
     * @param usersPhoneVo
     * @return
     */
    JsonResult verificationCode(UsersPhoneVo usersPhoneVo, Integer appid, String appkey);

    /**
     * 添加公司unionid
     * @param usersWxVo
     * @return
     */
    JsonResult addUCompanyUserOpenid(UsersWxVo usersWxVo);

    /**
     * 退出微信下级用户登录
     * @return
     */
    JsonResult quitXcxUserLogin(UsersIdVo usersIdVo);

    /**
     * 忘记密码获取验证码
     * @param phone
     * @param appid
     * @param appkey
     * @return
     */
    JsonResult getCode(String phone, Integer appid, String appkey);

    /**
     * 设置密码
     * @param CompanyUserCodeVo
     * @return
     */
    JsonResult updatePassword(UCompanyUserCodeVo CompanyUserCodeVo) throws Exception;
}
