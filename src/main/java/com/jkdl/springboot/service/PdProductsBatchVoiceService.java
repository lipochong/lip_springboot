package com.jkdl.springboot.service;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryVo;
import com.jkdl.springboot.domain.PdVoiceQueryByBatchNumberVo;
import com.jkdl.springboot.entity.PdProductsBatchVoiceKey;

import java.util.List;

public interface PdProductsBatchVoiceService {
    /**
     * 获取批次对应合成音信息
     * @param pdProductsBatchVoiceQueryVo
     * @return
     */

    List<PdProductsBatchVoiceKey> list(PdProductsBatchVoiceQueryVo pdProductsBatchVoiceQueryVo);

    /**
     * 获取批次对应合成音列表分页
     * @param pdProductsBatchVoiceQueryVo
     * @return
     */

    PageInfo<PdProductsBatchVoiceKey> selectPdProductsBatchVoiceList(PdProductsBatchVoiceQueryVo pdProductsBatchVoiceQueryVo);

    /**
     * 获取音频地址
     * @param pdVoiceQueryByBatchNumberVo
     * @return
     */
    JsonResult getAudioVoice(PdVoiceQueryByBatchNumberVo pdVoiceQueryByBatchNumberVo);

}
