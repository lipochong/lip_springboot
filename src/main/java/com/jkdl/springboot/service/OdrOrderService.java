package com.jkdl.springboot.service;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.OdrOrder;
import com.jkdl.springboot.entity.OdrOrderExample;
import com.jkdl.springboot.entity.OdrOrderGroup;
import com.jkdl.springboot.entity.OrderReportdatailExample;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface OdrOrderService {
    /**
     * 获取订单信息
     * @param odrOrderQueryVo
     * @return
     */

    List<OdrOrder> list(OdrOrderQueryVo odrOrderQueryVo);

    /**
     * 获取订单信息列表分页
     * @param odrOrderQueryVo
     * @return
     */

    PageInfo<OdrOrder> selectOdrOrderList(OdrOrderQueryVo odrOrderQueryVo);

    /**
     * 创建预支付订单
     * @param xcxCreateOrderVo
     * @return
     */
    Map<String,Object> createOrder(XcxCreateOrderVo xcxCreateOrderVo);

    /**
     * 小程序支付
     * @param xcxPayOrderVo
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    String payOrder(XcxPayOrderVo xcxPayOrderVo) throws JDOMException, IOException;

    /**
     * 支付回调
     * @param resultStr
     * @return
     */
    String weixinNotify(String resultStr);


    JsonResult orderList(OrderListQueryVo orderListQueryVo);

    JsonResult updateOrderStatus(OrderListVo orderListVo);

    List<OdrOrder> orderInfoList(CloseAnAccountVo closeAnAccountVo);

    //查询出最小日期 和最大日期
    List<OdrOrder> orderDateInfoList();
    //查询订单总金额
    OrderReportdatailExample gettotalMoney(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);
    //查询折线图数据
    List<OdrOrderGroup> getreportFormsList(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);
}
