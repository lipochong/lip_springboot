package com.jkdl.springboot.service;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.domain.PdProductsFaultAddVo;
import com.jkdl.springboot.domain.PdProductsFaultQueryVo;
import com.jkdl.springboot.entity.PdProductsFault;

import java.util.List;

public interface PdProductsFaultService {
    /**
     * 获取故障设备信息
     * @param pdProductsFaultQueryVo
     * @return
     */

    List<PdProductsFault> list(PdProductsFaultQueryVo pdProductsFaultQueryVo);

    /**
     * 获取故障设备列表分页
     * @param pdProductsFaultQueryVo
     * @return
     */

    PageInfo<PdProductsFault> selectPdProductsFaultList(PdProductsFaultQueryVo pdProductsFaultQueryVo);

    /**
     * 新增故障申报
     * @param pdProductsFaultAddVo
     * @return
     */
    int addPdProductsFault(PdProductsFaultAddVo pdProductsFaultAddVo);

}
