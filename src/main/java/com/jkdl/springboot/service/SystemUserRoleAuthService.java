package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.Authorities;
import com.jkdl.springboot.entity.USers;

import java.util.List;

public interface SystemUserRoleAuthService {

    /**
     * 用户登录
     */
    int systemLogin(SystemLoginVo systemLoginVo);

    /**
     * 根据用户id获取权限
     */
    JsonResult getUsersAuthList(int id);

    /**
     * 根据登录名获取用户权限（左边导航栏）
     * @param loginName
     * @return
     */
    JsonResult getUsersAuthListByCode(String loginName,int id);

    /**
     * 商家端下级用户登录
     */
    int systemUserLogin(SystemLoginVo systemLoginVo);
}
