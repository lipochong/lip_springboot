package com.jkdl.springboot.service;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.domain.OdrOrderQueryVo;
import com.jkdl.springboot.domain.XcxCreateOrderVo;
import com.jkdl.springboot.domain.XcxPayOrderVo;
import com.jkdl.springboot.entity.OdrOrder;
import com.jkdl.springboot.entity.SysProvince;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface SysProvinceService {

    List<SysProvince> getAllProvince();

}
