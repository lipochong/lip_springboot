package com.jkdl.springboot.service;
import com.jkdl.springboot.domain.SysDistrictQueryVo;
import com.jkdl.springboot.entity.SysCity;
import com.jkdl.springboot.entity.SysDistrict;

import java.util.List;

public interface SysDistrictService {

    List<SysDistrict> getAllDistrictByCityId(SysDistrictQueryVo sysDistrictQueryVo);

}
