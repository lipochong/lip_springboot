package com.jkdl.springboot.service;
import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.domain.PdProductsBatchAddVo;
import com.jkdl.springboot.domain.PdProductsBatchQueryVo;
import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryVo;
import com.jkdl.springboot.entity.PdProductsBatch;
import com.jkdl.springboot.entity.PdProductsBatchVoiceKey;

import java.util.List;

public interface PdProductsBatchService {
    /**
     * 获取批次管理信息
     * @param pdProductsBatchQueryVo
     * @return
     */

    List<PdProductsBatch> list(PdProductsBatchQueryVo pdProductsBatchQueryVo);

    /**
     * 获取批次管理列表分页
     * @param pdProductsBatchQueryVo
     * @return
     */

    PageInfo<PdProductsBatch> selectPdProductsBatchList(PdProductsBatchQueryVo pdProductsBatchQueryVo);

    /**
     * 新增批次管理
     * @param pdProductsBatchAddVo
     * @return
     */
    int addPdProductsBatch(PdProductsBatchAddVo pdProductsBatchAddVo);

    void savevoice(PdProductsBatch pdProductsBatch,PdProductsBatchAddVo pdProductsBatchAddVo);

}
