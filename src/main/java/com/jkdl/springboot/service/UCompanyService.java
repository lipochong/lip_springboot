package com.jkdl.springboot.service;

import com.github.pagehelper.PageInfo;
import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.SettleAccounts;
import com.jkdl.springboot.entity.UCompany;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface UCompanyService {

    /**
     * 添加用户信息
     */
    Long add(UCompany uCompany) throws Exception;

    /**
     * 获取用户列表
     */
    List<UCompany> selectAllUCompanyList(UCompanyQueryVo uCompanyQueryVo);

    /**
     * 获取用户列表分页
     */
    PageInfo<UCompany> selectUCompanyList(UCompanyQueryVo uCompanyQueryVo);

    /**
     * 根据登录名查询（判断添加用户的时候是否登录名重复）
     */
    int queryUCompanyByCode(UCompanyCodeVo code);

    /**
     * 新增保存公司
     */
    int addUCompany(UCompanyAddVo uCompanyAddVo) throws Exception;

    /**
     * 修改保存公司
     */
    JsonResult updateUCompany(UCompanyUpdateVo uCompanyUpdateVo);

    /**
     * 删除公司（修改bdelete字段）
     */
    int deleteUCompany(UCompanyDeleteVo uCompanyDeleteVo);


    /**
     * 测试添加grandParentId
     */
    int addGrandParentId();

    /**
     * 获取代理商
     */
    List<UCompany> getAgentList(UCompanyQueryPlatformVo uCompanyQueryPlatformVo);

    /**
     * 获取所有代理商和区域代理商  5 20
     */
    JsonResult getAllAgent();


    /**
     * 东莞10月订单情况
     */
     JsonResult getDongguanOrder(DongguanOVo dongguanOVo);


    /**
     * @param code
     * @author XOu
     * @description 判断用户是否已经注册
     */
    Map<String, Object> checkoutUCompanyExist(String code);

    /**
     * 微信unionid
     * @param usersWxVo
     * @return
     */
    int loginAuth(UsersWxVo usersWxVo) throws Exception;


    /**
     * 添加公司unionid
     * @param usersWxVo
     * @return
     */
    JsonResult addUCompanyOpenid(UsersWxVo usersWxVo);

    /**
     * 获取公司
     */
    List<UCompany> getAgencyCompanyList(CloseAnAccountVo closeAnAccountVo);


    /**
     * 获取结算列表
     */
    List<SettleAccounts> getJiesuanList(CloseAnAccountVo closeAnAccountVo);

    /**
     * 结算导出
     */
    HSSFWorkbook exportJiesuan(CloseAnAccountVo closeAnAccountVo,HttpServletResponse response) throws ParseException;

    UCompany getUCompany(int id);

    String getCompanyName(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);
    /**
     * 通过商户名获取设备数量
     */
    int getProductCountByName(PdProductCountQueryVo pdProductCountQueryVo);

    /**
     * 退出微信登录
     * @return
     */
    JsonResult quitXcxLogin(UsersIdVo usersIdVo);

}
