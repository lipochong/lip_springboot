package com.jkdl.springboot.service;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;

import java.util.Map;

public interface USecondUserService {

    /**
     * @param code 用户code
     * @author XOu
     * @description 判断用户是否已经注册
     */
    Map<String, Object> checkoutUserExist(String code);

    /**
     * 微信unionid
     * @param usersWxVo
     * @return
     */
    int loginAuth(UsersWxVo usersWxVo) throws Exception;

    /**
     * 添加微信用户
     */
    Integer addUser(USecondUserAddVo uSecondUserAddVo) throws Exception;

    JsonResult uSecondUserList(USecondUserListQueryVo uSecondUserListQueryVo);

    JsonResult updateUSeconsUserSmlock(USecondUserListQueryVo uSecondUserListQueryVo);

}
