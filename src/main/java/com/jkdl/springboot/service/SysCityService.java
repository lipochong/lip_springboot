package com.jkdl.springboot.service;
import com.jkdl.springboot.domain.SysCityQueryVo;
import com.jkdl.springboot.domain.SysDistrictQueryVo;
import com.jkdl.springboot.entity.SysCity;
import com.jkdl.springboot.entity.SysProvince;

import java.util.List;

public interface SysCityService{

    List<SysCity> getAllCityByProvinceId(SysCityQueryVo sysCityQueryVo);

}
