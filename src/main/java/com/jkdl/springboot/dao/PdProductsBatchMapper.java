package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.PdProductsBatchQueryVo;
import com.jkdl.springboot.domain.PdProductsBatchQueryidVo;
import com.jkdl.springboot.entity.PdProductsBatch;
import com.jkdl.springboot.entity.PdProductsBatchExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface PdProductsBatchMapper {
    int deleteByPrimaryKey(Double id);

    int insert(PdProductsBatch record);

    int insertSelective(PdProductsBatch record);

    List<PdProductsBatch> selectByExample(PdProductsBatchExample example);

    PdProductsBatch selectByPrimaryKey(Double id);

    int updateByPrimaryKeySelective(PdProductsBatch record);

    int updateByPrimaryKey(PdProductsBatch record);

    List<PdProductsBatch> list(PdProductsBatchQueryVo pdProductsBatchQueryVo);

    PdProductsBatch findBatchnumber(PdProductsBatchQueryidVo pdProductsBatchQueryidVo);
}