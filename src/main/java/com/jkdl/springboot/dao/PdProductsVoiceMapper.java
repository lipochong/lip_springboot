package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.PdProductsVoiceQueryVo;
import com.jkdl.springboot.entity.PdProductsVoice;
import com.jkdl.springboot.entity.PdProductsVoiceExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface PdProductsVoiceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PdProductsVoice record);

    int insertSelective(PdProductsVoice record);

    List<PdProductsVoice> selectByExample(PdProductsVoiceExample example);

    PdProductsVoice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PdProductsVoice record);

    int updateByPrimaryKey(PdProductsVoice record);

    List<PdProductsVoice> findVoiceList();

    List<PdProductsVoice> getSynthsrcByBatchNumber(Long batchNumber);
}