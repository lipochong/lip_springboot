package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.BusinessActivityMemberIdListVo;
import com.jkdl.springboot.domain.BusinessActivityMemberListVo;
import com.jkdl.springboot.domain.BusinessActivityMemberVo;
import com.jkdl.springboot.domain.BusinessActivityMemberWriteoffVo;
import com.jkdl.springboot.entity.BusinessActivityMember;
import com.jkdl.springboot.entity.BusinessActivityMemberExample;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface BusinessActivityMemberMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BusinessActivityMember record);

    int insertSelective(BusinessActivityMember record);

    List<BusinessActivityMember> selectByExample(BusinessActivityMemberExample example);

    BusinessActivityMember selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BusinessActivityMember record);

    int updateByPrimaryKey(BusinessActivityMember record);

    BusinessActivityMemberVo activityMemberList(BusinessActivityMemberVo businessActivityMemberVo);

    List<BusinessActivityMember> selectBusinessActivityMemberList(BusinessActivityMemberListVo businessActivityMemberListVo);

    BusinessActivityMember getActivityMemberDetails(BusinessActivityMemberVo businessActivityMemberVo);

    List<BusinessActivityMember> getActivityMemberList(BusinessActivityMemberVo businessActivityMemberVo);

    List<BusinessActivityMemberIdListVo> getBusinessActivityMemberList(Integer id);

    List<BusinessActivityMemberWriteoffVo> getBusinessActivityMemberWriteOffList(Integer id);

    /**
     * 查看是否已被领取
     * @param id
     * @return
     */
    int getMemberCount(@Param("id") Integer id);
}