package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.OdrOrder;
import com.jkdl.springboot.entity.OdrOrderExample;
import com.jkdl.springboot.entity.OdrOrderGroup;
import com.jkdl.springboot.entity.OrderReportdatailExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface OdrOrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OdrOrder record);

    int insertSelective(OdrOrder record);

    List<OdrOrder> selectByExample(OdrOrderExample example);

    OdrOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OdrOrder record);

    int updateByPrimaryKey(OdrOrder record);

    List<OdrOrder> list(OdrOrderQueryVo odrOrderQueryVo);

    List<OdrOrder> listByCompanyid(OdrOrderdetailQueryQyidsVo odrOrderdetailQueryQyidsVo);

    OdrOrder selectOdOrderByOrderId(OdrOrderdetailQueryByOrderIdVo odrOrderdetailQueryByOrderIdVo);

    List<OdrOrderListQueryVo> orderList(OrderListQueryVo orderListQueryVo);

    long orderListTotal(OrderListQueryVo orderListQueryVo);

    List<OdrOrder> orderListOneCompany(OrderListQueryVo orderListQueryVo);

    List<DongguanOrderQueryVo> getDongguanOrder(DongguanOrderVo dongguanOrderVo);

    int getDongguanOrderCount(DongguanOrderVo dongguanOrderVo);

    /**
     * 商家端  每天/月订单数量
     */
    int orderCount(OdrOrderDayCountVo odrOrderDayCountVo);

    List<OdrOrder> orderInfoList(CloseAnAccountVo closeAnAccountVo);
    List<OdrOrder> orderDateInfoList();
    //查询订单总金额
    OrderReportdatailExample gettotalMoney(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);
    //查询折线图数据
    List<OdrOrderGroup> getreportFormsList(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);
}