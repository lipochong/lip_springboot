package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.SystemLoginVo;
import com.jkdl.springboot.domain.UCompanyUserQueryVo;
import com.jkdl.springboot.domain.UsersIdVo;
import com.jkdl.springboot.entity.UCompanyUser;
import com.jkdl.springboot.entity.UCompanyUserExample;
import com.jkdl.springboot.entity.UCompanyWithBLOBs;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UCompanyUserMapper {

    int insert(UCompanyUser record);

    int insertSelective(UCompanyUser record);

    List<UCompanyUser> selectByExample(UCompanyUserExample example);

    UCompanyUser selectById(Integer id);

    UCompanyUser selectByOpenId(String openid);

    int selectByPhone(String phone);

    int updateByPrimaryKey(UCompanyUser uCompanyUser);

    int updateByPrimaryKeySelective(UCompanyUser uCompanyUser);

    /**
     * 登录
     * @param systemLoginVo
     * @return
     */
    UsersIdVo systemUserLogin(SystemLoginVo systemLoginVo);

    /**
     * 退出小程序登录
     */
    int updateByXcxOpenid(UsersIdVo usersIdVo);

    /**
     * 设置密码
     * @param systemLoginVo
     * @return
     */
    int updatePassword(SystemLoginVo systemLoginVo);
}