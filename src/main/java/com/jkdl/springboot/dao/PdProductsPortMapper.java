package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.PdProductPortQueryListAo;
import com.jkdl.springboot.entity.PdProductsPort;
import com.jkdl.springboot.entity.PdProductsPortExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface PdProductsPortMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PdProductsPort record);

    int insertSelective(PdProductsPort record);

    List<PdProductsPort> selectByExample(PdProductsPortExample example);

    PdProductsPort selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PdProductsPort record);

    int updateByPrimaryKey(PdProductsPort record);

    List<PdProductsPort> selectProductPortsByIdAndStatusSf(PdProductPortQueryListAo pdProductPortQueryListAo);


}