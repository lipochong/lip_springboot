package com.jkdl.springboot.dao;

import com.jkdl.springboot.entity.MembershipCardUser;
import com.jkdl.springboot.entity.MembershipCardUserExample;
import java.util.List;

public interface MembershipCardUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MembershipCardUser record);

    int insertSelective(MembershipCardUser record);

    List<MembershipCardUser> selectByExample(MembershipCardUserExample example);

    MembershipCardUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MembershipCardUser record);

    int updateByPrimaryKey(MembershipCardUser record);
}