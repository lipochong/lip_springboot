package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryBatchidVo;
import com.jkdl.springboot.domain.PdProductsBatchVoiceQueryVo;
import com.jkdl.springboot.entity.PdProductsBatchVoiceExample;
import com.jkdl.springboot.entity.PdProductsBatchVoiceKey;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface PdProductsBatchVoiceMapper {
    int deleteByPrimaryKey(PdProductsBatchVoiceKey key);

    int insert(PdProductsBatchVoiceKey record);

    int insertSelective(PdProductsBatchVoiceKey record);

    List<PdProductsBatchVoiceKey> selectByExample(PdProductsBatchVoiceExample example);

    List<PdProductsBatchVoiceKey> list(PdProductsBatchVoiceQueryVo pdProductsBatchVoiceQueryVo);

    /*List<PdProductsBatchVoiceKey> findbacthid(Integer batchid);*/

    List<PdProductsBatchVoiceKey> findbacthid(PdProductsBatchVoiceQueryBatchidVo pdProductsBatchVoiceQueryBatchidVo);
}