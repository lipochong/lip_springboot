package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.SysDistrictQueryVo;
import com.jkdl.springboot.entity.SysDistrict;
import com.jkdl.springboot.entity.SysDistrictExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SysDistrictMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysDistrict record);

    int insertSelective(SysDistrict record);

    List<SysDistrict> selectByExample(SysDistrictExample example);

    SysDistrict selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysDistrict record);

    int updateByPrimaryKey(SysDistrict record);

    List<SysDistrict> getAllDistrictByCityId(SysDistrictQueryVo sysDistrictQueryVo);
}