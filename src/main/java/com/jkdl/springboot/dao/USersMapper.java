package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.SystemLoginVo;
import com.jkdl.springboot.domain.UCompanyAddVo;
import com.jkdl.springboot.domain.UsersIdVo;
import com.jkdl.springboot.domain.UsersQueryVo;
import com.jkdl.springboot.entity.USers;
import com.jkdl.springboot.entity.USersExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface USersMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(USers record);

    int insertSelective(USers record);

    List<USers> selectByExample(USersExample example);

    USers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(USers record);

    int updateByPrimaryKey(USers record);

    List<USers> list(UsersQueryVo usersQueryVo);

    /**
     * 登录
     * @param systemLoginVo
     * @return
     */
    UsersIdVo systemLogin(SystemLoginVo systemLoginVo);

    /**
     * 添加用户
     */
    int addUsers(USers uSers);

}