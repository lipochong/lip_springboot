package com.jkdl.springboot.dao;

import com.jkdl.springboot.entity.MCardUserRecord;
import com.jkdl.springboot.entity.MCardUserRecordExample;
import java.util.List;

public interface MCardUserRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MCardUserRecord record);

    int insertSelective(MCardUserRecord record);

    List<MCardUserRecord> selectByExample(MCardUserRecordExample example);

    MCardUserRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MCardUserRecord record);

    int updateByPrimaryKey(MCardUserRecord record);
}