package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.PdProducts;
import com.jkdl.springboot.entity.PdProductsExample;
import com.jkdl.springboot.entity.PdProductsWithBLOBs;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface PdProductsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PdProductsWithBLOBs record);

    int insertSelective(PdProductsWithBLOBs record);

    List<PdProductsWithBLOBs> selectByExampleWithBLOBs(PdProductsExample example);

    List<PdProducts> selectByExample(PdProductsExample example);

    PdProductsWithBLOBs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PdProductsWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PdProductsWithBLOBs record);

    int updateByPrimaryKey(PdProducts record);

    List<PdProductsWithBLOBs> list(PdProductsQueryVo pdProductsQueryVo);

    List<PdProducts> listByCompanyid(OdrOrderdetailQueryQyidsVo odrOrderdetailQueryQyidsVo);

    UCompanyChargingVo getProductByProductId(PdProductSelectByIdVo productId);

    List<PdProducts> getmachineCount(CloseAnAccountVo closeAnAccountVo);
    //查询设备总数量
    int getequipmentNumber(OdrOrderdetailQueryCompanyidListVo odrOrderdetailQueryCompanyidListVo);
    List<String> getproductAll();
    int getcompanypdProductCount(OdrOrderdetailQueryCompanyidListVo odrOrderdetailQueryCompanyidListVo);


}