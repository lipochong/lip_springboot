package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.OdrOrderdetail;
import com.jkdl.springboot.entity.OdrOrderdetailExample;
import com.jkdl.springboot.entity.OdrOrderdetailWithBLOBs;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface OdrOrderdetailMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OdrOrderdetail record);

    int insertSelective(OdrOrderdetail record);

    List<OdrOrderdetail> selectByExample(OdrOrderdetailExample example);

    OdrOrderdetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OdrOrderdetail record);

    int updateByPrimaryKey(OdrOrderdetail record);

    List<OdrOrderdetail> list(OdrOrderdetailQueryVo odrOrderdetailQueryVo);

    List<OdrOrderdetailWithBLOBs> listWithBLOBs(OdrOrderdetailExportQueryVo odrOrderdetailExportQueryVo);

    List<OdrOrderdetailWithBLOBs> listByOpenid(OdrOrderdetailQueryOpenidVo odrOrderdetailQueryOpenidVo);

    OdrOrderdetail selectOdrOrderdetailByOrderId(OdrOrderdetailQueryByOrderIdVo odrOrderdetailQueryByOrderIdVo);

    OdrOrderdetail isOrderAndEnd(XcxIsOrderAndOrderEndVo xcxIsOrderAndOrderEndVo);

    List<OdrOrderdetail> orderList();

    List<OdrOrderdetail> selectOdrOrderdetailByProductId(UCompanyProductIdVo uCompanyProductIdVo);


    /**
     *   根据条件查询设备使用数量
     */

    List<String> getproductNumber(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);

    /**
     *   用于查询罗湖区大厅设备使用率
     */

    List<String> getproductLuohuTingCount(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);


}