package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.PdProductsFaultQueryVo;
import com.jkdl.springboot.entity.PdProductsFault;
import com.jkdl.springboot.entity.PdProductsFaultExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface PdProductsFaultMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PdProductsFault record);

    int insertSelective(PdProductsFault record);

    List<PdProductsFault> selectByExample(PdProductsFaultExample example);

    PdProductsFault selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PdProductsFault record);

    int updateByPrimaryKey(PdProductsFault record);

    List<PdProductsFault>  list(PdProductsFaultQueryVo pdProductsFaultQueryVo);
}