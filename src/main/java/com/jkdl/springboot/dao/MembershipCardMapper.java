package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.membershipcard.MembershipCardVo;
import com.jkdl.springboot.entity.MembershipCard;
import com.jkdl.springboot.entity.MembershipCardExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MembershipCardMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MembershipCard record);

    int insertSelective(MembershipCard record);

    List<MembershipCard> selectByExample(MembershipCardExample example);

    MembershipCard selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MembershipCard record);

    int updateByPrimaryKey(MembershipCard record);

    /**
     * 获取发布会员卡
     * @param membershipCardVo
     * @return
     */
    List<MembershipCard> getMembershipCardList(MembershipCardVo membershipCardVo);

    /**
     * 获取卡金额
     * @param companyId
     * @return
     */
    List<Integer> getMembershipCardListMoney(Integer companyId);
}