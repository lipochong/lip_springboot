package com.jkdl.springboot.dao;

import com.jkdl.springboot.entity.UsersRoles;
import com.jkdl.springboot.entity.UsersRolesExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UsersRolesMapper {
    int insert(UsersRoles record);

    int insertSelective(UsersRoles record);

    List<UsersRoles> selectByExample(UsersRolesExample example);
}