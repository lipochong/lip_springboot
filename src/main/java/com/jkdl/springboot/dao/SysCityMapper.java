package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.SysCityQueryVo;
import com.jkdl.springboot.entity.SysCity;
import com.jkdl.springboot.entity.SysCityExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SysCityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysCity record);

    int insertSelective(SysCity record);

    List<SysCity> selectByExample(SysCityExample example);

    SysCity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysCity record);

    int updateByPrimaryKey(SysCity record);

    List<SysCity> getAllCityByProvinceId(SysCityQueryVo sysCityQueryVo);
}