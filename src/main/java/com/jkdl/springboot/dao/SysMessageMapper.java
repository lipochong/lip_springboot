package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.SysMessageQueryVo;
import com.jkdl.springboot.entity.SysMessage;
import com.jkdl.springboot.entity.SysMessageExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
@Repository
@Mapper
public interface SysMessageMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysMessage record);

    int insertSelective(SysMessage record);

    List<SysMessage> selectByExample(SysMessageExample example);

    SysMessage selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysMessage record);

    int updateByPrimaryKey(SysMessage record);

    List<SysMessage> list(SysMessageQueryVo sysMessageQueryVo);
}