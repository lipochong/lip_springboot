package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.AllSecondUserCountVo;
import com.jkdl.springboot.domain.SelectAllWxUsersCountVo;
import com.jkdl.springboot.domain.USecondUserListQueryVo;
import com.jkdl.springboot.entity.USecondUsers;
import com.jkdl.springboot.entity.USecondUsersExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface USecondUsersMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(USecondUsers record);

    int insertSelective(USecondUsers record);

    List<USecondUsers> selectByExampleWithBLOBs(USecondUsersExample example);

    List<USecondUsers> selectByExample(USecondUsersExample example);

    USecondUsers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(USecondUsers record);

    int updateByPrimaryKeyWithBLOBs(USecondUsers record);

    int updateByPrimaryKey(USecondUsers record);

    Integer selectAllSecondUserCount(USecondUserListQueryVo uSecondUserListQueryVo);

    List<USecondUsers> selectUSecondUserList(USecondUserListQueryVo uSecondUserListQueryVo);
}