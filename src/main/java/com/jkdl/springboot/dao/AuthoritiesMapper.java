package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.AuthoritiesQueryIdParentIdVo;
import com.jkdl.springboot.domain.res.ResBaseAuthoritiesVo;
import com.jkdl.springboot.entity.Authorities;
import com.jkdl.springboot.entity.AuthoritiesExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AuthoritiesMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Authorities record);

    int insertSelective(Authorities record);

    List<Authorities> selectByExample(AuthoritiesExample example);

    Authorities selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Authorities record);

    int updateByPrimaryKey(Authorities record);

    List<ResBaseAuthoritiesVo> selectAuthListByRoleId(int roleId);

    List<ResBaseAuthoritiesVo> selectAuthListById(AuthoritiesQueryIdParentIdVo authoritiesQueryIdParentIdVo);
}