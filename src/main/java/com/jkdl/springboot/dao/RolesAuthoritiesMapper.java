package com.jkdl.springboot.dao;

import com.jkdl.springboot.entity.Authorities;
import com.jkdl.springboot.entity.RolesAuthoritiesExample;
import com.jkdl.springboot.entity.RolesAuthoritiesKey;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface RolesAuthoritiesMapper {
    int deleteByPrimaryKey(RolesAuthoritiesKey key);

    int insert(RolesAuthoritiesKey record);

    int insertSelective(RolesAuthoritiesKey record);

    List<RolesAuthoritiesKey> selectByExample(RolesAuthoritiesExample example);

    List<RolesAuthoritiesKey> selectAuthListByRoleId(int roleId);
}