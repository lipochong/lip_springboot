package com.jkdl.springboot.dao;

import com.jkdl.springboot.common.util.JsonResult;
import com.jkdl.springboot.domain.*;
import com.jkdl.springboot.entity.UCompany;
import com.jkdl.springboot.entity.UCompanyExample;
import com.jkdl.springboot.entity.UCompanyWithBLOBs;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UCompanyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UCompanyWithBLOBs record);

    int insertSelective(UCompanyWithBLOBs record);

    List<UCompanyWithBLOBs> selectByExampleWithBLOBs(UCompanyExample example);

    List<UCompany> selectByExample(UCompanyExample example);

    UCompanyWithBLOBs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UCompanyWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(UCompanyWithBLOBs record);

    int updateByPrimaryKey(UCompany record);

    List<UCompany> list();

    int queryUCompanyByCode(UCompanyCodeVo uCompanyCodeVo);

    List<UCompanyWithBLOBs> listByParentid(OdrOrderdetailQueryCompanyidVo odrOrderdetailQueryCompanyidVo);

    List<UCompanyWithBLOBs> listByParentidIn(OdrOrderdetailQueryCompanyidListVo odrOrderdetailQueryCompanyidListVo);

    List<UCompany> selectAllPlatformUCompanyList();

    List<UCompany> selectAllUCompanyList(UCompanyQueryVo uCompanyQueryVo);

    List<UCompany> addGrandParentId();

    List<UCompany> selectUCompanyListByParendIdAndCompanyType(UCompanyQueryPlatformVo uCompanyQueryPlatformVo);

    int updateUCompanyGrandParentId(UCompanyUpdateGrandParentIdVo uCompanyUpdateGrandParentIdVo);

    List<UCompany> getAllAgent();

    List<UCompany> selectUcompanyByCompanyName(UCompanyCompanynameVo uCompanyCompanynameVo);

    UCompanyWithBLOBs selectUcompanyInfo(UCompanyQueryInfoVo uCompanyQueryInfoVo);

    UCompany selectByOpenid(String openid);

    List<UCompany> getAgencyCompanyList(CloseAnAccountVo closeAnAccountVo);

    /**
     * 获取结算列表
     */
    JsonResult getJiesuanList(CloseAnAccountVo closeAnAccountVo);
    /**
     * 获取名称
     */
    String getCompanyName(OdrOrderdetailReportdatailVo odrOrderdetailReportdatailVo);
    /**
     * 通过商户名获取设备数量
     */
    int getProductCountByName(PdProductCountQueryVo pdProductCountQueryVo);

    /**
     * 退出小程序登录
     */
    int updateByXcxOpeeid(UsersIdVo usersIdVo);

}