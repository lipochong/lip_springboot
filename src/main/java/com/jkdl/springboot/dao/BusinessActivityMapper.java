package com.jkdl.springboot.dao;

import com.jkdl.springboot.domain.BusinessActivityAddVo;
import com.jkdl.springboot.domain.BusinessActivityListVo;
import com.jkdl.springboot.entity.BusinessActivity;
import com.jkdl.springboot.entity.BusinessActivityExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface BusinessActivityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BusinessActivity record);

    int insertSelective(BusinessActivity record);

    List<BusinessActivity> selectByExample(BusinessActivityExample example);

    BusinessActivity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BusinessActivity record);

    int updateByPrimaryKey(BusinessActivity record);

    List<BusinessActivity> getActivityList(BusinessActivityAddVo businessActivityAddVo);

    List<BusinessActivity> selectAllBusinessActivityList(BusinessActivityListVo businessActivityListVo);

}