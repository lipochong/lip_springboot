package com.jkdl.springboot.common.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderMasterUtil {
	public static SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmm");
	public static DecimalFormat df1=new DecimalFormat("000");
	public static DecimalFormat df2=new DecimalFormat("00");
	public static Date lastDate=null;
	//生成订单号
	public synchronized static String createOrderId(Date date,Integer qyid){
		StringBuffer orderid=new StringBuffer();
		String dateStr=formatDate(date);
		orderid.append("I");
		orderid.append(dateStr.substring(0,6));
//		orderid.append(df1.format(qyid)); //去掉企业判断
		orderid.append(dateStr.substring(6));
		orderid.append(createId());
		return orderid.toString();
	}
	//生成验证码
	public synchronized static String createCode(Date date){
		StringBuffer code=new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssSS");
		String formDate = sdf.format(date);
		String no = formDate.substring(12);
		code.append(no);
		return code.toString();
	}
	
	
	public static String formatDate(Date d){
		if(lastDate!=null&&df.format(lastDate).compareTo(df.format(d))<0){
			startNum=1;
		}
		lastDate=d;
		return df.format(d);
	}

	public static Integer startNum=1;
	public static String createId(){
		return df2.format(startNum++);
	}
	
	
	
	public static String[] temp={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9"};
	public static String createOrderCode(String id){
		StringBuffer bf = new StringBuffer();
//		Random rand = new Random();
//		for (int i=0;i<5;i++) {
//			int randNum = rand.nextInt(36);
//			bf.append(temp[randNum]);
//		}
		bf.append("obb");
		bf.append(id);
		return bf.toString();
	}
	
	
	public static void main(String[] args) {

		 
	}
 
	public static boolean handle(int n){
		int[] list=new int[5];
		for(int i=0;i<5;i++){
			list[i]=n%10;
			n=n/10;
		}
		for(int i=0;i<5;i++){
			for(int j=i+1;j<5;j++){
				if(list[i]==list[j]) {
					System.out.println("重复");
					return false;
				}
			}
		}
		return true;
	}
	
}
