package com.jkdl.springboot.enums;

public enum OrderPayType {

	Pay_WeiXPay(1, "微信支付"),	// 微信支付
	Pay_MonthPay(2, "月结算");		//月结算 先送货 按照月来结算金额
	
	private int code;
	private String alias;
	
	public int getCode() {
		return code;
	}

	public String getAlias() {
		return alias;
	}

	private OrderPayType(int code, String alias) {
		this.code = code;
		this.alias = alias;
	}

	public static OrderPayType valueOf(int code) {
		for (OrderPayType t : values()) {
			if (t.code == code) {
				return t;
			}
		}
		return Pay_WeiXPay;
	}
}
