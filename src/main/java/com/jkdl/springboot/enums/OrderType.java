package com.jkdl.springboot.enums;

public enum OrderType {

	pay_cd(0, "充电服务"),	// 充电服务
	pay_money(20, "消费付款"),		//月结算 先送货 按照月来结算金额
	pay_chongzhi(30,"用户充值");
	private int code;
	private String alias;
	
	public int getCode() {
		return code;
	}

	public String getAlias() {
		return alias;
	}

	private OrderType(int code, String alias) {
		this.code = code;
		this.alias = alias;
	}

	public static OrderType valueOf(int code) {
		for (OrderType t : values()) {
			if (t.code == code) {
				return t;
			}
		}
		return pay_cd;
	}
}
