package com.jkdl.springboot.enums;

public enum OrderStatus {

	PayNo(0, "待支付"),	// 未支付(提交订单成功、等待付款)，
	PayOn(1,"支付中"),//此状态为 在调用支付工具支付的状态  不是常态 在银行或者微信支付回调后改变状态

	PayFreeOrder(3,"免支付"),//此状态为 免支付订单
	PayRefundOrder(4,"退款"),//此状态为 退款订单

	PaySuccess(10, "支付成功"),//支付成功
	PayError(100, "支付失败"),	// 支付失败
	OrderDo(20,"处理订单"),//在支付成功后设置一个接受订单 开始处理订单 【审核通过】
	OrderMaked(25,"生产完毕"),//订单生产完成
	OrderOnSend(30,"已发货"),//配送中 或者是 已发货  已送货
	OrderSuccessSend(40,"已签收"),//已收货 
	RefundDo(50,"退款中"),//退款中
	RefundSuccess(60,"退款成功"),//退款成功
	RefundError(70,"退款失败"),//退款成功
	OrderClosed(110, "已关闭"),// 关闭订单  会员自己关闭自己的订单
	OrderNullity(200,"无效订单"); //无效订单 或者长期未付款订单 或者取消订单
	
	
	private int code;
	private String alias;
	
	public int getCode() {
		return code;
	}

	public String getAlias() {
		return alias;
	}

	private OrderStatus(int code, String alias) {
		this.code = code;
		this.alias = alias;
	}

	public static OrderStatus valueOf(int code) {
		for (OrderStatus t : values()) {
			if (t.code == code) {
				return t;
			}
		}
		return OrderNullity;
	}
}
